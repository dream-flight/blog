# 项目部署脚本
# update方法：参数：springboot（后端）、blog（博客前台）、admin（博客后台）
update() {
    local app=$app
    if [ "$app" == "admin" ]; then
      # ./start.sh update springboot，-e 参数目的是为了让Linux解释转义字符
      echo -e "Start update blog-vue admin"
      # 1. 删除原本的 admin 文件夹
      rm -rf blog-vue/admin
      # 2. 移动当前位置的 admin 文件夹到 ./blog-vue中
      mv admin ./blog-vue
      # 3. 重启 Nginx 容器
      docker restart nginx
      echo -e "Blog management system updated successfully"
    elif [ "$app" == "springboot" ]; then
      # ./start.sh update springboot
      echo -e "Start update blog-springboot"
      # 1. 删除原本服务器上的jar包
      rm -rf blog-springboot/blog-start.jar
      echo -e "Complete jar package deletion and move"
      # 2. 移动当前位置 (/usr/local/project/blog) 的jar包到 ./blog-springboot 目录中
      mv blog-start.jar ./blog-springboot
      # 3. 停止并删除当前正在运行的blog容器
      docker rm -f blog
      # 4. 删除当前的blog镜像
      docker rmi blog:"$old_version"
      echo -e "Complete stopping the blog container and deleting the blog image"
      # 5. 根据Dockerfile构建新的博客镜像
      docker build -f ./blog-springboot/Dockerfile -t blog:"$version" .
      echo -e "Completing the construction of a new blog image: blog:$version"
      # 6. 根据新的镜像运行blog容器
      docker run --name blog -p 8080:8080 -v /usr/local/project/blog/blog-springboot/upload/:/usr/local/project/blog/blog-springboot/upload --restart="always" -d blog:"$version"
      echo -e "Backend project update successfully"
    else
      echo "请输入正确的更新内容，[ blog(博客前台), admin(博客管理), springboot(后端服务) ]"
    fi
}

# 获取脚本的参数
action=$1
app=$2
# 定义当前博客版本
version=1.0.0
# 旧版博客版本
old_version=1.0.0

# 判断是否提供了足够的参数
if [ -z "$action" ] || [ -z "$app" ]; then
  echo "Error: Insufficient arguments $0 <action> <app>"
  exit 1
fi

# 根据提供的参数执行相应的操作
if [ "$action" == "update" ]; then
  # 调用定义的函数
  update "$app"
else
  echo "Unknown action: $action"
  exit 1
fi