-- ----------------------------
-- 字典类型表结构
-- ----------------------------
CREATE TABLE tb_dict_type
(
    `id`                 bigint              NOT NULL                    COMMENT '主键',
    `dict_name`          varchar(100)        NOT NULL DEFAULT ''         COMMENT '字典名称',
    `dict_type`          varchar(100)        NOT NULL DEFAULT ''         COMMENT '字典类型',
    `status`             tinyint             NOT NULL DEFAULT '1'        COMMENT '状态（1 正常 0 停用）',
    `remark`             varchar(255)        DEFAULT NULL                COMMENT '备注',
    `create_time`        datetime            DEFAULT NULL                COMMENT '创建时间',
    `update_time`        datetime            DEFAULT NULL                COMMENT '更新时间',
    PRIMARY KEY (`id`),
    UNIQUE (dict_type)
) COMMENT = '字典类型表';

-- ----------------------------
-- 字典数据表结构
-- ----------------------------
CREATE TABLE tb_dict_data
(
    `id`                 bigint              NOT NULL                    COMMENT '主键',
    `dict_label`         varchar(100)        NOT NULL DEFAULT ''         COMMENT '字典标签',
    `dict_value`         varchar(100)        DEFAULT ''                  COMMENT '字典键值',
    `dict_type`          varchar(100)        NOT NULL DEFAULT ''         COMMENT '字典类型',
    `sort`               int                 DEFAULT '0'                 COMMENT '字典排序',
    `is_default`         tinyint             DEFAULT '1'                 COMMENT '是否默认（1 是 0 否）',
    `status`             tinyint             NOT NULL DEFAULT '1'        COMMENT '状态（1 正常 0 停用）',
    `css_class`          varchar(100)        DEFAULT NULL                COMMENT '样式属性（其他样式扩展）',
    `list_class`         varchar(100)        DEFAULT NULL                COMMENT '表格回显样式',
    `remark`             varchar(255)        DEFAULT NULL                COMMENT '备注',
    `create_time`        datetime            DEFAULT NULL                COMMENT '创建时间',
    `update_time`        datetime            DEFAULT NULL                COMMENT '更新时间',
    PRIMARY KEY (`id`)
) COMMENT = '字典数据表';

-- ----------------------------
-- 代码生成日志表结构
-- ----------------------------
CREATE TABLE tb_generate_log
(
    `id`                 bigint              NOT NULL                    COMMENT '主键',
    `table_name`         varchar(100)        NOT NULL DEFAULT ''         COMMENT '生成表名',
    `module_name`        varchar(100)        NOT NULL DEFAULT ''         COMMENT '模块名称',
    `package_name`       varchar(100)        NOT NULL DEFAULT ''         COMMENT '包名称',
    `enable_cache`       tinyint             NOT NULL DEFAULT '0'        COMMENT '开启缓存',
    `enable_chain`       tinyint             NOT NULL DEFAULT '0'        COMMENT '链式编程',
    `create_time`        datetime            DEFAULT NULL                COMMENT '生成时间',
    `update_time`        datetime            DEFAULT NULL                COMMENT '更新时间',
    PRIMARY KEY (`id`)
) COMMENT = '代码生成日志表';

-- ----------------------------
-- 请求资源
-- ----------------------------
INSERT INTO `tb_resource` (`id`, `name`, `description`, `method`, `url`, `create_time`, `update_time`) VALUES (1744994308420796416, '分页获取数据库表', '分页获取数据库表', 'POST', '/admin/generator/listTable', '2024-01-10 16:08:39', '2024-01-10 16:08:57');
INSERT INTO `tb_resource` (`id`, `name`, `description`, `method`, `url`, `create_time`, `update_time`) VALUES (1745641016649580544, '预览代码', '预览代码', 'POST', '/admin/generator/preview', '2024-01-10 16:08:39', '2024-01-10 16:08:57');
INSERT INTO `tb_resource` (`id`, `name`, `description`, `method`, `url`, `create_time`, `update_time`) VALUES (1746734037193457664, '下载代码', '下载代码', 'POST', '/admin/generator/download', '2024-01-15 11:21:39', '2024-01-15 11:21:39');
INSERT INTO `tb_resource` (`id`, `name`, `description`, `method`, `url`, `create_time`, `update_time`) VALUES (1747078312749957120, '分页查询代码生成日志', '分页查询代码生成日志', 'POST', '/admin/generate-log/list', '2024-01-16 10:00:39', '2024-01-16 10:00:39');
INSERT INTO `tb_resource` (`id`, `name`, `description`, `method`, `url`, `create_time`, `update_time`) VALUES (1747078312754151424, '批量删除代码生成日志', '批量删除代码生成日志', 'DELETE', '/admin/generate-log/delete', '2024-01-16 10:00:39', '2024-01-16 10:00:39');
INSERT INTO `tb_resource` (`id`, `name`, `description`, `method`, `url`, `create_time`, `update_time`) VALUES (1747177876639186944, '根据生成日志下载代码', '根据生成日志下载代码', 'GET', '/admin/generate-log/download/**', '2024-01-16 10:00:39', '2024-01-16 10:00:39');
INSERT INTO `tb_resource` (`id`, `name`, `description`, `method`, `url`, `create_time`, `update_time`) VALUES (1747282790946177024, '分页查询字典类型', '分页查询字典类型', 'POST', '/admin/dict-type/list', '2024-01-16 23:41:45', '2024-01-16 23:41:50');
INSERT INTO `tb_resource` (`id`, `name`, `description`, `method`, `url`, `create_time`, `update_time`) VALUES (1747282790946177025, '编辑字典类型', '添加-更新字典类型', 'POST', '/admin/dict-type/save', '2024-01-16 23:42:38', '2024-01-16 23:42:43');
INSERT INTO `tb_resource` (`id`, `name`, `description`, `method`, `url`, `create_time`, `update_time`) VALUES (1747282790950371328, '获取指定字典类型信息', '获取指定字典类型信息', 'GET', '/admin/dict-type/getDictTypeById/**', '2024-01-16 23:43:13', '2024-01-16 23:43:17');
INSERT INTO `tb_resource` (`id`, `name`, `description`, `method`, `url`, `create_time`, `update_time`) VALUES (1747282790950371329, '批量删除字典类型数据', '批量删除字典类型数据', 'DELETE', '/admin/dict-type/delete', '2024-01-16 23:43:45', '2024-01-16 23:43:48');
INSERT INTO `tb_resource` (`id`, `name`, `description`, `method`, `url`, `create_time`, `update_time`) VALUES (1748725200716824576, '获取字典类型选项', '获取字典类型选项', 'GET', '/admin/dict-type/listOption', '2024-01-20 22:22:51', '2024-01-20 22:22:51');
INSERT INTO `tb_resource` (`id`, `name`, `description`, `method`, `url`, `create_time`, `update_time`) VALUES (1748725200716824577, '分页查询字典数据', '分页查询字典类型对应的字典数据', 'GET', '/admin/dict-data/list', '2024-01-20 22:22:51', '2024-01-20 22:22:51');
INSERT INTO `tb_resource` (`id`, `name`, `description`, `method`, `url`, `create_time`, `update_time`) VALUES (1748725200716824578, '编辑字典数据', '新增-添加字典数据', 'POST', '/admin/dict-data/save', '2024-01-20 22:22:51', '2024-01-20 22:22:51');
INSERT INTO `tb_resource` (`id`, `name`, `description`, `method`, `url`, `create_time`, `update_time`) VALUES (1748725200716824579, '获取指定字典数据信息', '获取指定字典数据信息', 'GET', '/admin/dict-data/getDictDataById/**', '2024-01-20 22:22:51', '2024-01-20 22:22:51');
INSERT INTO `tb_resource` (`id`, `name`, `description`, `method`, `url`, `create_time`, `update_time`) VALUES (1748725200716824580, '批量删除字典数据', '批量删除字典数据', 'DELETE', '/admin/dict-data/delete', '2024-01-20 22:22:51', '2024-01-20 22:22:51');

-- ----------------------------
-- 资源模块关联内容
-- ----------------------------
INSERT INTO `tb_module_resource` (`id`, `module_id`, `resource_id`, `create_time`) VALUES (1744998139917500416, 1636182933754609665, 1744994308420796416, '2024-01-10 16:23:39');
INSERT INTO `tb_module_resource` (`id`, `module_id`, `resource_id`, `create_time`) VALUES (1745641016649580545, 1636182933754609665, 1745641016649580544, '2024-01-10 16:23:39');
INSERT INTO `tb_module_resource` (`id`, `module_id`, `resource_id`, `create_time`) VALUES (1746734037193457665, 1636182933754609665, 1746734037193457664, '2024-01-15 11:21:39');
INSERT INTO `tb_module_resource` (`id`, `module_id`, `resource_id`, `create_time`) VALUES (1747078312754151425, 1636182933754609665, 1747078312749957120, '2024-01-15 11:21:39');
INSERT INTO `tb_module_resource` (`id`, `module_id`, `resource_id`, `create_time`) VALUES (1747078312754151426, 1636182933754609665, 1747078312754151424, '2024-01-15 11:21:39');
INSERT INTO `tb_module_resource` (`id`, `module_id`, `resource_id`, `create_time`) VALUES (1747177876639186945, 1636182933754609665, 1747177876639186944, '2024-01-15 11:21:39');
INSERT INTO `tb_module_resource` (`id`, `module_id`, `resource_id`, `create_time`) VALUES (1747282790950371330, 1636182933754609665, 1747282790946177024, '2024-01-15 11:21:39');
INSERT INTO `tb_module_resource` (`id`, `module_id`, `resource_id`, `create_time`) VALUES (1747282790950371331, 1636182933754609665, 1747282790946177025, '2024-01-15 11:21:39');
INSERT INTO `tb_module_resource` (`id`, `module_id`, `resource_id`, `create_time`) VALUES (1747282790950371332, 1636182933754609665, 1747282790950371328, '2024-01-15 11:21:39');
INSERT INTO `tb_module_resource` (`id`, `module_id`, `resource_id`, `create_time`) VALUES (1747282790950371333, 1636182933754609665, 1747282790950371329, '2024-01-15 11:21:39');
INSERT INTO `tb_module_resource` (`id`, `module_id`, `resource_id`, `create_time`) VALUES (1748725200716824581, 1636182933754609665, 1748725200716824576, '2024-01-15 11:21:39');
INSERT INTO `tb_module_resource` (`id`, `module_id`, `resource_id`, `create_time`) VALUES (1748725200716824582, 1636182933754609665, 1748725200716824577, '2024-01-15 11:21:39');
INSERT INTO `tb_module_resource` (`id`, `module_id`, `resource_id`, `create_time`) VALUES (1748725200716824583, 1636182933754609665, 1748725200716824578, '2024-01-15 11:21:39');
INSERT INTO `tb_module_resource` (`id`, `module_id`, `resource_id`, `create_time`) VALUES (1748725200716824584, 1636182933754609665, 1748725200716824579, '2024-01-15 11:21:39');
INSERT INTO `tb_module_resource` (`id`, `module_id`, `resource_id`, `create_time`) VALUES (1748725200716824585, 1636182933754609665, 1748725200716824580, '2024-01-15 11:21:39');

-- ----------------------------
-- 菜单内容
-- ----------------------------
INSERT INTO `tb_menu` (`id`, `name`, `type`, `parent_id`, `path`, `component`, `is_cache`, `is_link`, `visible`, `status`, `icon`, `sort`, `create_time`, `update_time`) VALUES (1745696963082321920, '代码生成', 'P', 1647951751875133441, 'generator', 'website/generator/index', 0, 0, 0, 1, 'code', 4, '2024-01-10 16:23:39', '2024-01-10 16:23:39');
INSERT INTO `tb_menu` (`id`, `name`, `type`, `parent_id`, `path`, `component`, `is_cache`, `is_link`, `visible`, `status`, `icon`, `sort`, `create_time`, `update_time`) VALUES (1747151945530867712, '系统组件', 'D', 0, 'system-content', NULL, 0, 0, 1, 1, 'tree-table', 9, '2024-01-16 00:05:45', '2024-01-16 00:05:45');
INSERT INTO `tb_menu` (`id`, `name`, `type`, `parent_id`, `path`, `component`, `is_cache`, `is_link`, `visible`, `status`, `icon`, `sort`, `create_time`, `update_time`) VALUES (1747151945530867713, '代码生成历史', 'P', 1747151945530867712, 'generate-log', 'website/generator/components/generate-log/index.vue', 0, 0, 1, 1, 'time', 0, '2024-01-16 00:07:17', '2024-01-16 00:10:22');
INSERT INTO `tb_menu` (`id`, `name`, `type`, `parent_id`, `path`, `component`, `is_cache`, `is_link`, `visible`, `status`, `icon`, `sort`, `create_time`, `update_time`) VALUES (1747282513481969666, '字典管理', 'P', 1652313423859417088, 'dict-type', 'system/dict/index.vue', 0, 0, 0, 1, 'dict', 5, '2024-01-16 23:39:49', '2024-01-16 23:39:49');
INSERT INTO `tb_menu` (`id`, `name`, `type`, `parent_id`, `path`, `component`, `is_cache`, `is_link`, `visible`, `status`, `icon`, `sort`, `create_time`, `update_time`) VALUES (1748712694704795650, '字典数据', 'P', 1747151945530867712, 'dict-data/:dictType', 'system/dict/components/dict-data/index.vue', 0, 0, 1, 1, 'table', 0, '2024-01-20 22:22:51', '2024-01-20 22:28:34');