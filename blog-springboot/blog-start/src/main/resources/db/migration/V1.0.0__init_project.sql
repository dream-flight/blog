-- ----------------------------
-- Table structure for tb_article
-- ----------------------------
CREATE TABLE `tb_article` (
  `id`                     bigint              NOT NULL            COMMENT '主键',
  `title`                  varchar(100)        NOT NULL            COMMENT '文章名称',
  `cover`                  varchar(255)        DEFAULT NULL        COMMENT '文章封面',
  `type`                   tinyint             NOT NULL            COMMENT '文章类型：0 原创，1 转载，2翻译',
  `summary`                varchar(255)        DEFAULT NULL        COMMENT '文章摘要',
  `content`                longtext            DEFAULT NULL        COMMENT '文章内容',
  `original_link`          varchar(120)        DEFAULT NULL        COMMENT '原文链接',
  `allow_comment`          tinyint             DEFAULT NULL        COMMENT '是否允许评论：1 允许 0 不允许',
  `is_sticky`              tinyint             DEFAULT NULL        COMMENT '是否置顶：1 置顶 0 不置顶',
  `allow_donation`         tinyint             DEFAULT NULL        COMMENT '是否开启打赏：1 开启',
  `visibility`             tinyint             DEFAULT NULL        COMMENT '可见范围 0 自己可见 1 全部可见',
  `visit_count`            int                 DEFAULT NULL        COMMENT '浏览数量',
  `comment_count`          int                 DEFAULT NULL        COMMENT '评论数量',
  `create_address`         varchar(50)         DEFAULT NULL        COMMENT '发布IP',
  `create_location`        varchar(50)         DEFAULT NULL        COMMENT '发布地址',
  `create_time`            datetime            DEFAULT NULL        COMMENT '发布时间',
  `update_time`            datetime            DEFAULT NULL        COMMENT '更新时间',
  PRIMARY KEY             (`id`)       USING BTREE,
          KEY `idx_title` (`title`)    USING BTREE    COMMENT '文章标题索引'
) COMMENT='文章表';

-- ----------------------------
-- Records of tb_article
-- ----------------------------

INSERT INTO `tb_article` (`id`, `title`, `cover`, `type`, `summary`, `content`, `original_link`, `allow_comment`, `is_sticky`, `allow_donation`, `visibility`, `visit_count`, `comment_count`, `create_address`, `create_location`, `create_time`, `update_time`) VALUES (1716112808115961856, '测试文章标题', 'https://blog-yk0504.oss-cn-hangzhou.aliyuncs.com/cover/20231018235505.jpg', 0, '测试零妹妹作为封面', '开始编辑...\nasdfsdaf', NULL, 1, 0, 1, 1, 0, 0, '127.0.0.1', '内网IP', '2023-10-22 23:22:32', '2023-12-21 10:39:08');
INSERT INTO `tb_article` (`id`, `title`, `cover`, `type`, `summary`, `content`, `original_link`, `allow_comment`, `is_sticky`, `allow_donation`, `visibility`, `visit_count`, `comment_count`, `create_address`, `create_location`, `create_time`, `update_time`) VALUES (1728431663593029632, 'iohhj ', 'https://blog-yk0504.oss-cn-hangzhou.aliyuncs.com/cover/20230530223559.png', 0, 'nijhiu hu', 'jiojio jioj ', NULL, 1, 1, 1, 0, 0, 0, '127.0.0.1', '内网IP', '2023-11-25 23:13:16', '2023-12-21 10:39:28');

-- ----------------------------
-- Table structure for tb_article_category
-- ----------------------------
CREATE TABLE `tb_article_category` (
  `id`                bigint        NOT NULL            COMMENT '主键',
  `article_id`        bigint        DEFAULT NULL        COMMENT '文章ID',
  `category_id`       bigint        DEFAULT NULL        COMMENT '分类ID',
  PRIMARY KEY (`id`) USING BTREE,
  KEY    `idx_article_id`     (`article_id`)     USING BTREE    COMMENT '文章ID索引',
  KEY    `idx_category_id`    (`category_id`)    USING BTREE    COMMENT '文章分类索引'
) COMMENT='文章分类关联表';

-- ----------------------------
-- Records of tb_article_category
-- ----------------------------

INSERT INTO `tb_article_category` (`id`, `article_id`, `category_id`) VALUES (1716112809571385344, 1716112808115961856, 1699078935502913536);
INSERT INTO `tb_article_category` (`id`, `article_id`, `category_id`) VALUES (1728431663689498624, 1728431663593029632, 1699085088492355584);

-- ----------------------------
-- Table structure for tb_article_tags
-- ----------------------------
CREATE TABLE `tb_article_tags` (
  `id`                bigint        NOT NULL            COMMENT '主键',
  `article_id`        bigint        DEFAULT NULL        COMMENT '文章ID',
  `tag_id`            bigint        DEFAULT NULL        COMMENT '标签ID',
  PRIMARY KEY (`id`) USING BTREE,
  KEY    `idx_article_id`    (`article_id`)    USING BTREE    COMMENT '文章ID索引',
  KEY    `idx_tag_id`        (`tag_id`)        USING BTREE    COMMENT '博客标签索引'
) COMMENT='文章标签关联表';

-- ----------------------------
-- Records of tb_article_tags
-- ----------------------------

INSERT INTO `tb_article_tags` (`id`, `article_id`, `tag_id`) VALUES (1737663965284007936, 1716112808115961856, 1712125141502132224);
INSERT INTO `tb_article_tags` (`id`, `article_id`, `tag_id`) VALUES (1737663965284007937, 1716112808115961856, 1714668306494914560);
INSERT INTO `tb_article_tags` (`id`, `article_id`, `tag_id`) VALUES (1737663965284007938, 1716112808115961856, 1714668325264424960);
INSERT INTO `tb_article_tags` (`id`, `article_id`, `tag_id`) VALUES (1737664046934523904, 1728431663593029632, 1714668325264424960);

-- ----------------------------
-- Table structure for tb_blog_category
-- ----------------------------
CREATE TABLE `tb_blog_category` (
  `id`                     bigint              NOT NULL            COMMENT '主键',
  `name`                   varchar(50)         NOT NULL            COMMENT '分类名称',
  `description`            varchar(255)        DEFAULT NULL        COMMENT '分类描述',
  `icon`                   varchar(255)        DEFAULT NULL        COMMENT '分类图标',
  `sort`                   int                 DEFAULT '0'         COMMENT '分类排序',
  `privacy_setting`        tinyint             NOT NULL            COMMENT '分类展示：0 不展示 1 公开 2 尽自己可见',
  `create_time`            datetime            DEFAULT NULL        COMMENT '创建时间',
  `update_time`            datetime            DEFAULT NULL        COMMENT '更新时间',
  PRIMARY KEY                       (`id`)                   USING BTREE,
          KEY `idx_privacy_setting` (`privacy_setting`)      USING BTREE      COMMENT '分类状态索引',
          KEY `idx_name`            (`name`)                 USING BTREE      COMMENT '分类名称索引'
) COMMENT='博客分类表';

-- ----------------------------
-- Records of tb_blog_category
-- ----------------------------

INSERT INTO `tb_blog_category` (`id`, `name`, `description`, `icon`, `sort`, `privacy_setting`, `create_time`, `update_time`) VALUES (1698725448252391424, '测试分类', '测试分类', 'bug', 0, 0, '2023-09-04 23:51:22', '2023-11-25 22:38:54');
INSERT INTO `tb_blog_category` (`id`, `name`, `description`, `icon`, `sort`, `privacy_setting`, `create_time`, `update_time`) VALUES (1699078935502913536, '测试', '测试哈哈哈', 'clipboard', 0, 2, '2023-09-05 23:16:00', '2023-11-25 22:39:01');
INSERT INTO `tb_blog_category` (`id`, `name`, `description`, `icon`, `sort`, `privacy_setting`, `create_time`, `update_time`) VALUES (1699085088492355584, '测试2', '测试', 'article-ranking', 0, 1, '2023-09-05 23:40:27', '2023-11-25 22:38:45');
INSERT INTO `tb_blog_category` (`id`, `name`, `description`, `icon`, `sort`, `privacy_setting`, `create_time`, `update_time`) VALUES (1726892315949137920, '1212', '121', 'QQ', 2, 1, '2023-11-21 17:16:27', NULL);

-- ----------------------------
-- Table structure for tb_blog_tags
-- ----------------------------
CREATE TABLE `tb_blog_tags` (
  `id` bigint NOT NULL COMMENT '主键',
  `name` varchar(50) NOT NULL COMMENT '标签名称',
  `sort` int DEFAULT '0' COMMENT '标签排序',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `update_time` datetime DEFAULT NULL COMMENT '更新时间',
  PRIMARY KEY (`id`) USING BTREE,
  KEY `idx_name` (`name`) USING BTREE COMMENT '标签名称索引'
) COMMENT='博客标签表';

-- ----------------------------
-- Records of tb_blog_tags
-- ----------------------------

INSERT INTO `tb_blog_tags` (`id`, `name`, `sort`, `create_time`, `update_time`) VALUES (1712125141502132224, '测试标签', 0, '2023-10-11 23:16:58', NULL);
INSERT INTO `tb_blog_tags` (`id`, `name`, `sort`, `create_time`, `update_time`) VALUES (1714668306494914560, '测试标签2', 0, '2023-10-18 23:42:36', NULL);
INSERT INTO `tb_blog_tags` (`id`, `name`, `sort`, `create_time`, `update_time`) VALUES (1714668325264424960, '哈哈哈哈', 0, '2023-10-18 23:42:40', NULL);

-- ----------------------------
-- Table structure for tb_file
-- ----------------------------
CREATE TABLE `tb_file` (
  `id` bigint NOT NULL COMMENT '主键',
  `name` varchar(50) DEFAULT NULL COMMENT '文件名称',
  `path` varchar(255) NOT NULL COMMENT '文件存储位置',
  `src` varchar(100) DEFAULT NULL COMMENT '文件访问路径',
  `size` bigint DEFAULT NULL COMMENT '文件大小',
  `type` varchar(20) DEFAULT NULL COMMENT '文件类型',
  `strategy` varchar(50) NOT NULL COMMENT '上传策略',
  `user_id` bigint DEFAULT NULL COMMENT '上传用户ID',
  `file_type_id` bigint DEFAULT NULL COMMENT '文件分类ID',
  `md5` varchar(50) DEFAULT NULL COMMENT '文件唯一标识（MD5哈希值）',
  `create_time` datetime NOT NULL COMMENT '创建时间',
  `update_time` datetime DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
  PRIMARY KEY (`id`) USING BTREE
) COMMENT='文件表';

-- ----------------------------
-- Records of tb_file
-- ----------------------------

INSERT INTO `tb_file` (`id`, `name`, `path`, `src`, `size`, `type`, `strategy`, `user_id`, `file_type_id`, `md5`, `create_time`, `update_time`) VALUES (1663144125785964544, '20230529192353.jpg', 'avatar/20230529192353.jpg', 'https://blog-yk0504.oss-cn-hangzhou.aliyuncs.com/avatar/20230529192353.jpg', 72708, '.jpg', 'oss', 1653794265890816000, 1656676089927303170, 'bee1e765d72ab1eb20e3cdfb3de68abb', '2023-05-29 19:23:54', '2023-05-30 09:36:07');
INSERT INTO `tb_file` (`id`, `name`, `path`, `src`, `size`, `type`, `strategy`, `user_id`, `file_type_id`, `md5`, `create_time`, `update_time`) VALUES (1663144536764841984, '20230529192531.jpg', 'avatar/20230529192531.jpg', 'https://blog-yk0504.oss-cn-hangzhou.aliyuncs.com/avatar/20230529192531.jpg', 152031, '.jpg', 'oss', 1653794265890816000, 1656676089927303170, 'fd43ed5490db1894cb5c46cb300e1cf2', '2023-05-29 19:25:32', '2023-05-30 09:36:07');
INSERT INTO `tb_file` (`id`, `name`, `path`, `src`, `size`, `type`, `strategy`, `user_id`, `file_type_id`, `md5`, `create_time`, `update_time`) VALUES (1663144640041189376, '20230529192556.jpg', 'avatar/20230529192556.jpg', 'https://blog-yk0504.oss-cn-hangzhou.aliyuncs.com/avatar/20230529192556.jpg', 149077, '.jpg', 'oss', 1653794265890816000, 1656676089927303170, '8a8aa84284c227bad891e840561cc2a4', '2023-05-29 19:25:57', '2023-05-30 09:36:07');
INSERT INTO `tb_file` (`id`, `name`, `path`, `src`, `size`, `type`, `strategy`, `user_id`, `file_type_id`, `md5`, `create_time`, `update_time`) VALUES (1663144694911074304, '20230529192609.jpeg', 'avatar/20230529192609.jpeg', 'https://blog-yk0504.oss-cn-hangzhou.aliyuncs.com/avatar/20230529192609.jpeg', 444745, '.jpeg', 'oss', 1653794265890816000, 1656676089927303170, '1a3376b08f3ed911caa689e479fb552d', '2023-05-29 19:26:10', '2023-05-30 09:36:07');
INSERT INTO `tb_file` (`id`, `name`, `path`, `src`, `size`, `type`, `strategy`, `user_id`, `file_type_id`, `md5`, `create_time`, `update_time`) VALUES (1663196287383633920, '20230529225103.jpg', 'avatar/20230529225103.jpg', 'https://blog-yk0504.oss-cn-hangzhou.aliyuncs.com/avatar/20230529225103.jpg', 97766, '.jpg', 'oss', 1653794265890816000, 1656676089927303170, 'e960ac942f4cd2c10af9bcbacba5f4e6', '2023-05-29 22:51:10', '2023-05-30 09:36:07');
INSERT INTO `tb_file` (`id`, `name`, `path`, `src`, `size`, `type`, `strategy`, `user_id`, `file_type_id`, `md5`, `create_time`, `update_time`) VALUES (1663554855068762112, '20230530223559.png', 'cover/20230530223559.png', 'https://blog-yk0504.oss-cn-hangzhou.aliyuncs.com/cover/20230530223559.png', 181272, '.png', 'oss', 1653794265890816000, 1656676089927303168, 'fa74451429d178e2682a3cf6e833c019', '2023-05-30 22:35:59', '2023-05-30 22:35:59');
INSERT INTO `tb_file` (`id`, `name`, `path`, `src`, `size`, `type`, `strategy`, `user_id`, `file_type_id`, `md5`, `create_time`, `update_time`) VALUES (1663556688776527872, '20230530224316.jpg', 'cover/20230530224316.jpg', 'https://blog-yk0504.oss-cn-hangzhou.aliyuncs.com/cover/20230530224316.jpg', 50746, '.jpg', 'oss', 1653794265890816000, 1656676089927303168, '8c2eb375053690cb1ae6e9cdc2253a23', '2023-05-30 22:43:17', '2023-05-30 22:43:17');
INSERT INTO `tb_file` (`id`, `name`, `path`, `src`, `size`, `type`, `strategy`, `user_id`, `file_type_id`, `md5`, `create_time`, `update_time`) VALUES (1665355748768481280, '20230604215201.jpeg', 'cover/20230604215201.jpeg', 'https://blog-yk0504.oss-cn-hangzhou.aliyuncs.com/cover/20230604215201.jpeg', 604332, '.jpeg', 'oss', 1653794265890816000, 1656676089927303168, '67bd2983a12ea08bffa358d68e1169f7', '2023-06-04 21:52:06', '2023-06-04 21:52:06');
INSERT INTO `tb_file` (`id`, `name`, `path`, `src`, `size`, `type`, `strategy`, `user_id`, `file_type_id`, `md5`, `create_time`, `update_time`) VALUES (1667556674283503616, '20230610233746.jpg', 'article/20230610233746.jpg', 'https://blog-yk0504.oss-cn-hangzhou.aliyuncs.com/article/20230610233746.jpg', 1284390, '.jpg', 'oss', 1653794265890816000, 1656676089927303169, 'e8afdebb8420ad7822f75df100c8cc99', '2023-06-10 23:37:48', '2023-06-10 23:37:48');
INSERT INTO `tb_file` (`id`, `name`, `path`, `src`, `size`, `type`, `strategy`, `user_id`, `file_type_id`, `md5`, `create_time`, `update_time`) VALUES (1695768706853896192, '20230827200219.jpg', 'article/20230827200219.jpg', 'https://blog-yk0504.oss-cn-hangzhou.aliyuncs.com/article/20230827200219.jpg', 152031, '.jpg', 'oss', 1653794265890816000, 1656676089927303169, 'fd43ed5490db1894cb5c46cb300e1cf2', '2023-08-27 20:02:20', '2023-08-27 20:02:20');
INSERT INTO `tb_file` (`id`, `name`, `path`, `src`, `size`, `type`, `strategy`, `user_id`, `file_type_id`, `md5`, `create_time`, `update_time`) VALUES (1714671281158225920, '20231018235424.jpg', 'cover/20231018235424.jpg', 'https://blog-yk0504.oss-cn-hangzhou.aliyuncs.com/cover/20231018235424.jpg', 1050254, '.jpg', 'oss', 1653794265890816000, 1656676089927303168, 'ad45e0ac4d6bc255aa005ce91582e4f0', '2023-10-18 23:54:25', '2023-10-18 23:54:25');
INSERT INTO `tb_file` (`id`, `name`, `path`, `src`, `size`, `type`, `strategy`, `user_id`, `file_type_id`, `md5`, `create_time`, `update_time`) VALUES (1714671393318109184, '20231018235451.jpg', 'cover/20231018235451.jpg', 'https://blog-yk0504.oss-cn-hangzhou.aliyuncs.com/cover/20231018235451.jpg', 647894, '.jpg', 'oss', 1653794265890816000, 1656676089927303168, 'bc37d5dedcc333168b6f2664c73456a2', '2023-10-18 23:54:52', '2023-10-18 23:54:52');
INSERT INTO `tb_file` (`id`, `name`, `path`, `src`, `size`, `type`, `strategy`, `user_id`, `file_type_id`, `md5`, `create_time`, `update_time`) VALUES (1714671450125762560, '20231018235505.jpg', 'cover/20231018235505.jpg', 'https://blog-yk0504.oss-cn-hangzhou.aliyuncs.com/cover/20231018235505.jpg', 615846, '.jpg', 'oss', 1653794265890816000, 1656676089927303168, 'd211834e55c9a61007a57a1aa0c2dd90', '2023-10-18 23:55:05', '2023-10-18 23:55:05');
INSERT INTO `tb_file` (`id`, `name`, `path`, `src`, `size`, `type`, `strategy`, `user_id`, `file_type_id`, `md5`, `create_time`, `update_time`) VALUES (1735568476707225600, '6f1887b84ab079a9d7b6f2e8947f2580.jpg', 'https://blog-yk0504.oss-cn-hangzhou.aliyuncs.com/article/6f1887b84ab079a9d7b6f2e8947f2580.jpg', 'https://blog-yk0504.oss-cn-hangzhou.aliyuncs.com/article/6f1887b84ab079a9d7b6f2e8947f2580.jpg', 1660299, '.jpg', 'OSS', 1653794265890816000, 1656676089927303169, '6f1887b84ab079a9d7b6f2e8947f2580', '2023-12-15 15:52:25', NULL);

-- ----------------------------
-- Table structure for tb_file_type
-- ----------------------------
CREATE TABLE `tb_file_type` (
  `id` bigint NOT NULL COMMENT '主键',
  `name` varchar(50) NOT NULL COMMENT '文件分类名称',
  `mark` varchar(50) DEFAULT NULL COMMENT '文件分类标识',
  `description` varchar(255) DEFAULT NULL COMMENT '文件分类描述',
  `sort` int DEFAULT NULL COMMENT '文件分类排序',
  `create_time` datetime NOT NULL COMMENT '创建时间',
  `update_time` datetime DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
  PRIMARY KEY (`id`) USING BTREE
) COMMENT='文件分类表';

-- ----------------------------
-- Records of tb_file_type
-- ----------------------------

INSERT INTO `tb_file_type` (`id`, `name`, `mark`, `description`, `sort`, `create_time`, `update_time`) VALUES (1656676089927303168, '封面图片', 'cover', '文章封面图片', 1, '2023-05-11 23:02:37', '2023-05-23 16:02:28');
INSERT INTO `tb_file_type` (`id`, `name`, `mark`, `description`, `sort`, `create_time`, `update_time`) VALUES (1656676089927303169, '文章图片', 'article', '文章中的图片', 2, '2023-05-11 23:03:05', '2023-05-23 16:02:50');
INSERT INTO `tb_file_type` (`id`, `name`, `mark`, `description`, `sort`, `create_time`, `update_time`) VALUES (1656676089927303170, '头像文件', 'avatar', '分类图片', 3, '2023-05-11 23:03:24', '2023-05-23 16:06:41');
INSERT INTO `tb_file_type` (`id`, `name`, `mark`, `description`, `sort`, `create_time`, `update_time`) VALUES (1656676089927303171, '其他文件', 'other', '其他类型文件', 4, '2023-05-11 23:03:45', '2023-05-23 16:03:01');

-- ----------------------------
-- Table structure for tb_link
-- ----------------------------
CREATE TABLE `tb_link` (
  `id` bigint NOT NULL COMMENT '主键',
  `name` varchar(60) NOT NULL COMMENT '网站名称',
  `website` varchar(255) DEFAULT NULL COMMENT '网站域名',
  `logo` varchar(255) DEFAULT NULL COMMENT '网站Logo',
  `introduce` varchar(255) DEFAULT NULL COMMENT '网站介绍',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `update_time` datetime DEFAULT NULL COMMENT '更新时间',
  PRIMARY KEY (`id`) USING BTREE,
  KEY `index_name` (`name`) USING BTREE COMMENT '网站名称索引'
) COMMENT='友情链接表';

-- ----------------------------
-- Records of tb_link
-- ----------------------------

INSERT INTO `tb_link` (`id`, `name`, `website`, `logo`, `introduce`, `create_time`, `update_time`) VALUES (1671411053646315520, '半晴Miko', 'https://banq.ink/', 'https://www.static.banq.ink/sunnyBlog/avatar/fcc72d35fc928185c3e70773a29a310f.jpg', 'Semisunny', '2023-06-21 14:53:43', '2023-06-21 15:17:31');

-- ----------------------------
-- Table structure for tb_login_log
-- ----------------------------
CREATE TABLE `tb_login_log` (
  `id` bigint NOT NULL COMMENT '主键',
  `username` varchar(255) NOT NULL COMMENT '登录用户名称',
  `login_ip` varchar(255) DEFAULT NULL COMMENT '登录IP',
  `login_location` varchar(255) DEFAULT NULL COMMENT '登录位置',
  `browser` varchar(255) DEFAULT NULL COMMENT '浏览器版本',
  `os` varchar(255) DEFAULT NULL COMMENT '操作系统',
  `status` tinyint NOT NULL COMMENT '登录状态：0 失败; 1 成功',
  `message` varchar(255) DEFAULT NULL COMMENT '登录消息提示',
  `login_time` datetime NOT NULL COMMENT '登录时间',
  PRIMARY KEY (`id`) USING BTREE
) COMMENT='用户登录信息表';

-- ----------------------------
-- Records of tb_login_log
-- ----------------------------


-- ----------------------------
-- Table structure for tb_menu
-- ----------------------------
CREATE TABLE `tb_menu` (
  `id` bigint NOT NULL COMMENT '主键',
  `name` varchar(50) DEFAULT NULL COMMENT '菜单名称',
  `type` char(1) DEFAULT NULL COMMENT '菜单类型：D：目录，P：页面',
  `parent_id` bigint DEFAULT NULL COMMENT '父菜单',
  `path` varchar(100) DEFAULT NULL COMMENT '访问路径',
  `component` varchar(100) DEFAULT NULL COMMENT '组件路径',
  `is_cache` tinyint DEFAULT NULL COMMENT '是否缓存 （0: 不缓存 1: 缓存）',
  `is_link` tinyint DEFAULT NULL COMMENT '是否外链 （0: 不是外链 1: 外链）',
  `visible` tinyint DEFAULT NULL COMMENT '是否隐藏 （0: 不隐藏 1: 隐藏）',
  `status` tinyint DEFAULT NULL COMMENT '状态，0：禁用，1：正常',
  `icon` varchar(50) DEFAULT NULL COMMENT '图标',
  `active_menu` varchar(100) DEFAULT NULL COMMENT '激活菜单',
  `sort` int DEFAULT NULL COMMENT '菜单顺序',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `update_time` datetime DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
  PRIMARY KEY (`id`) USING BTREE
) COMMENT='菜单表';

-- ----------------------------
-- Records of tb_menu
-- ----------------------------

INSERT INTO `tb_menu` (`id`, `name`, `type`, `parent_id`, `path`, `component`, `is_cache`, `is_link`, `visible`, `status`, `icon`, `sort`, `create_time`, `update_time`) VALUES (1647951751875133440, '博客管理', 'D', 0, 'blog', NULL, 0, 0, 0, 1, 'content', 1, '2023-04-17 21:17:07', '2023-12-21 14:27:20');
INSERT INTO `tb_menu` (`id`, `name`, `type`, `parent_id`, `path`, `component`, `is_cache`, `is_link`, `visible`, `status`, `icon`, `sort`, `create_time`, `update_time`) VALUES (1647951751875133441, '网站管理', 'D', 0, 'website', NULL, 0, 0, 0, 1, 'message', 2, '2023-04-17 21:17:46', '2023-08-03 19:34:56');
INSERT INTO `tb_menu` (`id`, `name`, `type`, `parent_id`, `path`, `component`, `is_cache`, `is_link`, `visible`, `status`, `icon`, `sort`, `create_time`, `update_time`) VALUES (1647951751875133442, '项目文档', 'D', 1714673277053116418, 'https://www.baidu.com', NULL, 0, 1, 0, 1, 'guide', 3, '2023-04-17 21:19:30', '2023-12-21 14:20:52');
INSERT INTO `tb_menu` (`id`, `name`, `type`, `parent_id`, `path`, `component`, `is_cache`, `is_link`, `visible`, `status`, `icon`, `sort`, `create_time`, `update_time`) VALUES (1647952968122630144, '文章管理', 'P', 1647951751875133440, 'article', 'blog/article/index', 0, 0, 0, 1, 'article-create', 1, '2023-04-17 21:22:42', '2023-12-21 14:28:03');
INSERT INTO `tb_menu` (`id`, `name`, `type`, `parent_id`, `path`, `component`, `is_cache`, `is_link`, `visible`, `status`, `icon`, `sort`, `create_time`, `update_time`) VALUES (1648539108017963008, '评论管理', 'P', 1647951751875133440, 'comment', 'blog/comment/index', 0, 0, 0, 1, 'comments', 2, '2023-04-19 12:09:46', '2023-12-21 14:28:10');
INSERT INTO `tb_menu` (`id`, `name`, `type`, `parent_id`, `path`, `component`, `is_cache`, `is_link`, `visible`, `status`, `icon`, `sort`, `create_time`, `update_time`) VALUES (1651040949146484736, '留言管理', 'P', 1647951751875133440, 'message', 'blog/message/index', 0, 0, 0, 1, 'message', 3, '2023-04-26 09:51:28', '2023-12-21 14:28:17');
INSERT INTO `tb_menu` (`id`, `name`, `type`, `parent_id`, `path`, `component`, `is_cache`, `is_link`, `visible`, `status`, `icon`, `sort`, `create_time`, `update_time`) VALUES (1651040949146484737, '分类管理', 'P', 1647951751875133440, 'category', 'blog/category/index', 0, 0, 0, 1, 'category', 4, '2023-04-26 09:52:57', '2023-12-21 14:28:23');
INSERT INTO `tb_menu` (`id`, `name`, `type`, `parent_id`, `path`, `component`, `is_cache`, `is_link`, `visible`, `status`, `icon`, `sort`, `create_time`, `update_time`) VALUES (1651042040491802625, '标签管理', 'P', 1647951751875133440, 'tags', 'blog/tags/index', 0, 0, 0, 1, 'tags', 5, '2023-04-26 09:53:51', '2023-12-21 14:28:28');
INSERT INTO `tb_menu` (`id`, `name`, `type`, `parent_id`, `path`, `component`, `is_cache`, `is_link`, `visible`, `status`, `icon`, `sort`, `create_time`, `update_time`) VALUES (1651217423560343552, '文件管理', 'P', 1647951751875133441, 'file', 'website/file/index', 0, 0, 0, 1, 'article-ranking', 1, '2023-04-26 21:33:29', '2023-08-03 19:35:08');
INSERT INTO `tb_menu` (`id`, `name`, `type`, `parent_id`, `path`, `component`, `is_cache`, `is_link`, `visible`, `status`, `icon`, `sort`, `create_time`, `update_time`) VALUES (1651217423560343553, '相册管理', 'P', 1647951751875133440, 'photo', 'blog/photo/index', 0, 0, 0, 1, 'tool', 7, '2023-04-26 21:33:31', '2023-12-21 14:28:44');
INSERT INTO `tb_menu` (`id`, `name`, `type`, `parent_id`, `path`, `component`, `is_cache`, `is_link`, `visible`, `status`, `icon`, `sort`, `create_time`, `update_time`) VALUES (1651217423560343554, '页面管理', 'P', 1647951751875133440, 'page', 'blog/page/index', 0, 0, 0, 1, 'documentation', 8, '2023-04-26 21:33:29', '2023-12-21 14:28:54');
INSERT INTO `tb_menu` (`id`, `name`, `type`, `parent_id`, `path`, `component`, `is_cache`, `is_link`, `visible`, `status`, `icon`, `sort`, `create_time`, `update_time`) VALUES (1651217423560343555, '角色管理', 'P', 1652313423859417088, 'role', 'system/role/index', 0, 0, 0, 1, 'role', 2, '2023-04-26 21:33:29', '2023-12-21 14:20:20');
INSERT INTO `tb_menu` (`id`, `name`, `type`, `parent_id`, `path`, `component`, `is_cache`, `is_link`, `visible`, `status`, `icon`, `sort`, `create_time`, `update_time`) VALUES (1651217423560343556, '友链管理', 'P', 1647951751875133440, 'link', 'blog/link/index', 0, 0, 0, 1, 'personnel', 6, '2023-04-26 21:33:29', '2023-12-21 14:28:37');
INSERT INTO `tb_menu` (`id`, `name`, `type`, `parent_id`, `path`, `component`, `is_cache`, `is_link`, `visible`, `status`, `icon`, `sort`, `create_time`, `update_time`) VALUES (1651217423560343557, '用户管理', 'P', 1652313423859417088, 'user', 'system/user/index', 0, 0, 0, 1, 'personnel-manage', 1, '2023-04-26 21:33:29', '2023-12-21 14:20:16');
INSERT INTO `tb_menu` (`id`, `name`, `type`, `parent_id`, `path`, `component`, `is_cache`, `is_link`, `visible`, `status`, `icon`, `sort`, `create_time`, `update_time`) VALUES (1651217423560343558, '公告管理', 'P', 1647951751875133441, 'notice', 'website/notice/index', 0, 0, 0, 1, 'email', 2, '2023-04-26 21:33:29', '2023-12-21 14:20:02');
INSERT INTO `tb_menu` (`id`, `name`, `type`, `parent_id`, `path`, `component`, `is_cache`, `is_link`, `visible`, `status`, `icon`, `sort`, `create_time`, `update_time`) VALUES (1651217423560343559, '权限管理', 'P', 1652313423859417088, 'security', 'system/security/index', 0, 0, 0, 1, 'dict', 3, '2023-04-26 21:33:29', '2023-12-21 14:20:25');
INSERT INTO `tb_menu` (`id`, `name`, `type`, `parent_id`, `path`, `component`, `is_cache`, `is_link`, `visible`, `status`, `icon`, `sort`, `create_time`, `update_time`) VALUES (1651217423560343560, '菜单管理', 'P', 1652313423859417088, 'menu', 'system/menu/index', 0, 0, 0, 1, 'menu', 4, '2023-04-26 21:33:29', '2023-12-21 14:20:29');
INSERT INTO `tb_menu` (`id`, `name`, `type`, `parent_id`, `path`, `component`, `is_cache`, `is_link`, `visible`, `status`, `icon`, `sort`, `create_time`, `update_time`) VALUES (1651217423560343561, '网站配置', 'P', 1647951751875133441, 'config', 'website/config/index', 0, 0, 0, 1, 'edit', 3, '2023-04-26 21:33:29', '2023-12-21 14:20:07');
INSERT INTO `tb_menu` (`id`, `name`, `type`, `parent_id`, `path`, `component`, `is_cache`, `is_link`, `visible`, `status`, `icon`, `sort`, `create_time`, `update_time`) VALUES (1651227500568641536, '系统监控', 'D', 0, 'monitor', NULL, 0, 0, 0, 1, 'monitor', 4, '2023-04-26 22:15:11', '2023-08-03 19:35:16');
INSERT INTO `tb_menu` (`id`, `name`, `type`, `parent_id`, `path`, `component`, `is_cache`, `is_link`, `visible`, `status`, `icon`, `sort`, `create_time`, `update_time`) VALUES (1651227500568641537, '在线用户', 'P', 1651227500568641536, 'online', 'monitor/online/index', 0, 0, 0, 1, 'online', 1, '2023-04-26 22:15:55', '2023-08-03 19:35:19');
INSERT INTO `tb_menu` (`id`, `name`, `type`, `parent_id`, `path`, `component`, `is_cache`, `is_link`, `visible`, `status`, `icon`, `sort`, `create_time`, `update_time`) VALUES (1651227500568641538, '服务监控', 'P', 1651227500568641536, 'server', 'monitor/server/index', 0, 0, 0, 1, 'server', 2, '2023-04-26 22:16:24', '2023-08-03 19:35:20');
INSERT INTO `tb_menu` (`id`, `name`, `type`, `parent_id`, `path`, `component`, `is_cache`, `is_link`, `visible`, `status`, `icon`, `sort`, `create_time`, `update_time`) VALUES (1651227500568641539, '缓存管理', 'P', 1651227500568641536, 'cache', 'monitor/cache/index', 0, 0, 0, 1, 'redis', 3, '2023-04-26 22:16:59', '2023-08-03 19:35:21');
INSERT INTO `tb_menu` (`id`, `name`, `type`, `parent_id`, `path`, `component`, `is_cache`, `is_link`, `visible`, `status`, `icon`, `sort`, `create_time`, `update_time`) VALUES (1651227500568641540, '操作日志', 'P', 1651227500568641536, 'operate-log', 'monitor/operate-log/index', 0, 0, 0, 1, 'form', 5, '2023-04-26 22:18:09', '2023-12-24 22:35:02');
INSERT INTO `tb_menu` (`id`, `name`, `type`, `parent_id`, `path`, `component`, `is_cache`, `is_link`, `visible`, `status`, `icon`, `sort`, `create_time`, `update_time`) VALUES (1651227500568641541, '登录日志', 'P', 1651227500568641536, 'login-log', 'monitor/login-log/index', 0, 0, 0, 1, 'logininfor', 6, '2023-04-26 22:18:32', '2023-12-24 22:35:07');
INSERT INTO `tb_menu` (`id`, `name`, `type`, `parent_id`, `path`, `component`, `is_cache`, `is_link`, `visible`, `status`, `icon`, `sort`, `create_time`, `update_time`) VALUES (1652313423859417088, '系统管理', 'D', 0, 'system', NULL, 0, 0, 0, 1, 'system', 3, '2023-04-29 22:08:18', '2023-08-03 19:35:24');
INSERT INTO `tb_menu` (`id`, `name`, `type`, `parent_id`, `path`, `component`, `is_cache`, `is_link`, `visible`, `status`, `icon`, `sort`, `create_time`, `update_time`) VALUES (1714673277053116418, '访问项目', 'D', 0, 'project', NULL, 0, 0, 0, 1, 'content', 5, '2023-10-19 00:02:21', '2023-12-20 16:24:06');
INSERT INTO `tb_menu` (`id`, `name`, `type`, `parent_id`, `path`, `component`, `is_cache`, `is_link`, `visible`, `status`, `icon`, `sort`, `create_time`, `update_time`) VALUES (1714673499896487938, '相册纪念馆', 'P', 1714673277053116418, 'https://album.zrkizzy.com', NULL, 0, 0, 0, 1, 'permission', 2, '2023-10-19 00:03:14', '2023-12-21 14:20:42');
INSERT INTO `tb_menu` (`id`, `name`, `type`, `parent_id`, `path`, `component`, `is_cache`, `is_link`, `visible`, `status`, `icon`, `sort`, `create_time`, `update_time`) VALUES (1714673667987415041, '博客首页', 'P', 1714673277053116418, 'https://www.zrkizzy.com', NULL, 0, 0, 0, 1, 'computer', 1, '2023-10-19 00:03:54', '2023-12-21 14:20:46');
INSERT INTO `tb_menu` (`id`, `name`, `type`, `parent_id`, `path`, `component`, `is_cache`, `is_link`, `visible`, `status`, `icon`, `sort`, `create_time`, `update_time`) VALUES (1719375047478120449, '发布文章', 'P', 1737661101677502466, 'article-create', 'blog/article/components/article-create/index', 0, 0, 1, 1, 'article-create', 0, '2023-10-31 23:25:30', '2023-12-21 14:29:32');
INSERT INTO `tb_menu` (`id`, `name`, `type`, `parent_id`, `path`, `component`, `is_cache`, `is_link`, `visible`, `status`, `icon`, `sort`, `create_time`, `update_time`) VALUES (1719376052710182914, '文章详情', 'P', 1737661101677502466, 'article/edit/:id', 'blog/article/components/article-detail/index', 0, 0, 1, 1, 'article', 0, '2023-10-31 23:29:30', '2023-12-21 14:29:41');
INSERT INTO `tb_menu` (`id`, `name`, `type`, `parent_id`, `path`, `component`, `is_cache`, `is_link`, `visible`, `status`, `icon`, `sort`, `create_time`, `update_time`) VALUES (1726889420524638209, '发布成功', 'P', 1737661101677502466, 'article/success', 'blog/article/components/article-success/index', 0, 0, 1, 1, 'computer', 0, '2023-11-21 17:04:57', '2023-12-21 14:29:48');
INSERT INTO `tb_menu` (`id`, `name`, `type`, `parent_id`, `path`, `component`, `is_cache`, `is_link`, `visible`, `status`, `icon`, `sort`, `create_time`, `update_time`) VALUES (1737661101677502466, '文章组件', 'D', 0, 'article-content', NULL, 0, 0, 1, 1, 'article-create', 6, '2023-12-21 10:27:45', NULL);
INSERT INTO `tb_menu` (`id`, `name`, `type`, `parent_id`, `path`, `component`, `is_cache`, `is_link`, `visible`, `status`, `icon`, `sort`, `create_time`, `update_time`) VALUES (1737664918557118466, '配置组件', 'D', 0, 'config', NULL, 0, 0, 1, 1, 'component', 7, '2023-12-21 10:42:55', NULL);
INSERT INTO `tb_menu` (`id`, `name`, `type`, `parent_id`, `path`, `component`, `is_cache`, `is_link`, `visible`, `status`, `icon`, `sort`, `create_time`, `update_time`) VALUES (1737665649209069569, '其他配置', 'P', 1737664918557118466, 'other', 'website/config/components/other/index.vue', 0, 0, 1, 1, 'system', 0, '2023-12-21 10:45:50', '2023-12-21 10:46:13');
INSERT INTO `tb_menu` (`id`, `name`, `type`, `parent_id`, `path`, `component`, `is_cache`, `is_link`, `visible`, `status`, `icon`, `sort`, `create_time`, `update_time`) VALUES (1737666447162822658, '关于我', 'P', 1737664918557118466, 'about-me', 'website/config/components/about-me/index.vue', 0, 0, 1, 1, 'personnel-manage', 0, '2023-12-21 10:49:00', NULL);
INSERT INTO `tb_menu` (`id`, `name`, `type`, `parent_id`, `path`, `component`, `is_cache`, `is_link`, `visible`, `status`, `icon`, `sort`, `create_time`, `update_time`) VALUES (1737666885635362818, '权限组件', 'D', 0, 'security-content', NULL, 0, 0, 1, 1, 'swagger', 8, '2023-12-21 10:50:44', '2023-12-21 14:16:23');
INSERT INTO `tb_menu` (`id`, `name`, `type`, `parent_id`, `path`, `component`, `is_cache`, `is_link`, `visible`, `status`, `icon`, `sort`, `create_time`, `update_time`) VALUES (1737668345395761153, '接口信息', 'P', 1737666885635362818, 'resource', 'system/security/components/resource/index.vue', 0, 0, 1, 1, 'article', 0, '2023-12-21 10:56:33', '2023-12-21 14:15:09');
INSERT INTO `tb_menu` (`id`, `name`, `type`, `parent_id`, `path`, `component`, `is_cache`, `is_link`, `visible`, `status`, `icon`, `sort`, `create_time`, `update_time`) VALUES (1737668522479276033, '模块管理', 'P', 1737666885635362818, 'module/:id', 'system/security/components/module/index.vue', 0, 0, 1, 1, 'tree-table', 0, '2023-12-21 10:57:15', '2023-12-21 14:15:14');
INSERT INTO `tb_menu` (`id`, `name`, `type`, `parent_id`, `path`, `component`, `is_cache`, `is_link`, `visible`, `status`, `icon`, `sort`, `create_time`, `update_time`) VALUES (1738931248316907521, '缓存监控', 'P', 1651227500568641536, 'cache-monitor', 'monitor/cache/monitor', 0, 0, 0, 1, 'monitor', 4, '2023-12-24 22:34:52', '2023-12-24 22:35:55');

-- ----------------------------
-- Table structure for tb_menu_role
-- ----------------------------
CREATE TABLE `tb_menu_role` (
  `id` bigint NOT NULL COMMENT '主键',
  `role_id` bigint DEFAULT NULL COMMENT '角色ID',
  `menu_id` bigint DEFAULT NULL COMMENT '菜单ID',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  PRIMARY KEY (`id`) USING BTREE
) COMMENT='菜单角色关联表';

-- ----------------------------
-- Records of tb_menu_role
-- ----------------------------

INSERT INTO `tb_menu_role` (`id`, `role_id`, `menu_id`, `create_time`) VALUES (1738939045255315456, 1633657944153260032, 1647951751875133440, NULL);
INSERT INTO `tb_menu_role` (`id`, `role_id`, `menu_id`, `create_time`) VALUES (1738939045255315457, 1633657944153260032, 1647952968122630144, NULL);
INSERT INTO `tb_menu_role` (`id`, `role_id`, `menu_id`, `create_time`) VALUES (1738939045255315458, 1633657944153260032, 1648539108017963008, NULL);
INSERT INTO `tb_menu_role` (`id`, `role_id`, `menu_id`, `create_time`) VALUES (1738939045255315459, 1633657944153260032, 1651040949146484736, NULL);
INSERT INTO `tb_menu_role` (`id`, `role_id`, `menu_id`, `create_time`) VALUES (1738939045255315460, 1633657944153260032, 1651040949146484737, NULL);
INSERT INTO `tb_menu_role` (`id`, `role_id`, `menu_id`, `create_time`) VALUES (1738939045255315461, 1633657944153260032, 1651042040491802625, NULL);
INSERT INTO `tb_menu_role` (`id`, `role_id`, `menu_id`, `create_time`) VALUES (1738939045255315462, 1633657944153260032, 1651217423560343553, NULL);
INSERT INTO `tb_menu_role` (`id`, `role_id`, `menu_id`, `create_time`) VALUES (1738939045255315463, 1633657944153260032, 1651217423560343554, NULL);
INSERT INTO `tb_menu_role` (`id`, `role_id`, `menu_id`, `create_time`) VALUES (1738939045255315464, 1633657944153260032, 1651217423560343556, NULL);
INSERT INTO `tb_menu_role` (`id`, `role_id`, `menu_id`, `create_time`) VALUES (1738939045255315465, 1633657944153260032, 1647951751875133441, NULL);
INSERT INTO `tb_menu_role` (`id`, `role_id`, `menu_id`, `create_time`) VALUES (1738939045255315466, 1633657944153260032, 1651217423560343561, NULL);
INSERT INTO `tb_menu_role` (`id`, `role_id`, `menu_id`, `create_time`) VALUES (1738939045255315467, 1633657944153260032, 1651217423560343558, NULL);
INSERT INTO `tb_menu_role` (`id`, `role_id`, `menu_id`, `create_time`) VALUES (1738939045255315468, 1633657944153260032, 1651217423560343552, NULL);
INSERT INTO `tb_menu_role` (`id`, `role_id`, `menu_id`, `create_time`) VALUES (1738939045255315469, 1633657944153260032, 1652313423859417088, NULL);
INSERT INTO `tb_menu_role` (`id`, `role_id`, `menu_id`, `create_time`) VALUES (1738939045255315470, 1633657944153260032, 1651217423560343557, NULL);
INSERT INTO `tb_menu_role` (`id`, `role_id`, `menu_id`, `create_time`) VALUES (1738939045255315471, 1633657944153260032, 1651217423560343559, NULL);
INSERT INTO `tb_menu_role` (`id`, `role_id`, `menu_id`, `create_time`) VALUES (1738939045255315472, 1633657944153260032, 1651217423560343560, NULL);
INSERT INTO `tb_menu_role` (`id`, `role_id`, `menu_id`, `create_time`) VALUES (1738939045255315473, 1633657944153260032, 1651217423560343555, NULL);
INSERT INTO `tb_menu_role` (`id`, `role_id`, `menu_id`, `create_time`) VALUES (1738939045255315474, 1633657944153260032, 1651227500568641536, NULL);
INSERT INTO `tb_menu_role` (`id`, `role_id`, `menu_id`, `create_time`) VALUES (1738939045255315475, 1633657944153260032, 1738931248316907521, NULL);
INSERT INTO `tb_menu_role` (`id`, `role_id`, `menu_id`, `create_time`) VALUES (1738939045255315476, 1633657944153260032, 1651227500568641541, NULL);
INSERT INTO `tb_menu_role` (`id`, `role_id`, `menu_id`, `create_time`) VALUES (1738939045255315477, 1633657944153260032, 1651227500568641540, NULL);
INSERT INTO `tb_menu_role` (`id`, `role_id`, `menu_id`, `create_time`) VALUES (1738939045255315478, 1633657944153260032, 1651227500568641539, NULL);
INSERT INTO `tb_menu_role` (`id`, `role_id`, `menu_id`, `create_time`) VALUES (1738939045255315479, 1633657944153260032, 1651227500568641538, NULL);
INSERT INTO `tb_menu_role` (`id`, `role_id`, `menu_id`, `create_time`) VALUES (1738939045255315480, 1633657944153260032, 1651227500568641537, NULL);
INSERT INTO `tb_menu_role` (`id`, `role_id`, `menu_id`, `create_time`) VALUES (1738939045255315481, 1633657944153260032, 1714673277053116418, NULL);
INSERT INTO `tb_menu_role` (`id`, `role_id`, `menu_id`, `create_time`) VALUES (1738939045255315482, 1633657944153260032, 1714673499896487938, NULL);
INSERT INTO `tb_menu_role` (`id`, `role_id`, `menu_id`, `create_time`) VALUES (1738939045255315483, 1633657944153260032, 1714673667987415041, NULL);
INSERT INTO `tb_menu_role` (`id`, `role_id`, `menu_id`, `create_time`) VALUES (1738939045255315484, 1633657944153260032, 1647951751875133442, NULL);
INSERT INTO `tb_menu_role` (`id`, `role_id`, `menu_id`, `create_time`) VALUES (1738939045255315485, 1633657944153260032, 1737661101677502466, NULL);
INSERT INTO `tb_menu_role` (`id`, `role_id`, `menu_id`, `create_time`) VALUES (1738939045255315486, 1633657944153260032, 1719375047478120449, NULL);
INSERT INTO `tb_menu_role` (`id`, `role_id`, `menu_id`, `create_time`) VALUES (1738939045255315487, 1633657944153260032, 1719376052710182914, NULL);
INSERT INTO `tb_menu_role` (`id`, `role_id`, `menu_id`, `create_time`) VALUES (1738939045255315488, 1633657944153260032, 1726889420524638209, NULL);
INSERT INTO `tb_menu_role` (`id`, `role_id`, `menu_id`, `create_time`) VALUES (1738939045255315489, 1633657944153260032, 1737664918557118466, NULL);
INSERT INTO `tb_menu_role` (`id`, `role_id`, `menu_id`, `create_time`) VALUES (1738939045255315490, 1633657944153260032, 1737665649209069569, NULL);
INSERT INTO `tb_menu_role` (`id`, `role_id`, `menu_id`, `create_time`) VALUES (1738939045255315491, 1633657944153260032, 1737666447162822658, NULL);
INSERT INTO `tb_menu_role` (`id`, `role_id`, `menu_id`, `create_time`) VALUES (1738939045255315492, 1633657944153260032, 1737666885635362818, NULL);
INSERT INTO `tb_menu_role` (`id`, `role_id`, `menu_id`, `create_time`) VALUES (1738939045255315493, 1633657944153260032, 1737668345395761153, NULL);
INSERT INTO `tb_menu_role` (`id`, `role_id`, `menu_id`, `create_time`) VALUES (1738939045255315494, 1633657944153260032, 1737668522479276033, NULL);
INSERT INTO `tb_menu_role` (`id`, `role_id`, `menu_id`, `create_time`) VALUES (1738939597733232640, 1667607066451116032, 1647951751875133440, NULL);
INSERT INTO `tb_menu_role` (`id`, `role_id`, `menu_id`, `create_time`) VALUES (1738939597733232641, 1667607066451116032, 1647952968122630144, NULL);
INSERT INTO `tb_menu_role` (`id`, `role_id`, `menu_id`, `create_time`) VALUES (1738939597733232642, 1667607066451116032, 1648539108017963008, NULL);
INSERT INTO `tb_menu_role` (`id`, `role_id`, `menu_id`, `create_time`) VALUES (1738939597733232643, 1667607066451116032, 1651040949146484736, NULL);
INSERT INTO `tb_menu_role` (`id`, `role_id`, `menu_id`, `create_time`) VALUES (1738939597733232644, 1667607066451116032, 1651040949146484737, NULL);
INSERT INTO `tb_menu_role` (`id`, `role_id`, `menu_id`, `create_time`) VALUES (1738939597733232645, 1667607066451116032, 1651042040491802625, NULL);
INSERT INTO `tb_menu_role` (`id`, `role_id`, `menu_id`, `create_time`) VALUES (1738939597733232646, 1667607066451116032, 1651217423560343553, NULL);
INSERT INTO `tb_menu_role` (`id`, `role_id`, `menu_id`, `create_time`) VALUES (1738939597733232647, 1667607066451116032, 1651217423560343554, NULL);
INSERT INTO `tb_menu_role` (`id`, `role_id`, `menu_id`, `create_time`) VALUES (1738939597733232648, 1667607066451116032, 1651217423560343556, NULL);
INSERT INTO `tb_menu_role` (`id`, `role_id`, `menu_id`, `create_time`) VALUES (1738939597733232649, 1667607066451116032, 1647951751875133441, NULL);
INSERT INTO `tb_menu_role` (`id`, `role_id`, `menu_id`, `create_time`) VALUES (1738939597733232650, 1667607066451116032, 1651217423560343561, NULL);
INSERT INTO `tb_menu_role` (`id`, `role_id`, `menu_id`, `create_time`) VALUES (1738939597733232651, 1667607066451116032, 1651217423560343558, NULL);
INSERT INTO `tb_menu_role` (`id`, `role_id`, `menu_id`, `create_time`) VALUES (1738939597733232652, 1667607066451116032, 1651217423560343552, NULL);
INSERT INTO `tb_menu_role` (`id`, `role_id`, `menu_id`, `create_time`) VALUES (1738939597733232653, 1667607066451116032, 1652313423859417088, NULL);
INSERT INTO `tb_menu_role` (`id`, `role_id`, `menu_id`, `create_time`) VALUES (1738939597733232654, 1667607066451116032, 1651217423560343557, NULL);
INSERT INTO `tb_menu_role` (`id`, `role_id`, `menu_id`, `create_time`) VALUES (1738939597733232655, 1667607066451116032, 1651217423560343559, NULL);
INSERT INTO `tb_menu_role` (`id`, `role_id`, `menu_id`, `create_time`) VALUES (1738939597733232656, 1667607066451116032, 1651217423560343560, NULL);
INSERT INTO `tb_menu_role` (`id`, `role_id`, `menu_id`, `create_time`) VALUES (1738939597733232657, 1667607066451116032, 1651217423560343555, NULL);
INSERT INTO `tb_menu_role` (`id`, `role_id`, `menu_id`, `create_time`) VALUES (1738939597733232658, 1667607066451116032, 1651227500568641536, NULL);
INSERT INTO `tb_menu_role` (`id`, `role_id`, `menu_id`, `create_time`) VALUES (1738939597733232659, 1667607066451116032, 1738931248316907521, NULL);
INSERT INTO `tb_menu_role` (`id`, `role_id`, `menu_id`, `create_time`) VALUES (1738939597733232660, 1667607066451116032, 1651227500568641541, NULL);
INSERT INTO `tb_menu_role` (`id`, `role_id`, `menu_id`, `create_time`) VALUES (1738939597733232661, 1667607066451116032, 1651227500568641540, NULL);
INSERT INTO `tb_menu_role` (`id`, `role_id`, `menu_id`, `create_time`) VALUES (1738939597733232662, 1667607066451116032, 1651227500568641539, NULL);
INSERT INTO `tb_menu_role` (`id`, `role_id`, `menu_id`, `create_time`) VALUES (1738939597733232663, 1667607066451116032, 1651227500568641538, NULL);
INSERT INTO `tb_menu_role` (`id`, `role_id`, `menu_id`, `create_time`) VALUES (1738939597733232664, 1667607066451116032, 1651227500568641537, NULL);
INSERT INTO `tb_menu_role` (`id`, `role_id`, `menu_id`, `create_time`) VALUES (1738939597733232665, 1667607066451116032, 1714673277053116418, NULL);
INSERT INTO `tb_menu_role` (`id`, `role_id`, `menu_id`, `create_time`) VALUES (1738939597733232666, 1667607066451116032, 1714673499896487938, NULL);
INSERT INTO `tb_menu_role` (`id`, `role_id`, `menu_id`, `create_time`) VALUES (1738939597733232667, 1667607066451116032, 1714673667987415041, NULL);
INSERT INTO `tb_menu_role` (`id`, `role_id`, `menu_id`, `create_time`) VALUES (1738939597733232668, 1667607066451116032, 1647951751875133442, NULL);
INSERT INTO `tb_menu_role` (`id`, `role_id`, `menu_id`, `create_time`) VALUES (1738939597733232669, 1667607066451116032, 1737661101677502466, NULL);
INSERT INTO `tb_menu_role` (`id`, `role_id`, `menu_id`, `create_time`) VALUES (1738939597733232670, 1667607066451116032, 1719375047478120449, NULL);
INSERT INTO `tb_menu_role` (`id`, `role_id`, `menu_id`, `create_time`) VALUES (1738939597733232671, 1667607066451116032, 1719376052710182914, NULL);
INSERT INTO `tb_menu_role` (`id`, `role_id`, `menu_id`, `create_time`) VALUES (1738939597733232672, 1667607066451116032, 1726889420524638209, NULL);
INSERT INTO `tb_menu_role` (`id`, `role_id`, `menu_id`, `create_time`) VALUES (1738939597733232673, 1667607066451116032, 1737664918557118466, NULL);
INSERT INTO `tb_menu_role` (`id`, `role_id`, `menu_id`, `create_time`) VALUES (1738939597733232674, 1667607066451116032, 1737665649209069569, NULL);
INSERT INTO `tb_menu_role` (`id`, `role_id`, `menu_id`, `create_time`) VALUES (1738939597733232675, 1667607066451116032, 1737666447162822658, NULL);
INSERT INTO `tb_menu_role` (`id`, `role_id`, `menu_id`, `create_time`) VALUES (1738939597733232676, 1667607066451116032, 1737666885635362818, NULL);
INSERT INTO `tb_menu_role` (`id`, `role_id`, `menu_id`, `create_time`) VALUES (1738939597733232677, 1667607066451116032, 1737668345395761153, NULL);
INSERT INTO `tb_menu_role` (`id`, `role_id`, `menu_id`, `create_time`) VALUES (1738939597733232678, 1667607066451116032, 1737668522479276033, NULL);

-- ----------------------------
-- Table structure for tb_module
-- ----------------------------
CREATE TABLE `tb_module` (
  `id` bigint NOT NULL COMMENT '主键',
  `name` varchar(50) NOT NULL COMMENT '模块名称',
  `description` varchar(255) DEFAULT NULL COMMENT '模块描述',
  `create_time` datetime NOT NULL COMMENT '创建时间',
  `update_time` datetime NOT NULL ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
  PRIMARY KEY (`id`) USING BTREE
) COMMENT='资源模块表';

-- ----------------------------
-- Records of tb_module
-- ----------------------------

INSERT INTO `tb_module` (`id`, `name`, `description`, `create_time`, `update_time`) VALUES (1636182933754609664, '博客管理模块', '博客管理模块集文章、评论、留言、分类、标签管理于一体，提供全面的博客内容控制与组织功能，助力用户高效管理和维护博客平台。', '2023-03-16 10:04:53', '2023-12-21 13:37:16');
INSERT INTO `tb_module` (`id`, `name`, `description`, `create_time`, `update_time`) VALUES (1636182933754609665, '系统管理模块', '系统管理模块综合处理菜单、文件、网站配置、公告、缓存等多方面功能，为用户提供全面而高效的系统操作和管理手段。', '2023-03-16 10:05:44', '2023-12-21 13:39:07');
INSERT INTO `tb_module` (`id`, `name`, `description`, `create_time`, `update_time`) VALUES (1686716084163444736, '项目测试模块', '该测试模块基于RBAC权限系统，具备全部资源请求权限，但在增删改方面受限。通过该模块，系统可以全面测试资源的访问情况，确保权限体系的完整性和稳定性，同时有效防范非授权修改操作。', '2023-08-02 20:30:27', '2023-12-21 13:39:16');

-- ----------------------------
-- Table structure for tb_module_resource
-- ----------------------------
CREATE TABLE `tb_module_resource` (
  `id` bigint NOT NULL COMMENT '主键',
  `module_id` bigint DEFAULT NULL COMMENT '模块主键',
  `resource_id` bigint DEFAULT NULL COMMENT '资源主键',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  PRIMARY KEY (`id`) USING BTREE,
  KEY `idx_module_id` (`module_id`) USING BTREE COMMENT '模块ID索引'
) COMMENT='资源模块关联表';

-- ----------------------------
-- Records of tb_module_resource
-- ----------------------------

INSERT INTO `tb_module_resource` (`id`, `module_id`, `resource_id`, `create_time`) VALUES (1737720467269091328, 1636182933754609664, 1698715961391054848, '2023-12-21 14:23:39');
INSERT INTO `tb_module_resource` (`id`, `module_id`, `resource_id`, `create_time`) VALUES (1737720467269091329, 1636182933754609664, 1698715961395249152, '2023-12-21 14:23:39');
INSERT INTO `tb_module_resource` (`id`, `module_id`, `resource_id`, `create_time`) VALUES (1737720467269091330, 1636182933754609664, 1698715961395249153, '2023-12-21 14:23:39');
INSERT INTO `tb_module_resource` (`id`, `module_id`, `resource_id`, `create_time`) VALUES (1737720467269091331, 1636182933754609664, 1698715961395249154, '2023-12-21 14:23:39');
INSERT INTO `tb_module_resource` (`id`, `module_id`, `resource_id`, `create_time`) VALUES (1737720467269091332, 1636182933754609664, 1709566124468731904, '2023-12-21 14:23:39');
INSERT INTO `tb_module_resource` (`id`, `module_id`, `resource_id`, `create_time`) VALUES (1737720467269091333, 1636182933754609664, 1671394657394753536, '2023-12-21 14:23:39');
INSERT INTO `tb_module_resource` (`id`, `module_id`, `resource_id`, `create_time`) VALUES (1737720467269091334, 1636182933754609664, 1671394657394753537, '2023-12-21 14:23:39');
INSERT INTO `tb_module_resource` (`id`, `module_id`, `resource_id`, `create_time`) VALUES (1737720467269091335, 1636182933754609664, 1671394657394753538, '2023-12-21 14:23:39');
INSERT INTO `tb_module_resource` (`id`, `module_id`, `resource_id`, `create_time`) VALUES (1737720467269091336, 1636182933754609664, 1671394657394753539, '2023-12-21 14:23:39');
INSERT INTO `tb_module_resource` (`id`, `module_id`, `resource_id`, `create_time`) VALUES (1737720467269091337, 1636182933754609664, 1694706666026565632, '2023-12-21 14:23:39');
INSERT INTO `tb_module_resource` (`id`, `module_id`, `resource_id`, `create_time`) VALUES (1737720467269091338, 1636182933754609664, 1694706666030759936, '2023-12-21 14:23:39');
INSERT INTO `tb_module_resource` (`id`, `module_id`, `resource_id`, `create_time`) VALUES (1737720467269091339, 1636182933754609664, 1704132386586886144, '2023-12-21 14:23:39');
INSERT INTO `tb_module_resource` (`id`, `module_id`, `resource_id`, `create_time`) VALUES (1737720467269091340, 1636182933754609664, 1704132386586886145, '2023-12-21 14:23:39');
INSERT INTO `tb_module_resource` (`id`, `module_id`, `resource_id`, `create_time`) VALUES (1737720467269091341, 1636182933754609664, 1704132386586886146, '2023-12-21 14:23:39');
INSERT INTO `tb_module_resource` (`id`, `module_id`, `resource_id`, `create_time`) VALUES (1737720467269091342, 1636182933754609664, 1704132386586886147, '2023-12-21 14:23:39');
INSERT INTO `tb_module_resource` (`id`, `module_id`, `resource_id`, `create_time`) VALUES (1737720467269091343, 1636182933754609664, 1704894548829798400, '2023-12-21 14:23:39');
INSERT INTO `tb_module_resource` (`id`, `module_id`, `resource_id`, `create_time`) VALUES (1737720467269091344, 1636182933754609664, 1699069650102386688, '2023-12-21 14:23:39');
INSERT INTO `tb_module_resource` (`id`, `module_id`, `resource_id`, `create_time`) VALUES (1737720467269091345, 1636182933754609664, 1699069650102386689, '2023-12-21 14:23:39');
INSERT INTO `tb_module_resource` (`id`, `module_id`, `resource_id`, `create_time`) VALUES (1737720467269091346, 1636182933754609664, 1699069650102386690, '2023-12-21 14:23:39');
INSERT INTO `tb_module_resource` (`id`, `module_id`, `resource_id`, `create_time`) VALUES (1737720467269091347, 1636182933754609664, 1699069650102386691, '2023-12-21 14:23:39');
INSERT INTO `tb_module_resource` (`id`, `module_id`, `resource_id`, `create_time`) VALUES (1737720467269091348, 1636182933754609664, 1709846740854636544, '2023-12-21 14:23:39');
INSERT INTO `tb_module_resource` (`id`, `module_id`, `resource_id`, `create_time`) VALUES (1737720467269091349, 1636182933754609664, 1700868600539119616, '2023-12-21 14:23:39');
INSERT INTO `tb_module_resource` (`id`, `module_id`, `resource_id`, `create_time`) VALUES (1737720467269091350, 1636182933754609664, 1700868600543313920, '2023-12-21 14:23:39');
INSERT INTO `tb_module_resource` (`id`, `module_id`, `resource_id`, `create_time`) VALUES (1738022306116009984, 1636182933754609665, 1686025120788774912, '2023-12-22 10:23:03');
INSERT INTO `tb_module_resource` (`id`, `module_id`, `resource_id`, `create_time`) VALUES (1738022306116009985, 1636182933754609665, 1686370540320718848, '2023-12-22 10:23:03');
INSERT INTO `tb_module_resource` (`id`, `module_id`, `resource_id`, `create_time`) VALUES (1738022306116009986, 1636182933754609665, 1677234418973933568, '2023-12-22 10:23:03');
INSERT INTO `tb_module_resource` (`id`, `module_id`, `resource_id`, `create_time`) VALUES (1738022306116009987, 1636182933754609665, 1647952968122630145, '2023-12-22 10:23:03');
INSERT INTO `tb_module_resource` (`id`, `module_id`, `resource_id`, `create_time`) VALUES (1738022306116009988, 1636182933754609665, 1686722622882054144, '2023-12-22 10:23:03');
INSERT INTO `tb_module_resource` (`id`, `module_id`, `resource_id`, `create_time`) VALUES (1738022306116009989, 1636182933754609665, 1688037885271343104, '2023-12-22 10:23:03');
INSERT INTO `tb_module_resource` (`id`, `module_id`, `resource_id`, `create_time`) VALUES (1738022306116009990, 1636182933754609665, 1688049564466020352, '2023-12-22 10:23:03');
INSERT INTO `tb_module_resource` (`id`, `module_id`, `resource_id`, `create_time`) VALUES (1738022306116009991, 1636182933754609665, 1688106647613865984, '2023-12-22 10:23:03');
INSERT INTO `tb_module_resource` (`id`, `module_id`, `resource_id`, `create_time`) VALUES (1738022306116009992, 1636182933754609665, 1688106647613865993, '2023-12-22 10:23:03');
INSERT INTO `tb_module_resource` (`id`, `module_id`, `resource_id`, `create_time`) VALUES (1738022306116009993, 1636182933754609665, 1678704512069533696, '2023-12-22 10:23:03');
INSERT INTO `tb_module_resource` (`id`, `module_id`, `resource_id`, `create_time`) VALUES (1738022306116009994, 1636182933754609665, 1679151716034936832, '2023-12-22 10:23:03');
INSERT INTO `tb_module_resource` (`id`, `module_id`, `resource_id`, `create_time`) VALUES (1738022306116009995, 1636182933754609665, 1679512161577074688, '2023-12-22 10:23:03');
INSERT INTO `tb_module_resource` (`id`, `module_id`, `resource_id`, `create_time`) VALUES (1738022306116009996, 1636182933754609665, 1679675931226013696, '2023-12-22 10:23:03');
INSERT INTO `tb_module_resource` (`id`, `module_id`, `resource_id`, `create_time`) VALUES (1738022306116009997, 1636182933754609665, 1679732738346713088, '2023-12-22 10:23:03');
INSERT INTO `tb_module_resource` (`id`, `module_id`, `resource_id`, `create_time`) VALUES (1738022306116009998, 1636182933754609665, 1680179775546589184, '2023-12-22 10:23:03');
INSERT INTO `tb_module_resource` (`id`, `module_id`, `resource_id`, `create_time`) VALUES (1738022306116009999, 1636182933754609665, 1680179775546589185, '2023-12-22 10:23:03');
INSERT INTO `tb_module_resource` (`id`, `module_id`, `resource_id`, `create_time`) VALUES (1738022306116010000, 1636182933754609665, 1738021576667824128, '2023-12-22 10:23:03');
INSERT INTO `tb_module_resource` (`id`, `module_id`, `resource_id`, `create_time`) VALUES (1738022306116010001, 1636182933754609665, 1684832097144930304, '2023-12-22 10:23:03');
INSERT INTO `tb_module_resource` (`id`, `module_id`, `resource_id`, `create_time`) VALUES (1738022306116010002, 1636182933754609665, 1685590302728912896, '2023-12-22 10:23:03');
INSERT INTO `tb_module_resource` (`id`, `module_id`, `resource_id`, `create_time`) VALUES (1738022306116010003, 1636182933754609665, 1685641090620719104, '2023-12-22 10:23:03');
INSERT INTO `tb_module_resource` (`id`, `module_id`, `resource_id`, `create_time`) VALUES (1738022306116010004, 1636182933754609665, 1685664653591445512, '2023-12-22 10:23:03');
INSERT INTO `tb_module_resource` (`id`, `module_id`, `resource_id`, `create_time`) VALUES (1738022306116010005, 1636182933754609665, 1682422693888000001, '2023-12-22 10:23:03');
INSERT INTO `tb_module_resource` (`id`, `module_id`, `resource_id`, `create_time`) VALUES (1738022306116010006, 1636182933754609665, 1683094177929232384, '2023-12-22 10:23:03');
INSERT INTO `tb_module_resource` (`id`, `module_id`, `resource_id`, `create_time`) VALUES (1738022306116010007, 1636182933754609665, 1676421084687106050, '2023-12-22 10:23:03');
INSERT INTO `tb_module_resource` (`id`, `module_id`, `resource_id`, `create_time`) VALUES (1738022306116010008, 1636182933754609665, 1676421084687106051, '2023-12-22 10:23:03');
INSERT INTO `tb_module_resource` (`id`, `module_id`, `resource_id`, `create_time`) VALUES (1738022306116010009, 1636182933754609665, 1676421084687106052, '2023-12-22 10:23:03');
INSERT INTO `tb_module_resource` (`id`, `module_id`, `resource_id`, `create_time`) VALUES (1738022306116010010, 1636182933754609665, 1636187548919267329, '2023-12-22 10:23:03');
INSERT INTO `tb_module_resource` (`id`, `module_id`, `resource_id`, `create_time`) VALUES (1738022306116010011, 1636182933754609665, 1667050222783561728, '2023-12-22 10:23:03');
INSERT INTO `tb_module_resource` (`id`, `module_id`, `resource_id`, `create_time`) VALUES (1738022306116010012, 1636182933754609665, 1667578577488445440, '2023-12-22 10:23:03');
INSERT INTO `tb_module_resource` (`id`, `module_id`, `resource_id`, `create_time`) VALUES (1738022306116010013, 1636182933754609665, 1667598029198196736, '2023-12-22 10:23:03');
INSERT INTO `tb_module_resource` (`id`, `module_id`, `resource_id`, `create_time`) VALUES (1738022306116010014, 1636182933754609665, 1684082256563404800, '2023-12-22 10:23:03');
INSERT INTO `tb_module_resource` (`id`, `module_id`, `resource_id`, `create_time`) VALUES (1738022306116010015, 1636182933754609665, 1683131008930545664, '2023-12-22 10:23:03');
INSERT INTO `tb_module_resource` (`id`, `module_id`, `resource_id`, `create_time`) VALUES (1738022306116010016, 1636182933754609665, 1675800348175892480, '2023-12-22 10:23:03');
INSERT INTO `tb_module_resource` (`id`, `module_id`, `resource_id`, `create_time`) VALUES (1738022306116010017, 1636182933754609665, 1675800348175892481, '2023-12-22 10:23:03');
INSERT INTO `tb_module_resource` (`id`, `module_id`, `resource_id`, `create_time`) VALUES (1738022306116010018, 1636182933754609665, 1676236928191561728, '2023-12-22 10:23:03');
INSERT INTO `tb_module_resource` (`id`, `module_id`, `resource_id`, `create_time`) VALUES (1738022306116010019, 1636182933754609665, 1675800348175892482, '2023-12-22 10:23:03');
INSERT INTO `tb_module_resource` (`id`, `module_id`, `resource_id`, `create_time`) VALUES (1738022306116010020, 1636182933754609665, 1684541750435119104, '2023-12-22 10:23:03');
INSERT INTO `tb_module_resource` (`id`, `module_id`, `resource_id`, `create_time`) VALUES (1738022306116010021, 1636182933754609665, 1684541750435119105, '2023-12-22 10:23:03');
INSERT INTO `tb_module_resource` (`id`, `module_id`, `resource_id`, `create_time`) VALUES (1738022306116010022, 1636182933754609665, 1684548449283866624, '2023-12-22 10:23:03');
INSERT INTO `tb_module_resource` (`id`, `module_id`, `resource_id`, `create_time`) VALUES (1738022306116010023, 1636182933754609665, 1684548449283866625, '2023-12-22 10:23:03');
INSERT INTO `tb_module_resource` (`id`, `module_id`, `resource_id`, `create_time`) VALUES (1738022306116010024, 1636182933754609665, 1684568413176856576, '2023-12-22 10:23:03');
INSERT INTO `tb_module_resource` (`id`, `module_id`, `resource_id`, `create_time`) VALUES (1738022306116010025, 1636182933754609665, 1684568413176856577, '2023-12-22 10:23:03');
INSERT INTO `tb_module_resource` (`id`, `module_id`, `resource_id`, `create_time`) VALUES (1738022306116010026, 1636182933754609665, 1684568413176856578, '2023-12-22 10:23:03');
INSERT INTO `tb_module_resource` (`id`, `module_id`, `resource_id`, `create_time`) VALUES (1738022306116010027, 1636182933754609665, 1684128046551924736, '2023-12-22 10:23:03');
INSERT INTO `tb_module_resource` (`id`, `module_id`, `resource_id`, `create_time`) VALUES (1738022306116010028, 1636182933754609665, 1736776127176769536, '2023-12-22 10:23:03');
INSERT INTO `tb_module_resource` (`id`, `module_id`, `resource_id`, `create_time`) VALUES (1738022306116010029, 1636182933754609665, 1737312904681619456, '2023-12-22 10:23:03');
INSERT INTO `tb_module_resource` (`id`, `module_id`, `resource_id`, `create_time`) VALUES (1738022306116010030, 1636182933754609665, 1656679330433990656, '2023-12-22 10:23:03');
INSERT INTO `tb_module_resource` (`id`, `module_id`, `resource_id`, `create_time`) VALUES (1738022306116010031, 1636182933754609665, 1664296403540639744, '2023-12-22 10:23:03');
INSERT INTO `tb_module_resource` (`id`, `module_id`, `resource_id`, `create_time`) VALUES (1738022306116010032, 1636182933754609665, 1664914212519936000, '2023-12-22 10:23:03');
INSERT INTO `tb_module_resource` (`id`, `module_id`, `resource_id`, `create_time`) VALUES (1738022306116010033, 1636182933754609665, 1664914212519936001, '2023-12-22 10:23:03');
INSERT INTO `tb_module_resource` (`id`, `module_id`, `resource_id`, `create_time`) VALUES (1738022306116010034, 1636182933754609665, 1661030954518446080, '2023-12-22 10:23:03');
INSERT INTO `tb_module_resource` (`id`, `module_id`, `resource_id`, `create_time`) VALUES (1738022306116010035, 1636182933754609665, 1662978940790112256, '2023-12-22 10:23:03');
INSERT INTO `tb_module_resource` (`id`, `module_id`, `resource_id`, `create_time`) VALUES (1738022306116010036, 1636182933754609665, 1663469788107636736, '2023-12-22 10:23:03');
INSERT INTO `tb_module_resource` (`id`, `module_id`, `resource_id`, `create_time`) VALUES (1738022306116010037, 1636182933754609665, 1683471580354576384, '2023-12-22 10:23:03');
INSERT INTO `tb_module_resource` (`id`, `module_id`, `resource_id`, `create_time`) VALUES (1738022306116010038, 1636182933754609665, 1636187548919267328, '2023-12-22 10:23:03');
INSERT INTO `tb_module_resource` (`id`, `module_id`, `resource_id`, `create_time`) VALUES (1738022306116010039, 1636182933754609665, 1647860341062762496, '2023-12-22 10:23:03');
INSERT INTO `tb_module_resource` (`id`, `module_id`, `resource_id`, `create_time`) VALUES (1738022306116010040, 1636182933754609665, 1653929645323583488, '2023-12-22 10:23:03');
INSERT INTO `tb_module_resource` (`id`, `module_id`, `resource_id`, `create_time`) VALUES (1738022306116010041, 1636182933754609665, 1654642840078123008, '2023-12-22 10:23:03');
INSERT INTO `tb_module_resource` (`id`, `module_id`, `resource_id`, `create_time`) VALUES (1738022306116010042, 1636182933754609665, 1663579937522581504, '2023-12-22 10:23:03');
INSERT INTO `tb_module_resource` (`id`, `module_id`, `resource_id`, `create_time`) VALUES (1738022306116010043, 1636182933754609665, 1675800348175892483, '2023-12-22 10:23:03');
INSERT INTO `tb_module_resource` (`id`, `module_id`, `resource_id`, `create_time`) VALUES (1738022306116010044, 1636182933754609665, 1683471580354576385, '2023-12-22 10:23:03');
INSERT INTO `tb_module_resource` (`id`, `module_id`, `resource_id`, `create_time`) VALUES (1738022306116010045, 1636182933754609665, 1683841299993591808, '2023-12-22 10:23:03');
INSERT INTO `tb_module_resource` (`id`, `module_id`, `resource_id`, `create_time`) VALUES (1738022306116010046, 1636182933754609665, 1683841299993591809, '2023-12-22 10:23:03');
INSERT INTO `tb_module_resource` (`id`, `module_id`, `resource_id`, `create_time`) VALUES (1738022306116010047, 1636182933754609665, 1683841299993591810, '2023-12-22 10:23:03');
INSERT INTO `tb_module_resource` (`id`, `module_id`, `resource_id`, `create_time`) VALUES (1738022306116010048, 1636182933754609665, 1684216181440905216, '2023-12-22 10:23:03');
INSERT INTO `tb_module_resource` (`id`, `module_id`, `resource_id`, `create_time`) VALUES (1738022306116010049, 1636182933754609665, 1652715951180742656, '2023-12-22 10:23:03');
INSERT INTO `tb_module_resource` (`id`, `module_id`, `resource_id`, `create_time`) VALUES (1738022306116010050, 1636182933754609665, 1681231793782521856, '2023-12-22 10:23:03');
INSERT INTO `tb_module_resource` (`id`, `module_id`, `resource_id`, `create_time`) VALUES (1738935231219499008, 1686716084163444736, 1686025120788774912, '2023-12-24 22:50:42');
INSERT INTO `tb_module_resource` (`id`, `module_id`, `resource_id`, `create_time`) VALUES (1738935231223693312, 1686716084163444736, 1698715961391054848, '2023-12-24 22:50:42');
INSERT INTO `tb_module_resource` (`id`, `module_id`, `resource_id`, `create_time`) VALUES (1738935231223693313, 1686716084163444736, 1698715961395249153, '2023-12-24 22:50:42');
INSERT INTO `tb_module_resource` (`id`, `module_id`, `resource_id`, `create_time`) VALUES (1738935231223693314, 1686716084163444736, 1709566124468731904, '2023-12-24 22:50:42');
INSERT INTO `tb_module_resource` (`id`, `module_id`, `resource_id`, `create_time`) VALUES (1738935231223693315, 1686716084163444736, 1677234418973933568, '2023-12-24 22:50:42');
INSERT INTO `tb_module_resource` (`id`, `module_id`, `resource_id`, `create_time`) VALUES (1738935231223693316, 1686716084163444736, 1671394657394753536, '2023-12-24 22:50:42');
INSERT INTO `tb_module_resource` (`id`, `module_id`, `resource_id`, `create_time`) VALUES (1738935231223693317, 1686716084163444736, 1671394657394753538, '2023-12-24 22:50:42');
INSERT INTO `tb_module_resource` (`id`, `module_id`, `resource_id`, `create_time`) VALUES (1738935231223693318, 1686716084163444736, 1647952968122630145, '2023-12-24 22:50:42');
INSERT INTO `tb_module_resource` (`id`, `module_id`, `resource_id`, `create_time`) VALUES (1738935231223693319, 1686716084163444736, 1686722622882054144, '2023-12-24 22:50:42');
INSERT INTO `tb_module_resource` (`id`, `module_id`, `resource_id`, `create_time`) VALUES (1738935231223693320, 1686716084163444736, 1688037885271343104, '2023-12-24 22:50:42');
INSERT INTO `tb_module_resource` (`id`, `module_id`, `resource_id`, `create_time`) VALUES (1738935231223693321, 1686716084163444736, 1688049564466020352, '2023-12-24 22:50:42');
INSERT INTO `tb_module_resource` (`id`, `module_id`, `resource_id`, `create_time`) VALUES (1738935231223693322, 1686716084163444736, 1694706666030759936, '2023-12-24 22:50:42');
INSERT INTO `tb_module_resource` (`id`, `module_id`, `resource_id`, `create_time`) VALUES (1738935231223693323, 1686716084163444736, 1704132386586886144, '2023-12-24 22:50:42');
INSERT INTO `tb_module_resource` (`id`, `module_id`, `resource_id`, `create_time`) VALUES (1738935231223693324, 1686716084163444736, 1704132386586886146, '2023-12-24 22:50:42');
INSERT INTO `tb_module_resource` (`id`, `module_id`, `resource_id`, `create_time`) VALUES (1738935231223693325, 1686716084163444736, 1678704512069533696, '2023-12-24 22:50:42');
INSERT INTO `tb_module_resource` (`id`, `module_id`, `resource_id`, `create_time`) VALUES (1738935231223693326, 1686716084163444736, 1679512161577074688, '2023-12-24 22:50:42');
INSERT INTO `tb_module_resource` (`id`, `module_id`, `resource_id`, `create_time`) VALUES (1738935231223693327, 1686716084163444736, 1679675931226013696, '2023-12-24 22:50:42');
INSERT INTO `tb_module_resource` (`id`, `module_id`, `resource_id`, `create_time`) VALUES (1738935231223693328, 1686716084163444736, 1679732738346713088, '2023-12-24 22:50:42');
INSERT INTO `tb_module_resource` (`id`, `module_id`, `resource_id`, `create_time`) VALUES (1738935231223693329, 1686716084163444736, 1738021576667824128, '2023-12-24 22:50:42');
INSERT INTO `tb_module_resource` (`id`, `module_id`, `resource_id`, `create_time`) VALUES (1738935231223693330, 1686716084163444736, 1684832097144930304, '2023-12-24 22:50:42');
INSERT INTO `tb_module_resource` (`id`, `module_id`, `resource_id`, `create_time`) VALUES (1738935231223693331, 1686716084163444736, 1685590302728912896, '2023-12-24 22:50:42');
INSERT INTO `tb_module_resource` (`id`, `module_id`, `resource_id`, `create_time`) VALUES (1738935231223693332, 1686716084163444736, 1682422693888000001, '2023-12-24 22:50:42');
INSERT INTO `tb_module_resource` (`id`, `module_id`, `resource_id`, `create_time`) VALUES (1738935231223693333, 1686716084163444736, 1676421084687106050, '2023-12-24 22:50:42');
INSERT INTO `tb_module_resource` (`id`, `module_id`, `resource_id`, `create_time`) VALUES (1738935231223693334, 1686716084163444736, 1636187548919267329, '2023-12-24 22:50:42');
INSERT INTO `tb_module_resource` (`id`, `module_id`, `resource_id`, `create_time`) VALUES (1738935231223693335, 1686716084163444736, 1667578577488445440, '2023-12-24 22:50:42');
INSERT INTO `tb_module_resource` (`id`, `module_id`, `resource_id`, `create_time`) VALUES (1738935231223693336, 1686716084163444736, 1684082256563404800, '2023-12-24 22:50:42');
INSERT INTO `tb_module_resource` (`id`, `module_id`, `resource_id`, `create_time`) VALUES (1738935231223693337, 1686716084163444736, 1683131008930545664, '2023-12-24 22:50:42');
INSERT INTO `tb_module_resource` (`id`, `module_id`, `resource_id`, `create_time`) VALUES (1738935231223693338, 1686716084163444736, 1675800348175892480, '2023-12-24 22:50:42');
INSERT INTO `tb_module_resource` (`id`, `module_id`, `resource_id`, `create_time`) VALUES (1738935231223693339, 1686716084163444736, 1675800348175892482, '2023-12-24 22:50:42');
INSERT INTO `tb_module_resource` (`id`, `module_id`, `resource_id`, `create_time`) VALUES (1738935231223693340, 1686716084163444736, 1684541750435119104, '2023-12-24 22:50:42');
INSERT INTO `tb_module_resource` (`id`, `module_id`, `resource_id`, `create_time`) VALUES (1738935231223693341, 1686716084163444736, 1684541750435119105, '2023-12-24 22:50:42');
INSERT INTO `tb_module_resource` (`id`, `module_id`, `resource_id`, `create_time`) VALUES (1738935231223693342, 1686716084163444736, 1684568413176856576, '2023-12-24 22:50:42');
INSERT INTO `tb_module_resource` (`id`, `module_id`, `resource_id`, `create_time`) VALUES (1738935231223693343, 1686716084163444736, 1684568413176856578, '2023-12-24 22:50:42');
INSERT INTO `tb_module_resource` (`id`, `module_id`, `resource_id`, `create_time`) VALUES (1738935231223693344, 1686716084163444736, 1736776127176769536, '2023-12-24 22:50:42');
INSERT INTO `tb_module_resource` (`id`, `module_id`, `resource_id`, `create_time`) VALUES (1738935231223693345, 1686716084163444736, 1699069650102386688, '2023-12-24 22:50:42');
INSERT INTO `tb_module_resource` (`id`, `module_id`, `resource_id`, `create_time`) VALUES (1738935231223693346, 1686716084163444736, 1699069650102386690, '2023-12-24 22:50:42');
INSERT INTO `tb_module_resource` (`id`, `module_id`, `resource_id`, `create_time`) VALUES (1738935231223693347, 1686716084163444736, 1709846740854636544, '2023-12-24 22:50:42');
INSERT INTO `tb_module_resource` (`id`, `module_id`, `resource_id`, `create_time`) VALUES (1738935231223693348, 1686716084163444736, 1656679330433990656, '2023-12-24 22:50:42');
INSERT INTO `tb_module_resource` (`id`, `module_id`, `resource_id`, `create_time`) VALUES (1738935231223693349, 1686716084163444736, 1664296403540639744, '2023-12-24 22:50:42');
INSERT INTO `tb_module_resource` (`id`, `module_id`, `resource_id`, `create_time`) VALUES (1738935231223693350, 1686716084163444736, 1661030954518446080, '2023-12-24 22:50:42');
INSERT INTO `tb_module_resource` (`id`, `module_id`, `resource_id`, `create_time`) VALUES (1738935231223693351, 1686716084163444736, 1700868600539119616, '2023-12-24 22:50:42');
INSERT INTO `tb_module_resource` (`id`, `module_id`, `resource_id`, `create_time`) VALUES (1738935231223693352, 1686716084163444736, 1636187548919267328, '2023-12-24 22:50:42');
INSERT INTO `tb_module_resource` (`id`, `module_id`, `resource_id`, `create_time`) VALUES (1738935231223693353, 1686716084163444736, 1647860341062762496, '2023-12-24 22:50:42');
INSERT INTO `tb_module_resource` (`id`, `module_id`, `resource_id`, `create_time`) VALUES (1738935231223693354, 1686716084163444736, 1675800348175892483, '2023-12-24 22:50:42');
INSERT INTO `tb_module_resource` (`id`, `module_id`, `resource_id`, `create_time`) VALUES (1738935231223693355, 1686716084163444736, 1652715951180742656, '2023-12-24 22:50:42');
INSERT INTO `tb_module_resource` (`id`, `module_id`, `resource_id`, `create_time`) VALUES (1738935231223693356, 1686716084163444736, 1681231793782521856, '2023-12-24 22:50:42');

-- ----------------------------
-- Table structure for tb_module_role
-- ----------------------------
CREATE TABLE `tb_module_role` (
  `id` bigint NOT NULL COMMENT '主键',
  `module_id` bigint NOT NULL COMMENT '模块ID',
  `role_id` bigint NOT NULL COMMENT '角色ID',
  `create_time` datetime NOT NULL COMMENT '创建时间',
  PRIMARY KEY (`id`) USING BTREE
) COMMENT='模块角色表';

-- ----------------------------
-- Records of tb_module_role
-- ----------------------------

INSERT INTO `tb_module_role` (`id`, `module_id`, `role_id`, `create_time`) VALUES (1636182933754609666, 1636182933754609665, 1000000000000000000, '2023-03-16 10:06:39');
INSERT INTO `tb_module_role` (`id`, `module_id`, `role_id`, `create_time`) VALUES (1636182933754609667, 1636182933754609664, 1000000000000000000, '2023-12-21 13:49:31');
INSERT INTO `tb_module_role` (`id`, `module_id`, `role_id`, `create_time`) VALUES (1686716440331157504, 1686716084163444736, 1667607066451116032, '2023-08-02 20:31:52');
INSERT INTO `tb_module_role` (`id`, `module_id`, `role_id`, `create_time`) VALUES (1737710750782193664, 1636182933754609664, 1633657944153260032, '2023-12-21 13:45:03');
INSERT INTO `tb_module_role` (`id`, `module_id`, `role_id`, `create_time`) VALUES (1737710750782193665, 1636182933754609665, 1633657944153260032, '2023-12-21 13:45:03');

-- ----------------------------
-- Table structure for tb_operate_log
-- ----------------------------
CREATE TABLE `tb_operate_log` (
  `id` bigint NOT NULL COMMENT '主键',
  `trace_id` bigint DEFAULT NULL COMMENT '请求追踪值',
  `operate_content` varchar(255) DEFAULT NULL COMMENT '操作内容',
  `type` varchar(10) NOT NULL COMMENT '操作类型 OTHER 其他操作，ADD 新增，UPDATE 修改， DELETE 删除， QUERY 查询',
  `method_name` varchar(100) DEFAULT NULL COMMENT '操作方法名称',
  `request_method` varchar(50) NOT NULL COMMENT '请求方式',
  `user_id` bigint NOT NULL COMMENT '操作用户ID',
  `operate_ip` varchar(255) DEFAULT NULL COMMENT '操作IP',
  `operate_location` varchar(255) DEFAULT NULL COMMENT '操作地址',
  `operate_param` longtext COMMENT '操作参数',
  `operate_result` longtext COMMENT '操作结果描述',
  `status` tinyint NOT NULL COMMENT '操作状态 0 失败 1 成功 ',
  `cost_time` bigint DEFAULT NULL COMMENT '操作消耗时间',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `update_time` datetime DEFAULT NULL COMMENT '更新时间',
  PRIMARY KEY (`id`) USING BTREE,
  KEY `idx_type` (`type`) USING BTREE COMMENT '操作类型索引',
  KEY `idx_request` (`request_method`) USING BTREE COMMENT '操作方法类型索引',
  KEY `idx_user_id` (`user_id`) USING BTREE COMMENT '用户主键索引',
  KEY `idx_status` (`status`) USING BTREE COMMENT '操作状态索引'
) COMMENT='操作日志表';

-- ----------------------------
-- Records of tb_operate_log
-- ----------------------------


-- ----------------------------
-- Table structure for tb_resource
-- ----------------------------
CREATE TABLE `tb_resource` (
  `id` bigint NOT NULL COMMENT '主键',
  `name` varchar(255) NOT NULL COMMENT '资源名称',
  `description` varchar(255) DEFAULT NULL COMMENT '资源描述',
  `method` varchar(20) NOT NULL COMMENT '资源请求方式',
  `url` varchar(255) NOT NULL COMMENT '资源请求路径',
  `create_time` datetime NOT NULL COMMENT '创建时间',
  `update_time` datetime NOT NULL ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
  PRIMARY KEY (`id`) USING BTREE
) COMMENT='资源表';

-- ----------------------------
-- Records of tb_resource
-- ----------------------------

INSERT INTO `tb_resource` (`id`, `name`, `description`, `method`, `url`, `create_time`, `update_time`) VALUES (1636187548919267328, '获取所有用户', '获取当前系统中所有用户信息', 'POST', '/admin/user/list', '2023-03-16 10:17:39', '2023-04-13 07:06:14');
INSERT INTO `tb_resource` (`id`, `name`, `description`, `method`, `url`, `create_time`, `update_time`) VALUES (1636187548919267329, '获取所有角色', '获取当前系统中所有角色的信息', 'POST', '/admin/role/list', '2023-03-16 10:24:57', '2023-04-13 07:06:16');
INSERT INTO `tb_resource` (`id`, `name`, `description`, `method`, `url`, `create_time`, `update_time`) VALUES (1647860341062762496, '获取当前登录用户', '获取当前系统登录用户', 'GET', '/admin/user/getLoginUser', '2023-04-17 15:12:14', '2023-04-17 21:24:19');
INSERT INTO `tb_resource` (`id`, `name`, `description`, `method`, `url`, `create_time`, `update_time`) VALUES (1647952968122630145, '获取菜单列表', '获取菜单列表', 'GET', '/admin/menu/getRoutes', '2023-04-17 21:24:19', '2023-04-19 08:28:04');
INSERT INTO `tb_resource` (`id`, `name`, `description`, `method`, `url`, `create_time`, `update_time`) VALUES (1652715951180742656, '获取用户个人信息', '获取用户个人信息', 'GET', '/admin/user-info/getUserInfo', '2023-05-01 00:46:55', '2023-07-18 09:23:14');
INSERT INTO `tb_resource` (`id`, `name`, `description`, `method`, `url`, `create_time`, `update_time`) VALUES (1653929645323583488, '更新用户个人信息', '更新用户个人信息', 'PUT', '/admin/user/updateLoginUser', '2023-05-01 00:46:55', '2023-07-25 22:42:51');
INSERT INTO `tb_resource` (`id`, `name`, `description`, `method`, `url`, `create_time`, `update_time`) VALUES (1654642840078123008, '用户更新密码', '用户个人信息更新密码', 'PUT', '/admin/user/updatePassword', '2023-05-06 08:22:27', '2023-07-25 22:42:49');
INSERT INTO `tb_resource` (`id`, `name`, `description`, `method`, `url`, `create_time`, `update_time`) VALUES (1656679330433990656, '获取文件列表', '用户获取文件列表', 'GET', '/admin/file-type/list', '2023-05-11 23:15:37', '2023-06-01 13:44:33');
INSERT INTO `tb_resource` (`id`, `name`, `description`, `method`, `url`, `create_time`, `update_time`) VALUES (1661030954518446080, '获取文件上传策略', '用户获取文件上传策略', 'GET', '/admin/file/listUploadStrategy', '2023-05-23 23:26:23', '2023-12-13 09:25:18');
INSERT INTO `tb_resource` (`id`, `name`, `description`, `method`, `url`, `create_time`, `update_time`) VALUES (1662978940790112256, '上传文件', '用户进行文件上传', 'POST', '/admin/file/upload', '2023-05-29 08:28:17', '2023-05-29 00:28:26');
INSERT INTO `tb_resource` (`id`, `name`, `description`, `method`, `url`, `create_time`, `update_time`) VALUES (1663469788107636736, '批量删除文件', '用户进行批量删除文件操作', 'DELETE', '/admin/file/delete', '2023-05-30 16:58:49', '2023-05-30 08:59:01');
INSERT INTO `tb_resource` (`id`, `name`, `description`, `method`, `url`, `create_time`, `update_time`) VALUES (1663579937522581504, '更新用户头像', '更新用户头像', 'PUT', '/admin/user/updateLoginUserAvatar', '2023-05-31 00:16:36', '2023-07-25 22:42:57');
INSERT INTO `tb_resource` (`id`, `name`, `description`, `method`, `url`, `create_time`, `update_time`) VALUES (1664296403540639744, '获取指定文件分类对象', '根据文件分类ID获取指定文件分类', 'GET', '/admin/file-type/getFileTypeById/**', '2023-06-01 23:46:32', '2023-06-01 15:47:52');
INSERT INTO `tb_resource` (`id`, `name`, `description`, `method`, `url`, `create_time`, `update_time`) VALUES (1664914212519936000, '新增或编辑文件分类', '用户新增或编辑文件分类', 'POST', '/admin/file-type/save', '2023-06-03 16:38:24', '2023-06-03 08:41:48');
INSERT INTO `tb_resource` (`id`, `name`, `description`, `method`, `url`, `create_time`, `update_time`) VALUES (1664914212519936001, '删除指定文件分类', '用户删除指定文件分类', 'DELETE', '/admin/file-type/delete/**', '2023-06-03 16:41:39', '2023-06-03 08:41:50');
INSERT INTO `tb_resource` (`id`, `name`, `description`, `method`, `url`, `create_time`, `update_time`) VALUES (1667050222783561728, '新增或编辑更新角色信息', '新增或编辑角色信息', 'POST', '/admin/role/save', '2023-06-09 14:05:59', '2023-06-09 06:06:11');
INSERT INTO `tb_resource` (`id`, `name`, `description`, `method`, `url`, `create_time`, `update_time`) VALUES (1667578577488445440, '获取指定角色信息', '根据角色ID获取指定角色', 'GET', '/admin/role/getRoleById/**', '2023-06-11 01:05:40', '2023-06-11 01:05:51');
INSERT INTO `tb_resource` (`id`, `name`, `description`, `method`, `url`, `create_time`, `update_time`) VALUES (1667598029198196736, '批量删除角色信息', '批量删除角色信息', 'DELETE', '/admin/role/delete', '2023-06-11 02:23:06', '2023-06-13 21:47:51');
INSERT INTO `tb_resource` (`id`, `name`, `description`, `method`, `url`, `create_time`, `update_time`) VALUES (1671394657394753536, '获取友链信息', '分页查询友链', 'POST', '/admin/link/list', '2023-06-21 13:49:53', '2023-06-21 05:52:34');
INSERT INTO `tb_resource` (`id`, `name`, `description`, `method`, `url`, `create_time`, `update_time`) VALUES (1671394657394753537, '添加-更新友情链接', '编辑友链信息', 'POST', '/admin/link/save', '2023-06-21 13:50:35', '2023-06-21 05:52:36');
INSERT INTO `tb_resource` (`id`, `name`, `description`, `method`, `url`, `create_time`, `update_time`) VALUES (1671394657394753538, '获取指定友情链接信息', '根据友情链接ID获取到指定友请链接', 'GET', '/admin/link/getLinkById/**', '2023-06-21 13:51:41', '2023-06-21 05:52:37');
INSERT INTO `tb_resource` (`id`, `name`, `description`, `method`, `url`, `create_time`, `update_time`) VALUES (1671394657394753539, '批量删除友情链接数据', '根据友情链接集合批量删除友情链接数据', 'DELETE', '/admin/link/delete', '2023-06-21 13:52:23', '2023-06-21 05:52:39');
INSERT INTO `tb_resource` (`id`, `name`, `description`, `method`, `url`, `create_time`, `update_time`) VALUES (1675800348175892480, '获取操作日志信息', '分页查询操作日志信息', 'POST', '/admin/operate-log/list', '2023-07-03 17:36:35', '2023-07-03 09:38:51');
INSERT INTO `tb_resource` (`id`, `name`, `description`, `method`, `url`, `create_time`, `update_time`) VALUES (1675800348175892481, '批量删除操作日志信息', '批量删除操作日志信息', 'DELETE', '/admin/operate-log/delete', '2023-06-21 13:50:35', '2023-06-21 05:52:36');
INSERT INTO `tb_resource` (`id`, `name`, `description`, `method`, `url`, `create_time`, `update_time`) VALUES (1675800348175892482, '获取模块选项数据', '获取模块选项数据', 'GET', '/admin/module/listModuleOptions', '2023-07-03 22:34:42', '2023-07-03 22:34:53');
INSERT INTO `tb_resource` (`id`, `name`, `description`, `method`, `url`, `create_time`, `update_time`) VALUES (1675800348175892483, '获取用户选项数据', '获取用户选项数据集', 'GET', '/admin/user/listUserOptions', '2023-07-03 22:55:11', '2023-07-03 22:55:27');
INSERT INTO `tb_resource` (`id`, `name`, `description`, `method`, `url`, `create_time`, `update_time`) VALUES (1676236928191561728, '清空操作日志', '清空操作日志中的所有数据', 'GET', '/admin/operate-log/clear', '2023-07-04 22:31:05', '2023-07-04 22:31:14');
INSERT INTO `tb_resource` (`id`, `name`, `description`, `method`, `url`, `create_time`, `update_time`) VALUES (1676421084687106050, '获取登录日志信息', '获取所有登录日志信息', 'POST', '/admin/login-log/list', '2023-07-05 10:43:50', '2023-11-21 08:26:24');
INSERT INTO `tb_resource` (`id`, `name`, `description`, `method`, `url`, `create_time`, `update_time`) VALUES (1676421084687106051, '批量删除登录日志信息', '批量删除用户登录日志信息', 'DELETE', '/admin/login-log/delete', '2023-07-05 10:45:47', '2023-11-21 08:27:03');
INSERT INTO `tb_resource` (`id`, `name`, `description`, `method`, `url`, `create_time`, `update_time`) VALUES (1676421084687106052, '清空登录日志信息', '清空所有用户登录日志信息', 'GET', '/admin/login-log/clear', '2023-07-05 10:46:30', '2023-11-21 08:27:08');
INSERT INTO `tb_resource` (`id`, `name`, `description`, `method`, `url`, `create_time`, `update_time`) VALUES (1677234418973933568, '获取服务监控信息', '获取系统服务监控信息', 'GET', '/admin/service-monitor/getMonitorInfo', '2023-07-07 16:34:54', '2023-07-07 08:35:04');
INSERT INTO `tb_resource` (`id`, `name`, `description`, `method`, `url`, `create_time`, `update_time`) VALUES (1678704512069533696, '获取所有在线用户', '获取当前所有在线用户', 'POST', '/admin/online/list', '2023-07-11 17:56:01', '2023-07-12 23:33:21');
INSERT INTO `tb_resource` (`id`, `name`, `description`, `method`, `url`, `create_time`, `update_time`) VALUES (1679151716034936832, '下线指定用户', '将指定用户下线', 'DELETE', '/admin/online/offline/**', '2023-07-12 23:33:03', '2023-07-12 23:33:14');
INSERT INTO `tb_resource` (`id`, `name`, `description`, `method`, `url`, `create_time`, `update_time`) VALUES (1679512161577074688, '获取所有缓存键类型', '获取所有Redis缓存键类型', 'GET', '/admin/cache/listCacheType', '2023-07-13 23:26:11', '2023-07-14 02:43:40');
INSERT INTO `tb_resource` (`id`, `name`, `description`, `method`, `url`, `create_time`, `update_time`) VALUES (1679675931226013696, '获取所有缓存键', '获取所有缓存键', 'GET', '/admin/cache/listCacheKeys/**', '2023-07-14 10:16:09', '2023-07-14 02:16:25');
INSERT INTO `tb_resource` (`id`, `name`, `description`, `method`, `url`, `create_time`, `update_time`) VALUES (1679732738346713088, '获取指定缓存', '获取指定缓存', 'GET', '/admin/cache/getCacheInfoByKey/**', '2023-07-14 14:01:49', '2023-07-14 06:01:59');
INSERT INTO `tb_resource` (`id`, `name`, `description`, `method`, `url`, `create_time`, `update_time`) VALUES (1680179775546589184, '清除缓存列表', '清除缓存列表', 'DELETE', '/admin/cache/clearCacheKeys/**', '2023-07-15 19:38:03', '2023-07-15 19:44:28');
INSERT INTO `tb_resource` (`id`, `name`, `description`, `method`, `url`, `create_time`, `update_time`) VALUES (1680179775546589185, '删除指定缓存', '删除指定缓存', 'DELETE', '/admin/cache/deleteCacheKey/**', '2023-07-15 21:39:56', '2023-07-15 21:40:07');
INSERT INTO `tb_resource` (`id`, `name`, `description`, `method`, `url`, `create_time`, `update_time`) VALUES (1681231793782521856, '获取指定用户信息', '根据用户ID获取指定用户信息', 'GET', '/admin/user-info/getUserInfoById/**', '2023-07-18 17:20:09', '2023-07-18 09:23:06');
INSERT INTO `tb_resource` (`id`, `name`, `description`, `method`, `url`, `create_time`, `update_time`) VALUES (1682422693888000001, '获取系统配置', '获取系统基本配置', 'GET', '/admin/system-config/getSystemConfig', '2023-07-22 22:05:04', '2023-12-13 22:52:31');
INSERT INTO `tb_resource` (`id`, `name`, `description`, `method`, `url`, `create_time`, `update_time`) VALUES (1683094177929232384, '保存系统基本配置', '更新系统基本配置', 'POST', '/admin/system-config/save', '2023-07-23 20:38:02', '2023-12-13 22:52:37');
INSERT INTO `tb_resource` (`id`, `name`, `description`, `method`, `url`, `create_time`, `update_time`) VALUES (1683131008930545664, '获取首页内容', '获取系统后台首页信息', 'GET', '/admin/index/getHomeInfo', '2023-07-23 23:04:49', '2023-07-23 23:04:52');
INSERT INTO `tb_resource` (`id`, `name`, `description`, `method`, `url`, `create_time`, `update_time`) VALUES (1683471580354576384, '富文本上传图片', '富文本上传图片，默认保存到文章图片中', 'POST', '/admin/file/addImage', '2023-07-24 21:38:56', '2023-07-24 21:39:05');
INSERT INTO `tb_resource` (`id`, `name`, `description`, `method`, `url`, `create_time`, `update_time`) VALUES (1683471580354576385, '新增用户', '新增用户', 'POST', '/admin/user/insert', '2023-07-24 22:47:55', '2023-07-24 22:48:04');
INSERT INTO `tb_resource` (`id`, `name`, `description`, `method`, `url`, `create_time`, `update_time`) VALUES (1683841299993591808, '更新指定用户信息', '更新指定用户信息', 'PUT', '/admin/user/updateUser', '2023-07-25 22:08:08', '2023-07-26 23:20:41');
INSERT INTO `tb_resource` (`id`, `name`, `description`, `method`, `url`, `create_time`, `update_time`) VALUES (1683841299993591809, '重置指定用户密码', '重置指定用户密码', 'GET', '/admin/user/resetPassword/**', '2023-07-25 22:28:23', '2023-07-25 23:20:56');
INSERT INTO `tb_resource` (`id`, `name`, `description`, `method`, `url`, `create_time`, `update_time`) VALUES (1683841299993591810, '更新指定用户状态', '更新指定用户状态', 'GET', '/admin/user/updateUserStatus/**', '2023-07-25 22:58:29', '2023-07-25 22:58:31');
INSERT INTO `tb_resource` (`id`, `name`, `description`, `method`, `url`, `create_time`, `update_time`) VALUES (1684082256563404800, '获取角色选项', '获取角色选项', 'GET', '/admin/role/listRoleOptions', '2023-07-26 14:03:37', '2023-07-26 14:03:40');
INSERT INTO `tb_resource` (`id`, `name`, `description`, `method`, `url`, `create_time`, `update_time`) VALUES (1684128046551924736, '更新用户角色', '更新用户角色', 'PUT', '/admin/user-role/update', '2023-07-26 17:06:18', '2023-07-26 17:06:21');
INSERT INTO `tb_resource` (`id`, `name`, `description`, `method`, `url`, `create_time`, `update_time`) VALUES (1684216181440905216, '批量删除用户', '批量删除指定用户', 'DELETE', '/admin/user/delete', '2023-07-26 22:57:17', '2023-07-26 22:57:26');
INSERT INTO `tb_resource` (`id`, `name`, `description`, `method`, `url`, `create_time`, `update_time`) VALUES (1684541750435119104, '分页查询资源模块', '分页查询资源模块', 'POST', '/admin/module/list', '2023-07-27 20:34:10', '2023-07-27 20:34:12');
INSERT INTO `tb_resource` (`id`, `name`, `description`, `method`, `url`, `create_time`, `update_time`) VALUES (1684541750435119105, '根据ID查询模块内容', '获取指定模块信息', 'GET', '/admin/module/getModuleById/**', '2023-07-27 20:51:12', '2023-07-27 20:51:14');
INSERT INTO `tb_resource` (`id`, `name`, `description`, `method`, `url`, `create_time`, `update_time`) VALUES (1684548449283866624, '添加-更新资源模块', '添加-更新资源模块', 'POST', '/admin/module/save', '2023-07-27 20:57:44', '2023-07-27 20:57:47');
INSERT INTO `tb_resource` (`id`, `name`, `description`, `method`, `url`, `create_time`, `update_time`) VALUES (1684548449283866625, '批量删除资源模块数据', '批量删除资源模块数据', 'DELETE', '/admin/module/delete', '2023-07-27 20:58:18', '2023-07-27 20:58:21');
INSERT INTO `tb_resource` (`id`, `name`, `description`, `method`, `url`, `create_time`, `update_time`) VALUES (1684568413176856576, '分页获取所有资源', '分页获取所有资源', 'POST', '/admin/resource/list', '2023-07-27 22:17:43', '2023-07-27 22:17:45');
INSERT INTO `tb_resource` (`id`, `name`, `description`, `method`, `url`, `create_time`, `update_time`) VALUES (1684568413176856577, '更新指定请求资源', '更新指定请求资源', 'PUT', '/admin/resource/save', '2023-07-27 22:38:18', '2023-07-27 22:43:58');
INSERT INTO `tb_resource` (`id`, `name`, `description`, `method`, `url`, `create_time`, `update_time`) VALUES (1684568413176856578, '获取指定资源信息', '根据ID获取指定资源信息', 'GET', '/admin/resource/getResourceById/**', '2023-07-27 22:39:15', '2023-07-27 22:39:17');
INSERT INTO `tb_resource` (`id`, `name`, `description`, `method`, `url`, `create_time`, `update_time`) VALUES (1684832097144930304, '分页获取指定模块请求资源', '分页获取指定模块请求资源', 'POST', '/admin/module-resource/list', '2023-07-28 15:44:41', '2023-07-28 15:44:44');
INSERT INTO `tb_resource` (`id`, `name`, `description`, `method`, `url`, `create_time`, `update_time`) VALUES (1685590302728912896, '获取指定模块可以添加的接口', '获取指定模块可以添加的接口', 'GET', '/admin/module-resource/listResourceById/**', '2023-07-30 17:57:37', '2023-07-30 17:57:39');
INSERT INTO `tb_resource` (`id`, `name`, `description`, `method`, `url`, `create_time`, `update_time`) VALUES (1685641090620719104, '批量删除模块对应请求资源', '批量删除模块对应请求资源', 'DELETE', '/admin/module-resource/delete', '2023-07-30 21:56:44', '2023-07-30 21:56:48');
INSERT INTO `tb_resource` (`id`, `name`, `description`, `method`, `url`, `create_time`, `update_time`) VALUES (1685664653591445512, '为指定模块分配资源请求', '为指定模块分配资源请求', 'POST', '/admin/module-resource/save', '2023-07-30 23:11:07', '2023-07-30 23:11:10');
INSERT INTO `tb_resource` (`id`, `name`, `description`, `method`, `url`, `create_time`, `update_time`) VALUES (1686025120788774912, '获取角色对应模块权限', '获取角色对应模块权限', 'GET', '/admin/module-role/listByRoleId/**', '2023-07-31 22:45:26', '2023-07-31 22:45:28');
INSERT INTO `tb_resource` (`id`, `name`, `description`, `method`, `url`, `create_time`, `update_time`) VALUES (1686370540320718848, '分配角色模块权限', '分配角色模块权限', 'POST', '/admin/module-role/save', '2023-08-01 21:37:38', '2023-08-01 21:38:41');
INSERT INTO `tb_resource` (`id`, `name`, `description`, `method`, `url`, `create_time`, `update_time`) VALUES (1686722622882054144, '获取菜单数据', '获取菜单数据', 'POST', '/admin/menu/list', '2023-08-02 20:57:14', '2023-08-02 21:33:42');
INSERT INTO `tb_resource` (`id`, `name`, `description`, `method`, `url`, `create_time`, `update_time`) VALUES (1688037885271343104, '获取菜单选项', '获取菜单选项', 'GET', '/admin/menu/listMenuOptions', '2023-08-06 12:03:44', '2023-08-06 12:03:47');
INSERT INTO `tb_resource` (`id`, `name`, `description`, `method`, `url`, `create_time`, `update_time`) VALUES (1688049564466020352, '获取指定菜单数据', '获取指定菜单数据', 'GET', '/admin/menu/getMenuById/**', '2023-08-06 12:49:57', '2023-08-06 12:50:00');
INSERT INTO `tb_resource` (`id`, `name`, `description`, `method`, `url`, `create_time`, `update_time`) VALUES (1688106647613865984, '保存菜单数据', '保存菜单数据', 'POST', '/admin/menu/save', '2023-08-06 16:36:41', '2023-08-06 16:36:44');
INSERT INTO `tb_resource` (`id`, `name`, `description`, `method`, `url`, `create_time`, `update_time`) VALUES (1688106647613865993, '删除指定菜单', '删除指定菜单', 'DELETE', '/admin/menu/delete/**', '2023-08-06 23:13:27', '2023-08-06 23:13:32');
INSERT INTO `tb_resource` (`id`, `name`, `description`, `method`, `url`, `create_time`, `update_time`) VALUES (1694706666026565632, '更新博客配置', '更新博客配置', 'POST', '/admin/blog-config/save', '2023-08-24 21:42:45', '2023-08-24 21:42:47');
INSERT INTO `tb_resource` (`id`, `name`, `description`, `method`, `url`, `create_time`, `update_time`) VALUES (1694706666030759936, '获取博客配置', '获取博客配置', 'GET', '/admin/blog-config/getBlogConfig', '2023-08-24 23:42:42', '2023-08-24 23:42:46');
INSERT INTO `tb_resource` (`id`, `name`, `description`, `method`, `url`, `create_time`, `update_time`) VALUES (1698715961391054848, '分页获取所有博客分类', '分页获取所有博客分类', 'POST', '/admin/blog-category/list', '2023-09-04 23:14:31', '2023-09-05 22:43:09');
INSERT INTO `tb_resource` (`id`, `name`, `description`, `method`, `url`, `create_time`, `update_time`) VALUES (1698715961395249152, '编辑博客分类', '编辑博客分类', 'POST', '/admin/blog-category/save', '2023-09-04 23:15:25', '2023-09-05 22:43:13');
INSERT INTO `tb_resource` (`id`, `name`, `description`, `method`, `url`, `create_time`, `update_time`) VALUES (1698715961395249153, '获取指定博客分类', '获取指定博客分类', 'GET', '/admin/blog-category/getCategoryById/**', '2023-09-04 23:16:34', '2023-09-05 22:43:15');
INSERT INTO `tb_resource` (`id`, `name`, `description`, `method`, `url`, `create_time`, `update_time`) VALUES (1698715961395249154, '批量删除博客分类', '批量删除博客分类', 'DELETE', '/admin/blog-category/delete', '2023-09-04 23:18:14', '2023-09-05 22:43:17');
INSERT INTO `tb_resource` (`id`, `name`, `description`, `method`, `url`, `create_time`, `update_time`) VALUES (1699069650102386688, '分页获取所有博客标签', '获取所有博客标签', 'POST', '/admin/blog-tags/list', '2023-09-05 22:42:29', '2023-09-05 22:42:32');
INSERT INTO `tb_resource` (`id`, `name`, `description`, `method`, `url`, `create_time`, `update_time`) VALUES (1699069650102386689, '编辑博客标签', '编辑博客标签', 'POST', '/admin/blog-tags/save', '2023-09-05 22:47:26', '2023-09-05 22:47:29');
INSERT INTO `tb_resource` (`id`, `name`, `description`, `method`, `url`, `create_time`, `update_time`) VALUES (1699069650102386690, '获取指定博客标签信息', '获取指定博客标签信息', 'GET', '/admin/blog-tags/getBlogTagsById/**', '2023-09-05 22:48:11', '2023-09-05 22:48:14');
INSERT INTO `tb_resource` (`id`, `name`, `description`, `method`, `url`, `create_time`, `update_time`) VALUES (1699069650102386691, '批量删除博客标签数据', '批量删除博客标签数据', 'DELETE', '/admin/blog-tags/delete', '2023-09-05 22:48:50', '2023-09-05 22:48:53');
INSERT INTO `tb_resource` (`id`, `name`, `description`, `method`, `url`, `create_time`, `update_time`) VALUES (1700868600539119616, '获取关于我内容', '获取关于我内容', 'GET', '/admin/about-me/getAboutMe', '2023-09-10 21:50:25', '2023-09-10 21:50:28');
INSERT INTO `tb_resource` (`id`, `name`, `description`, `method`, `url`, `create_time`, `update_time`) VALUES (1700868600543313920, '更新关于我内容', '更新关于我内容', 'POST', '/admin/about-me/update', '2023-09-10 21:50:55', '2023-09-10 21:50:57');
INSERT INTO `tb_resource` (`id`, `name`, `description`, `method`, `url`, `create_time`, `update_time`) VALUES (1704132386586886144, '获取所有文章', '获取所有文章', 'POST', '/admin/article/list', '2023-09-19 21:57:44', '2023-09-19 13:58:41');
INSERT INTO `tb_resource` (`id`, `name`, `description`, `method`, `url`, `create_time`, `update_time`) VALUES (1704132386586886145, '编辑文章', '编辑文章', 'POST', '/admin/article/save', '2023-09-19 21:58:30', '2023-09-19 21:58:32');
INSERT INTO `tb_resource` (`id`, `name`, `description`, `method`, `url`, `create_time`, `update_time`) VALUES (1704132386586886146, '根据ID获取指定文章', '获取指定文章信息', 'GET', '/admin/article/getArticleById/**', '2023-09-19 22:01:59', '2023-09-19 22:02:01');
INSERT INTO `tb_resource` (`id`, `name`, `description`, `method`, `url`, `create_time`, `update_time`) VALUES (1704132386586886147, '批量删除文章', '批量删除文章', 'DELETE', '/admin/article/delete', '2023-09-19 22:03:38', '2023-09-19 22:03:40');
INSERT INTO `tb_resource` (`id`, `name`, `description`, `method`, `url`, `create_time`, `update_time`) VALUES (1704894548829798400, '更新文章状态属性', '更新文章状态属性', 'POST', '/admin/article/updateStatus', '2023-09-22 00:26:30', '2023-09-22 00:26:37');
INSERT INTO `tb_resource` (`id`, `name`, `description`, `method`, `url`, `create_time`, `update_time`) VALUES (1709566124468731904, '获取博客分类选项', '获取博客分类选项', 'GET', '/admin/blog-category/listOptions', '2023-10-04 21:49:20', '2023-10-04 21:51:52');
INSERT INTO `tb_resource` (`id`, `name`, `description`, `method`, `url`, `create_time`, `update_time`) VALUES (1709846740854636544, '获取博客标签选项', '获取博客标签选项', 'GET', '/admin/blog-tags/listOptions', '2023-10-05 16:24:16', '2023-10-05 16:24:18');
INSERT INTO `tb_resource` (`id`, `name`, `description`, `method`, `url`, `create_time`, `update_time`) VALUES (1736776127176769536, '获取角色对应菜单权限', '获取角色对应菜单权限', 'GET', '/admin/menu-role/listByRoleId/**', '2023-12-18 23:53:11', '2023-12-18 23:53:21');
INSERT INTO `tb_resource` (`id`, `name`, `description`, `method`, `url`, `create_time`, `update_time`) VALUES (1737312904681619456, '保存角色菜单权限', '保存角色菜单权限', 'POST', '/admin/menu-role/save', '2023-12-20 11:24:59', '2023-12-20 11:25:04');
INSERT INTO `tb_resource` (`id`, `name`, `description`, `method`, `url`, `create_time`, `update_time`) VALUES (1738021576667824128, '获取缓存监控信息', '获取缓存监控信息', 'GET', '/admin/cache/getMonitorInfo', '2023-12-22 10:21:39', '2023-12-22 10:21:42');

-- ----------------------------
-- Table structure for tb_role
-- ----------------------------
CREATE TABLE `tb_role` (
  `id` bigint NOT NULL COMMENT '角色ID',
  `name` varchar(50) NOT NULL COMMENT '角色名称',
  `mark` varchar(50) DEFAULT NULL COMMENT '角色标识',
  `description` varchar(255) DEFAULT NULL COMMENT '角色描述',
  `create_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_time` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
  PRIMARY KEY (`id`) USING BTREE,
  KEY `index_01` (`name`,`mark`,`create_time`) USING BTREE,
  KEY `idx_mark_id` (`mark`,`id`) USING BTREE COMMENT '角色标识、ID联合索引',
  KEY `idx_name_id` (`name`,`id`) USING BTREE COMMENT '角色名称、ID联合索引'
) COMMENT='角色表';

-- ----------------------------
-- Records of tb_role
-- ----------------------------

INSERT INTO `tb_role` (`id`, `name`, `mark`, `description`, `create_time`, `update_time`) VALUES (1000000000000000000, '超级管理员', 'ROLE_ADMIN', '超级管理员', '2023-03-09 09:50:27', '2023-06-11 02:31:37');
INSERT INTO `tb_role` (`id`, `name`, `mark`, `description`, `create_time`, `update_time`) VALUES (1633657944153260032, '系统管理员', 'ROLE_SYSTEM', '系统管理员负责维护系统安全、配置管理、日志监控，确保顺畅运行。专业管理权限、解决技术问题，保障系统高效、可靠运行。', '2023-03-09 10:36:20', '2023-12-21 13:44:28');
INSERT INTO `tb_role` (`id`, `name`, `mark`, `description`, `create_time`, `update_time`) VALUES (1667607066451116032, '系统测试员', 'ROLE_TEST', '系统测试员专注于测试模块，确保系统功能完备性。负责发现和报告潜在问题，保障软件质量，促进系统稳定性和用户满意度。', '2023-06-11 02:58:02', '2023-12-21 13:45:55');

-- ----------------------------
-- Table structure for tb_system_config
-- ----------------------------
CREATE TABLE `tb_system_config` (
  `id` bigint NOT NULL COMMENT '主键',
  `avatar` varchar(255) DEFAULT NULL COMMENT '用户默认头像',
  `notice` text COMMENT '系统公告',
  `upload_strategy` varchar(20) DEFAULT NULL COMMENT '文件上传策略：LOCAL 本地策略，OSS OSS策略',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `update_time` datetime DEFAULT NULL COMMENT '更新时间',
  PRIMARY KEY (`id`) USING BTREE
) COMMENT='系统配置表';

-- ----------------------------
-- Records of tb_system_config
-- ----------------------------

INSERT INTO `tb_system_config` (`id`, `avatar`, `notice`, `upload_strategy`, `create_time`, `update_time`) VALUES (1682761996493127680, 'https://blog-yk0504.oss-cn-hangzhou.aliyuncs.com/avatar/20230529192531.jpg', '<p><strong style=\"color: rgb(96, 98, 102);\">尊敬的用户：</strong></p><p>欢迎来到我的个人博客! 我是Dream_飞翔，非常感谢您的来访。这里是我分享自己生活、学习和工作中的一些体验与见解的地方。您可以随意浏览我的文章以及后台的所有页面，也可以在评论区分享您的想法和反馈。如果您有任何问题或建议，请与我联系，祝您生活愉快！</p><p><strong style=\"color: rgb(96, 98, 102);\"><span class=\"ql-cursor\">﻿﻿﻿</span></strong></p><p><strong style=\"color: rgb(96, 98, 102);\"><img src=\"https://blog-yk0504.oss-cn-hangzhou.aliyuncs.com/article/20230827200219.jpg\" width=\"89\" style=\"cursor: nesw-resize;\"></strong></p><p><strong style=\"color: rgb(96, 98, 102);\">其他：</strong></p><p>目前项目的技术栈为SpringBoot2，后续我会向SpringBoot3进行迁移</p><p><br></p>', 'OSS', '2023-07-22 22:38:28', '2023-08-27 20:02:40');

-- ----------------------------
-- Table structure for tb_user
-- ----------------------------
CREATE TABLE `tb_user` (
  `id` bigint NOT NULL COMMENT '主键ID',
  `username` varchar(50) NOT NULL COMMENT '用户名',
  `password` varchar(255) NOT NULL COMMENT '密码',
  `nickname` varchar(50) DEFAULT NULL COMMENT '昵称',
  `avatar` varchar(255) DEFAULT NULL COMMENT '头像',
  `status` tinyint NOT NULL DEFAULT '1' COMMENT '状态，0：禁用，1：启用',
  `remark` varchar(255) DEFAULT NULL COMMENT '备注',
  `create_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_time` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE KEY `username` (`username`) USING BTREE
) COMMENT='用户表';

-- ----------------------------
-- Records of tb_user
-- ----------------------------

INSERT INTO `tb_user` (`id`, `username`, `password`, `nickname`, `avatar`, `status`, `remark`, `create_time`, `update_time`) VALUES (1653794265890816000, 'admin@qq.com', '$2a$10$INorakGPR9WOUBOSQGh3k.QhZLcbjr..hgALnlgdQ1t3D.hgZKqey', '系统管理员', 'https://blog-yk0504.oss-cn-hangzhou.aliyuncs.com/avatar/20230529192556.jpg', 1, '自古英雄出炼狱，破马长枪定乾坤！', '2023-05-03 16:15:23', '2023-12-21 13:50:39');
INSERT INTO `tb_user` (`id`, `username`, `password`, `nickname`, `avatar`, `status`, `remark`, `create_time`, `update_time`) VALUES (1684221352883519488, 'test@qq.com', '$2a$10$JcnvFXeXlcU7YFjHje2n5eaiiVxO/byA0hSBMT.RubSbhjXlcKUve', '系统测试员', 'https://blog-yk0504.oss-cn-hangzhou.aliyuncs.com/avatar/20230529192531.jpg', 1, '备注', '2023-07-26 23:17:17', '2023-12-21 14:26:40');

-- ----------------------------
-- Table structure for tb_user_info
-- ----------------------------
CREATE TABLE `tb_user_info` (
  `id` bigint NOT NULL COMMENT '主键（同user主键）',
  `phone` varchar(50) NOT NULL COMMENT '手机号码',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `update_time` datetime DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
  PRIMARY KEY (`id`) USING BTREE
) COMMENT='用户信息表';

-- ----------------------------
-- Records of tb_user_info
-- ----------------------------



-- ----------------------------
-- Table structure for tb_user_role
-- ----------------------------
CREATE TABLE `tb_user_role` (
  `id` bigint NOT NULL COMMENT '主键',
  `user_id` bigint NOT NULL COMMENT '用户ID',
  `role_id` bigint NOT NULL COMMENT '角色ID',
  PRIMARY KEY (`id`) USING BTREE
) COMMENT='用户角色表';

-- ----------------------------
-- Records of tb_user_role
-- ----------------------------

INSERT INTO `tb_user_role` (`id`, `user_id`, `role_id`) VALUES (1653794265890816001, 1653794265890816000, 1000000000000000000);
INSERT INTO `tb_user_role` (`id`, `user_id`, `role_id`) VALUES (1683843226475167744, 1683843225900548096, 1633657944153260032);
INSERT INTO `tb_user_role` (`id`, `user_id`, `role_id`) VALUES (1684217316075634688, 1684217315379380224, 1633657944153260032);
INSERT INTO `tb_user_role` (`id`, `user_id`, `role_id`) VALUES (1684217357158842368, 1684217356554862592, 1633657944153260032);
INSERT INTO `tb_user_role` (`id`, `user_id`, `role_id`) VALUES (1684221353441361920, 1684221352883519488, 1667607066451116032);
