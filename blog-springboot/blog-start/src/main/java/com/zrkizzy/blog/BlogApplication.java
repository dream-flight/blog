package com.zrkizzy.blog;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.core.env.ConfigurableEnvironment;

/**
 * 博客启动类
 *
 * @author zhangrongkang
 * @since 2023/3/6
 */
@ComponentScan(basePackages = {"com.zrkizzy.*"})
@SpringBootApplication(exclude = { DataSourceAutoConfiguration.class})
public class BlogApplication {

    private static final Logger LOG = LoggerFactory.getLogger(BlogApplication.class);

    public static void main(String[] args) {
        SpringApplication app = new SpringApplication(BlogApplication.class);
        ConfigurableEnvironment environment = app.run(args).getEnvironment();
        // 输出项目的地址
        LOG.info("项目启动成功");
        LOG.info("项目后端地址: \thttp://127.0.0.1:{}", environment.getProperty("server.port"));
        LOG.info("Swagger地址: \thttp://127.0.0.1:{}/doc.html", environment.getProperty("server.port"));
        LOG.info("Druid地址: \thttp://127.0.0.1:{}/druid/login.html", environment.getProperty("server.port"));
    }

}
