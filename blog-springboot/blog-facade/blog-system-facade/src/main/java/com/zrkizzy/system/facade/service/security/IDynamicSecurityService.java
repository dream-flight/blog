package com.zrkizzy.system.facade.service.security;


import com.zrkizzy.common.models.dto.system.resource.security.RoleSecurityDTO;

import java.util.List;

/**
 * 动态权限业务接口
 *
 * @author zhangrongkang
 * @since 2023/3/13
 */
public interface IDynamicSecurityService {

    /**
     * 加载资源角色数据
     *
     * @return 资源角色数据集合
     */
    List<RoleSecurityDTO> loadResourceRoleData();

}
