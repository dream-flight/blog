package com.zrkizzy.system.facade.service.config;

import com.zrkizzy.common.models.domain.system.config.SystemConfig;
import com.zrkizzy.common.models.dto.system.config.SystemConfigDTO;

/**
 * 系统配置业务逻辑接口
 *
 * @author zhangrongkang
 * @since 2023/7/21
 */
public interface ISystemConfigService {

    /**
     * 获取所有系统配置
     *
     * @return 系统配置实体
     */
    SystemConfig getSystemConfig();

    /**
     * 更新系统基本配置
     *
     * @param systemConfigDTO 系统配置数据接收对象
     * @return 受影响行数
     */
    Boolean saveConfig(SystemConfigDTO systemConfigDTO);

}