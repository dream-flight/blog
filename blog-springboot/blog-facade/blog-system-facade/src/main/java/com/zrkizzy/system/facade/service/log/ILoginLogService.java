package com.zrkizzy.system.facade.service.log;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.zrkizzy.common.models.query.system.monitor.LoginLogQuery;
import com.zrkizzy.common.models.vo.system.log.LoginLogVO;

import java.util.List;

/**
 * 用户登录信息业务逻辑接口
 *
 * @author zhangrongkang
 * @since 2023/7/4
 */
public interface ILoginLogService {

    /**
     * 获取所有用户登录信息
     *
     * @param loginLogQuery 用户登录信息信息查询对象
     * @return 用户登录信息分页数据
     */
    Page<LoginLogVO> listLoginLogs(LoginLogQuery loginLogQuery);

    /**
     * 批量删除用户登录信息数据
     *
     * @param ids 用户登录信息ID
     * @return true：删除成功，false：删除失败
     */
    Boolean deleteBatch(List<Long> ids);

    /**
     * 清空登录日志
     */
    void clear();
}