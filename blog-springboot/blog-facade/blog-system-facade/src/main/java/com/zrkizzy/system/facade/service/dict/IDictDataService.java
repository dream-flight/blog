package com.zrkizzy.system.facade.service.dict;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.zrkizzy.common.models.domain.system.dict.DictData;
import com.zrkizzy.common.models.dto.system.dict.DictDataDTO;
import com.zrkizzy.common.models.query.system.dict.DictDataQuery;
import com.zrkizzy.common.models.vo.system.dict.DictDataOptionVO;

import java.util.List;

/**
 * 字典数据业务逻辑接口
 *
 * @author zhangrongkang
 * @since 2024/1/18
 */
public interface IDictDataService {

    /**
     * 获取所有字典数据
     *
     * @param dictDataQuery 字典数据信息查询对象
     * @return 字典数据分页数据
     */
    Page<DictData> listDictData(DictDataQuery dictDataQuery);

    /**
     * 添加或更新字典数据
     *
     * @param dictDataDTO 字典数据数据接收对象
     * @return 是否添加/更新成功
     */
    Boolean saveDictData(DictDataDTO dictDataDTO);

    /**
     * 获取指定字典数据信息
     *
     * @param dictDataId 字典数据ID
     * @return 字典数据对象
     */
    DictData getDictDataById(Long dictDataId);

    /**
     * 批量删除字典数据
     *
     * @param ids 字典数据ID
     * @return true：删除成功，false：删除失败
     */
    Boolean deleteBatch(List<Long> ids);

    /**
     * 获取字典类型对应数据
     *
     * @param dictType 字典类型
     * @return 字典数据选项
     */
    List<DictDataOptionVO> listDictDataByType(String dictType);
}