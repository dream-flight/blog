package com.zrkizzy.system.facade.service.menu;

import com.zrkizzy.common.core.domain.response.OptionsVO;
import com.zrkizzy.common.models.dto.system.menu.MenuDTO;
import com.zrkizzy.common.models.query.system.menu.MenuQuery;
import com.zrkizzy.common.models.vo.system.menu.MenuVO;
import com.zrkizzy.common.models.vo.system.menu.route.RouterVO;

import java.util.List;

/**
 * 菜单业务逻辑接口
 *
 * @author zhangrongkang
 * @since 2023/4/17
 */
public interface IMenuService {
    /**
     * 获取菜单数据
     *
     * @return 当前登录用户的菜单
     */
    List<RouterVO> getRoutes();

    /**
     * 获取菜单列表
     *
     * @param menuQuery 菜单信息查询对象
     * @return 菜单列表
     */
    List<MenuVO> listMenu(MenuQuery menuQuery);

    /**
     * 获取菜单选项
     *
     * @return 菜单选项集合
     */
    List<OptionsVO> listMenuOptions();

    /**
     * 获取指定菜单数据
     *
     * @param menuId 菜单ID
     * @return 菜单数据返回对象
     */
    MenuVO getMenuById(Long menuId);

    /**
     * 保存菜单信息
     *
     * @param menuDTO 菜单数据传输对象
     * @return 是否保存成功
     */
    Boolean save(MenuDTO menuDTO);

    /**
     * 删除指定菜单
     *
     * @param menuId 菜单ID
     * @return 是否删除成功
     */
    Boolean delete(Long menuId);

    /**
     * 清除菜单缓存数据
     *
     * @param result 上一次操作是否执行成功
     */
    void clearMenuCache(Boolean result);
}
