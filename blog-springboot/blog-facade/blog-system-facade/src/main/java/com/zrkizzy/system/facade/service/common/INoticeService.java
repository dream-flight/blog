package com.zrkizzy.system.facade.service.common;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.zrkizzy.common.models.domain.system.common.Notice;
import com.zrkizzy.common.models.dto.system.common.NoticeDTO;
import com.zrkizzy.common.models.query.system.common.NoticeQuery;

import java.util.List;

/**
 * 通知公告业务逻辑接口
 *
 * @author zhangrongkang
 * @since 2024/3/29
 */
public interface INoticeService {

    /**
     * 获取所有通知公告
     *
     * @param noticeQuery 通知公告信息查询对象
     * @return 通知公告分页数据
     */
    Page<Notice> listNotices(NoticeQuery noticeQuery);

    /**
     * 添加或更新通知公告
     *
     * @param noticeDTO 通知公告数据接收对象
     * @return 是否添加/更新成功
     */
    Boolean saveNotice(NoticeDTO noticeDTO);

    /**
     * 获取指定通知公告信息
     *
     * @param noticeId 通知公告ID
     * @return 通知公告对象
     */
    Notice getNoticeById(Long noticeId);

    /**
     * 批量删除通知公告数据
     *
     * @param ids 通知公告ID
     * @return true：删除成功，false：删除失败
     */
    Boolean deleteBatch(List<Long> ids);

}