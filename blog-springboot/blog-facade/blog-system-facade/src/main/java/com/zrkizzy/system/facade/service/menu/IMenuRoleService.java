package com.zrkizzy.system.facade.service.menu;

import com.zrkizzy.common.models.dto.system.menu.MenuRoleDTO;
import com.zrkizzy.common.models.vo.system.menu.MenuTreeVO;

import java.util.List;

/**
 * 菜单角色关联业务逻辑接口
 *
 * @author zhangrongkang
 * @since 2023/12/18
 */
public interface IMenuRoleService {

    /**
     * 获取当前角色已有的菜单权限
     *
     * @param roleId 角色权限
     * @return 当前角色的具备的菜单权限
     */
    List<String> listMenuIdByRoleId(Long roleId);

    /**
     * 获取所有菜单树型数据
     *
     * @return 菜单树型数据集合
     */
    List<MenuTreeVO> listAllMenuTree();

    /**
     * 保存菜单角色关联数据
     *
     * @param menuRoleDTO 角色菜单关联数据传输对象
     * @return 是否保存成功
     */
    Boolean save(MenuRoleDTO menuRoleDTO);
}
