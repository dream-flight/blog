package com.zrkizzy.system.facade.service.dict;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.zrkizzy.common.models.domain.system.dict.DictType;
import com.zrkizzy.common.models.dto.system.dict.DictTypeDTO;
import com.zrkizzy.common.models.query.system.dict.DictTypeQuery;
import com.zrkizzy.common.models.vo.system.dict.DictTypeOptionVO;

import java.util.List;

/**
 * 字典类型业务逻辑接口
 *
 * @author zhangrongkang
 * @since 2024/1/16
 */
public interface IDictTypeService {

    /**
     * 获取所有字典类型
     *
     * @param dictTypeQuery 字典类型信息查询对象
     * @return 字典类型分页数据
     */
    Page<DictType> listDictTypes(DictTypeQuery dictTypeQuery);

    /**
     * 添加或更新字典类型
     *
     * @param dictTypeDTO 字典类型数据接收对象
     * @return 是否添加/更新成功
     */
    Boolean saveDictType(DictTypeDTO dictTypeDTO);

    /**
     * 获取指定字典类型信息
     *
     * @param dictTypeId 字典类型ID
     * @return 字典类型对象
     */
    DictType getDictTypeById(Long dictTypeId);

    /**
     * 批量删除字典类型数据
     *
     * @param ids 字典类型ID
     * @return true：删除成功，false：删除失败
     */
    Boolean deleteBatch(List<Long> ids);

    /**
     * 获取字典选项
     *
     * @return 字典选项集合
     */
    List<DictTypeOptionVO> listOption();

    /**
     * 通过字典类型ID删除字典缓存
     *
     * @param dictTypeId 字典类型ID
     */
    void deleteCacheById(Long dictTypeId);

    /**
     * 根据字典类型删除字典缓存
     *
     * @param dictType 字典类型
     */
    void deleteCacheByType(String dictType);

    /**
     * 根据字典类型获取对应字典类型ID
     *
     * @param dictType 字典类型
     * @return 字典类型ID
     */
    Long getDictTypeIdByType(String dictType);

    /**
     * 刷新缓存
     */
    void reloadDictCache();

    /**
     * 清除字典缓存
     */
    void clearDictCache();

    /**
     * 加载字典缓存
     */
    void loadDictCache();
}