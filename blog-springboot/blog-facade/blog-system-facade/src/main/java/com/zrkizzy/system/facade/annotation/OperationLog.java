package com.zrkizzy.system.facade.annotation;

import com.zrkizzy.common.models.enums.system.log.OperateType;

import java.lang.annotation.*;

/**
 * 操作日志自定义注解
 *
 * @author zhangrongkang
 * @since 2023/6/23
 */
@Target({ElementType.METHOD})
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface OperationLog {

    /**
     * 操作类型
     * <p>
     *     OTHER--其他操作<br/>
     *     ADD--新增<br/>
     *     UPDATE--修改<br/>
     *     DELETE--删除<br/>
     *     QUERY--查询<br/>
     * </p>
     * @see OperateType
     */
    OperateType type() default OperateType.QUERY;

}
