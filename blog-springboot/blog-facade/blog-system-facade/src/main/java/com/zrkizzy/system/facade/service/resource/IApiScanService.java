package com.zrkizzy.system.facade.service.resource;

import com.zrkizzy.common.models.domain.system.resource.Resource;

import java.util.List;

/**
 * API接口扫描业务逻辑
 *
 * @author zhangrongkang
 * @since 2023/11/13
 */
public interface IApiScanService {

    /**
     * 获取权限集合
     *
     * @return 所有请求权限集合
     */
    List<Resource> listSecurityApi();

}
