package com.zrkizzy.system.facade.service.common;

import com.zrkizzy.common.models.dto.system.common.LoginDTO;

/**
 * 登录业务逻辑接口
 *
 * @author zhangrongkang
 * @since 2023/8/30
 */
public interface ILoginService {

    /**
     * 用户登录
     *
     * @param loginDTO 用户登录数据传输对象
     * @return Token
     */
    String processLogin(LoginDTO loginDTO);
}
