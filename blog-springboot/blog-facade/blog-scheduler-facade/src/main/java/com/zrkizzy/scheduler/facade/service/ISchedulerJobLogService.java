package com.zrkizzy.scheduler.facade.service;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.zrkizzy.common.models.domain.system.scheduler.SchedulerJobLog;
import com.zrkizzy.common.models.query.system.scheduler.SchedulerJobLogQuery;

import java.util.List;

/**
 * 定时任务调度日志业务逻辑接口
 *
 * @author zhangrongkang
 * @since 2024/3/6
 */
public interface ISchedulerJobLogService {

    /**
     * 获取所有定时任务调度日志
     *
     * @param schedulerJobLogQuery 定时任务调度日志信息查询对象
     * @return 定时任务调度日志分页数据
     */
    Page<SchedulerJobLog> listSchedulerJobLogs(SchedulerJobLogQuery schedulerJobLogQuery);

    /**
     * 获取指定定时任务调度日志信息
     *
     * @param schedulerJobLogId 定时任务调度日志ID
     * @return 定时任务调度日志对象
     */
    SchedulerJobLog getSchedulerJobLogById(Long schedulerJobLogId);

    /**
     * 批量删除定时任务调度日志数据
     *
     * @param ids 定时任务调度日志ID
     * @return true：删除成功，false：删除失败
     */
    Boolean deleteBatch(List<Long> ids);

}