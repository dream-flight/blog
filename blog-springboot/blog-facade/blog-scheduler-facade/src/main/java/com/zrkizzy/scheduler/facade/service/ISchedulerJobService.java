package com.zrkizzy.scheduler.facade.service;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.zrkizzy.common.models.domain.system.scheduler.SchedulerJob;
import com.zrkizzy.common.models.dto.system.scheduler.SchedulerJobDTO;
import com.zrkizzy.common.models.dto.system.scheduler.SchedulerJobStatusDTO;
import com.zrkizzy.common.models.query.system.scheduler.SchedulerJobQuery;

import java.util.List;

/**
 * 定时任务业务逻辑接口
 *
 * @author zhangrongkang
 * @since 2024/2/23
 */
public interface ISchedulerJobService {

    /**
     * 获取所有定时任务
     *
     * @param schedulerJobQuery 定时任务信息查询对象
     * @return 定时任务分页数据
     */
    Page<SchedulerJob> listSchedulerJob(SchedulerJobQuery schedulerJobQuery);

    /**
     * 添加或更新定时任务
     *
     * @param schedulerJobDTO 定时任务数据接收对象
     * @return 是否添加/更新成功
     */
    Boolean saveSchedulerJob(SchedulerJobDTO schedulerJobDTO);

    /**
     * 获取指定定时任务信息
     *
     * @param schedulerJobId 定时任务ID
     * @return 定时任务对象
     */
    SchedulerJob getSchedulerJobById(Long schedulerJobId);

    /**
     * 批量删除定时任务数据
     *
     * @param ids 定时任务ID
     */
    void deleteBatch(List<Long> ids);

    /**
     * 修改定时任务状态
     *
     * @param schedulerJobStatusDTO 定时任务状态数据传输对象
     * @return 是否修改成功
     */
    Boolean changeJobStatus(SchedulerJobStatusDTO schedulerJobStatusDTO);

    /**
     * 恢复定时任务
     *
     * @param jobId 任务ID
     * @param jobGroup 任务分组
     * @return 是否恢复成功
     */
    Boolean resumeSchedulerJob(Long jobId, String jobGroup);

    /**
     * 暂停定时任务
     *
     * @param jobId 任务ID
     * @param jobGroup 任务分组
     * @return 是否暂停成功
     */
    Boolean pauseSchedulerJob(Long jobId, String jobGroup);

    /**
     * 删除定时任务
     *
     * @param jobId 任务ID
     * @param jobGroup 任务分组
     * @return 是否删除成功
     */
    Boolean deleteSchedulerJob(Long jobId, String jobGroup);

    /**
     * 执行一次定时任务
     *
     * @param schedulerJobId 定时任务ID
     * @return 是否执行成功
     */
    Boolean run(Long schedulerJobId);

    /**
     * 构建不可并发定时任务对象
     *
     * @param time 定时任务执行时间
     * @return 定时任务数据传输对象
     */
    SchedulerJobDTO buildNonConcurrentSchedulerJobDTO(String time);

    /**
     * 查询所有定时任务
     *
     * @return 所有定时任务集合
     */
    List<SchedulerJob> listAllSchedulerJob();
}