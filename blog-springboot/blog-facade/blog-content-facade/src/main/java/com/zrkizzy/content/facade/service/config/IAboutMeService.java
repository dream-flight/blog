package com.zrkizzy.content.facade.service.config;

import com.zrkizzy.common.models.domain.blog.article.AboutMe;
import com.zrkizzy.common.models.dto.blog.article.AboutMeDTO;

/**
 * 关于我业务逻辑接口
 *
 * @author zhangrongkang
 * @since 2023/9/8
 */
public interface IAboutMeService {

    /**
     * 获取关于我内容
     *
     * @return 关于我对象
     */
    AboutMe getAboutMe();

    /**
     * 更新关于我数据
     *
     * @param aboutMeDTO 关于我数据传输对象
     * @return 是否更新成功
     */
    Boolean update(AboutMeDTO aboutMeDTO);
}
