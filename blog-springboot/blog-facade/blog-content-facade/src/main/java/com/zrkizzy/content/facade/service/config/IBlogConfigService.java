package com.zrkizzy.content.facade.service.config;

import com.zrkizzy.common.models.domain.blog.config.BlogConfig;
import com.zrkizzy.common.models.dto.system.config.BlogConfigDTO;

/**
 * 博客配置业务逻辑接口
 *
 * @author zhangrongkang
 * @since 2023/8/24
 */
public interface IBlogConfigService {

    /**
     * 获取博客配置信息
     *
     * @return 博客配置实体类
     */
    BlogConfig getBlogConfig();

    /**
     * 更新博客配置信息
     *
     * @param blogConfigDTO 博客配置数据传输对象
     * @return 博客配置实体
     */
    BlogConfig save(BlogConfigDTO blogConfigDTO);

}
