package com.zrkizzy.content.facade.service.tag;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.zrkizzy.common.core.domain.response.OptionsVO;
import com.zrkizzy.common.models.domain.blog.tag.BlogTags;
import com.zrkizzy.common.models.dto.blog.tag.BlogTagsDTO;
import com.zrkizzy.common.models.query.blog.tag.BlogTagsQuery;

import java.util.List;

/**
 * 博客标签业务逻辑接口
 *
 * @author zhangrongkang
 * @since 2023/9/5
 */
public interface IBlogTagsService {

    /**
     * 获取所有博客标签
     *
     * @param blogTagsQuery 博客标签信息查询对象
     * @return 博客标签分页数据
     */
    Page<BlogTags> listBlogTags(BlogTagsQuery blogTagsQuery);

    /**
     * 添加或更新博客标签
     *
     * @param blogTagsDTO 博客标签数据接收对象
     * @return 是否添加/更新成功
     */
    Boolean saveBlogTags(BlogTagsDTO blogTagsDTO);

    /**
     * 获取指定博客标签信息
     *
     * @param blogTagsId 博客标签ID
     * @return 博客标签对象
     */
    BlogTags getBlogTagsById(Long blogTagsId);

    /**
     * 根据博客标签名称获取指定博客标签
     *
     * @param name 博客标签名称
     * @return 博客标签对象
     */
    BlogTags getBlogTagsByName(String name);

    /**
     * 批量删除博客标签数据
     *
     * @param ids 博客标签ID
     * @return true：删除成功，false：删除失败
     */
    Boolean deleteBatch(List<Long> ids);

    /**
     * 获取所有博客标签选项
     *
     * @return 所有博客标签集合
     */
    List<OptionsVO> listOptions();
}