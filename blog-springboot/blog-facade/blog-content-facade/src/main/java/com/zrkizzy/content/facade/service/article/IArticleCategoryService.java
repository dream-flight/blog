package com.zrkizzy.content.facade.service.article;

import com.zrkizzy.common.models.domain.blog.category.ArticleCategory;

/**
 * 文章分类关联业务逻辑接口
 *
 * @author zhangrongkang
 * @since 2023/10/7
 */
public interface IArticleCategoryService {

    /**
     * 关联文章与分类
     *
     * @param articleId 文章ID
     * @param categoryId 分类ID
     * @return 是否关联成功
     */
    Boolean insert(Long articleId, Long categoryId);

    /**
     * 根据文章ID获取分类ID
     *
     * @param articleId 文章ID
     * @return 分类ID
     */
    Long getCategoryIdByArticleId(Long articleId);

    /**
     * 根据文章ID获取文章分类关联对象
     *
     * @param articleId 文章ID
     * @return 文章分类关联对象
     */
    ArticleCategory getByArticleId(Long articleId);

    /**
     * 更新文章分类
     *
     * @param articleCategory 文章分类关联对象
     * @return 是否更新成功
     */
    Boolean update(ArticleCategory articleCategory);

    /**
     * 删除文章分类关联数据
     *
     * @param articleId 文章ID
     * @return 是否删除成功
     */
    Boolean deleteByArticleId(Long articleId);
}
