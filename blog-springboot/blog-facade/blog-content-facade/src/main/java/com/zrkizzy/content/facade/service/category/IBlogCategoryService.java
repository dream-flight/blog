package com.zrkizzy.content.facade.service.category;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.zrkizzy.common.core.domain.response.OptionsVO;
import com.zrkizzy.common.models.domain.blog.category.BlogCategory;
import com.zrkizzy.common.models.dto.blog.category.BlogCategoryDTO;
import com.zrkizzy.common.models.query.blog.category.BlogCategoryQuery;

import java.util.List;

/**
 * 博客分类业务逻辑接口
 *
 * @author zhangrongkang
 * @since 2023/9/4
 */
public interface IBlogCategoryService {

    /**
     * 获取所有博客分类
     *
     * @param categoryQuery 分类信息查询对象
     * @return 分类分页数据
     */
    Page<BlogCategory> listBlogCategories(BlogCategoryQuery categoryQuery);

    /**
     * 编辑博客分类
     *
     * @param categoryDTO 分类数据接收对象
     * @return 是否添加/更新成功
     */
    Boolean saveBlogCategory(BlogCategoryDTO categoryDTO);

    /**
     * 获取指定分类信息
     *
     * @param categoryId 分类ID
     * @return 分类对象
     */
    BlogCategory getBlogCategoryById(Long categoryId);

    /**
     * 根据博客分类名称查询博客分类
     *
     * @param name 博客分类名称
     * @return 博客分类对象
     */
    BlogCategory getBlogCategoryByName(String name);

    /**
     * 批量删除分类数据
     *
     * @param ids 分类ID
     * @return true：删除成功，false：删除失败
     */
    Boolean deleteBatch(List<Long> ids);

    /**
     * 获取所有博客分类选项
     *
     * @return 分类选项集合
     */
    List<OptionsVO> listOptions();
}