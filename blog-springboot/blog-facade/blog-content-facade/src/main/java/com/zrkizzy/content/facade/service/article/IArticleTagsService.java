package com.zrkizzy.content.facade.service.article;

import com.zrkizzy.common.core.domain.response.OptionsVO;
import com.zrkizzy.common.models.domain.blog.tag.ArticleTags;

import java.util.List;

/**
 * 文章标签关联业务逻辑接口
 *
 * @author zhangrongkang
 * @since 2023/10/7
 */
public interface IArticleTagsService {

    /**
     * 获取指定文章对应标签
     *
     * @param articleId 文章ID
     * @return 文章标签集合
     */
    List<ArticleTags> listTagsByArticleId(Long articleId);

    /**
     * 为指定文章添加标签
     *
     * @param tagIds 标签集合
     * @param articleId 文章ID
     * @return 是否添加成功
     */
    Boolean insert(List<Long> tagIds, Long articleId);

    /**
     * 根据文章ID获取对应标签
     *
     * @param articleId 文章ID
     * @return 文章对应选择的标签
     */
    List<OptionsVO> listTagOptionsByArticleId(Long articleId);

    /**
     * 根据文章ID获取选择的标签ID集合
     *
     * @param articleId 文章ID
     * @return 文章对应选择的标签ID
     */
    List<String> listTagIdsByArticleId(Long articleId);

    /**
     * 更新文章标签数据
     *
     * @param tagIds 标签集合
     * @param articleId 文章ID
     * @return 是否更新成功
     */
    Boolean update(List<Long> tagIds, Long articleId);

    /**
     * 删除指定文章对应标签
     *
     * @param articleId 文章ID
     * @return 是否删除成功
     */
    Boolean deleteByArticleId(Long articleId);

}
