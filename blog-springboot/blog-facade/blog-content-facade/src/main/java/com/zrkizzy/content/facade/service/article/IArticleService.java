package com.zrkizzy.content.facade.service.article;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.zrkizzy.common.models.domain.blog.article.Article;
import com.zrkizzy.common.models.dto.blog.article.ArticleDTO;
import com.zrkizzy.common.models.dto.blog.article.ArticleSettingsDTO;
import com.zrkizzy.common.models.query.blog.article.ArticleQuery;

import java.util.List;

/**
 * 文章业务逻辑接口
 *
 * @author zhangrongkang
 * @since 2023/9/18
 */
public interface IArticleService {

    /**
     * 获取所有文章
     *
     * @param articleQuery 文章信息查询对象
     * @return 文章分页数据
     */
    Page<Article> listArticles(ArticleQuery articleQuery);

    /**
     * 添加或更新文章
     *
     * @param articleDTO 文章数据接收对象
     * @return 文章ID
     */
    String saveArticle(ArticleDTO articleDTO);

    /**
     * 获取指定文章信息
     *
     * @param articleId 文章ID
     * @return 文章对象
     */
    Article getArticleById(Long articleId);

    /**
     * 批量删除文章数据
     *
     * @param ids 文章ID
     * @return true：删除成功，false：删除失败
     */
    Boolean deleteBatch(List<Long> ids);

    /**
     * 更新文章设置属性
     *
     * @param articleSettingsDTO 文章设置属性数据传输对象
     * @return 是否更新成功
     */
    Boolean updateSettings(ArticleSettingsDTO articleSettingsDTO);

    /**
     * 更新文章发布状态
     *
     * @param articleId 文章ID
     * @return 是否更新成功
     */
    Boolean updateArticleStatus(Long articleId);
}