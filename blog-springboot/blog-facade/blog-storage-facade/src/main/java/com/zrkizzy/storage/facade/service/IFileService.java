package com.zrkizzy.storage.facade.service;

import com.zrkizzy.common.models.dto.system.file.FileDTO;
import com.zrkizzy.common.models.dto.system.file.FileUploadDTO;
import com.zrkizzy.common.models.vo.system.file.FileUploadStrategyVO;

import java.util.List;

/**
 * 文件业务逻辑接口
 *
 * @author zhangrongkang
 * @since 2023/5/12
 */
public interface IFileService {

    /**
     * 保存文件数据
     *
     * @param fileDTO 文件数据传输对象
     * @return 受影响行数
     */
    Integer save(FileDTO fileDTO);

    /**
     * 上传文件
     *
     * @param uploadDTO 文件数据传输对象
     * @return 文件访问路径
     */
    String upload(FileUploadDTO uploadDTO);

    /**
     * 文本编辑器上传图片
     *
     * @param uploadDTO 文件数据传输对象
     * @return 图片访问路径
     */
    String addImage(FileUploadDTO uploadDTO);

    /**
     * 批量删除文件
     *
     * @param fileList 文件集合
     * @return 是否删除成功
     */
    Boolean delete(List<FileDTO> fileList);

    /**
     * 获取文件上传策略集合
     *
     * @return 文件上传策略返回集合
     */
    List<FileUploadStrategyVO> listUploadStrategy();
}
