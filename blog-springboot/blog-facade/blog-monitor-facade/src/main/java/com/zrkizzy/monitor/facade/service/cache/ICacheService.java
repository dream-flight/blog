package com.zrkizzy.monitor.facade.service.cache;


import com.zrkizzy.common.models.vo.system.monitor.CacheInfoVO;
import com.zrkizzy.common.models.vo.system.monitor.CacheKeyVO;
import com.zrkizzy.common.models.vo.system.monitor.CacheTypeVO;

import java.util.List;
import java.util.Map;

/**
 * 缓存管理业务逻辑接口
 *
 * @author zhangrongkang
 * @since 2023/7/13
 */
public interface ICacheService {

    /**
     * 获取缓存键列表
     *
     * @param type 缓存类型
     * @return 缓存键列表
     */
    List<CacheKeyVO> listCacheKeys(String type);

    /**
     * 根据缓存键获取缓存信息
     *
     * @param key 缓存键
     * @return 缓存信息
     */
    CacheInfoVO getCacheInfoByKey(String key);

    /**
     * 清理缓存列表
     *
     * @param type 缓存键列表
     */
    void clearCacheKeys(String type);

    /**
     * 获取所有缓存键类型
     *
     * @return 缓存键类型集合
     */
    List<CacheTypeVO> listRedisKeyType();

    /**
     * 获取缓存监控内容
     *
     * @return 缓存监控内容
     */
    Map<String, Object> getMonitorInfo();
}
