package com.zrkizzy.monitor.facade.service.user;

import com.zrkizzy.common.core.domain.response.PageResult;
import com.zrkizzy.common.models.query.system.monitor.OnlineQuery;
import com.zrkizzy.common.models.vo.system.monitor.OnlineUserVO;

/**
 * 在线用户业务逻辑接口
 *
 * @author zhangrongkang
 * @since 2023/7/11
 */
public interface IOnlineService {

    /**
     * 分页获取所有在线用户
     *
     * @param onlineQuery 在线用户信息查询对象
     * @return 在线用户分页对象
     */
    PageResult<OnlineUserVO> listOnlineUsers(OnlineQuery onlineQuery);
}
