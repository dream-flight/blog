package com.zrkizzy.module.content.mapper.category;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.zrkizzy.common.core.domain.response.OptionsVO;
import com.zrkizzy.common.models.domain.blog.category.BlogCategory;
import com.zrkizzy.common.models.query.blog.category.BlogCategoryQuery;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * <p>
 * 博客分类数据持久化接口
 * </p>
 *
 * @author zhangrongkang
 * @since 2023/9/4
 */
@Mapper
public interface BlogCategoryMapper extends BaseMapper<BlogCategory> {

    /**
     * 分页获取博客分类
     *
     * @param page 分页对象
     * @param categoryQuery 博客分类查询对象
     * @return 博客分页数据
     */
    Page<BlogCategory> listBlogCategories(@Param("page") Page<BlogCategory> page, @Param("categoryQuery") BlogCategoryQuery categoryQuery);

    /**
     * 获取所有博客分类选项
     *
     * @return 博客分类选项集合
     */
    List<OptionsVO> listOptions();


}
