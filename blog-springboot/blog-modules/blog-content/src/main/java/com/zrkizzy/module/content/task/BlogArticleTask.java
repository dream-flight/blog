package com.zrkizzy.module.content.task;

import com.zrkizzy.content.facade.service.article.IArticleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * 博客文章定时任务
 *
 * @author zhangrongkang
 * @since 2024/3/11
 */
@Component("blogArticleTask")
public class BlogArticleTask {

    @Autowired
    private IArticleService articleService;

    /**
     * 定时发布文章
     *
     * @param articleId 文章ID
     */
    public void publishArticle(Long articleId) {
        // 更新文章发布状态
        articleService.updateArticleStatus(articleId);
    }
}
