package com.zrkizzy.module.content.service.config;

import com.zrkizzy.common.core.utils.bean.BeanCopyUtil;
import com.zrkizzy.common.models.domain.blog.config.BlogConfig;
import com.zrkizzy.common.models.dto.system.config.BlogConfigDTO;
import com.zrkizzy.common.mongo.service.IMongoService;
import com.zrkizzy.common.redis.service.IRedisService;
import com.zrkizzy.content.facade.service.config.IBlogConfigService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import static com.zrkizzy.common.redis.enums.RedisKey.CONFIG_BLOG_KEY;

/**
 * 博客配置业务逻辑接口实现类
 *
 * @author zhangrongkang
 * @since 2023/8/24
 */
@Slf4j
@Service
public class BlogConfigServiceImpl implements IBlogConfigService {

    @Autowired
    private IRedisService redisService;

    @Autowired
    private IMongoService<BlogConfig> mongoDbService;

    /**
     * 获取博客配置信息
     *
     * @return 博客配置实体类
     */
    @Override
    public BlogConfig getBlogConfig() {
        String configKey = CONFIG_BLOG_KEY.getKey();
        // 从Redis中获取博客配置对象
        BlogConfig blogConfig = redisService.get(configKey, BlogConfig.class);
        // 如果Redis中不存在当前配置信息
        if (null == blogConfig) {
            // 从MongoDB获取博客配置信息，博客配置类的对象只有一个
            blogConfig = mongoDbService.findAll(BlogConfig.class).get(0);
            // 存储配置信息到Redis，失效时间为永不过期
            redisService.set(configKey, blogConfig);
        }
        return blogConfig;
    }

    /**
     * 更新博客配置信息
     *
     * @param blogConfigDTO 博客配置数据传输对象
     * @return 博客配置实体
     */
    @Override
    public BlogConfig save(BlogConfigDTO blogConfigDTO) {
        // 采取先更新MongoDB后更新Redis策略
        BlogConfig blogConfig = mongoDbService.save(BeanCopyUtil.copy(blogConfigDTO, BlogConfig.class));
        String blogConfigKey = CONFIG_BLOG_KEY.getKey();
        // 更新Redis中博客基本配置信息
        redisService.del(blogConfigKey);
        // 由于数据更新频率低，需要确保数据的一致性，采用删除再添加的方案
        redisService.set(blogConfigKey, blogConfig);
        // 返回博客配置实体
        return blogConfig;
    }
}
