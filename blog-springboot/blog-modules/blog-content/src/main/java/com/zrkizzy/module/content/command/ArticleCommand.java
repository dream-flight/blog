package com.zrkizzy.module.content.command;

import com.zrkizzy.common.models.dto.blog.article.command.ArticleCommandDTO;

/**
 * 文章相关命令接口
 *
 * @author zhangrongkang
 * @since 2023/10/9
 */
public interface ArticleCommand {

    /**
     * 添加文章相关内容命令
     *
     * @param articleCommandDTO 文章命令数据传输对象
     */
    void executeAddArticle(ArticleCommandDTO articleCommandDTO);

    /**
     * 更新文章相关命令
     *
     * @param articleCommandDTO 文章命令数据传输对象
     */
    void executeUpdateArticle(ArticleCommandDTO articleCommandDTO);

    /**
     * 删除文章相关命令
     *
     * @param articleCommandDTO 文章命令数据传输对象
     */
    void executeDeleteArticle(ArticleCommandDTO articleCommandDTO);

}
