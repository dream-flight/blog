package com.zrkizzy.module.content.constant;

/**
 * 默认值全局静态常量
 *
 * @author zhangrongkang
 * @since 2023/9/5
 */
public class DefaultValueConst {

    /**
     * 默认文章数量
     */
    public static final Integer DEFAULT_ARTICLE_COUNT = 0;

    /**
     * 默认关于我标题
     */
    public static final String DEFAULT_ABOUT_ME_TITLE = "关于我";

    /**
     * 默认评论量
     */
    public static final Integer DEFAULT_COMMENT_COUNT = 0;

    /**
     * 默认浏览量
     */
    public static final Integer DEFAULT_VISIT_COUNT = 0;

}
