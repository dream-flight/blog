package com.zrkizzy.module.content.exception;

import com.zrkizzy.common.core.exception.ErrorCode;
import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * 博客异常枚举
 *
 * @author zhangrongkang
 * @since 2024/2/9
 */
@Getter
@AllArgsConstructor
public enum BlogErrorCode implements ErrorCode {

    ARTICLE_TAGS_ASSOCIATION_INSERT_ERROR("B0511", "文章与标签关联信息添加失败"),

    ARTICLE_TAGS_ASSOCIATION_UPDATE_ERROR("B0512", "文章与标签关联信息更新失败"),

    ARTICLE_CATEGORY_ASSOCIATION_INSERT_ERROR("B0513", "文章与分类 {} 关联信息添加失败"),

    ARTICLE_CATEGORY_ASSOCIATION_UPDATE_ERROR("B0514", "文章与分类 {} 关联信息更新失败"),

    TAGS_NOT_EMPTY("B0515", "文章标签不能为空"),

    CATEGORY_NAME_EXIST("B0516", "已存在同名博客分类"),

    TAGS_NAME_EXIST("B0517", "已存在同名博客标签"),;

    /**
     * 状态码
     */
    private final String code;

    /**
     * 状态码描述
     */
    private final String message;

    @Override
    public String message() {
        return this.message;
    }

    /**
     * 错误码
     */
    @Override
    public String code() {
        return this.code;
    }

}
