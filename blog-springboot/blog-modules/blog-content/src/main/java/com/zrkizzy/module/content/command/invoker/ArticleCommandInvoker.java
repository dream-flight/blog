package com.zrkizzy.module.content.command.invoker;

import com.zrkizzy.common.models.dto.blog.article.command.ArticleCommandDTO;
import com.zrkizzy.module.content.command.ArticleCommand;
import com.zrkizzy.module.content.command.impl.ArticleCategoryCommand;
import com.zrkizzy.module.content.command.impl.ArticleTagsCommand;
import jakarta.annotation.PostConstruct;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

/**
 * 文章命令调用者
 *
 * @author zhangrongkang
 * @since 2023/10/9
 */
@Slf4j
@Service
public class ArticleCommandInvoker {

    @Autowired
    private ArticleTagsCommand articleTagsCommand;

    @Autowired
    private ArticleCategoryCommand articleCategoryCommand;

    /**
     * 文章相关命令接口
     */
    private final List<ArticleCommand> commands = new ArrayList<>();

    /**
     * 添加文章相关命令
     */
    @PostConstruct
    private void initArticleCommand() {
        log.info("start assembling and adding article commands");
        // 文章标签命令
        commands.add(articleTagsCommand);
        // 文章分类命令
        commands.add(articleCategoryCommand);
    }

    /**
     * 执行文章添加相关命令
     *
     * @param articleCommandDTO 文章命令数据传输对象
     * @return 是否执行成功
     */
    public void executeAddCommands(ArticleCommandDTO articleCommandDTO) {
        // 遍历当前所有数据
        for (ArticleCommand command : commands) {
            command.executeAddArticle(articleCommandDTO);
        }
    }

    /**
     * 执行文章更新相关命令
     *
     * @param articleCommandDTO 文章命令数据传输对象
     * @return 是否执行成功
     */
    public void executeUpdateCommands(ArticleCommandDTO articleCommandDTO) {
        // 执行所有命令
        for (ArticleCommand command : commands) {
            command.executeUpdateArticle(articleCommandDTO);
        }
    }

    /**
     * 删除文章
     *
     * @param articleCommandDTO 文章命令数据传输对象
     */
    public void executeDeleteCommands(ArticleCommandDTO articleCommandDTO) {
        // 执行所有命令
        for (ArticleCommand command : commands) {
            command.executeDeleteArticle(articleCommandDTO);
        }
    }
}
