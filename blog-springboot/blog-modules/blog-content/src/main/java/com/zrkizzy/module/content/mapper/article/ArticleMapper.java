package com.zrkizzy.module.content.mapper.article;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.zrkizzy.common.models.domain.blog.article.Article;
import com.zrkizzy.common.models.query.blog.article.ArticleQuery;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

/**
 * <p>
 * 文章数据持久化接口
 * </p>
 *
 * @author zhangrongkang
 * @since 2023/9/18
 */
@Mapper
public interface ArticleMapper extends BaseMapper<Article> {

    /**
     * 根据文章ID和列名获取对应状态值
     *
     * @param articleId 文章ID
     * @param field 列名
     * @return 列状态值
     */
    Boolean getFieldValue(@Param("articleId") Long articleId, @Param("field") String field);

    /**
     * 根据文章ID和列名更新对应值
     *
     * @param articleId 文章ID
     * @param field 列名
     * @param value 更新值
     * @return 受影响行数
     */
    Integer updateSettings(@Param("articleId") Long articleId,
                         @Param("field") String field,
                         @Param("value") Boolean value);

    /**
     * 分页查询文章数据
     *
     * @param page 分页对象
     * @param articleQuery 文章查询对象
     * @return 文章分页对象
     */
    Page<Article> listArticles(@Param("page") Page<Article> page, @Param("articleQuery") ArticleQuery articleQuery);

    /**
     * 更新文章发布状态
     *
     * @param articleId 文章ID
     * @return 受到影响行数
     */
    Integer updateArticleStatusById(Long articleId);
}
