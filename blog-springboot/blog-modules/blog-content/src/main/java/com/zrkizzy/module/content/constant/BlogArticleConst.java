package com.zrkizzy.module.content.constant;

/**
 * 博客文章常量
 *
 * @author zhangrongkang
 * @since 2024/3/16
 */
public class BlogArticleConst {

    /**
     * 定时发布文章任务名称
     */
    public static final String ARTICLE_JOB_NAME = "定时发布文章 {}";

    /**
     * 定时发布文章分组
     */
    public static final String ARTICLE_JOB_GROUP = "ARTICLE";

    /**
     * 定时发布文章任务备注
     */
    public static final String ARTICLE_JOB_REMARK = "用户 {} 于 {} 定时发布文章 {}";

    /**
     * 定时发布文章调用目标
     */
    public static final String ARTICLE_JOB_INVOKE_TARGET = "blogArticleTask.publishArticle({}L)";

}
