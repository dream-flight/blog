package com.zrkizzy.module.content.mapper.article;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.zrkizzy.common.models.domain.blog.category.ArticleCategory;
import io.lettuce.core.dynamic.annotation.Param;
import org.apache.ibatis.annotations.Mapper;

/**
 * 文章分类关联数据持久化接口
 *
 * @author zhangrongkang
 * @since 2023/10/6
 */
@Mapper
public interface ArticleCategoryMapper extends BaseMapper<ArticleCategory> {

    /**
     * 根据文章ID获取分类ID
     *
     * @param articleId 文章ID
     * @return 分类ID
     */
    Long getCategoryIdByArticleId(@Param("articleId") Long articleId);
}
