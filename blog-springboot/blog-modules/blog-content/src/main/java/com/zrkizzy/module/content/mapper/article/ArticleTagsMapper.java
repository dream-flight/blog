package com.zrkizzy.module.content.mapper.article;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.zrkizzy.common.core.domain.response.OptionsVO;
import com.zrkizzy.common.models.domain.blog.tag.ArticleTags;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * 文章标签关联数据持久化接口
 *
 * @author zhangrongkang
 * @since 2023/10/6
 */
@Mapper
public interface ArticleTagsMapper extends BaseMapper<ArticleTags> {

    /**
     * 获取指定文章对应标签
     *
     * @param articleId 文章ID
     * @return 文章标签关联对象
     */
    List<ArticleTags> listTagsByArticleId(@Param("articleId") Long articleId);

    /**
     * 批量插入文章标签关联数据
     *
     * @param articleTags 文章标签关联集合
     * @return 受影响行数
     */
    Integer batchInsert(@Param("articleTags") List<ArticleTags> articleTags);

    /**
     * 根据文章ID获取对应标签
     *
     * @param articleId 文章ID
     * @return 文章对应选择的标签
     */
    List<OptionsVO> listTagOptionsByArticleId(@Param("articleId") Long articleId);
}
