package com.zrkizzy.module.content.service.index;

import com.zrkizzy.common.models.vo.blog.dashboard.HomeVO;
import com.zrkizzy.content.facade.service.index.IHomeService;
import com.zrkizzy.system.facade.service.config.ISystemConfigService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * 首页信息业务逻辑接口实现类
 *
 * @author zhangrongkang
 * @since 2023/7/23
 */
@Service
public class HomeServiceImpl implements IHomeService {
    @Autowired
    private ISystemConfigService systemConfigService;

    /**
     * 获取首页数据
     *
     * @return 首页数据返回对象
     */
    @Override
    public HomeVO getHomeInfo() {
        HomeVO homeVO = new HomeVO();
        homeVO.setNotice(systemConfigService.getSystemConfig().getNotice());
        return homeVO;
    }
}
