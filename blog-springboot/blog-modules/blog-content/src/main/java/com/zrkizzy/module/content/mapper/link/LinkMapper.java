package com.zrkizzy.module.content.mapper.link;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.zrkizzy.common.models.domain.blog.link.Link;
import org.apache.ibatis.annotations.Mapper;

/**
 * <p>
 * 友情链接数据持久化接口
 * </p>
 *
 * @author zhangrongkang
 * @since 2023/6/19
 */
@Mapper
public interface LinkMapper extends BaseMapper<Link> {

}
