package com.zrkizzy.module.content.command.impl;

import com.zrkizzy.common.core.domain.response.OptionsVO;
import com.zrkizzy.common.core.exception.ErrorCode;
import com.zrkizzy.common.core.exception.GlobalException;
import com.zrkizzy.common.core.utils.StringUtil;
import com.zrkizzy.common.models.domain.blog.category.ArticleCategory;
import com.zrkizzy.common.models.dto.blog.article.command.ArticleCommandDTO;
import com.zrkizzy.content.facade.service.article.IArticleCategoryService;
import com.zrkizzy.content.facade.service.category.IBlogCategoryService;
import com.zrkizzy.module.content.command.ArticleCommand;
import com.zrkizzy.module.content.exception.BlogErrorCode;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * 文章分类关联命令实现类
 *
 * @author zhangrongkang
 * @since 2023/10/9
 */
@Service
public class ArticleCategoryCommand implements ArticleCommand {

    @Autowired
    private IBlogCategoryService blogCategoryService;

    @Autowired
    private IArticleCategoryService articleCategoryService;

    /**
     * 添加文章相关内容命令
     *
     * @param articleCommandDTO 文章命令数据传输对象
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public void executeAddArticle(ArticleCommandDTO articleCommandDTO) {
        //  分类ID
        String categoryId = String.valueOf(articleCommandDTO.getCategoryId());
        if (!articleCategoryService.insert(articleCommandDTO.getArticleId(), articleCommandDTO.getCategoryId())) {
            // 抛出异常
            render(categoryId, BlogErrorCode.ARTICLE_CATEGORY_ASSOCIATION_INSERT_ERROR);
        }
    }

    /**
     * 更新文章相关命令
     *
     * @param articleCommandDTO 文章命令数据传输对象
     */
    @Transactional(rollbackFor = Exception.class)
    @Override
    public void executeUpdateArticle(ArticleCommandDTO articleCommandDTO) {
        // 查询文章分类关联对象
        ArticleCategory articleCategory = articleCategoryService.getByArticleId(articleCommandDTO.getArticleId());
        // 判断是否需要更新操作
        if (!articleCategory.getCategoryId().equals(articleCommandDTO.getCategoryId())) {
            if (!articleCategoryService.update(articleCategory)) {
                // 更新失败则抛出异常
                render(String.valueOf(articleCommandDTO.getCategoryId()), BlogErrorCode.ARTICLE_CATEGORY_ASSOCIATION_UPDATE_ERROR);
            }
        }
    }

    /**
     * 删除文章相关命令
     *
     * @param articleCommandDTO 文章命令数据传输对象
     */
    @Override
    public void executeDeleteArticle(ArticleCommandDTO articleCommandDTO) {
        // 删除文章分类关联表记录
        articleCategoryService.deleteByArticleId(articleCommandDTO.getArticleId());
    }


    /**
     * 根据分类ID抛出对应异常
     *
     * @param categoryId 分类ID
     */
    private void render(String categoryId, ErrorCode errorCode) {
        String name = null;
        List<OptionsVO> categories = blogCategoryService.listOptions();
        // 遍历当前所有博客分类
        for (OptionsVO category : categories) {
            if (StringUtil.equals(String.valueOf(category.getValue()), categoryId)) {
                // 传递当前数据博客分类名称
                name = category.getLabel();
                break;
            }
        }
        // 传递当前数据博客分类名称
        throw new GlobalException(errorCode, name);
    }
}
