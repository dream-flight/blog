package com.zrkizzy.module.content.mapper.tag;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.zrkizzy.common.core.domain.response.OptionsVO;
import com.zrkizzy.common.models.domain.blog.tag.BlogTags;
import com.zrkizzy.common.models.query.blog.tag.BlogTagsQuery;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * <p>
 * 博客标签数据持久化接口
 * </p>
 *
 * @author zhangrongkang
 * @since 2023/9/5
 */
@Mapper
public interface BlogTagsMapper extends BaseMapper<BlogTags> {

    /**
     * 查询博客标签集合
     *
     * @param page 分页对象
     * @param blogTagsQuery 博客标签查询对象
     * @return 博客标签分页对象
     */
    Page<BlogTags> listBlogTags(@Param("page") Page<BlogTags> page, @Param("blogTagsQuery") BlogTagsQuery blogTagsQuery);

    /**
     * 获取博客标签选项
     *
     * @return 博客标签选项集合
     */
    List<OptionsVO> listOptions();

}
