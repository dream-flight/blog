package com.zrkizzy.module.content.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * 文章状态枚举
 *
 * @author zhangrongkang
 * @since 2024/3/11
 */
@Getter
@AllArgsConstructor
public enum ArticleStatusEnum {

    /**
     * 草稿箱
     */
    DRAFTS((byte) 0),

    /**
     * 已发布
     */
    PUBLISHED((byte) 1),

    /**
     * 待发布
     */
    WAIT_PUBLISH((byte) 2);

    /**
     * 文章状态
     */
    private final Byte status;

}
