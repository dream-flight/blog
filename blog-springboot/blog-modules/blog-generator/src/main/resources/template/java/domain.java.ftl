package ${domainPackage};

import com.baomidou.mybatisplus.annotation.TableName;
import com.zrkizzy.common.datasource.domain.BaseEntity;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;
import lombok.EqualsAndHashCode;
<#if enableChain>
import lombok.experimental.Accessors;
</#if>

<#list fieldList as field>
    <#if field.javaType == "LocalDateTime">
import java.time.LocalDateTime;
        <#break>
    </#if>
</#list>
import java.io.Serial;

/**
 * ${objectName}实体类
 *
 * @author ${author}
 * @since ${date}
 */
@Data
<#if enableChain>
@Accessors(chain = true)
</#if>
@TableName("${tableName}")
@Schema(name = "${objectName}对象")
@EqualsAndHashCode(callSuper = false)
public class ${entityName} extends BaseEntity {

    @Serial
    private static final long serialVersionUID = 1L;
<#-- ----------  BEGIN 字段循环遍历  ---------->
<#list fieldList as field>

    /**
     * ${field.comment}
     */
    @Schema(description = "${field.comment}")
    private ${field.javaType} ${field.javaName};
</#list>
<#------------  END 字段循环遍历  ---------->

}