package ${controllerPackage};

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.zrkizzy.common.core.domain.response.PageResult;
import com.zrkizzy.common.core.domain.response.Result;
import com.zrkizzy.common.core.enums.CommonErrorCode;
import cn.hutool.core.bean.BeanUtil;
import ${domainPackage}.${entityName};
import ${dtoPackage}.${entityName}DTO;
import ${queryPackage}.${entityName}Query;
import ${voPackage}.${entityName}VO;
import ${servicePackage}.I${entityName}Service;
import io.swagger.v3.oas.annotations.tags.Tag;
import io.swagger.v3.oas.annotations.Operation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * ${objectName}数据返回对象
 *
 * @author ${author}
 * @since ${date}
 */
@Tag(name = "${objectName}控制器")
@RestController
@RequestMapping("/admin/${mapping}")
public class ${entityName}Controller {

    @Autowired
    private I${entityName}Service ${entityLowerName}Service;

    @Operation(summary = "分页查询${objectName}")
    @PostMapping("/list")
    <#assign P = "PageResult">
    <#assign V = "VO">
    public Result<${P}<${entityName}${V}>> list${entityName}s(@RequestBody ${entityName}Query ${entityLowerName}Query) {
        // 查询到对应的${objectName}集合
        Page<${entityName}> ${entityLowerName}Page = ${entityLowerName}Service.list${entityName}s(${entityLowerName}Query);
        // 处理对应数据集合并返回数据
        return Result.ok(PageResult.<${entityName}${V}>builder().total(${entityLowerName}Page.getTotal())
            .list(BeanCopyUtil.copyList(${entityLowerName}Page.getRecords(), ${entityName}VO.class)).build());
    }

    @Operation(summary = "编辑${objectName}")
    @PostMapping("/save")
    <#assign B = "Boolean">
    public Result<${B}> save${entityName}(@RequestBody ${entityName}DTO ${entityLowerName}DTO) {
        // 保存${objectName}数据
        return Result.ok(${entityLowerName}Service.save${entityName}(${entityLowerName}DTO));
    }

    @Operation(summary = "获取指定${objectName}信息")
    @GetMapping("/get${entityName}ById/{${entityLowerName}Id}")
    public Result<${entityName}${V}> get${entityName}ById (@PathVariable Long ${entityLowerName}Id) {
        return Result.ok(BeanCopyUtil.copy(${entityLowerName}Service.get${entityName}ById(${entityLowerName}Id), ${entityName}VO.class));
    }

    @Operation(summary = "批量删除${objectName}数据")
    @DeleteMapping("/delete")
    <#assign L = "Long">
    public Result<${B}> delete(@RequestBody List<${L}> ids) {
        return Result.ok(${entityLowerName}Service.deleteBatch(ids));
    }

}
