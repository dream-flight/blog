package com.zrkizzy.data.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import ${domainPackage}.${entityName};

/**
 * <p>
 * ${objectName}数据持久化接口
 * </p>
 *
 * @author ${author}
 * @since ${date}
 */
public interface ${entityName}Mapper extends BaseMapper<${entityName}> {

}
