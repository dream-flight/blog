package com.zrkizzy.generator.vo;

import com.zrkizzy.common.core.domain.response.BaseVO;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

/**
 * 代码生成日志返回对象
 * 
 * @author zhangrongkang
 * @since 2024/1/10
 */
@Data
@ToString(callSuper = true)
@EqualsAndHashCode(callSuper = false)
public class GenerateLogVO extends BaseVO {

    /**
     * 表名
     */
    private String tableName;

    /**
     * 模块名
     */
    private String moduleName;

    /**
     * 包名
     */
    private String packageName;

    /**
     * 是否开启二级缓存
     */
    private Boolean enableCache;

    /**
     * 是否开启链式编程
     */
    private Boolean enableChain;
    
}
