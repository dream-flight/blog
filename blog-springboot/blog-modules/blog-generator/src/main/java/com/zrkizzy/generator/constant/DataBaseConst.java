package com.zrkizzy.generator.constant;

/**
 * 数据库静态常量
 *
 * @author zhangrongkang
 * @since 2023/6/8
 */
public class DataBaseConst {

    /**
     * 字段为空
     */
    public static final String YES = "YES";

    /**
     * 字段不能为空
     */
    public static final String NO = "NO";

    /**
     * 表格字段名称
     */
    public static final String FIELD = "Field";

    /**
     * 表格字段类型
     */
    public static final String TYPE = "Type";

    /**
     * 表格字段是否为空
     */
    public static final String NULL = "Null";

    /**
     * 表格字段注释
     */
    public static final String COMMENT = "Comment";

    /**
     * ID主键
     */
    public static final String ID = "id";

    /**
     * 创建时间
     */
    public static final String CREATE_TIME = "create_time";

    /**
     * 更新时间
     */
    public static final String UPDATE_TIME = "update_time";

    /**
     * 是否开启二级缓存
     */
    public static final String ENABLE_CACHE = "enableCache";

    /**
     * 是否开启链式编程
     */
    public static final String ENABLE_CHAIN = "enableChain";

    /**
     * varchar类型
     */
    public static final String VARCHAR = "varchar";

    /**
     * char类型
     */
    public static final String CHAR = "char";

    /**
     * text类型
     */
    public static final String TEXT = "text";

    /**
     * longtext类型
     */
    public static final String LONG_TEXT = "longtext";

    /**
     * datetime类型
     */
    public static final String DATE_TIME = "datetime";

    /**
     * int类型
     */
    public static final String INT = "int";

    /**
     * bigint类型
     */
    public static final String BIG_INT = "bigint";

    /**
     * tinyint类型
     */
    public static final String TINY_INT = "tinyint";

    /**
     * double类型
     */
    public static final String DOUBLE = "double";

}
