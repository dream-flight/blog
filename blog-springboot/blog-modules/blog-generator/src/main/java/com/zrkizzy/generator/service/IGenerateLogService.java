package com.zrkizzy.generator.service;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.zrkizzy.generator.dto.GenerateOptionDTO;
import com.zrkizzy.generator.domain.GenerateLog;
import com.zrkizzy.generator.query.GenerateLogQuery;

import java.util.List;

/**
 * 代码生成日志业务逻辑接口
 *
 * @author zhangrongkang
 * @since 2024/1/10
 */
public interface IGenerateLogService {

    /**
     * 获取所有代码生成日志
     *
     * @param generateLogQuery 代码生成日志信息查询对象
     * @return 代码生成日志分页数据
     */
    Page<GenerateLog> listGenerateLogs(GenerateLogQuery generateLogQuery);

    /**
     * 批量删除代码生成日志数据
     *
     * @param ids 代码生成日志ID
     * @return true：删除成功，false：删除失败
     */
    Boolean deleteBatch(List<Long> ids);

    /**
     * 获取指定代码生成日志信息
     *
     * @param generateLogId 代码生成日志ID
     * @return 代码生成日志对象
     */
    GenerateLog getGenerateLogById(Long generateLogId);

    /**
     * 新增代码生成选项数据
     *
     * @param generateOptionDTO 代码生成选项数据传输对象
     * @return 是否新增成功
     */
    Boolean save(GenerateOptionDTO generateOptionDTO);
}
