package com.zrkizzy.generator.domain;

import com.baomidou.mybatisplus.annotation.TableName;
import com.zrkizzy.common.datasource.domain.BaseEntity;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

/**
 * 代码生成日志对象
 *
 * @author zhangrongkang
 * @since 2024/1/10
 */
@Data
@ToString(callSuper = true)
@EqualsAndHashCode(callSuper = false)
@Schema(name = "代码生成日志对象")
@TableName("tb_generate_log")
public class GenerateLog extends BaseEntity {

    /**
     * 表名
     */
    @Schema(description = "表名")
    private String tableName;

    /**
     * 模块名
     */
    @Schema(description = "模块名")
    private String moduleName;

    /**
     * 包名
     */
    @Schema(description = "包名")
    private String packageName;

    /**
     * 是否开启二级缓存
     */
    @Schema(description = "二级缓存")
    private Boolean enableCache;

    /**
     * 是否开启链式编程
     */
    @Schema(description = "链式编程")
    private Boolean enableChain;

}
