package com.zrkizzy.generator.service;

import com.zrkizzy.common.core.domain.response.PageResult;
import com.zrkizzy.generator.dto.GenerateOptionDTO;
import com.zrkizzy.generator.query.GenerateQuery;
import com.zrkizzy.generator.vo.TableDataVO;

import java.util.Map;

/**
 * 代码生成业务逻辑接口
 *
 * @author zhangrongkang
 * @since 2024/1/11
 */
public interface IGenerateService {

    /**
     * 分页获取数据库表格
     *
     * @param generateQuery 代码生成查询对象
     * @return 表格分页数据
     */
    PageResult<TableDataVO> listTable(GenerateQuery generateQuery);

    /**
     * 预览代码
     *
     * @param generateOptionDTO 代码生成选项数据传输对象
     * @return 生成代码内容
     */
    Map<String, String> preview(GenerateOptionDTO generateOptionDTO);

    /**
     * 下载代码
     *
     * @param generateOptionDTO 代码生成选项数据传输对象
     * @return 生成代码 byte[] 内容
     */
    byte[] download(GenerateOptionDTO generateOptionDTO);
}
