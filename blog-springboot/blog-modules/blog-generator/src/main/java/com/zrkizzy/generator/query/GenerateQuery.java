package com.zrkizzy.generator.query;

import com.zrkizzy.common.core.domain.request.BasePage;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

import java.util.List;

/**
 * 代码生成查询对象
 *
 * @author zhangrongkang
 * @since 2024/1/9
 */
@Data
@ToString(callSuper = true)
@EqualsAndHashCode(callSuper = false)
public class GenerateQuery extends BasePage {

    /**
     * 表名
     */
    private String tableName;

    /**
     * 表描述
     */
    private String tableComment;

    /**
     * 时间范围
     */
    private List<String> dataRange;

}
