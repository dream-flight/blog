package com.zrkizzy.generator.dto;

import jakarta.validation.constraints.NotBlank;
import lombok.Data;

import java.io.Serializable;

/**
 * 代码生成选项数据传输对象
 *
 * @author zhangrongkang
 * @since 2024/1/11
 */
@Data
public class GenerateOptionDTO implements Serializable {

    /**
     * 表名
     */
    @NotBlank(message = "表名不能为空")
    private String tableName;

    /**
     * 模块名称
     */
    @NotBlank(message = "模块名称不能为空")
    private String moduleName;

    /**
     * 包名称
     */
    @NotBlank(message = "包名称不能为空")
    private String packageName;

    /**
     * 是否开启链式编程
     * <p>
     *     true--开启<br/>
     *     false--不开启
     * </p>
     */
    private Boolean enableChain;

    /**
     * 是否开启二级缓存
     * <p>
     *     true--开启<br/>
     *     false--不开启
     * </p>
     */
    private Boolean enableCache;

}
