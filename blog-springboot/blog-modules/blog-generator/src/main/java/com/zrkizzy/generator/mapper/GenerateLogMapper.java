package com.zrkizzy.generator.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.zrkizzy.generator.domain.GenerateLog;
import org.apache.ibatis.annotations.Mapper;

/**
 * 代码生成日志持久化接口
 *
 * @author zhangrongkang
 * @since 2024/1/10
 */
@Mapper
public interface GenerateLogMapper extends BaseMapper<GenerateLog> {

}
