package com.zrkizzy.generator.util;

import java.util.ArrayList;
import java.util.List;

/**
 * Freemarker模板工具类
 *
 * @author zhangrongkang
 * @since 2023/6/11
 */
public class FreemarkerUtil {

    /**
     * 获取模板生成列表
     *
     * @return 模板生成列表
     */
    public static List<String> getTemplateList() {
        List<String> templates = new ArrayList<>();
        templates.add("/template/java/controller.java.ftl");
        templates.add("/template/java/dto.java.ftl");
        templates.add("/template/java/domain.java.ftl");
        templates.add("/template/java/mapper.java.ftl");
        templates.add("/template/java/query.java.ftl");
        templates.add("/template/java/service.java.ftl");
        templates.add("/template/java/serviceImpl.java.ftl");
        templates.add("/template/java/vo.java.ftl");
        templates.add("/template/js/api.js.ftl");
        templates.add("/template/vue/index.vue.ftl");
        templates.add("/template/xml/mapper.xml.ftl");
        return templates;
    }

    /**
     * 获取模板文件名称
     *
     * @param fullPath 模板文件全路径
     * @return 模板文件名称
     */
    public static String getTemplateName(String fullPath) {
        return fullPath.substring(fullPath.lastIndexOf("/") + 1);
    }

    public static String getTemplateLoading(String fullPath) {
        return fullPath.substring(0, fullPath.lastIndexOf("/") + 1);
    }

}
