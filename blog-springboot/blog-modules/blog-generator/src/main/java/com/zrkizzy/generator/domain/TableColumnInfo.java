package com.zrkizzy.generator.domain;

import lombok.Data;

import java.io.Serializable;

/**
 * 表格字段数据对象
 *
 * @author zhangrongkang
 * @since 2024/1/11
 */
@Data
public class TableColumnInfo implements Serializable {

    /**
     * 列名
     */
    private String columnName;

    /**
     * 列注释
     */
    private String columnComment;

    /**
     * 列类型
     */
    private String columnType;

    /**
     * 是否为空
     */
    private String isNullable;

}
