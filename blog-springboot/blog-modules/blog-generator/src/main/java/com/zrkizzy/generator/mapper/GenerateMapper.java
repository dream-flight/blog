package com.zrkizzy.generator.mapper;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.zrkizzy.generator.domain.TableColumnInfo;
import com.zrkizzy.generator.query.GenerateQuery;
import com.zrkizzy.generator.vo.TableDataVO;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * 代码生成持久化接口
 *
 * @author zhangrongkang
 * @since 2024/1/11
 */
@Mapper
public interface GenerateMapper {

    /**
     * 分页获取表格数据
     *
     * @param page 分页对象
     * @param generateQuery 代码生成查询对象
     * @return 表格分页数据
     */
    Page<TableDataVO> listTable(@Param("page") Page<TableDataVO> page, @Param("generateQuery") GenerateQuery generateQuery);

    /**
     * 根据表名获取列信息
     *
     * @param tableName 表名
     * @return 表对应列集合
     */
    List<TableColumnInfo> getTableColumnByTableName(String tableName);

    /**
     * 获取表名注释
     *
     * @param tableName 表名
     * @return 表注释
     */
    String getTableCommentByTableName(String tableName);
}
