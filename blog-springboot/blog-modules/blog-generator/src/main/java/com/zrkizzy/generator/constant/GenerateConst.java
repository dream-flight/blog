package com.zrkizzy.generator.constant;

/**
 * 代码生成全局静态常量
 *
 * @author zhangrongkang
 * @since 2024/1/14
 */
public class GenerateConst {


    /**
     * 作者
     */
    public static final String AUTHOR = "author";

    /**
     * 创建时间
     */
    public static final String DATE = "date";

    /**
     * 表格名称
     */
    public static final String TABLE_NAME = "tableName";

    /**
     * 对象/实体类名称，例如：文件对象/文件实体类
     */
    public static final String OBJECT_NAME = "objectName";

    /**
     * Java类名称
     */
    public static final String ENTITY_NAME = "entityName";

    /**
     * Java小驼峰
     */
    public static final String ENTITY_LOWER_NAME = "entityLowerName";

    /**
     * 表格注释
     */
    public static final String TABLE_COMMENT = "tableComment";

    /**
     * 文件列数据Key
     */
    public static final String FIELD_LIST = "fieldList";

    /**
     * 映射路径
     */
    public static final String MAPPING = "mapping";

    /**
     * Controller包路径
     */
    public static final String CONTROLLER_PACKAGE = "controllerPackage";

    /**
     * Service包路径
     */
    public static final String SERVICE_PACKAGE = "servicePackage";

    /**
     * ServiceImpl包路径
     */
    public static final String SERVICE_IMPL_PACKAGE = "serviceImplPackage";

    /**
     * Mapper包路径
     */
    public static final String MAPPER_PACKAGE = "mapperPackage";

    /**
     * Domain包路径
     */
    public static final String DOMAIN_PACKAGE = "domainPackage";

    /**
     * Dto包路径
     */
    public static final String DTO_PACKAGE = "dtoPackage";

    /**
     * Vo包路径
     */
    public static final String VO_PACKAGE = "voPackage";

    /**
     * Query包路径
     */
    public static final String QUERY_PACKAGE = "queryPackage";

}
