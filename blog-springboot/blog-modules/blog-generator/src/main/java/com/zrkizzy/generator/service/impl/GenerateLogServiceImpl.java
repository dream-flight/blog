package com.zrkizzy.generator.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.zrkizzy.common.core.utils.IdUtil;
import com.zrkizzy.common.core.utils.StringUtil;
import com.zrkizzy.common.core.utils.bean.BeanCopyUtil;
import com.zrkizzy.generator.dto.GenerateOptionDTO;
import com.zrkizzy.generator.domain.GenerateLog;
import com.zrkizzy.generator.mapper.GenerateLogMapper;
import com.zrkizzy.generator.query.GenerateLogQuery;
import com.zrkizzy.generator.service.IGenerateLogService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import java.util.List;
import java.util.Objects;

/**
 * 代码生成日志业务逻辑接口实现类
 *
 * @author zhangrongkang
 * @since 2024/1/10
 */
@Slf4j
@Service
public class GenerateLogServiceImpl implements IGenerateLogService {

    @Autowired
    private IdUtil idUtil;

    @Autowired
    private GenerateLogMapper generateLogMapper;

    /**
     * 获取所有代码生成日志
     *
     * @param generateLogQuery 代码生成日志查询对象
     * @return 代码生成日志分页数据
     */
    @Override
    public Page<GenerateLog> listGenerateLogs(GenerateLogQuery generateLogQuery) {
        // 开启分页
        Page<GenerateLog> page = new Page<>(generateLogQuery.getCurrentPage(), generateLogQuery.getPageSize());
        // 定义查询条件
        QueryWrapper<GenerateLog> queryWrapper = getGenerateLogQueryWrapper(generateLogQuery);
        // 获取时间范围
        List<String> dataRange = generateLogQuery.getDataRange();
        // 如果时间范围不为空
        if (!CollectionUtils.isEmpty(dataRange)) {
            // 拼接时间范围查询条件
            queryWrapper.between("create_time", dataRange.get(0), dataRange.get(1));
        }
        // 查询分页
        return generateLogMapper.selectPage(page, queryWrapper);
    }

    /**
     * 定义查询对象
     *
     * @param generateLogQuery 代码生成日志查询对象
     * @return 代码生成日志查询条件
     */
    private QueryWrapper<GenerateLog> getGenerateLogQueryWrapper(GenerateLogQuery generateLogQuery) {
        QueryWrapper<GenerateLog> queryWrapper = new QueryWrapper<>();
        // 开启缓存
        if (Objects.nonNull(generateLogQuery.getEnableCache())) {
            queryWrapper.eq("enable_cache", generateLogQuery.getEnableCache());
        }
        // 链式编程
        if (Objects.nonNull(generateLogQuery.getEnableChain())) {
            queryWrapper.eq("enable_chain", generateLogQuery.getEnableChain());
        }
        // 生成表名
        if (StringUtil.isNoneBlank(generateLogQuery.getTableName())) {
            queryWrapper.eq("table_name", generateLogQuery.getTableName());
        }
        // 模块名
        if (StringUtil.isNoneBlank(generateLogQuery.getModuleName())) {
            queryWrapper.eq("module_name", generateLogQuery.getModuleName());
        }
        // 包名
        if (StringUtil.isNoneBlank(generateLogQuery.getPackageName())) {
            queryWrapper.eq("package_name", generateLogQuery.getPackageName());
        }
        // 倒序
        queryWrapper.orderByDesc("create_time");
        return queryWrapper;
    }

    /**
     * 新增代码生成选项数据
     *
     * @param generateOptionDTO 代码生成选项数据传输对象
     * @return 是否新增成功
     */
    @Override
    public Boolean save(GenerateOptionDTO generateOptionDTO) {
        GenerateLog generateLog = BeanCopyUtil.copy(generateOptionDTO, GenerateLog.class);
        generateLog.setId(idUtil.nextId());
        return generateLogMapper.insert(generateLog) == 1;
    }

    /**
     * 批量删除代码生成日志数据
     *
     * @param ids 代码生成日志ID
     * @return true：删除成功，false：删除失败
     */
    @Override
    public Boolean deleteBatch(List<Long> ids) {
        return generateLogMapper.deleteBatchIds(ids) == ids.size();
    }

    /**
     * 获取指定代码生成日志信息
     *
     * @param generateLogId 代码生成日志ID
     * @return 代码生成日志数据返回对象
     */
    @Override
    public GenerateLog getGenerateLogById(Long generateLogId) {
        return generateLogMapper.selectById(generateLogId);
    }

}
