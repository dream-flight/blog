package com.zrkizzy.generator.config.properties;

import lombok.Data;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Component;

/**
 * 代码生成配置类
 *
 * @author zhangrongkang
 * @since 2024/1/5
 */
@Data
@Component
@PropertySource(value = { "classpath:generator.yml" })
@ConfigurationProperties(prefix = "generator")
public class GeneratorProperties {

    /**
     * 作者
     */
    @Value("${author}")
    private String author;

    /**
     * 表格前缀
     */
    @Value("${table-prefix}")
    private String tablePrefix;

    /**
     * 实体类生成路径
     */
    @Value("${base-domain-package}")
    private String baseDomainPackage;

    /**
     * DTO对象基础生成路径
     */
    @Value("${base-dto-package}")
    private String baseDtoPackage;

    /**
     * Query对象基础生成路径
     */
    @Value("${base-query-package}")
    private String baseQueryPackage;

    /**
     * Vo对象基础生成路径
     */
    @Value("${base-vo-package}")
    private String baseVoPackage;

    /**
     * Controller控制器基础生成路径
     */
    @Value("${base-controller-package}")
    private String baseControllerPackage;

    /**
     * Service接口基础生成路径
     */
    @Value("${base-service-package}")
    private String baseServicePackage;

    /**
     * ServiceImpl基础生成路径
     */
    @Value("${base-service-impl-package}")
    private String baseServiceImplPackage;

    /**
     * Mapper接口基础生成路径
     */
    @Value("${base-mapper-package}")
    private String baseMapperPackage;

}
