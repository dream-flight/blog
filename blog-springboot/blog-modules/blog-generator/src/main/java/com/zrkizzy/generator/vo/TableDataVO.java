package com.zrkizzy.generator.vo;

import lombok.Data;

import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * 表格数据返回对象
 *
 * @author zhangrongkang
 * @since 2024/1/9
 */
@Data
public class TableDataVO implements Serializable {

    /**
     * 表名
     */
    private String tableName;

    /**
     * 表注释
     */
    private String tableComment;

    /**
     * 创建时间
     */
    private LocalDateTime createTime;

    /**
     * 更新时间
     */
    private LocalDateTime updateTime;

}
