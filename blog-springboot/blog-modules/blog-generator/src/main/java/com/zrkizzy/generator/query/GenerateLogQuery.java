package com.zrkizzy.generator.query;

import com.zrkizzy.common.core.domain.request.BasePage;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

import java.util.List;

/**
 * 代码生成日志信息查询对象
 *
 * @author zhangrongkang
 * @since 2024/1/15
 */
@Data
@ToString(callSuper = true)
@EqualsAndHashCode(callSuper = false)
public class GenerateLogQuery extends BasePage {

    /**
     * 生成表名
     */
    private String tableName;

    /**
     * 模块名
     */
    private String moduleName;

    /**
     * 包名
     */
    private String packageName;

    /**
     * 开启缓存
     */
    private Boolean enableCache;

    /**
     * 链式编程
     */
    private Boolean enableChain;

    /**
     * 时间范围
     */
    private List<String> dataRange;

}