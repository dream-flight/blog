package com.zrkizzy.module.monitor.service;

import com.zrkizzy.common.core.domain.response.PageResult;
import com.zrkizzy.common.core.utils.PageUtil;
import com.zrkizzy.common.core.utils.StringUtil;
import com.zrkizzy.common.core.utils.bean.BeanCopyUtil;
import com.zrkizzy.common.models.domain.system.core.User;
import com.zrkizzy.common.models.query.system.monitor.OnlineQuery;
import com.zrkizzy.common.models.vo.system.monitor.OnlineUserVO;
import com.zrkizzy.common.redis.enums.RedisKey;
import com.zrkizzy.common.redis.service.IRedisService;
import com.zrkizzy.monitor.facade.service.user.IOnlineService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Set;

/**
 * 在线用户业务逻辑接口实现类
 *
 * @author zhangrongkang
 * @since 2023/7/11
 */
@Service
public class OnlineServiceImpl implements IOnlineService {
    @Autowired
    private IRedisService redisService;

    /**
     * 分页获取所有在线用户
     *
     * @param onlineQuery 在线用户信息查询对象
     * @return 在线用户分页对象
     */
    @Override
    public PageResult<OnlineUserVO> listOnlineUsers(OnlineQuery onlineQuery) {
        // 定义返回对象
        PageResult<OnlineUserVO> result = new PageResult<>();
        // 根据在线用户关键字前缀获取所有在线用户
        Set<String> keys = redisService.scanKeys(RedisKey.USER_KEY.getKey());
        // 当前页面用户总数
        List<OnlineUserVO> list = new ArrayList<>();
        // 拿到所有用户对应关键字
        for (String key : keys) {
            // 根据键获取到对应用户，单独处理
            User user = redisService.get(key, User.class);
            // 用户昵称
            String nickname = onlineQuery.getNickname();
            // 用户名
            String username = onlineQuery.getUsername();
            // 根据条件筛选
            if (StringUtil.isNotBlank(nickname) && !StringUtil.equals(user.getNickname(), nickname)) {
                continue;
            }
            if (StringUtil.isNotBlank(username) && !StringUtil.equals(user.getUsername(), username)) {
                continue;
            }
            // 添加当前符合条件的用户到集合中
            list.add(BeanCopyUtil.copy(user, OnlineUserVO.class));
        }
        // 根据登录时间对当前用户进行排序
        list.sort(Comparator.comparing(OnlineUserVO::getLoginTime, Comparator.reverseOrder()));
        // 在线用户总数
        long total = list.size();
        // 根据用户集合进行分页
        result.setList(PageUtil.page(list, onlineQuery.getCurrentPage(), onlineQuery.getPageSize()));
        result.setTotal(total);
        // 返回结果集
        return result;
    }
}
