package com.zrkizzy.module.security.interceptor;

import com.zrkizzy.common.security.context.SecurityContext;
import com.zrkizzy.common.security.context.SystemContext;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.validation.constraints.NotNull;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.HandlerInterceptor;

/**
 * 权限系统拦截器
 *
 * @author zhangrongkang
 * @since 2023/7/12
 */
@Component
public class SecurityInterceptor implements HandlerInterceptor {

    @Override
    public void afterCompletion(@NotNull HttpServletRequest request, @NotNull HttpServletResponse response, @NotNull Object handler, Exception ex) throws Exception {
        // 清除拦截器中的资源，避免内存溢出
        SecurityContext.remove();
        SystemContext.remove();
    }
}
