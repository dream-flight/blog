package com.zrkizzy.module.security.config;

import com.zrkizzy.module.security.interceptor.SecurityInterceptor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

/**
 * 拦截器配置类
 *
 * @author zhangrongkang
 * @since 2023/7/12
 */
@Configuration
public class InterceptorConfig implements WebMvcConfigurer {
    @Value("${upload.local.path}")
    private String localPath;

    @Autowired
    private SecurityInterceptor securityInterceptor;

    @Override
    public void addInterceptors(InterceptorRegistry registry) {
        // 启用自定义拦截器
        registry.addInterceptor(securityInterceptor).addPathPatterns("/**");
    }

    @Override
    public void addResourceHandlers(ResourceHandlerRegistry registry) {
        // 所有带 "/files" 的请求都认为是静态资源请求
        registry.addResourceHandler("/files/**").addResourceLocations("file:" + localPath);
    }
}
