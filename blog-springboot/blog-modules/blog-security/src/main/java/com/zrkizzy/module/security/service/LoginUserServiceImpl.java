package com.zrkizzy.module.security.service;

import com.zrkizzy.common.security.context.SecurityContext;
import com.zrkizzy.common.security.service.LoginUserService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

/**
 * 登录用户权限业务逻辑接口实现类
 *
 * @author zhangrongkang
 * @since 2024/2/22
 */
@Slf4j
@Service
public class LoginUserServiceImpl implements LoginUserService {

    /**
     * 获取当前登录用户唯一标识
     *
     * @return 登录用户唯一标识
     */
    @Override
    public String getLoginUserTraceId() {
        return SecurityContext.getTraceId();
    }
}
