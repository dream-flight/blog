package com.zrkizzy.module.security.utils;

import com.zrkizzy.common.models.domain.system.core.User;
import com.zrkizzy.common.redis.service.IRedisService;
import com.zrkizzy.module.security.constant.SecurityConst;
import com.zrkizzy.common.security.context.SecurityContext;
import com.zrkizzy.common.security.domain.LoginUserDetails;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;

import static com.zrkizzy.common.redis.enums.RedisKey.USER_KEY;

/**
 * Security数据工具类
 *
 * @author zhangrongkang
 * @since 2023/4/17
 */
@Component
public class SecurityUtil {

    @Autowired
    private IRedisService redisService;

    /**
     * 获取当前登录的用户
     *
     * @return 登录用户对象
     */
    public User getLoginUser() {
        // 从载荷中获取用户对象
        User user = getUserDetails().getUser();
        if (null == user) {
            // 如果载荷中为空则去Redis中获取
            return redisService.get(USER_KEY.getKey() + SecurityContext.getTraceId(), User.class);
        }
        return user;
    }

    /**
     * 获取当前登录用户的用户名
     *
     * @return 用户名
     */
    public String getLoginUsername() {
        return getUserDetails().getUsername();
    }

    /**
     * 获取当前登录用户角色
     *
     * @return 用户角色
     */
    public String getLoginUserRole() {
        return getUserDetails().getRoleMark();
    }

    /**
     * 获取当前登录角色ID
     *
     * @return 角色ID
     */
    public Long getLoginUserRoleId() {
        return getUserDetails().getRole().getId();
    }

    /**
     * 获取当前登录用户的角色名称
     *
     * @return 角色名称
     */
    public String getLoginUserRoleName() {
        return getUserDetails().getRoleName();
    }

    /**
     * 获取当前登录的用户昵称
     *
     * @return 用户昵称
     */
    public String getLoginUserNickname() {
        return getLoginUser().getNickname();
    }

    /**
     * 获取用户登录设备信息
     *
     * @return 用户登录设备信息（操作系统 浏览器版本）
     */
    public String getUserAgent() {
        // 获取用户对象
        User user = getLoginUser();
        return user.getOs() + " " + user.getBrowser();
    }

    /**
     * 判断当前登录用户是否为管理员
     *
     * @return true：管理员；false：不是管理员
     */
    public Boolean isAdmin() {
        String role = getLoginUserRole();
        return role != null && role.equals(SecurityConst.ADMIN);
    }

    /**
     * 获取用户信息对象
     *
     * @return 用户信息对象
     */
    private LoginUserDetails getUserDetails() {
        return (LoginUserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
    }

}
