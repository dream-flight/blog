package com.zrkizzy.module.security.response;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.zrkizzy.common.core.domain.response.Result;
import com.zrkizzy.common.security.exception.SecurityErrorCode;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.web.access.AccessDeniedHandler;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.io.PrintWriter;

import static com.zrkizzy.common.core.constant.HttpConst.APPLICATION_JSON;
import static com.zrkizzy.common.core.constant.HttpConst.UTF_8;


/**
 * 权限不足自定义返回结果
 *
 * @author zhangrongkang
 * @since 2023/3/9
 */
@Component
public class LoginUserAccessDeniedHandler implements AccessDeniedHandler {
    @Override
    public void handle(HttpServletRequest request, HttpServletResponse response, AccessDeniedException e) throws IOException, ServletException {
        response.setCharacterEncoding(UTF_8);
        response.setContentType(APPLICATION_JSON);
        // 获取输出流
        PrintWriter out = response.getWriter();
        out.write(new ObjectMapper().writeValueAsString(Result.failure(SecurityErrorCode.UNAUTHORIZED_USE_API)));
        out.flush();
        out.close();
    }
}
