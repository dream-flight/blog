package com.zrkizzy.module.security.response;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.zrkizzy.common.core.domain.response.Result;
import com.zrkizzy.common.security.exception.SecurityErrorCode;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.AuthenticationEntryPoint;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.io.PrintWriter;

import static com.zrkizzy.common.core.constant.HttpConst.APPLICATION_JSON;
import static com.zrkizzy.common.core.constant.HttpConst.UTF_8;


/**
 * 未授权自定义返回结果
 *
 * @author zhangrongkang
 * @since 2023/3/9
 */
@Component
public class LoginUserAuthenticationEntryPoint implements AuthenticationEntryPoint {

    @Override
    public void commence(HttpServletRequest request, HttpServletResponse response, AuthenticationException e) throws IOException, ServletException {
        // 设置编码格式
        response.setCharacterEncoding(UTF_8);
        // 定义返回的对象格式
        response.setContentType(APPLICATION_JSON);
        PrintWriter out = response.getWriter();
        out.write(new ObjectMapper().writeValueAsString(Result.failure(SecurityErrorCode.UNAUTHORIZED_ACCESS)));
        out.flush();
        out.close();
    }

}
