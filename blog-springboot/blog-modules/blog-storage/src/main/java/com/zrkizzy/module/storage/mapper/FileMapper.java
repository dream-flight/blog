package com.zrkizzy.module.storage.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.zrkizzy.common.models.domain.system.file.File;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

/**
 * 文件数据持久化接口
 *
 * @author zhangrongkang
 * @since 2023/5/12
 */
@Mapper
public interface FileMapper extends BaseMapper<File> {
    /**
     * 查询指定文件分类中的文件数量
     *
     * @param fileTypeId 文件分类ID
     * @return 文件数量
     */
    Integer countByFileTypeId(Long fileTypeId);

    /**
     * 通过MD5查询文件分类对应文件
     *
     * @param md5        MD5哈希值
     * @param fileTypeId 文件分类ID
     * @return 对应文件对象
     */
    File getFileByMd5(@Param("md5") String md5, @Param("fileTypeId") Long fileTypeId);
}
