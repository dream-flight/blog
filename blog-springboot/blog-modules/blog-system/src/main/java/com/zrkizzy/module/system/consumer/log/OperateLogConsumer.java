package com.zrkizzy.module.system.consumer.log;

import com.zrkizzy.common.models.domain.system.log.OperateLog;
import com.zrkizzy.common.mq.service.impl.AbstractConsumer;
import com.zrkizzy.module.system.mapper.log.OperateLogMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Objects;

/**
 * 操作日志消费者
 *
 * @author zhangrongkang
 * @since 2023/7/27
 */
@Service("operateLogConsumer")
public class OperateLogConsumer extends AbstractConsumer<OperateLog> {

    @Autowired
    private OperateLogMapper operateLogLogMapper;

    /**
     * 扩展消费方法
     *
     * @param operateLog 操作日志实体
     */
    @Override
    public void onConsumer(OperateLog operateLog) {
        // 发送操作日志到数据库中
        if (Objects.nonNull(operateLog)) {
            operateLogLogMapper.insert(operateLog);
        }
    }

}
