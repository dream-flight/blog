package com.zrkizzy.module.system.service.security;

import com.zrkizzy.common.models.dto.system.resource.security.RoleSecurityDTO;
import com.zrkizzy.system.facade.service.security.IDynamicSecurityService;
import com.zrkizzy.module.system.mapper.resource.ResourceMapper;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * 动态权限业务接口实现类
 *
 * @author zhangrongkang
 * @since 2023/3/13
 */
@Slf4j
@Service
public class DynamicSecurityServiceImpl implements IDynamicSecurityService {

    @Autowired
    private ResourceMapper resourceMapper;

    /**
     * 加载资源角色数据
     *
     * @return 资源角色数据集合
     */
    @Override
    public List<RoleSecurityDTO> loadResourceRoleData() {
        // 获取到角色与资源对应关系
        return resourceMapper.loadResourceRoleData();
    }

}
