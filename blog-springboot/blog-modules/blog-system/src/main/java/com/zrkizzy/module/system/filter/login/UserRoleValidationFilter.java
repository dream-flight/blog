package com.zrkizzy.module.system.filter.login;

import com.zrkizzy.common.models.domain.system.core.Role;
import com.zrkizzy.common.models.domain.system.core.User;
import com.zrkizzy.common.models.dto.system.common.LoginDTO;
import com.zrkizzy.common.models.enums.system.common.login.LoginTarget;
import com.zrkizzy.common.security.context.UserContext;
import com.zrkizzy.common.security.exception.SecurityErrorCode;
import com.zrkizzy.module.security.constant.SecurityConst;
import com.zrkizzy.module.system.filter.LoginFilter;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

/**
 * 用户角色校验过滤器
 *
 * @author zhangrongkang
 * @since 2024/2/22
 */
@Slf4j
@Component("userRoleValidationFilter")
public class UserRoleValidationFilter extends LoginFilter {

    /**
     * 当前节点登录处理逻辑
     *
     * @param loginDTO 用户登录数据传输对象
     */
    @Override
    public void handleLogin(LoginDTO loginDTO) {
        log.info("-------------------- 进入用户角色校验责任链节点 --------------------");
        // 获取当前进行登录的User对象
        User user = UserContext.getUser();
        // 校验当前用户角色是否能够进行对应登录
        if (loginDTO.getTarget().equals(LoginTarget.ADMIN)) {
            // 获取当前角色
            Role role = user.getRoles().getFirst();
            // 如果当前用户角色为博客前台角色则不允许登录后台
            if (SecurityConst.BLOG.equals(role.getMark())) {
                log.error("当前用户角色为: {}, 不允许登录后台管理系统", role.getName());
                log.info("-------------------- 离开用户角色校验责任链节点 --------------------");
                throw SecurityErrorCode.USER_NOT_ALLOW_LOGIN_MANAGE_SYSTEM.exception();
            }
        }
    }
}
