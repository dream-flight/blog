package com.zrkizzy.module.system.consumer.dead;

import com.zrkizzy.common.models.domain.system.log.LoginLog;
import com.zrkizzy.common.mq.service.impl.AbstractConsumer;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.io.IOException;

/**
 * 登录日志死信队列消费者
 *
 * @author zhangrongkang
 * @since 2023/12/22
 */
@Slf4j
@Service("loginLogDeadConsumer")
public class LoginLogDeadConsumer extends AbstractConsumer<LoginLog> {

    /**
     * 扩展消费方法
     *
     * @param data 消息体
     * @throws IOException IO异常
     */
    @Override
    public void onConsumer(LoginLog data) throws Exception {
        // 确定消息
        ack();
    }
}
