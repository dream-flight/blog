package com.zrkizzy.module.system.service.log;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.zrkizzy.common.models.domain.system.log.OperateLog;
import com.zrkizzy.common.models.query.system.monitor.OperateLogQuery;
import com.zrkizzy.common.models.vo.system.log.OperateLogVO;
import com.zrkizzy.system.facade.service.log.IOperateLogService;
import com.zrkizzy.module.system.mapper.log.OperateLogMapper;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * <p>
 * 操作日志业务逻辑接口实现类
 * </p>
 *
 * @author zhangrongkang
 * @since 2023/7/3
 */
@Service
@Slf4j
public class OperateLogServiceImpl implements IOperateLogService {

    @Autowired
    private OperateLogMapper operateLogMapper;

    /**
     * 获取所有操作日志
     *
     * @param operateLogQuery 操作日志查询对象
     * @return 操作日志分页数据
     */
    @Override
    public Page<OperateLogVO> listOperateLogs(OperateLogQuery operateLogQuery) {
        // 开启分页
        Page<OperateLog> page = new Page<>(operateLogQuery.getCurrentPage(), operateLogQuery.getPageSize());
        // 查询分页
        return operateLogMapper.listOperateLog(page, operateLogQuery);
    }
    
    /**
     * 批量删除操作日志数据
     *
     * @param ids 操作日志ID
     * @return true：删除成功，false：删除失败
     */
    @Override
    public Boolean deleteBatch(List<Long> ids) {
        return operateLogMapper.deleteBatchIds(ids) == ids.size();
    }

    /**
     * 清空操作日志
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public void clearOperateLogs() {
        // 清空当前表格数据
        operateLogMapper.truncate();
    }

}
