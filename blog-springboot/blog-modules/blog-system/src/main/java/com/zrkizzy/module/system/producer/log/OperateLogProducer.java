package com.zrkizzy.module.system.producer.log;

import com.zrkizzy.common.mq.service.impl.AbstractProducer;
import org.springframework.stereotype.Service;

/**
 * 操作日志生产者
 *
 * @author zhangrongkang
 * @since 2023/10/26
 */
@Service("operateLogProducer")
public class OperateLogProducer extends AbstractProducer {
}
