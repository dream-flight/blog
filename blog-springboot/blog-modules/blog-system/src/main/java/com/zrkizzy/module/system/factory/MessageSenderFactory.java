package com.zrkizzy.module.system.factory;

import com.zrkizzy.common.models.enums.system.message.MessageSenderChannel;
import com.zrkizzy.module.system.template.AbstractMessageSender;
import com.zrkizzy.module.system.template.impl.EmailMessageSender;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * 消息发送工厂
 *
 * @author zhangrongkang
 * @since 2023/5/5
 */
@Component
public class MessageSenderFactory {
    @Autowired
    private EmailMessageSender emailMessageSender;

    /**
     * 根据发送消息渠道注入不同的实例
     *
     * @param channel 消息发送渠道
     * @return 具体消息发送类
     */
    public AbstractMessageSender getInstance(MessageSenderChannel channel) {
        AbstractMessageSender messageSender = null;
        // 根据类型判断注入哪种实例
        messageSender = switch (channel) {
            // 发送邮件
            case EMAIL -> emailMessageSender;
            // 发送短信
            case SMS -> null;
        };
        // 返回具体实例
        return messageSender;
    }
}
