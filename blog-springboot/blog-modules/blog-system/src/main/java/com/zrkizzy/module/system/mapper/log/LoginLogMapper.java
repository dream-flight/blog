package com.zrkizzy.module.system.mapper.log;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.zrkizzy.common.models.domain.system.log.LoginLog;
import com.zrkizzy.common.models.query.system.monitor.LoginLogQuery;
import com.zrkizzy.common.models.vo.system.log.LoginLogVO;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

/**
 * <p>
 * 用户登录日志数据持久化接口
 * </p>
 *
 * @author zhangrongkang
 * @since 2023/7/4
 */
@Mapper
public interface LoginLogMapper extends BaseMapper<LoginLog> {

    /**
     * 分页查询用户登录日志数据
     *
     * @param page 分页对象
     * @param loginLogQuery 登录日志查询对象
     * @return 登录日志分页对象
     */
    Page<LoginLogVO> listLoginLog(@Param("page") Page<LoginLog> page, @Param("loginLogQuery") LoginLogQuery loginLogQuery);

    /**
     * 清空所有登录日志
     */
    void truncate();
}
