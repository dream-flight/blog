package com.zrkizzy.module.system.exception;

import com.zrkizzy.common.core.exception.ErrorCode;
import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * 登录异常枚举
 *
 * @author zhangrongkang
 * @since 2024/2/5
 */
@Getter
@AllArgsConstructor
public enum LoginErrorCode implements ErrorCode {

    USERNAME_ALREADY_EXIST("A0111", "用户名已存在"),

    PHONE_FORMAT_VALIDATE_FAILED("A0151", "手机格式校验失败"),

    EMAIL_FORMAT_VALIDATE_FAILED("A0153", "邮箱格式校验失败"),

    USER_ACCOUNT_NOT_EXIST("A0201", "用户账户不存在"),

    USER_ACCOUNT_FROZEN("A0202", "用户账户被冻结"),

    USER_PASSWORD_ERROR("A0210", "用户密码错误"),

    USER_LOGIN_EXPIRED("A0230", "用户登录已过期"),

    USER_CAPTCHA_ERROR("A0240", "用户验证码错误"),

    USER_CAPTCHA_EXPIRED("A0242", "用户验证码已过期"),

    MOBILE_NOT_NULL("A0415", "电话号码不能为空"),

    EMAIL_NOT_NULL("A0416", "邮箱不能为空"),

    CAPTCHA_NOT_NULL("A0417", "验证码不能为空"),

    PASSWORD_NOT_NULL("A0418", "用户密码不能为空"),

    LOGIN_CHANNEL_ERROR("A0403", "用户登录渠道错误"),

    NOT_OPEN_THIRD_CHANNEL("600", "当前未开放第三方登录"),;

    /**
     * 状态码
     */
    private final String code;

    /**
     * 状态码描述
     */
    private final String message;

    /**
     * 错误信息描述
     */
    @Override
    public String message() {
        return this.message;
    }

    /**
     * 错误码
     */
    @Override
    public String code() {
        return this.code;
    }
}
