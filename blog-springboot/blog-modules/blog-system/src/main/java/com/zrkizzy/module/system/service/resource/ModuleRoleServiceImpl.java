package com.zrkizzy.module.system.service.resource;

import com.zrkizzy.common.core.utils.IdUtil;
import com.zrkizzy.common.models.domain.system.resource.ModuleRole;
import com.zrkizzy.common.models.dto.system.resource.ModuleRoleDTO;
import com.zrkizzy.module.security.OpenPolicyAgentAuthorizationManager;
import com.zrkizzy.module.system.exception.SystemErrorCode;
import com.zrkizzy.module.system.mapper.resource.ModuleRoleMapper;
import com.zrkizzy.system.facade.service.resource.IModuleRoleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

/**
 * 模块角色业务逻辑接口实现类
 *
 * @author zhangrongkang
 * @since 2023/7/31
 */
@Service
public class ModuleRoleServiceImpl implements IModuleRoleService {

    @Autowired
    private OpenPolicyAgentAuthorizationManager authorizationManager;

    @Autowired
    private IdUtil idUtil;

    @Autowired
    private ModuleRoleMapper moduleRoleMapper;

    /**
     * 分配角色模块权限
     *
     * @param moduleRoleDTO 模块角色关联数据传输对象
     * @return 是否分配成功
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public Boolean save(ModuleRoleDTO moduleRoleDTO) {
        // 角色ID
        Long roleId = moduleRoleDTO.getRoleId();
        // 先根据角色ID删除模块权限
        moduleRoleMapper.deleteByRoleId(roleId);
        // 模块ID
        List<Long> moduleIds = moduleRoleDTO.getModuleIds();
        List<ModuleRole> moduleRoles = new ArrayList<>();
        for (Long moduleId : moduleIds) {
            ModuleRole moduleRole = ModuleRole.builder()
                    .id(idUtil.nextId())
                    .roleId(roleId)
                    .moduleId(moduleId)
                    .createTime(LocalDateTime.now()).build();
            // 添加模块角色关联对象
            moduleRoles.add(moduleRole);
        }
        // 添加当前模块权限
        if (moduleRoleMapper.insertBatch(moduleRoles) == moduleIds.size()) {
            // 清空内存中的动态权限
            authorizationManager.clearResourceData();
            return Boolean.TRUE;
        }
        // 抛出模块角色分配异常
        throw SystemErrorCode.ASSOCIATION_MODULE_ROLE_ERROR.exception();
    }

    /**
     * 根据角色ID获取角色模块权限
     *
     * @param roleId 角色ID
     * @return 所有选中的模块ID集合
     */
    @Override
    public List<Long> listModuleIdByRoleId(Long roleId) {
        // 获取角色模块数据并返回结果
        return moduleRoleMapper.listModuleIdByRoleId(roleId);
    }
}
