package com.zrkizzy.module.system.consumer.dead;

import com.zrkizzy.common.models.dto.system.message.MessageSenderDTO;
import com.zrkizzy.common.mq.service.impl.AbstractConsumer;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.io.IOException;

/**
 * 消息发送私信队列消费者
 *
 * @author zhangrongkang
 * @since 2023/12/22
 */
@Slf4j
@Service("messageSenderDeadConsumer")
public class MessageSenderDeadConsumer extends AbstractConsumer<MessageSenderDTO> {
    /**
     * 扩展消费方法
     *
     * @param data 消息体
     * @throws IOException IO异常
     */
    @Override
    public void onConsumer(MessageSenderDTO data) throws Exception {
        ack();
    }
}
