package com.zrkizzy.module.system.producer;

import com.zrkizzy.common.mq.service.impl.AbstractProducer;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

/**
 * 消息发送生产者
 * <p>
 *   发送邮件 / 短信
 * </p>
 *
 * @author zhangrongkang
 * @since 2023/10/26
 */
@Slf4j
@Service("messageSenderProducer")
public class MessageSenderProducer extends AbstractProducer {

}
