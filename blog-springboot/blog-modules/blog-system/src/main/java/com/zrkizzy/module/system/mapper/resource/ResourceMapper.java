package com.zrkizzy.module.system.mapper.resource;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.zrkizzy.common.models.domain.system.resource.Resource;
import com.zrkizzy.common.models.dto.system.resource.security.RoleSecurityDTO;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

/**
 * 资源数据持久化接口
 *
 * @author zhangrongkang
 * @since 2023/3/16
 */
@Mapper
public interface ResourceMapper extends BaseMapper<Resource> {

    /**
     * 加载角色权限数据
     *
     * @return 角色权限数据集合
     */
    List<RoleSecurityDTO> loadResourceRoleData();
}
