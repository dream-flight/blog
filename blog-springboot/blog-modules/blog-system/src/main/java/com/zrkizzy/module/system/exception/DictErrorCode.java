package com.zrkizzy.module.system.exception;

import com.zrkizzy.common.core.exception.ErrorCode;
import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * 字典异常枚举
 *
 * @author zhangrongkang
 * @since 2024/1/31
 */
@Getter
@AllArgsConstructor
public enum DictErrorCode implements ErrorCode {

    DICT_SERVE_ERROR("B0400", "字典服务出错"),

    DICT_TYPE_ALREADY_EXIST("B0401", "字典类型已存在"),

    DICT_TYPE_ALREADY_DISABLE("B0402", "字典{}已禁用");

    /**
     * 状态码
     */
    private final String code;

    /**
     * 状态码描述
     */
    private final String message;

    /**
     * 错误信息描述
     */
    @Override
    public String message() {
        return this.message;
    }

    /**
     * 错误码
     */
    @Override
    public String code() {
        return this.code;
    }
}
