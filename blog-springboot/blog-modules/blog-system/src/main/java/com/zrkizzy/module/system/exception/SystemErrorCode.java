package com.zrkizzy.module.system.exception;

import com.zrkizzy.common.core.exception.ErrorCode;
import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * 系统异常枚举
 *
 * @author zhangrongkang
 * @since 2024/2/5
 */
@Getter
@AllArgsConstructor
public enum SystemErrorCode implements ErrorCode {

    USER_CAPTCHA_ERROR("A0240", "用户验证码错误"),

    USER_CAPTCHA_EXPIRED("A0242", "用户验证码已过期"),

    SYSTEM_CONFIG_UPDATE_ERROR("B0401", "系统配置更新失败"),

    ROLE_NAME_EXIST("B0411", "角色名称已存在"),

    ROLE_MARK_EXIST("B0412", "角色标识已存在"),

    SUPER_ADMIN_DATA_UNABLE_OPERATE("B0413", "超级管理员数据不允许操作"),

    ROLE_ALLOCATION_ERROR("B0414", "角色分配失败"),

    USER_UPDATE_ERROR("B0420", "用户更新失败"),

    NEW_PASSWORD_CANNOT_SAME_AS_OLD_PASSWORD("B0421", "新密码不能与旧密码相同"),

    USER_PASSWORD_UPDATE_ERROR("B0422", "用户密码更新失败"),

    USER_NOT_ASSIGN_ROLE("B0423", "用户未分配角色"),

    ASSOCIATION_DEFAULT_ROLE_ERROR("B0424", "关联默认角色失败"),

    DIRECTORY_NOT_EMPTY("B0441", "当前目录不为空，不允许删除"),

    ASSOCIATION_RESOURCE_ERROR("B0451", "关联请求资源失败"),

    ASSOCIATION_MODULE_ROLE_ERROR("B0452", "关联模块角色失败"),

    URL_EXTRACT_LABEL_ERROR("B0453", "URL: [{}]截取标签失败"),

    MODULE_ROLE_ASSOCIATION("B0454", "部分模块有角色关联，不允许删除"),

    MODULE_RESOURCE_DELETE("B0455", "模块资源删除失败"),;

    /**
     * 状态码
     */
    private final String code;

    /**
     * 状态码描述
     */
    private final String message;

    /**
     * 错误信息描述
     */
    @Override
    public String message() {
        return this.message;
    }

    /**
     * 错误码
     */
    @Override
    public String code() {
        return this.code;
    }
}
