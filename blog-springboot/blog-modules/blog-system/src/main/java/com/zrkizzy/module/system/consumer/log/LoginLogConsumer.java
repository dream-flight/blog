package com.zrkizzy.module.system.consumer.log;

import com.zrkizzy.common.models.domain.system.log.LoginLog;
import com.zrkizzy.common.mq.service.impl.AbstractConsumer;
import com.zrkizzy.module.system.mapper.log.LoginLogMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Objects;

/**
 * 登录日志消费者
 *
 * @author zhangrongkang
 * @since 2023/7/27
 */
@Service("loginLogConsumer")
public class LoginLogConsumer extends AbstractConsumer<LoginLog> {

    @Autowired
    private LoginLogMapper loginLogMapper;

    /**
     * 扩展消费方法
     *
     * @param loginLog 用户登录日志实体类
     */
    @Override
    public void onConsumer(LoginLog loginLog) {
        // 发送操作日志到数据库中
        if (Objects.nonNull(loginLog)) {
            loginLogMapper.insert(loginLog);
        }
    }

}
