package com.zrkizzy.module.system.mapper.menu;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.zrkizzy.common.models.domain.system.menu.MenuRole;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

/**
 * 菜单角色数据持久化接口
 *
 * @author zhangrongkang
 * @since 2023/4/17
 */
@Mapper
public interface MenuRoleMapper extends BaseMapper<MenuRole> {

    /**
     * 获取当前角色已有的菜单权限
     *
     * @param roleId 角色权限
     * @return 当前角色的具备的菜单权限
     */
    List<Long> listMenuIdByRoleId(Long roleId);

    /**
     * 批量添加菜单角色关联数据
     *
     * @param menuRoles 菜单角色集合
     * @return 受影响行数
     */
    Integer insertBatch(List<MenuRole> menuRoles);
}
