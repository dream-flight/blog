package com.zrkizzy.module.system.service.resource;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.zrkizzy.common.core.utils.IdUtil;
import com.zrkizzy.common.core.utils.bean.BeanCopyUtil;
import com.zrkizzy.common.models.domain.system.resource.Resource;
import com.zrkizzy.common.models.dto.system.resource.ResourceDTO;
import com.zrkizzy.common.models.query.system.resource.ResourceQuery;
import com.zrkizzy.module.system.mapper.resource.ResourceMapper;
import com.zrkizzy.system.facade.service.resource.IResourceService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;
import org.springframework.util.StringUtils;

import java.util.List;
import java.util.Objects;

/**
 * <p>
 * 资源业务逻辑接口实现类
 * </p>
 *
 * @author zhangrongkang
 * @since 2023/7/27
 */
@Service
@Slf4j
public class ResourceServiceImpl implements IResourceService {

    @Autowired
    private IdUtil idUtil;

    @Autowired
    private ResourceMapper resourceMapper;

    /**
     * 获取所有资源
     *
     * @param resourceQuery 资源查询对象
     * @return 资源分页数据
     */
    @Override
    public Page<Resource> listResources(ResourceQuery resourceQuery) {
        // 开启分页
        Page<Resource> page = new Page<>(resourceQuery.getCurrentPage(), resourceQuery.getPageSize());
        // 定义查询条件
        QueryWrapper<Resource> queryWrapper = getQueryWrapper(resourceQuery);
        // 获取时间范围
        List<String> dataRange = resourceQuery.getDataRange();
        // 如果时间范围不为空
        if (!CollectionUtils.isEmpty(dataRange)) {
            // 拼接时间范围查询条件
            queryWrapper.between("create_time", dataRange.get(0), dataRange.get(1));
        }
        queryWrapper.orderByDesc("create_time");
        // 查询分页
        return resourceMapper.selectPage(page, queryWrapper);
    }

    private QueryWrapper<Resource> getQueryWrapper(ResourceQuery resourceQuery) {
        QueryWrapper<Resource> queryWrapper = new QueryWrapper<>();
        // 资源名称
        if (StringUtils.hasLength(resourceQuery.getName())) {
            queryWrapper.like("name", resourceQuery.getName());
        }
        // 资源请求方式
        if (StringUtils.hasLength(resourceQuery.getMethod())) {
            queryWrapper.eq("method", resourceQuery.getMethod());
        }
        // 资源请求路径
        if (StringUtils.hasLength(resourceQuery.getUrl())) {
            queryWrapper.like("url", resourceQuery.getUrl());
        }
        return queryWrapper;
    }

    /**
     * 更新指定请求资源
     *
     * @param resourceDTO 资源数据接收对象
     * @return 是否更新成功
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public Boolean saveResource(ResourceDTO resourceDTO) {
        // 对资源进行更新操作并返回更新结果
        if (Objects.nonNull(resourceDTO.getId())) {
            return resourceMapper.updateById(BeanCopyUtil.copy(resourceDTO, Resource.class)) == 1;
        }
        // 新增接口
        return insertResource(resourceDTO);
    }

    /**
     * 新增接口信息
     *
     * @param resourceDTO 接口数据传输对象
     * @return 是否新增成功
     */
    private Boolean insertResource(ResourceDTO resourceDTO) {
        resourceDTO.setId(idUtil.nextId());
        return resourceMapper.insert(BeanCopyUtil.copy(resourceDTO, Resource.class)) == 1;
    }

    /**
     * 获取指定资源信息
     *
     * @param resourceId 资源ID
     * @return 资源数据返回对象
     */
    @Override
    public Resource getResourceById(Long resourceId) {
        return resourceMapper.selectById(resourceId);
    }

    /**
     * 批量删除请求资源
     *
     * @param ids 资源ID集合
     * @return true：删除成功，false：删除失败
     */
    @Override
    public Boolean deleteBatch(List<Long> ids) {
        return resourceMapper.deleteBatchIds(ids) == ids.size();
    }

}
