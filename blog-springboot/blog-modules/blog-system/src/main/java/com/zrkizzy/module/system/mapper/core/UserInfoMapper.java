package com.zrkizzy.module.system.mapper.core;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.zrkizzy.common.models.domain.system.core.UserInfo;
import org.apache.ibatis.annotations.Mapper;

/**
 * 用户信息数据持久化接口
 *
 * @author zhangrongkang
 * @since 2023/5/4
 */
@Mapper
public interface UserInfoMapper extends BaseMapper<UserInfo> {
}
