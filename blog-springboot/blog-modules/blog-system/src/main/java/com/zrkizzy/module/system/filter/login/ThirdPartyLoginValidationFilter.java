package com.zrkizzy.module.system.filter.login;

import com.zrkizzy.common.models.dto.system.common.LoginDTO;
import com.zrkizzy.common.models.enums.system.common.login.LoginChannel;
import com.zrkizzy.module.system.exception.LoginErrorCode;
import com.zrkizzy.module.system.filter.LoginFilter;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import static com.zrkizzy.common.models.enums.system.common.login.LoginChannel.SYSTEM;

/**
 * 第三方登录校验责任链节点
 *
 * @author zhangrongkang
 * @since 2023/8/31
 */
@Slf4j
@Component("thirdPartyLoginValidationFilter")
public class ThirdPartyLoginValidationFilter extends LoginFilter {

    /**
     * 当前节点登录处理逻辑
     *
     * @param loginDTO 用户登录数据传输对象
     */
    @Override
    public void handleLogin(LoginDTO loginDTO) {
        log.info("-------------------- 进入第三方校验责任链节点 --------------------");
        // 获取登录渠道
        LoginChannel channel = loginDTO.getChannel();
        log.info("登录渠道为：{}", channel);
        // 如果登录类型不是系统登录则进入第三方校验
        if (channel != SYSTEM) {
            // 根据登录类型对登录方式进行相应校验 QQ、Gitee、WeiBo、Mobile
            // TODO 根据type使用第三方校验工厂生成对应的校验工具类
            switch (channel) {
                // Gitee
                case GIE_EE -> System.out.println("Gitee登录");
                // 微博
                case WEI_BO -> System.out.println("微博登录");
                // QQ
                case QQ -> System.out.println("QQ登录");
                // 电话
                case MOBILE -> System.out.println("电话登录");
                // 默认抛出异常
                default -> throw LoginErrorCode.LOGIN_CHANNEL_ERROR.exception();
            }
            // 目前先抛出未开放第三方登录错误
            throw LoginErrorCode.NOT_OPEN_THIRD_CHANNEL.exception();
//            User user = switch () ...
//            UserContext.setUser(user);
//            return;
        }
        log.info("-------------------- 离开第三方校验责任链节点 --------------------");
        // 如果不是第三方登录则进行其余校验
        loginFilter.handleLogin(loginDTO);
    }
}
