package com.zrkizzy.module.system.handler;

import com.zrkizzy.common.core.domain.response.Result;
import com.zrkizzy.common.core.enums.CommonErrorCode;
import com.zrkizzy.common.core.exception.ErrorCode;
import com.zrkizzy.common.core.exception.GlobalException;
import com.zrkizzy.common.core.utils.StringUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import java.util.Objects;

/**
 * 全局异常处理类
 *
 * @author zhangrongkang
 * @since 2023/03/14
 */
@Slf4j
@RestControllerAdvice
public class GlobalExceptionHandler {

    /**
     * 参数异常全局处理
     *
     * @param e 参数异常
     * @return 前端响应对象
     */
    @ExceptionHandler(MethodArgumentNotValidException.class)
    public Result<Boolean> exceptionHandler(MethodArgumentNotValidException e) {
        // 返回对应的错误信息到前端
        return Result.failure(CommonErrorCode.BAD_REQUEST, Objects.requireNonNull(e.getBindingResult().getFieldError()).getDefaultMessage());
    }

    /**
     * 自定义全局异常全局处理
     *
     * @param e 参数异常
     * @return 前端响应对象
     */
    @ExceptionHandler(GlobalException.class)
    public Result<?> exceptionHandler(GlobalException e) {
        ErrorCode errorCode = e.getErrorCode();
        if (Objects.nonNull(e.getArgs())) {
            // 定义返回信息
            String message = StringUtil.messageFormat(errorCode.message(), e.getArgs());
            return Result.failure(errorCode, message);
        }
        // 返回对应的错误信息到前端
        return Result.failure(e.getErrorCode());
    }

    /**
     * 全局异常处理
     *
     * @param e 大异常
     * @return 前端响应对象
     */
//    @ExceptionHandler(Exception.class)
//    public Result<?> exceptionHandler(Exception e) {
//        log.error("error: {}", e.getMessage());
//        return Result.failure();
//    }
}