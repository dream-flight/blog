package com.zrkizzy.module.system.producer.dead;

import com.zrkizzy.common.mq.service.impl.AbstractProducer;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

/**
 * 消息发送死信队列生产者
 *
 * @author zhangrongkang
 * @since 2023/12/22
 */
@Slf4j
@Service("messageSenderDeadProducer")
public class MessageSenderDeadProducer extends AbstractProducer {
}
