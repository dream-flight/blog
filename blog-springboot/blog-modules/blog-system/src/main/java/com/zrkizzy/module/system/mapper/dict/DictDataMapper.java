package com.zrkizzy.module.system.mapper.dict;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.zrkizzy.common.models.domain.system.dict.DictData;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * <p>
 * 字典数据数据持久化接口
 * </p>
 *
 * @author zhangrongkang
 * @since 2024/1/18
 */
public interface DictDataMapper extends BaseMapper<DictData> {
    /**
     * 删除字典类型ID对应字典数据
     *
     * @param ids 字典类型ID
     * @return 受影响行数
     */
    Integer deleteByDictTypeIds(@Param("ids") List<Long> ids);

    /**
     * 获取字典类型ID
     *
     * @param dictDataId 字典数据ID
     * @return 字典类型ID
     */
    Long getDictTypeIdById(Long dictDataId);
}
