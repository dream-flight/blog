package com.zrkizzy.module.system.service.log;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.zrkizzy.common.models.domain.system.log.LoginLog;
import com.zrkizzy.common.models.query.system.monitor.LoginLogQuery;
import com.zrkizzy.common.models.vo.system.log.LoginLogVO;
import com.zrkizzy.system.facade.service.log.ILoginLogService;
import com.zrkizzy.module.system.mapper.log.LoginLogMapper;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * <p>
 * 用户登录日志业务逻辑接口实现类
 * </p>
 *
 * @author zhangrongkang
 * @since 2023/7/4
 */
@Service
@Slf4j
public class LoginLogServiceImpl implements ILoginLogService {

    @Autowired
    private LoginLogMapper loginLogMapper;

    /**
     * 获取所有用户登录日志
     *
     * @param loginLogQuery 用户登录日志查询对象
     * @return 用户登录日志分页数据
     */
    @Override
    public Page<LoginLogVO> listLoginLogs(LoginLogQuery loginLogQuery) {
        // 开启分页
        Page<LoginLog> page = new Page<>(loginLogQuery.getCurrentPage(), loginLogQuery.getPageSize());
        // 查询分页
        return loginLogMapper.listLoginLog(page, loginLogQuery);
    }
    
    /**
     * 批量删除用户登录日志数据
     *
     * @param ids 用户登录日志ID
     * @return true：删除成功，false：删除失败
     */
    @Override
    public Boolean deleteBatch(List<Long> ids) {
        return loginLogMapper.deleteBatchIds(ids) == ids.size();
    }

    /**
     * 清空登录日志
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public void clear() {
        loginLogMapper.truncate();
    }

}
