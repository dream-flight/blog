package com.zrkizzy.module.system.service.resource;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.zrkizzy.common.core.domain.response.OptionsVO;
import com.zrkizzy.common.core.utils.IdUtil;
import com.zrkizzy.common.core.utils.bean.BeanCopyUtil;
import com.zrkizzy.common.models.domain.system.resource.Module;
import com.zrkizzy.common.models.dto.system.resource.ModuleDTO;
import com.zrkizzy.common.models.query.system.resource.ModuleQuery;
import com.zrkizzy.common.models.vo.system.resource.ModuleTreeVO;
import com.zrkizzy.module.security.OpenPolicyAgentAuthorizationManager;
import com.zrkizzy.module.system.exception.SystemErrorCode;
import com.zrkizzy.module.system.mapper.resource.ModuleMapper;
import com.zrkizzy.module.system.mapper.resource.ModuleResourceMapper;
import com.zrkizzy.module.system.mapper.resource.ModuleRoleMapper;
import com.zrkizzy.system.facade.service.resource.IModuleService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;
import org.springframework.util.StringUtils;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Objects;

/**
 * 请求模块业务逻辑接口实现类
 *
 * @author zhangrongkang
 * @since 2023/7/3
 */
@Service
@Slf4j
public class ModuleServiceImpl implements IModuleService {

    @Autowired
    private OpenPolicyAgentAuthorizationManager authorizationManager;

    @Autowired
    private IdUtil idUtil;

    @Autowired
    private ModuleRoleMapper moduleRoleMapper;

    @Autowired
    private ModuleResourceMapper moduleResourceMapper;

    @Autowired
    private ModuleMapper moduleMapper;

    /**
     * 获取模块选项集合
     *
     * @return 模块选项集合
     */
    @Override
    public List<OptionsVO> listModuleOptions() {
        return moduleMapper.listModuleOptions();
    }

    /**
     * 分页获取资源模块
     *
     * @param moduleQuery 资源模块查询对象
     * @return 资源模块分页数据
     */
    @Override
    public Page<Module> listModules(ModuleQuery moduleQuery) {
        // 开启分页
        Page<Module> page = new Page<>(moduleQuery.getCurrentPage(), moduleQuery.getPageSize());
        // 定义查询条件
        QueryWrapper<Module> queryWrapper = new QueryWrapper<>();
        // 模块名称
        String moduleName = moduleQuery.getName();
        if (StringUtils.hasLength(moduleName)) {
            // 拼接查询条件
            queryWrapper.like("name", moduleName);
        }
        // 获取时间范围
        List<String> dataRange = moduleQuery.getDataRange();
        // 如果时间范围不为空
        if (!CollectionUtils.isEmpty(dataRange)) {
            // 拼接时间范围查询条件
            queryWrapper.between("create_time", dataRange.get(0), dataRange.get(1));
        }
        return moduleMapper.selectPage(page, queryWrapper);
    }

    /**
     * 添加或更新资源模块
     *
     * @param moduleDTO 资源模块数据接收对象
     * @return 是否添加/更新成功
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public Boolean saveModule(ModuleDTO moduleDTO) {
        // 根据是否包含ID来判断添加/更新操作
        return Objects.nonNull(moduleDTO.getId()) ? updateModule(moduleDTO) : insertModule(moduleDTO);
    }

    /**
     * 获取指定资源模块信息
     *
     * @param moduleId 资源模块ID
     * @return 资源模块数据返回对象
     */
    @Override
    public Module getModuleById(Long moduleId) {
        return moduleMapper.selectById(moduleId);
    }

    /**
     * 批量删除资源模块数据
     *
     * @param ids 资源模块ID
     * @return true：删除成功，false：删除失败
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public Boolean deleteBatch(List<Long> ids) {
        // 1. tb_module_role中如果有role_id对应则不能删除
        Integer count = moduleRoleMapper.countByModuleIds(ids);
        if (count > 0) {
            // 抛出当前模块有角色关联无法删除异常
            throw SystemErrorCode.MODULE_ROLE_ASSOCIATION.exception();
        }
        // 2. 删除tb_module_resource对应的数据
        if (moduleResourceMapper.deleteByModuleIds(ids) == ids.size()) {
            // 删除失败则抛出模块资源删除异常
            throw SystemErrorCode.MODULE_RESOURCE_DELETE.exception();
        }
        // 3. 删除成功后清除动态权限
        if (moduleMapper.deleteBatchIds(ids) == ids.size()) {
            authorizationManager.clearResourceData();
            return Boolean.TRUE;
        }
        return Boolean.FALSE;
    }

    /**
     * 获取所有模块ID
     *
     * @return 模块ID集合
     */
    @Override
    public List<Long> getAllModuleId() {
        return moduleMapper.selectAllModuleId();
    }

    /**
     * 获取所有模块树形数据集合
     *
     * @return 所有模块树形数据
     */
    @Override
    public List<ModuleTreeVO> getAllModuleTree() {
        return moduleMapper.getAllModuleTree();
    }

    /**
     * 更新当前资源模块
     *
     * @param moduleDTO 资源模块数据接收对象
     * @return 是否更新成功
     */
    private Boolean updateModule(ModuleDTO moduleDTO) {
        // 对资源模块进行更新操作并返回响应结果
        return moduleMapper.updateById(BeanCopyUtil.copy(moduleDTO, Module.class)) == 1;
    }

    /**
     * 添加新的资源模块
     *
     * @param moduleDTO 资源模块数据接收对象
     * @return 是否添加成功
     */
    private Boolean insertModule(ModuleDTO moduleDTO) {
        // 设置ID
        moduleDTO.setId(idUtil.nextId());
        moduleDTO.setUpdateTime(LocalDateTime.now());
        // 添加资源模块数据并返回添加结果
        return moduleMapper.insert(BeanCopyUtil.copy(moduleDTO, Module.class)) == 1;
    }

}
