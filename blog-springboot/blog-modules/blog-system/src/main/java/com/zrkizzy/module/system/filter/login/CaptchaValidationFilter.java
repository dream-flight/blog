package com.zrkizzy.module.system.filter.login;

import com.zrkizzy.common.models.dto.system.common.LoginDTO;
import com.zrkizzy.common.redis.enums.RedisKey;
import com.zrkizzy.common.redis.service.IRedisService;
import com.zrkizzy.module.system.exception.LoginErrorCode;
import com.zrkizzy.module.system.filter.LoginFilter;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

/**
 * 验证码校验节点
 *
 * @author zhangrongkang
 * @since 2023/8/30
 */
@Slf4j
@Component("captchaValidationFilter")
public class CaptchaValidationFilter extends LoginFilter {

    @Autowired
    private IRedisService redisService;

    /**
     * 当前节点登录处理逻辑
     *
     * @param loginDTO 用户登录数据传输对象
     */
    @Override
    public void handleLogin(LoginDTO loginDTO) {
        log.info("-------------------- 进入了验证码责任链节点过滤器 --------------------");
        // Redis中获取验证码
        String redisCode = redisService.get(RedisKey.CAPTCHA_KEY.getKey() + loginDTO.getTraceId(), String.class);
        log.info("验证码为：{}", redisCode);
        log.info("正在校验验证码是否过期...");
        // 验证码是否过期
        if (!StringUtils.hasLength(redisCode)) {
            // 抛出验证码过期异常
            throw LoginErrorCode.USER_CAPTCHA_EXPIRED.exception();
        }
        log.info("正在校验验证码是否正确...");
        // 判断验证码是否一致
        if (!redisCode.equals(loginDTO.getCode())) {
            // 抛出验证码错误异常
            throw LoginErrorCode.USER_CAPTCHA_ERROR.exception();
        }
        log.info("-------------------- 离开了验证码责任链节点过滤器 --------------------");
        // 下一个链表执行校验逻辑
        loginFilter.handleLogin(loginDTO);
    }

}
