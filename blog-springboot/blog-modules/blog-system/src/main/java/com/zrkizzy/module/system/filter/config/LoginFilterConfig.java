package com.zrkizzy.module.system.filter.config;

import com.zrkizzy.module.system.filter.login.*;
import jakarta.annotation.PostConstruct;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;

/**
 * 组装登录责任链
 *
 * @author zhangrongkang
 * @since 2023/8/30
 */
@Configuration
public class LoginFilterConfig {

    @Autowired
    private ParameterValidationFilter parameterValidationFilter;

    @Autowired
    private ThirdPartyLoginValidationFilter thirdPartyLoginValidationFilter;

    @Autowired
    private CaptchaValidationFilter captchaValidationFilter;

    @Autowired
    private UsernamePasswordValidationFilter usernamePasswordValidationFilter;

    @Autowired
    private UserRoleValidationFilter userRoleValidationFilter;

    @PostConstruct
    public void init() {
        // 第一个组装参数非空校验节点
        parameterValidationFilter.setNext(thirdPartyLoginValidationFilter);
        // 第二个组装第三方登录节点，如果第三方授权成功存储User对象后直接返回
        thirdPartyLoginValidationFilter.setNext(captchaValidationFilter);
        // 验证码验证结束后进入到用户名密码校验节点
        captchaValidationFilter.setNext(usernamePasswordValidationFilter);
        // 用户名密码节点校验完成后校验用户角色节点
        usernamePasswordValidationFilter.setNext(userRoleValidationFilter);
    }
}
