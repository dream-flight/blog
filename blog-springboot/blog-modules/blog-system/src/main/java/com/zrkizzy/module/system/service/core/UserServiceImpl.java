package com.zrkizzy.module.system.service.core;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.zrkizzy.common.core.domain.response.OptionsVO;
import com.zrkizzy.common.core.domain.response.PageResult;
import com.zrkizzy.common.core.utils.IdUtil;
import com.zrkizzy.common.core.utils.bean.BeanCopyUtil;
import com.zrkizzy.common.models.domain.system.core.Role;
import com.zrkizzy.common.models.domain.system.core.User;
import com.zrkizzy.common.models.dto.system.core.AvatarDTO;
import com.zrkizzy.common.models.dto.system.core.PasswordDTO;
import com.zrkizzy.common.models.dto.system.core.UserDTO;
import com.zrkizzy.common.models.dto.system.core.UserUpdateDTO;
import com.zrkizzy.common.models.query.system.core.UserQuery;
import com.zrkizzy.common.models.vo.system.core.UserVO;
import com.zrkizzy.common.redis.enums.RedisKey;
import com.zrkizzy.common.redis.service.IRedisService;
import com.zrkizzy.module.security.constant.SecurityConst;
import com.zrkizzy.common.security.context.SecurityContext;
import com.zrkizzy.module.system.exception.LoginErrorCode;
import com.zrkizzy.module.system.exception.SystemErrorCode;
import com.zrkizzy.module.system.mapper.core.UserMapper;
import com.zrkizzy.system.facade.service.config.ISystemConfigService;
import com.zrkizzy.system.facade.service.core.IUserRoleService;
import com.zrkizzy.system.facade.service.core.IUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Objects;

import static com.zrkizzy.common.redis.enums.RedisKey.USER_KEY;


/**
 * 用户业务逻辑接口实现类
 *
 * @author zhangrongkang
 * @since 2023/3/7
 */
@Service
public class UserServiceImpl implements IUserService {

    @Autowired
    private ISystemConfigService systemConfigService;

    @Autowired
    private IUserRoleService userRoleService;

    @Autowired
    private IRedisService redisService;

    @Autowired
    private UserMapper userMapper;

    @Autowired
    private IdUtil idUtil;

    @Autowired
    private PasswordEncoder passwordEncoder;

    /**
     * 获取所有用户
     *
     * @param userQuery 用户查询对象
     * @return 所有用户集合
     */
    @Override
    public PageResult<UserVO> listUsers(UserQuery userQuery) {
        // 开启分页
        Page<User> page = new Page<>(userQuery.getCurrentPage(), userQuery.getPageSize());
        Page<UserVO> result = userMapper.listUsers(page, userQuery);
        return PageResult.<UserVO>builder().list(result.getRecords()).total(result.getTotal()).build();
    }

    /**
     * 更新用户个人信息
     *
     * @param userUpdateDTO 用户个人信息数据传输对象
     * @return 受影响的行数
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public Integer updateLoginUser(UserUpdateDTO userUpdateDTO) {
        // 校验当期更新用户数据
        User user = checkUser(userUpdateDTO);
        // 更新用户对象
        int row = userMapper.updateById(user);
        if (row != 1) {
            // 抛出更新失败异常
            throw SystemErrorCode.USER_UPDATE_ERROR.exception();
        }
        // 刷新Redis中缓存的用户信息
        refreshUserCache(user);
        // 数据并返回受影响的行数
        return row;
    }

    /**
     * 刷新Redis中缓存的用户信息
     *
     * @param user 用户对象
     */
    private void refreshUserCache(User user) {
        // Redis中不展示密码
        user.setPassword(null);
        // 获取用户全局唯一标识
        String traceId = SecurityContext.getTraceId();
        String key = USER_KEY.getKey();
        // 获取当前更新个人信息用户缓存的失效时间
        Long expire = redisService.getExpire(key + traceId);
        // 删除Redis中缓存的用户个人信息
        redisService.del(key + traceId);
        // 更新缓存中的用户个人信息，缓存失效时间不改变
        redisService.set(key + traceId, user, expire);
    }

    /**
     * 更新用户密码
     *
     * @param passwordDTO 用户更新密码数据传递对象
     * @return 受影响行数
     */
    @Override
    public Integer updatePassword(PasswordDTO passwordDTO) {
        // TODO 短信验证码Key
        // Redis中邮件验证码Key
        String emailKey = RedisKey.CAPTCHA_EMAIL_KEY.getKey() + passwordDTO.getUsername();
        // 判断Redis中的验证码是否过期
        if (!redisService.hasKey(emailKey)) {
            // 抛出验证码过期业务异常
            throw SystemErrorCode.USER_CAPTCHA_EXPIRED.exception();
        }
        // 验证码存在则获取到验证码
        String code = redisService.get(emailKey, String.class);
        // 对比当前验证码与用户输入验证码是否一致
        if (!code.equals(passwordDTO.getCode())) {
            // 抛出验证码错误业务异常
            throw SystemErrorCode.USER_CAPTCHA_ERROR.exception();
        }
        // 获取数据库中用户
        User user = userMapper.selectById(passwordDTO.getId());
        // 与原密码对比是否一致
        if (passwordEncoder.matches(passwordDTO.getPassword(), user.getPassword())) {
            // 抛出原密码和新密码不能相同业务异常被全局异常管理器捕获并返回前端
            throw SystemErrorCode.NEW_PASSWORD_CANNOT_SAME_AS_OLD_PASSWORD.exception();
        }
        // 获取Redis中存储的Key
        String key = RedisKey.USER_KEY.getKey() + SecurityContext.getTraceId();
        // 更新密码
        user.setPassword(passwordEncoder.encode(passwordDTO.getPassword()));
        // 更新用户更新时间
        user.setUpdateTime(LocalDateTime.now());
        // 更新用户并返回受影响的行数
        int row = userMapper.updateById(user);
        // 根据受影响的行数判断是否更新成功
        if (row > 0) {
            // 清除Redis重新登录
            redisService.del(key);
            return row;
        }
        // 抛出业务异常被全局异常管理器捕获并返回前端
        throw SystemErrorCode.USER_PASSWORD_UPDATE_ERROR.exception();
    }

    /**
     * 更新登录用户头像
     *
     * @param avatarDTO 用户头像数据传输对象
     * @return 用户头像访问路径
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public String updateLoginUserAvatar(AvatarDTO avatarDTO) {
        // 获取当前用户对象
        User user = userMapper.getUserByUserId(avatarDTO.getUserId());
        // 用户头像
        user.setAvatar(avatarDTO.getSrc());
        // 更新数据库中用户信息
        userMapper.updateUserAvatar(avatarDTO.getUserId(), avatarDTO.getSrc());
        // 刷新缓存中的用户
        refreshUserCache(user);
        // 将头像返回
        return avatarDTO.getSrc();
    }

    /**
     * 获取用户选项集合
     *
     * @return 获取用户选项集合
     */
    @Override
    public List<OptionsVO> listUserOptions() {
        return userMapper.listUserOptions();
    }

    /**
     * 获取指定用户
     *
     * @param id 用户ID
     * @return 用户信息返回对象
     */
    @Override
    public UserVO getUserById(Long id) {
        // 获取当前登录用户对象
        User user = redisService.get(USER_KEY.getKey() + SecurityContext.getTraceId(), User.class);
        if (Objects.isNull(user)) {
            user = userMapper.getUserByUserId(id);
        }
        // 转换为用户数据返回对象
        UserVO userVO = BeanCopyUtil.copy(user, UserVO.class);
        // 单独定义用户角色标识与角色ID（用户只有一个角色）
        Role role = user.getRole();
        if (Objects.isNull(role)) {
            // 抛出当前用户未分配角色异常
            throw SystemErrorCode.USER_NOT_ASSIGN_ROLE.exception();
        }
        userVO.setRoles(role.getMark());
        userVO.setRoleId(role.getId());
        // 返回用户登录对象
        return userVO;
    }

    /**
     * 新增用户
     *
     * @param userDTO 用户数据传输对象
     * @return 是否新增成功
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public Boolean insert(UserDTO userDTO) {
        // 校验用户名合法性
        checkUsername(userDTO.getUsername());
        // 获取用户默认头像
        String avatar = systemConfigService.getSystemConfig().getAvatar();
        User user = BeanCopyUtil.copy(userDTO, User.class);
        // ID
        user.setId(idUtil.nextId());
        // 默认头像
        user.setAvatar(avatar);
        // 默认密码123456
        user.setPassword(passwordEncoder.encode(SecurityConst.DEFAULT_PASSWORD));
        // 用户默认角色，如果没有成功添加用户角色关联信息则抛出异常
        if (!userRoleService.setDefaultRole(user.getId())) {
            throw SystemErrorCode.ASSOCIATION_DEFAULT_ROLE_ERROR.exception();
        }
        // TODO 后期添加手机号码时校验手机

        // 添加新用户到数据库，受影响行数是否为1
        return userMapper.insert(user) == 1;
    }

    /**
     * 更新指定用户信息
     *
     * @param userUpdateDTO 用户更新数据对象
     * @return 是否跟新成功
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public Boolean updateUser(UserUpdateDTO userUpdateDTO) {
        // 校验用户并返回用户信息
        User user = checkUser(userUpdateDTO);
        // 获取当前用户角色
        Role role = user.getRole();
        if (role.getId().equals(SecurityConst.ADMIN_ID)) {
            // 抛出不能编辑管理员信息异常
            throw SystemErrorCode.SUPER_ADMIN_DATA_UNABLE_OPERATE.exception();
        }
        // 更新用户信息
        return userMapper.updateById(user) == 1;
    }

    /**
     * 修改用户状态
     *
     * @param id 用户ID
     * @return 是否修改成功
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public Boolean updateUserStatus(Long id) {
        // 获取当前用户状态
        User user = userMapper.selectById(id);
        // 设置用户状态
        user.setStatus(!user.getStatus());
        return userMapper.updateById(user) == 1;
    }

    /**
     * 重置用户密码
     *
     * @param id 用户ID
     * @return 是否重置成功
     */
    @Override
    public Boolean resetPassword(Long id) {
        String password = passwordEncoder.encode(SecurityConst.DEFAULT_PASSWORD);
        // 更新用户并返回结果
        return userMapper.resetPassword(id, password) == 1;
    }

    /**
     * 批量删除用户
     *
     * @param ids 用户集合
     * @return 是否删除成功
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public Boolean delete(List<Long> ids) {
        // 检测要删除的用户中是否包括管理员
        checkAdmin(ids);
        userRoleService.deleteByUserIds(ids);
        // 通过校验后可以进行删除
        return userMapper.deleteBatchIds(ids) == ids.size();
    }

    /**
     * 校验传来的用户是否都可以被删除
     *
     * @param ids 用户ID
     */
    private void checkAdmin(List<Long> ids) {
        // 通过用户ID获取角色ID集合
        List<Long> roleIds = userRoleService.listRoleIdByUserId(ids);
        for (Long roleId : roleIds) {
            if (roleId.equals(SecurityConst.ADMIN_ID)) {
                // 抛出不能删除管理员异常
                throw SystemErrorCode.SUPER_ADMIN_DATA_UNABLE_OPERATE.exception();
            }
        }
    }

    /**
     * 校验并返回User对象
     *
     * @param userUpdateDTO 用户数据更新对象
     * @return 受影响的行数
     */
    private User checkUser(UserUpdateDTO userUpdateDTO) {
        // 先更新数据库，再更新Redis
        User user = userMapper.getUserByUserId(userUpdateDTO.getId());
        String username = userUpdateDTO.getUsername();
        // 如果修改用户名
        if (!user.getUsername().equals(username)) {
            // 校验修改后用户名的合法性
            checkUsername(username);
        }
        // 设置用户信息
        user.setNickname(userUpdateDTO.getNickname());
        user.setUsername(username);
        user.setRemark(userUpdateDTO.getRemark());
        // 更新并返回最新的User对象
        return user;
    }

    /**
     * 校验用户名合法性
     *
     * @param username 用户名
     */
    private void checkUsername(String username) {
        // 校验修改后用户名的合法性
        Long count = userMapper.selectCount(new QueryWrapper<User>().eq("username", username));
        // 校验修改内容的合法性，如果没有修改用户名则能查出1条
        if (null != count && count > 0) {
            // 确保用户名是唯一的
            throw LoginErrorCode.USERNAME_ALREADY_EXIST.exception();
        }
    }

}
