package com.zrkizzy.module.system.mapper.resource;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.zrkizzy.common.core.domain.response.OptionsVO;
import com.zrkizzy.common.models.domain.system.resource.Module;
import com.zrkizzy.common.models.vo.system.resource.ModuleTreeVO;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

/**
 * 模块数据持久化接口
 *
 * @author zhangrongkang
 * @since 2023/3/16
 */
@Mapper
public interface ModuleMapper extends BaseMapper<Module> {

    /**
     * 获取模块选项集合
     *
     * @return 模块选项集合
     */
    List<OptionsVO> listModuleOptions();

    /**
     * 获取所有模块ID
     *
     * @return 模块ID集合
     */
    List<Long> selectAllModuleId();

    /**
     * 获取所有模块树形数据集合
     *
     * @return 所有模块树形数据
     */
    List<ModuleTreeVO> getAllModuleTree();
}
