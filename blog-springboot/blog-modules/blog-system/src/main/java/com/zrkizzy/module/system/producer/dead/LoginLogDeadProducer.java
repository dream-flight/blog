package com.zrkizzy.module.system.producer.dead;

import com.zrkizzy.common.mq.service.impl.AbstractProducer;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

/**
 * 登录日志死信队列生产者
 *
 * @author zhangrongkang
 * @since 2023/12/22
 */
@Slf4j
@Service("loginLogDeadProducer")
public class LoginLogDeadProducer extends AbstractProducer {
}
