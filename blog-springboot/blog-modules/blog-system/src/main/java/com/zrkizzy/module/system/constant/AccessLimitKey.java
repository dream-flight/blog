package com.zrkizzy.module.system.constant;

/**
 * 通用策略限流Key
 *
 * @author zhangrongkang
 * @since 2024/2/20
 */
public class AccessLimitKey {

    /**
     * 获取验证码
     */
    public static final String GET_CAPTCHA = "limit:common:captcha:getCaptcha";

}
