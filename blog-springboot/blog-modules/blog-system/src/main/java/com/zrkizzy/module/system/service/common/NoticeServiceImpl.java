package com.zrkizzy.module.system.service.common;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.zrkizzy.common.core.utils.IdUtil;
import com.zrkizzy.common.core.utils.StringUtil;
import com.zrkizzy.common.core.utils.bean.BeanCopyUtil;
import com.zrkizzy.common.models.domain.system.common.Notice;
import com.zrkizzy.common.models.dto.system.common.NoticeDTO;
import com.zrkizzy.common.models.query.system.common.NoticeQuery;
import com.zrkizzy.module.system.mapper.common.NoticeMapper;
import com.zrkizzy.system.facade.service.common.INoticeService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;

import java.util.List;
import java.util.Objects;

/**
 * <p>
 * 通知公告业务逻辑接口实现类
 * </p>
 *
 * @author zhangrongkang
 * @since 2024/3/29
 */
@Service
@Slf4j
public class NoticeServiceImpl implements INoticeService {

    @Autowired
    private IdUtil idUtil;

    @Autowired
    private NoticeMapper noticeMapper;

    /**
     * 获取所有通知公告
     *
     * @param noticeQuery 通知公告查询对象
     * @return 通知公告分页数据
     */
    @Override
    public Page<Notice> listNotices(NoticeQuery noticeQuery) {
        // 开启分页
        Page<Notice> page = new Page<>(noticeQuery.getCurrentPage(), noticeQuery.getPageSize());
        // 定义查询条件
        QueryWrapper<Notice> queryWrapper = new QueryWrapper<>();
        // 公告标题
        if (StringUtil.isNoneBlank(noticeQuery.getTitle())) {
            queryWrapper.eq("title", noticeQuery.getTitle());
        }
        // 公告类型（1通知 2公告）
        if (Objects.nonNull(noticeQuery.getType())) {
            queryWrapper.eq("type", noticeQuery.getType());
        }
        // 获取时间范围
        List<String> dataRange = noticeQuery.getDataRange();
        // 如果时间范围不为空
        if (!CollectionUtils.isEmpty(dataRange)) {
            // 拼接时间范围查询条件
            queryWrapper.between("create_time", dataRange.get(0), dataRange.get(1));
        }
        // 查询分页
        return noticeMapper.selectPage(page, queryWrapper);
    }

    /**
     * 添加或更新通知公告
     *
     * @param noticeDTO 通知公告数据接收对象
     * @return 是否添加/更新成功
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public Boolean saveNotice(NoticeDTO noticeDTO) {
        log.info("noticeDTO: {}", noticeDTO);
        // 根据是否包含ID来判断添加-更新操作
        if (null != noticeDTO.getId()) {
            // 更新通知公告
            return updateNotice(noticeDTO);
        }
        // 添加通知公告
        return insertNotice(noticeDTO);
    }

    /**
     * 获取指定通知公告信息
     *
     * @param noticeId 通知公告ID
     * @return 通知公告数据返回对象
     */
    @Override
    public Notice getNoticeById(Long noticeId) {
        return noticeMapper.selectById(noticeId);
    }
    
    /**
     * 批量删除通知公告数据
     *
     * @param ids 通知公告ID
     * @return true：删除成功，false：删除失败
     */
    @Override
    public Boolean deleteBatch(List<Long> ids) {
        return noticeMapper.deleteBatchIds(ids) == ids.size();
    }

    /**
     * 更新当前通知公告
     *
     * @param noticeDTO 通知公告数据接收对象
     * @return 是否更新成功
     */
    private Boolean updateNotice(NoticeDTO noticeDTO) {
        // 对通知公告进行更新操作并返回响应结果
        return noticeMapper.updateById(BeanCopyUtil.copy(noticeDTO, Notice.class)) == 1;
    }
    
    /**
     * 添加新的通知公告
     *
     * @param noticeDTO 通知公告数据接收对象
     * @return 是否添加成功
     */
    private Boolean insertNotice(NoticeDTO noticeDTO) {
        // 生成通知公告ID
        Long id = idUtil.nextId();
        // 设置ID
        noticeDTO.setId(id);
        // 添加通知公告数据并返回添加结果
        return noticeMapper.insert(BeanCopyUtil.copy(noticeDTO, Notice.class)) == 1;
    }

}
