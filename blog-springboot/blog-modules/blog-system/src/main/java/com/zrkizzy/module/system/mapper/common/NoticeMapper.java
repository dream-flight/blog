package com.zrkizzy.module.system.mapper.common;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.zrkizzy.common.models.domain.system.common.Notice;

/**
 * <p>
 * 通知公告数据持久化接口
 * </p>
 *
 * @author zhangrongkang
 * @since 2024/3/29
 */
public interface NoticeMapper extends BaseMapper<Notice> {

}
