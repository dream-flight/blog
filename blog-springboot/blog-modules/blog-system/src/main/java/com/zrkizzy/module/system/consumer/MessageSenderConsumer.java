package com.zrkizzy.module.system.consumer;

import com.zrkizzy.common.models.dto.system.message.MessageSenderDTO;
import com.zrkizzy.common.mq.service.impl.AbstractConsumer;
import com.zrkizzy.module.system.factory.MessageSenderFactory;
import com.zrkizzy.module.system.template.AbstractMessageSender;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.IOException;

/**
 * 消息发送消费者
 * <p>
 *   发送邮件 / 短信
 * </p>
 *
 * @author zhangrongkang
 * @since 2023/10/26
 */
@Slf4j
@Service("messageSenderConsumer")
public class MessageSenderConsumer extends AbstractConsumer<MessageSenderDTO> {

    @Autowired
    private MessageSenderFactory messageSenderFactory;

    /**
     * 扩展消费方法
     *
     * @param messageSenderDTO 消息发送对象
     * @throws IOException IO异常
     */
    @Override
    public void onConsumer(MessageSenderDTO messageSenderDTO) throws Exception {
        // 获取具体消息发送实例
        AbstractMessageSender messageSender = messageSenderFactory.getInstance(messageSenderDTO.getChannel());
        // 调用钩子方法发送邮件
        messageSender.sendMessage(messageSenderDTO);
    }

}
