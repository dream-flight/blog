package com.zrkizzy.module.system.template;

import com.zrkizzy.common.models.dto.system.message.MessageSenderDTO;
import com.zrkizzy.common.models.enums.system.message.MessageSenderChannel;
import com.zrkizzy.common.redis.service.IRedisService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Random;

import static com.zrkizzy.common.core.constant.TimeConst.FIVE_MINUTE;
import static com.zrkizzy.common.redis.enums.RedisKey.CAPTCHA_EMAIL_KEY;
import static com.zrkizzy.common.redis.enums.RedisKey.CAPTCHA_SMS_KEY;

/**
 * 消息发送模板类
 *
 * @author zhangrongkang
 * @since 2023/5/5
 */
@Component
public abstract class AbstractMessageSender {

    @Autowired
    private IRedisService redisService;

    /**
     * 发送消息到指定用户
     *
     * @param messageDTO 邮件发送数据传递对象
     */
    public abstract void sendMessage(MessageSenderDTO messageDTO) throws Exception;

    /**
     * 生成当前登录用户验证码并存储到Redis中
     *
     * @param senderTo 邮件接收人
     * @param channel 消息发送渠道
     * @return 验证码
     */
    public String verifyCode(MessageSenderChannel channel, String senderTo) {
        // 定义Redis中的存储前缀
        String prefix = null;
        // 随机生成六位验证码
        String verifyCode = String.valueOf(new Random().nextInt(899999) + 100000);
        prefix = switch (channel) {
            // 电子邮箱验证码
            case EMAIL -> CAPTCHA_EMAIL_KEY.getKey();
            // 手机短信验证码
            case SMS -> CAPTCHA_SMS_KEY.getKey();
            // 默认前缀为空
            default -> "";
        };
        // 存储验证码到Redis，过期时间五分钟
        redisService.set(prefix + senderTo, verifyCode, FIVE_MINUTE);
        // 将验证码返回
        return verifyCode;
    }

}
