package com.zrkizzy.module.system.aspect;

import cn.hutool.json.JSONUtil;
import com.zrkizzy.common.core.domain.response.Result;
import com.zrkizzy.common.core.exception.GlobalException;
import com.zrkizzy.common.core.utils.IdUtil;
import com.zrkizzy.common.core.utils.IpUtil;
import com.zrkizzy.common.core.utils.ServletUtil;
import com.zrkizzy.common.models.domain.system.log.OperateLog;
import com.zrkizzy.common.mq.service.IRabbitService;
import com.zrkizzy.common.security.context.SystemContext;
import com.zrkizzy.module.security.utils.SecurityUtil;
import com.zrkizzy.system.facade.annotation.OperationLog;
import jakarta.servlet.http.HttpServletRequest;
import lombok.extern.slf4j.Slf4j;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.*;
import org.aspectj.lang.reflect.MethodSignature;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

import java.lang.reflect.Method;

/**
 * 操作日志AOP切面
 *
 * @author zhangrongkang
 * @since 2023/6/25
 */
@Aspect
@Slf4j
@Component
public class OperateLogAspect {

    @Autowired
    private IdUtil idUtil;

    @Autowired
    private SecurityUtil securityUtil;

    @Autowired
    @Qualifier("operateLogProducer")
    private IRabbitService operateLogProducer;

    /**
     * 声明切入点位置
     */
    @Pointcut("@annotation(com.zrkizzy.system.facade.annotation.OperationLog)")
    public void operateLogCut() {
    }

    /**
     * 操作日志AOP切入点
     *
     * @param joinPoint 切入点
     */
    @Before(value = "operateLogCut()")
    public void handleBefore(JoinPoint joinPoint) {
        // 记录操作时间
        SystemContext.setOperateStartTime(System.currentTimeMillis());
    }

    @AfterReturning(value = "operateLogCut()", returning = "jsonResult")
    public void doAfterReturning(JoinPoint joinPoint, Object jsonResult) {
        // 记录操作日志信息
        handleOperateInfo(joinPoint, null, jsonResult);
    }

    @AfterThrowing(value = "operateLogCut()", throwing = "e")
    public void doAfterThrowing(JoinPoint joinPoint, Exception e) {
        // 记录操作日志信息
        handleOperateInfo(joinPoint, e, null);
    }

    /**
     * 添加操作日志信息到数据库中
     *
     * @param joinPoint 切入点
     * @param e 操作异常
     * @param jsonResult 返回结果
     */
    protected void handleOperateInfo(final JoinPoint joinPoint, final Exception e, Object jsonResult) {
        // 定义操作日志对象
        OperateLog operateLog = new OperateLog();
        // 当前方法消耗时间
        operateLog.setCostTime(System.currentTimeMillis() - SystemContext.getOperateStartTime());
        try {
            // 设置本次操作日志信息
            setOperateInfo(operateLog, joinPoint);
            Result<?> result = null;
            if (null != jsonResult) {
                // 转换结果对象
                result = JSONUtil.toBean(JSONUtil.parse(jsonResult).toString(), Result.class);
            }
            // 设置操作结果
            if (null != result) {
                // 操作结果
                operateLog.setStatus(Boolean.TRUE);
                // 设置操作结果
                operateLog.setOperateResult(String.valueOf(result));
            }
            // 判断本次请求是否成功
            if (null != e) {
                // 设置请求状态为失败
                operateLog.setStatus(Boolean.FALSE);
                // 设置返回消息
                operateLog.setOperateResult(String.valueOf(e));
                if (e instanceof GlobalException) {
                    // 重新设置返回消息
                    operateLog.setOperateResult(((GlobalException) e).getErrorCode().message());
                }
            }
            // 设置操作日志属性
            setOperateLogAttribute(operateLog);

            // 添加当前操作信息推送到MQ中
            operateLogProducer.sendMessage(operateLog);

        } catch (Exception exception) {
            log.error("保存操作日志出错: {}", exception.getMessage());
        } finally {
            // 清除系统变量中的数据
            SystemContext.remove();
        }

    }

    /**
     * 设置操作日志属性
     *
     * @param operateLog 操作日志实体类
     */
    private void setOperateLogAttribute(OperateLog operateLog) {
        // 操作用户
        operateLog.setUserId(securityUtil.getLoginUser().getId());
        // 获取当前线程的请求
        HttpServletRequest request = ServletUtil.getRequest();
        // 请求方式
        operateLog.setRequestMethod(request.getMethod());
        // 当前请求IP地址
        String ip = IpUtil.getIpAddress(request);
        // 操作IP
        operateLog.setOperateIp(ip);
        // IP属地
        operateLog.setOperateLocation(IpUtil.getIpLocation(ip));
        // ID
        operateLog.setId(idUtil.nextId());
        // traceId
        operateLog.setTraceId(Long.parseLong(securityUtil.getLoginUser().getTraceId()));
        // 操作内容
        operateLog.setOperateContent(SystemContext.getOperateContent());
    }

    /**
     * 设置当前操作对象信息
     *
     * @param operateLog 操作对象
     * @param joinPoint 连接点
     */
    private void setOperateInfo(OperateLog operateLog, JoinPoint joinPoint) {
        // 操作参数
        operateLog.setOperateParam(JSONUtil.toJsonStr(joinPoint.getArgs()));
        // 获取连接点的方法签名信息
        MethodSignature signature = (MethodSignature) joinPoint.getSignature();
        // 获取操作的方法
        Method method = signature.getMethod();
        // 通过执行的方法得到自定义注解
        OperationLog logOperation = method.getAnnotation(OperationLog.class);
        // 操作类型
        operateLog.setType(logOperation.type().name());

        // 请求的类名
        String className = joinPoint.getTarget().getClass().getName();
        // 操作方法名称
        operateLog.setMethodName(className + "." + signature.getName() + "()");
    }

}
