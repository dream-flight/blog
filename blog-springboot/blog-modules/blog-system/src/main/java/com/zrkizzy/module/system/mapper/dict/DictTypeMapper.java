package com.zrkizzy.module.system.mapper.dict;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.zrkizzy.common.models.domain.system.dict.DictType;
import com.zrkizzy.common.models.dto.system.dict.DictTypeOptionDTO;
import com.zrkizzy.common.models.vo.system.dict.DictTypeOptionVO;

import java.util.List;

/**
 * <p>
 * 字典类型数据持久化接口
 * </p>
 *
 * @author zhangrongkang
 * @since 2024/1/16
 */
public interface DictTypeMapper extends BaseMapper<DictType> {

    /**
     * 获取字典类型选项数据
     *
     * @return 字典类型选项数据集合
     */
    List<DictTypeOptionVO> listOption();

    /**
     * 获取字典类型ID
     *
     * @param dictType 字典类型
     * @return 字典类型ID
     */
    Long getDictTypeIdByType(String dictType);

    /**
     * 根据ID获取字典类型
     *
     * @param dictTypeId 字典类型ID
     * @return 字典类型
     */
    String getDictTypeById(Long dictTypeId);

    /**
     * 获取所有字典类型集合
     *
     * @return 字典类型选项数据传输对象集合
     */
    List<DictTypeOptionDTO> listDictTypeOption();
}
