package com.zrkizzy.module.system.filter;

import com.zrkizzy.common.models.dto.system.common.LoginDTO;

/**
 * 登录责任链处理器
 *
 * @author zhangrongkang
 * @since 2023/8/30
 */
public abstract class LoginFilter {

    /**
     * 登录责任链下一个节点
     */
    protected LoginFilter loginFilter;

    /**
     * 设置下一个责任链节点
     *
     * @param loginFilter 责任链节点
     */
    public void setNext(LoginFilter loginFilter) {
        this.loginFilter = loginFilter;
    }

    /**
     * 当前节点登录处理逻辑
     *
     * @param loginDTO 用户登录数据传输对象
     */
    public abstract void handleLogin(LoginDTO loginDTO);

}
