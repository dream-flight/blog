package com.zrkizzy.module.system.mapper.config;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.zrkizzy.common.models.domain.system.config.SystemConfig;
import org.apache.ibatis.annotations.Mapper;

/**
 * <p>
 * 系统配置数据持久化接口
 * </p>
 *
 * @author zhangrongkang
 * @since 2023/7/22
 */
@Mapper
public interface SystemConfigMapper extends BaseMapper<SystemConfig> {

}
