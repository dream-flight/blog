package com.zrkizzy.module.system.filter.login;

import com.zrkizzy.common.core.utils.StringUtil;
import com.zrkizzy.common.core.utils.ValidatorUtil;
import com.zrkizzy.common.models.dto.system.common.LoginDTO;
import com.zrkizzy.common.models.enums.system.common.login.LoginChannel;
import com.zrkizzy.module.system.exception.LoginErrorCode;
import com.zrkizzy.module.system.filter.LoginFilter;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

/**
 * 参数非空校验责任链节点
 *
 * @author zhangrongkang
 * @since 2023/8/31
 */
@Slf4j
@Primary
@Component("parameterValidationFilter")
public class ParameterValidationFilter extends LoginFilter {

    /**
     * 当前节点登录处理逻辑
     *
     * @param loginDTO 用户登录数据传输对象
     */
    @Override
    public void handleLogin(LoginDTO loginDTO) {
        // 第三方登录的追踪值和登录类型已经在实体类中验证过
        log.info("-------------------- 进入了参数校验责任链节点过滤器 --------------------");
        // 手机登录只看手机号码和验证码
        if (loginDTO.getChannel() == (LoginChannel.MOBILE)) {
            // 校验手机号码
            validationMobile(loginDTO.getMobile());
            // 校验验证码
            validationCaptcha(loginDTO.getCode());
        }
        // 系统登录需要查看用户名、密码、验证码
        if (loginDTO.getChannel() == (LoginChannel.SYSTEM)) {
            // 校验用户名
            validationEmail(loginDTO.getUsername());
            // 校验密码
            validationPassword(loginDTO.getPassword());
            // 校验验证码
            validationCaptcha(loginDTO.getCode());
        }
        log.info("参数校验通过...");
        log.info("-------------------- 离开了参数校验责任链节点过滤器 --------------------");
        // 如果是第三方登录在当前节点可以直接放行，传递到下一个节点校验
        loginFilter.handleLogin(loginDTO);
    }

    /**
     * 校验手机号是否合法
     *
     * @param mobile 手机号
     */
    private void validationMobile(String mobile) {
        // 检查是否为空
        if (!StringUtils.hasLength(mobile)) {
            // 抛出电话号码不能为空异常
            throw LoginErrorCode.MOBILE_NOT_NULL.exception();
        }
        // 不合法则抛出异常
        if (!ValidatorUtil.validateMobile(mobile)) {
            // 抛出手机不合法异常
            throw LoginErrorCode.PHONE_FORMAT_VALIDATE_FAILED.exception();
        }
    }

    /**
     * 校验邮箱是否合法
     *
     * @param email 邮箱
     */
    private void validationEmail(String email) {
        // 检查是否为空
        if (StringUtil.isBlank(email)) {
            // 抛出邮箱不能为空异常
            throw LoginErrorCode.EMAIL_NOT_NULL.exception();
        }
        // 不合法则抛出异常
        if (!ValidatorUtil.validateEmail(email)) {
            // 抛出邮箱格式不合法异常
            throw LoginErrorCode.EMAIL_FORMAT_VALIDATE_FAILED.exception();
        }
    }

    /**
     * 校验验证码
     *
     * @param captcha 验证码
     */
    private void validationCaptcha(String captcha) {
        // 校验验证码是否为空
        if (StringUtil.isBlank(captcha)) {
            // 直接抛出验证码不能为空异常
            throw LoginErrorCode.CAPTCHA_NOT_NULL.exception();
        }
    }

    /**
     * 校验密码
     *
     * @param password 密码
     */
    private void validationPassword(String password) {
        // 校验验证码是否为空
        if (!StringUtils.hasLength(password)) {
            // 直接抛出密码不能为空异常
            throw LoginErrorCode.PASSWORD_NOT_NULL.exception();
        }
    }


}
