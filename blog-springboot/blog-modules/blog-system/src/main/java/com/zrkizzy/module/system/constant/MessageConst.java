package com.zrkizzy.module.system.constant;

/**
 * 消息发送全局静态常量
 *
 * @author zhangrongkang
 * @since 2023/5/5
 */
public class MessageConst {

    /**
     * 验证码字段
     */
    public static final String VERIFY_CODE = "verifyCode";

    /**
     * 收件人昵称字段
     */
    public static final String NICK_NAME = "nickname";

    /**
     * 网站域名字段
     */
    public static final String WEBSITE = "website";

}
