package com.zrkizzy.module.scheduler.filter.type;

import com.zrkizzy.module.scheduler.dto.TypeFilterDTO;
import com.zrkizzy.module.scheduler.filter.TypeFilter;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import static com.zrkizzy.module.scheduler.constant.SchedulerJobConst.TYPE_FALSE;
import static com.zrkizzy.module.scheduler.constant.SchedulerJobConst.TYPE_TRUE;

/**
 * 布尔类型处理节点
 *
 * @author zhangrongkang
 * @since 2024/3/4
 */
@Slf4j
@Component("booleanTypeFilter")
public class BooleanTypeFilter extends TypeFilter {

    /**
     * 当前参数类型处理逻辑
     *
     * @param typeFilterDTO 类型责任链数据传输对象
     */
    @Override
    public void handleType(TypeFilterDTO typeFilterDTO) {
        // 获取字符值
        String type = typeFilterDTO.getType();
        // Boolean布尔类型，等于 true 或 false
        if (TYPE_TRUE.equalsIgnoreCase(type) || TYPE_FALSE.equalsIgnoreCase(type)) {
            // 构建参数值
            Object[] value = { Boolean.valueOf(type), Boolean.class };
            typeFilterDTO.setValue(value);
            // 结束当前参数类型处理
            return;
        }
        // 当前参数不为布尔类型，执行下一个节点处理逻辑
        typeFilter.handleType(typeFilterDTO);
    }
}
