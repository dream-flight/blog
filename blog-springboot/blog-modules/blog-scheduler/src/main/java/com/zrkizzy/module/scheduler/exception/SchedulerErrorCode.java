package com.zrkizzy.module.scheduler.exception;

import com.zrkizzy.common.core.exception.ErrorCode;
import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * 定时任务异常枚举
 *
 * @author zhangrongkang
 * @since 2024/2/29
 */
@Getter
@AllArgsConstructor
public enum SchedulerErrorCode implements ErrorCode {

    SCHEDULER_BUILD_JOB_ERROR("B0600", "构建定时任务失败"),

    CRON_EXPRESSION_ERROR("B0601", "Cron表达式不合法"),

    INVOKE_TARGET_ERROR("B0602", "目标字符串不允许 {} 调用"),

    PACKAGE_NOT_INVOKE("B0603", "当前包不允许被调用"),

    NOT_FOUND_CLASS("B0604", "反射 {} 类失败"),

    EXECUTE_STRATEGY_BUILD_ERROR("B0605", "定时任务执行策略构建失败"),

    CRON_EXPRESSION_EXPIRED("B0606", "Cron表达式已过期"),

    SCHEDULER_JOB_RESUME_ERROR("B0607", "定时任务恢复失败"),

    SCHEDULER_JOB_PAUSE_ERROR("B0608", "定时任务暂停失败"),

    SCHEDULER_JOB_DELETE_ERROR("B0609", "定时任务删除失败"),

    SCHEDULER_JOB_UPDATE_ERROR("B0610", "定时任务更新失败"),

    SCHEDULER_JOB_EXECUTE_ERROR("B0611", "定时任务执行失败");

    /**
     * 状态码
     */
    private final String code;

    /**
     * 状态码描述
     */
    private final String message;

    @Override
    public String message() {
        return this.message;
    }

    /**
     * 错误码
     */
    @Override
    public String code() {
        return this.code;
    }
}
