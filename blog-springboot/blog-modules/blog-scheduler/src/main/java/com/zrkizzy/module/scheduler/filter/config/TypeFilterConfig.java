package com.zrkizzy.module.scheduler.filter.config;

import com.zrkizzy.module.scheduler.filter.type.*;
import jakarta.annotation.PostConstruct;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;

/**
 * 组装参数校验责任链
 *
 * @author zhangrongkang
 * @since 2024/3/3
 */
@Slf4j
@Configuration
public class TypeFilterConfig {

    @Autowired
    private StringTypeFilter stringTypeFilter;

    @Autowired
    private BooleanTypeFilter booleanTypeFilter;

    @Autowired
    private LongTypeFilter longTypeFilter;

    @Autowired
    private DoubleTypeFilter doubleTypeFilter;

    @Autowired
    private IntegerTypeFilter integerTypeFilter;

    @PostConstruct
    public void init() {
        stringTypeFilter.setNext(booleanTypeFilter);
        booleanTypeFilter.setNext(longTypeFilter);
        longTypeFilter.setNext(doubleTypeFilter);
        doubleTypeFilter.setNext(integerTypeFilter);
    }
}
