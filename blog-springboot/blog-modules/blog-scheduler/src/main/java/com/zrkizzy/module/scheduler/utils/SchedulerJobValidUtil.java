package com.zrkizzy.module.scheduler.utils;

import com.zrkizzy.common.core.utils.StringUtil;
import com.zrkizzy.module.scheduler.constant.SchedulerJobConst;
import com.zrkizzy.common.models.dto.system.scheduler.SchedulerJobDTO;
import com.zrkizzy.module.scheduler.enums.InvokeTargetEnum;
import com.zrkizzy.module.scheduler.exception.SchedulerErrorCode;
import lombok.extern.slf4j.Slf4j;

/**
 * 定时任务校验工具类
 *
 * @author zhangrongkang
 * @since 2024/2/29
 */
@Slf4j
public class SchedulerJobValidUtil {

    /**
     * 校验定时任务对象是否有效
     *
     * @param schedulerJobDTO 定时任务数据传输对象
     */
    public static void validJobParam(SchedulerJobDTO schedulerJobDTO) {
        // 校验cron表达式
        CronUtil.validCronExpression(schedulerJobDTO.getCron());
        // 校验调用目标
        validInvokeTarget(schedulerJobDTO.getInvokeTarget());
    }

    /**
     * 校验调用目标
     *
     * @param invokeTarget 调用目标
     */
    private static void validInvokeTarget(String invokeTarget) {
        // 校验是否包含不允许调用的包
        if (StringUtil.containsAnyIgnoreCase(invokeTarget, SchedulerJobConst.JOB_ERROR_ARRAY)) {
            throw SchedulerErrorCode.PACKAGE_NOT_INVOKE.exception();
        }
        // 校验是否有不合法的调用方式
        InvokeTargetEnum invokeTargetEnum = InvokeTargetEnum.validInvokerTarget(invokeTarget);
        if (!InvokeTargetEnum.LEGAL.equals(invokeTargetEnum)) {
            // 返回对应错误信息
            throw SchedulerErrorCode.INVOKE_TARGET_ERROR.exception(invokeTargetEnum.getValue());
        }
    }
}
