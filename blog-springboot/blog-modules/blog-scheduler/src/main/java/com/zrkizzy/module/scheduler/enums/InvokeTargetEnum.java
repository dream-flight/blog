package com.zrkizzy.module.scheduler.enums;

import com.zrkizzy.common.core.utils.StringUtil;
import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * 调用目标字符串枚举
 *
 * @author zhangrongkang
 * @since 2024/2/29
 */
@Getter
@AllArgsConstructor
public enum InvokeTargetEnum {

    /**
     * 合法
     */
    LEGAL("legal"),

    RMI("rmi:"),

    LDAP("ldap:"),

    LDAPS("ldaps:"),

    HTTP("http://") ,

    HTTPS("https://");

    /**
     * 调用目标值
     */
    private final String value;

    /**
     * 校验调用目标字符串是否包含非法调用
     *
     * @param invokeTarget 调用目标字符串
     * @return 调用目标枚举
     */
    public static InvokeTargetEnum validInvokerTarget(String invokeTarget) {
        for (InvokeTargetEnum invokeTargetEnum : values()) {
            // 判读当前目标字符是否包含非法值
            if (StringUtil.containsIgnoreCase(invokeTarget, invokeTargetEnum.value)) {
                return invokeTargetEnum;
            }
        }
        // 否则返回合法
        return LEGAL;
    }
}
