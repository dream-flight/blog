package com.zrkizzy.module.scheduler.producer;

import com.zrkizzy.common.mq.service.impl.AbstractProducer;
import org.springframework.stereotype.Component;

/**
 * 定时任务日志生产者
 *
 * @author zhangrongkang
 * @since 2024/3/9
 */
@Component("schedulerJobLogProducer")
public class SchedulerJobLogProducer extends AbstractProducer {
}
