package com.zrkizzy.module.scheduler.consumer;

import com.zrkizzy.common.mq.service.impl.AbstractConsumer;
import com.zrkizzy.common.models.domain.system.scheduler.SchedulerJobLog;
import com.zrkizzy.module.scheduler.mapper.SchedulerJobLogMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.io.IOException;

/**
 * 定时任务日志消费者
 *
 * @author zhangrongkang
 * @since 2024/3/9
 */
@Component("schedulerJobLogConsumer")
public class SchedulerJobLogConsumer extends AbstractConsumer<SchedulerJobLog> {

    @Autowired
    private SchedulerJobLogMapper schedulerJobLogMapper;

    /**
     * 扩展消费方法
     *
     * @param schedulerJobLog 消息体
     * @throws IOException IO异常
     */
    @Override
    public void onConsumer(SchedulerJobLog schedulerJobLog) throws Exception {
        // 保存定时任务日志
        schedulerJobLogMapper.insert(schedulerJobLog);
    }
}
