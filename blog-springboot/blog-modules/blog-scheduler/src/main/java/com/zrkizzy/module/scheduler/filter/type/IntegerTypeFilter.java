package com.zrkizzy.module.scheduler.filter.type;

import com.zrkizzy.module.scheduler.dto.TypeFilterDTO;
import com.zrkizzy.module.scheduler.filter.TypeFilter;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

/**
 * 整型类型处理节点
 *
 * @author zhangrongkang
 * @since 2024/3/4
 */
@Slf4j
@Component("integerTypeFilter")
public class IntegerTypeFilter extends TypeFilter {

    /**
     * 当前参数类型处理逻辑
     *
     * @param typeFilterDTO 类型责任链数据传输对象
     */
    @Override
    public void handleType(TypeFilterDTO typeFilterDTO) {
        // 暂时将剩余类型参数设定为整型
        String type = typeFilterDTO.getType();
        typeFilterDTO.setValue(new Object[] { Integer.valueOf(type), Integer.class });
    }

}
