package com.zrkizzy.module.scheduler.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.zrkizzy.common.models.domain.system.scheduler.SchedulerJobLog;

/**
 * <p>
 * 定时任务调度日志数据持久化接口
 * </p>
 *
 * @author zhangrongkang
 * @since 2024/3/6
 */
public interface SchedulerJobLogMapper extends BaseMapper<SchedulerJobLog> {

}
