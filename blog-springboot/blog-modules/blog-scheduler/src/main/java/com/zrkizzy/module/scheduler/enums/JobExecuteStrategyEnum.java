package com.zrkizzy.module.scheduler.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * 任务执行策略枚举
 *
 * @author zhangrongkang
 * @since 2024/3/4
 */
@Getter
@AllArgsConstructor
public enum JobExecuteStrategyEnum {

    /**
     * 立即执行
     */
    EXECUTE_IMMEDIATELY((byte) 1),

    /**
     * 执行一次
     */
    EXECUTE_ONCE((byte) 2),

    /**
     * 放弃执行
     */
    EXECUTE_ABANDON((byte) 3);

    /**
     * 执行策略
     */
    private final Byte value;

    /**
     * 获取任务执行策略
     */
    public static JobExecuteStrategyEnum getJobExecuteStrategy(byte executeStrategy) {
        for (JobExecuteStrategyEnum jobExecuteStrategyEnum : values()) {
            if (jobExecuteStrategyEnum.getValue() == executeStrategy) {
                return jobExecuteStrategyEnum;
            }
        }
        return null;
    }
}
