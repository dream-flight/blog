package com.zrkizzy.module.scheduler.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.zrkizzy.common.models.domain.system.scheduler.SchedulerJob;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

/**
 * <p>
 * 定时任务数据持久化接口
 * </p>
 *
 * @author zhangrongkang
 * @since 2024/2/23
 */
@Mapper
public interface SchedulerJobMapper extends BaseMapper<SchedulerJob> {

    /**
     * 更新定时任务状态
     *
     * @param jobId 定时任务ID
     * @param status 定时任务状态
     * @return 受影响行数
     */
    Integer updateJobStatus(@Param("jobId") Long jobId, @Param("status") Boolean status);
}
