package com.zrkizzy.module.scheduler.filter.type;

import com.zrkizzy.common.core.utils.StringUtil;
import com.zrkizzy.module.scheduler.dto.TypeFilterDTO;
import com.zrkizzy.module.scheduler.filter.TypeFilter;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Component;

/**
 * 字符串类型处理节点
 *
 * @author zhangrongkang
 * @since 2024/3/4
 */
@Slf4j
@Primary
@Component("stringTypeFilter")
public class StringTypeFilter extends TypeFilter {

    /**
     * 当前参数类型处理逻辑
     *
     * @param typeFilterDTO 类型责任链数据传输对象
     */
    @Override
    public void handleType(TypeFilterDTO typeFilterDTO) {
        // 获取字符值
        String type = typeFilterDTO.getType();
        // String字符串类型，以'或"开头
        if (StringUtil.startsWithAny(type, "'", "\"")) {
            // 构建参数值
            Object[] value = { StringUtil.substring(type, 1, type.length() - 1), String.class };
            typeFilterDTO.setValue(value);
            // 结束当前参数类型处理
            return;
        }
        // 当前参数不为字符类型，执行下一个节点处理逻辑
        typeFilter.handleType(typeFilterDTO);
    }
}
