package com.zrkizzy.module.scheduler.filter;

import com.zrkizzy.module.scheduler.dto.TypeFilterDTO;

/**
 * 参数类型责任链处理器
 *
 * @author zhangrongkang
 * @since 2024/3/3
 */
public abstract class TypeFilter {

    /**
     * 参数类型责任链下一个节点
     */
    protected TypeFilter typeFilter;

    /**
     * 设置下一个责任链节点
     *
     * @param typeFilter 参数类型责任链节点
     */
    public void setNext(TypeFilter typeFilter) {
        this.typeFilter = typeFilter;
    }

    /**
     * 当前参数类型处理逻辑
     *
     * @param typeFilterDTO 类型责任链数据传输对象
     */
    public abstract void handleType(TypeFilterDTO typeFilterDTO);

}
