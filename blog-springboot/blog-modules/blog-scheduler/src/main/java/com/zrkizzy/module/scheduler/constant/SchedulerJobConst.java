package com.zrkizzy.module.scheduler.constant;

/**
 * 定时任务全局静态常量
 *
 * @author zhangrongkang
 * @since 2024/3/1
 */
public class SchedulerJobConst {

    /**
     * 定时任务违规的字符
     */
    public static final String[] JOB_ERROR_ARRAY = {
            "java.net.URL",
            "javax.naming.InitialContext",
            "org.yaml.snakeyaml",
            "org.springframework",
            "org.apache"
    };

    /**
     * 执行任务类名称
     */
    public static final String TASK_CLASS_NAME = "TASK_CLASS_NAME";

    /**
     * 定时任务数据传输对象
     */
    public static final String TASK_PROPERTIES = "TASK_PROPERTIES";

    /**
     * Boolean类型值true
     */
    public static final String TYPE_TRUE = "true";

    /**
     * Boolean类型值false
     */
    public static final String TYPE_FALSE = "false";

}
