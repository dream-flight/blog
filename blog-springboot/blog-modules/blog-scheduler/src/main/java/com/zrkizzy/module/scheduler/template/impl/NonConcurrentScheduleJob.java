package com.zrkizzy.module.scheduler.template.impl;

import com.zrkizzy.common.models.dto.system.scheduler.SchedulerJobDTO;
import com.zrkizzy.module.scheduler.template.AbstractScheduleJob;
import com.zrkizzy.module.scheduler.utils.SchedulerJobInvokeUtil;
import org.quartz.DisallowConcurrentExecution;
import org.quartz.JobExecutionContext;

/**
 * 定时任务执行类（不允许并发）
 *
 * @author zhangrongkang
 * @since 2024/3/2
 */
@DisallowConcurrentExecution
public class NonConcurrentScheduleJob extends AbstractScheduleJob {

    /**
     * 定时任务执行方法
     * <p>
     * 由子类重载
     * </p>
     *
     * @param context      工作执行上下文对象
     * @param schedulerJobDTO 定时任务数据传输对象
     * @throws Exception 执行过程中的异常
     */
    @Override
    protected void executeJob(JobExecutionContext context, SchedulerJobDTO schedulerJobDTO) throws Exception {
        // 执行定时任务
        SchedulerJobInvokeUtil.invokeMethod(schedulerJobDTO);
    }

}
