package com.zrkizzy.module.scheduler.task;

import com.zrkizzy.common.core.utils.StringUtil;
import com.zrkizzy.common.core.utils.bean.BeanCopyUtil;
import com.zrkizzy.common.models.domain.system.scheduler.SchedulerJob;
import com.zrkizzy.common.models.dto.system.scheduler.SchedulerJobStatusDTO;
import com.zrkizzy.module.scheduler.utils.CronUtil;
import com.zrkizzy.scheduler.facade.service.ISchedulerJobService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * 定时任务调度测试
 * 
 * @author zhangrongkang
 * @since 2024/3/4
 */
@Component("systemTask")
public class SystemTask {

    @Autowired
    private ISchedulerJobService schedulerJobService;

    public void multipleParams(String s, Boolean b, Long l, Double d, Integer i) {
        System.out.println(StringUtil.messageFormat("执行多参方法： 字符串类型{}，布尔类型{}，长整型{}，浮点型{}，整形{}", s, b, l, d, i));
    }

    public void paramsMethod(String params) {
        System.out.println("执行有参方法：" + params);
    }

    public void noParamsMethod() {
        System.out.println("执行无参方法");
    }

    /**
     * 关闭过期的定时任务
     */
    public void closeSchedulerJob() {
        System.out.println("开始关闭过期的定时任务...");
        // 获取所有
        List<SchedulerJob> schedulerJobs = schedulerJobService.listAllSchedulerJob();
        for (SchedulerJob schedulerJob : schedulerJobs) {
            // 如果当前定时任务cron表达式已过期
            if (CronUtil.validCronExpire(schedulerJob.getCron())) {
                SchedulerJobStatusDTO schedulerJobStatusDTO = BeanCopyUtil.copy(schedulerJob, SchedulerJobStatusDTO.class);
                // 设置定时任务状态为关闭
                schedulerJobStatusDTO.setStatus(false);
                // 关闭当前定时任务
                schedulerJobService.changeJobStatus(schedulerJobStatusDTO);
            }
        }
    }
}