package com.zrkizzy.module.scheduler.producer.dead;

import com.zrkizzy.common.mq.service.impl.AbstractProducer;
import org.springframework.stereotype.Component;

/**
 * 定时任务日志死信队列生产者
 *
 * @author zhangrongkang
 * @since 2024/3/9
 */
@Component("schedulerJobLogDeadProducer")
public class SchedulerJobLogDeadProducer extends AbstractProducer {
}
