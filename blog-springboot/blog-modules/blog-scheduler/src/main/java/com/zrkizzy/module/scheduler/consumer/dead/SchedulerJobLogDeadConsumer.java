package com.zrkizzy.module.scheduler.consumer.dead;

import com.zrkizzy.common.mq.service.impl.AbstractConsumer;
import com.zrkizzy.common.models.domain.system.scheduler.SchedulerJobLog;
import org.springframework.stereotype.Component;

import java.io.IOException;

/**
 * 定时任务日志私信队列消费者
 *
 * @author zhangrongkang
 * @since 2024/3/9
 */
@Component("schedulerJobLogDeadConsumer")
public class SchedulerJobLogDeadConsumer extends AbstractConsumer<SchedulerJobLog> {

    /**
     * 扩展消费方法
     *
     * @param schedulerJobLog 消息体
     * @throws IOException IO异常
     */
    @Override
    public void onConsumer(SchedulerJobLog schedulerJobLog) throws Exception {
        ack();
    }
}
