package com.zrkizzy.module.scheduler.filter.type;

import com.zrkizzy.common.core.utils.StringUtil;
import com.zrkizzy.module.scheduler.dto.TypeFilterDTO;
import com.zrkizzy.module.scheduler.filter.TypeFilter;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Component;

/**
 * 长整型类型处理节点
 *
 * @author zhangrongkang
 * @since 2024/3/4
 */
@Slf4j
@Component("longTypeFilter")
public class LongTypeFilter extends TypeFilter {

    /**
     * 当前参数类型处理逻辑
     *
     * @param typeFilterDTO 类型责任链数据传输对象
     */
    @Override
    public void handleType(TypeFilterDTO typeFilterDTO) {
        // 获取字符值
        String type = typeFilterDTO.getType();
        // long长整形，以L结尾
        if (StringUtil.endsWith(type, "L")) {
            // 构建参数值
            Object[] value = { Long.valueOf(StringUtils.substring(type, 0, type.length() - 1)), Long.class };
            typeFilterDTO.setValue(value);
            // 结束本次参数类型处理
            return;
        }
        // 当前参数不为长整型，执行下一个节点处理逻辑
        typeFilter.handleType(typeFilterDTO);
    }

}
