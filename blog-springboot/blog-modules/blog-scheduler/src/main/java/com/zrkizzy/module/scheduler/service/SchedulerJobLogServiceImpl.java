package com.zrkizzy.module.scheduler.service;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.zrkizzy.common.core.utils.StringUtil;
import com.zrkizzy.common.models.domain.system.scheduler.SchedulerJobLog;
import com.zrkizzy.module.scheduler.mapper.SchedulerJobLogMapper;
import com.zrkizzy.common.models.query.system.scheduler.SchedulerJobLogQuery;
import com.zrkizzy.scheduler.facade.service.ISchedulerJobLogService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import java.util.List;
import java.util.Objects;

/**
 * <p>
 * 定时任务调度日志业务逻辑接口实现类
 * </p>
 *
 * @author zhangrongkang
 * @since 2024/3/6
 */
@Service
@Slf4j
public class SchedulerJobLogServiceImpl implements ISchedulerJobLogService {

    @Autowired
    private SchedulerJobLogMapper schedulerJobLogMapper;

    /**
     * 获取所有定时任务调度日志
     *
     * @param schedulerJobLogQuery 定时任务调度日志查询对象
     * @return 定时任务调度日志分页数据
     */
    @Override
    public Page<SchedulerJobLog> listSchedulerJobLogs(SchedulerJobLogQuery schedulerJobLogQuery) {
        // 开启分页
        Page<SchedulerJobLog> page = new Page<>(schedulerJobLogQuery.getCurrentPage(), schedulerJobLogQuery.getPageSize());
        // 定义查询条件
        QueryWrapper<SchedulerJobLog> queryWrapper = new QueryWrapper<>();
        // 任务ID
        if (Objects.nonNull(schedulerJobLogQuery.getJobId())) {
            queryWrapper.eq("job_id", schedulerJobLogQuery.getJobId());
        }
        // 执行状态： 1 成功， 0 失败
        if (Objects.nonNull(schedulerJobLogQuery.getStatus())) {
            queryWrapper.eq("status", schedulerJobLogQuery.getStatus());
        }
        // 任务名称
        if (StringUtil.isNoneBlank(schedulerJobLogQuery.getJobName())) {
            queryWrapper.like("job_name", schedulerJobLogQuery.getJobName());
        }
        // 任务分组
        if (StringUtil.isNoneBlank(schedulerJobLogQuery.getJobGroup())) {
            queryWrapper.eq("job_group", schedulerJobLogQuery.getJobGroup());
        }
        // 获取时间范围
        List<String> dataRange = schedulerJobLogQuery.getDataRange();
        // 如果时间范围不为空
        if (!CollectionUtils.isEmpty(dataRange)) {
            // 拼接时间范围查询条件
            queryWrapper.between("create_time", dataRange.get(0), dataRange.get(1));
        }
        // 降序排列
        queryWrapper.orderByDesc("create_time");
        // 查询分页
        return schedulerJobLogMapper.selectPage(page, queryWrapper);
    }

    /**
     * 获取指定定时任务调度日志信息
     *
     * @param schedulerJobLogId 定时任务调度日志ID
     * @return 定时任务调度日志数据返回对象
     */
    @Override
    public SchedulerJobLog getSchedulerJobLogById(Long schedulerJobLogId) {
        return schedulerJobLogMapper.selectById(schedulerJobLogId);
    }
    
    /**
     * 批量删除定时任务调度日志数据
     *
     * @param ids 定时任务调度日志ID
     * @return true：删除成功，false：删除失败
     */
    @Override
    public Boolean deleteBatch(List<Long> ids) {
        return schedulerJobLogMapper.deleteBatchIds(ids) == ids.size();
    }

}
