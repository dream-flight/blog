package com.zrkizzy.module.scheduler.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;

import java.io.Serializable;

/**
 * 类型责任链数据传输对象
 *
 * @author zhangrongkang
 * @since 2024/3/4
 */
@Data
@Builder
@AllArgsConstructor
public class TypeFilterDTO implements Serializable {

    /**
     * 参数字符值
     */
    private String type;

    /**
     * 参数值数组
     */
    private Object[] value;

}
