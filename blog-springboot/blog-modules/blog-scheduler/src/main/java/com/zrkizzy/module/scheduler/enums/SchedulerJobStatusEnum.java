package com.zrkizzy.module.scheduler.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * 定时任务状态枚举
 *
 * @author zhangrongkang
 * @since 2024/3/4
 */
@Getter
@AllArgsConstructor
public enum SchedulerJobStatusEnum {

    /**
     * 正常
     */
    NORMAL(Boolean.TRUE),

    /**
     * 暂停
     */
    PAUSE(Boolean.FALSE);

    private final Boolean value;
}
