package com.zrkizzy.common.redis.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * Redis缓存前缀
 *
 * @author zhangrongkang
 * @since 2023/12/4
 */
@Getter
@AllArgsConstructor
public enum RedisPrefix {

    /**
     * 用户信息
     */
    USER("user:", "用户信息"),
    /**
     * 系统信息
     */
    SYSTEM("system:", "系统信息"),
    /**
     * 数据字典
     */
    DICT("dict:", "数据字典"),
    /**
     * 验证码信息前缀
     */
    CAPTCHA("captcha:", "验证码"),
    /**
     * 博客选项信息
     */
    OPTIONS("options:", "博客选项"),
    /**
     * 配置信息
     */
    CONFIG("config:", "配置信息");

    /**
     * 缓存键
     */
    private final String key;

    /**
     * 备注
     */
    private final String remark;

}
