package com.zrkizzy.common.redis.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * Redis数据类型枚举
 *
 * @author zhangrongkang
 * @since 2023/12/4
 */
@Getter
@AllArgsConstructor
public enum RedisType {

    /**
     * String类型
     */
    STRING_TYPE("STRING"),

    /**
     * 集合类型
     */
    List_TYPE("LIST");

    /**
     * 类型
     */
    private final String type;

    public static RedisType getRedisType(String type) {
        for (RedisType value : RedisType.values()) {
            if (value.getType().equals(type)) {
                return value;
            }
        }
        return null;
    }

}
