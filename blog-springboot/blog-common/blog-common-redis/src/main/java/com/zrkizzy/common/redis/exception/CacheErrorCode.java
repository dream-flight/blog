package com.zrkizzy.common.redis.exception;

import com.zrkizzy.common.core.exception.ErrorCode;
import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * 缓存自定义异常码
 *
 * @author zhangrongkang
 * @since 2024/2/5
 */
@Getter
@AllArgsConstructor
public enum CacheErrorCode implements ErrorCode {

    NO_VALID_KEY_PROVIDED("C0135", "未提供有效key"),

    PROVIDE_INVALID_VALUE("C0136", "提供无效的value"),

    CACHE_TYPE_NOT_EXIST("C0137", "缓存类型不存在");

    /**
     * 状态码
     */
    private final String code;

    /**
     * 状态码描述
     */
    private final String message;

    /**
     * 错误信息描述
     */
    @Override
    public String message() {
        return this.message;
    }

    /**
     * 错误码
     */
    @Override
    public String code() {
        return this.code;
    }
}
