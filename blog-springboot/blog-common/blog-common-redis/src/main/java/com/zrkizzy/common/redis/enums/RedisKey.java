package com.zrkizzy.common.redis.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * RedisKey枚举
 *
 * @author zhangrongkang
 * @since 2023/11/30
 */
@Getter
@AllArgsConstructor
public enum RedisKey {
    
    /**
     * 用户信息Key
     */
    USER_KEY("user:"),
    
    /**
     * 登录验证码Key
     */
    CAPTCHA_KEY("captcha:"),

    /**
     * 邮箱验证码Key
     */
    CAPTCHA_EMAIL_KEY("captcha:email:"),

    /**
     * 手机短信验证码Key
     */
    CAPTCHA_SMS_KEY("captcha:sms:"),

    /**
     * 系统菜单Key
     */
    SYSTEM_MENU_KEY("system:menus:"),

    /**
     * 数据字典Key
     */
    DICT_KEY("dict:"),

    /**
     * 系统基本配置Key
     */
    CONFIG_BASIC_KEY("config:basic"),

    /**
     * 博客配置信息Key
     */
    CONFIG_BLOG_KEY("config:blog"),

    /**
     * 博客标签Key
     */
    OPTIONS_TAGS_KEY("options:tags"),

    /**
     * 博客分类Key
     */
    OPTIONS_CATEGORY_KEY("options:category");

    private final String key;
    
}
