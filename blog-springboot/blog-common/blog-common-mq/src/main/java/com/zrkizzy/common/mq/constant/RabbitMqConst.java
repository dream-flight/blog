package com.zrkizzy.common.mq.constant;

/**
 * Rabbit全局静态常量
 *
 * @author zhangrongkang
 * @since 2023/5/4
 */
public class RabbitMqConst {

    /**
     * TTL类型数据
     */
    public static final String X_MESSAGE_TTL = "x-message-ttl";

    /**
     * 死信队列交换机
     */
    public static final String X_DEAD_LETTER_EXCHANGE = "x-dead-letter-exchange";

    /**
     * 死信队列路由
     */
    public static final String X_DEAD_LETTER_ROUTING_KEY = "x-dead-letter-routing-key";


}
