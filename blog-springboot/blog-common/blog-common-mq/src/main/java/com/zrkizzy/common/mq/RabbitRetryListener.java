package com.zrkizzy.common.mq;


import org.springframework.retry.RetryCallback;
import org.springframework.retry.RetryContext;

/**
 * RabbitMQ重试监听器
 *
 * @author zhangrongkang
 * @since 2023/10/24
 */
public interface RabbitRetryListener {

    /**
     * 最后一次重试失败回调
     *
     * @param context 重试操作上下文
     * @param callback 执行重试操作的核心逻辑
     * @param throwable 重试操作期间捕获到的异常对象
     * @param <E> 可能抛出的异常类型
     * @param <T> 操作的返回类型
     */
    <E extends Throwable, T> void lastRetry(RetryContext context, RetryCallback<T, E> callback, Throwable throwable);

    /**
     * 每一次重试失败回调
     *
     * @param context 重试操作上下文
     * @param callback 执行重试操作的核心逻辑
     * @param throwable 重试操作期间捕获到的异常对象
     * @param <E> 可能抛出的异常类型
     * @param <T> 操作的返回类型
     */
    <E extends Throwable, T> void onceRetry(RetryContext context, RetryCallback<T, E> callback, Throwable throwable);

}
