package com.zrkizzy.common.mq.config.properties;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

import java.util.List;

/**
 * RabbitMQ模块集合配置类
 *
 * @author zhangrongkang
 * @since 2023/10/24
 */
@Data
@Configuration
@ConfigurationProperties("spring.rabbitmq")
public class RabbitModuleProperties {

    /**
     * RabbitMQ模块配置集合
     */
    private List<RabbitProperties> modules;

}
