package com.zrkizzy.common.mq.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * RabbitMQ队列、交换机、路由枚举
 *
 * @author zhangrongkang
 * @since 2023/10/24
 */
@Getter
@AllArgsConstructor
public enum RabbitEnum {

    /**
     * 队列
     */
    QUEUE("blog.{}.queue", "队列名称"),

    /**
     * 交换机
     */
    EXCHANGE("blog.{}.exchange", "交换机名称"),

    /**
     * 路由
     */
    ROUTER_KEY("blog.{}.key", "路由名称");

    /**
     * 枚举值
     */
    private final String value;

    /**
     * 描述
     */
    private final String description;

}
