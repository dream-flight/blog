package com.zrkizzy.common.mq.exception;

import com.zrkizzy.common.core.enums.CommonErrorCode;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

/**
 * RabbitMQ错误自定义异常
 *
 * @author zhangrongkang
 * @since 2023/10/25
 */
@Data
@EqualsAndHashCode(callSuper = false)
@AllArgsConstructor
@NoArgsConstructor
public class RabbitMqException extends RuntimeException {

    /**
     * 服务器响应错误枚举
     */
    private CommonErrorCode errorCode = CommonErrorCode.CONFIGURATION_ERROR;
}