package com.zrkizzy.common.mq.service;

/**
 * RabbitMQ生产者操作接口
 *
 * @author zhangrongkang
 * @since 2023/10/24
 */
public interface IRabbitService {

    /**
     * 发送消息方法
     *
     * @param message 消息对象
     */
    void sendMessage(Object message);

}
