package com.zrkizzy.common.mq.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * 交换机类型枚举
 *
 * @author zhangrongkang
 * @since 2023/10/24
 */
@Getter
@AllArgsConstructor
public enum RabbitExchangeTypeEnum {

    /**
     * 直连交换机
     * <p>
     *     根据routing-key进行队列的精确匹配
     * </p>
     */
    DIRECT,

    /**
     * 主题交换机
     * <p>
     *     根据routing-key模糊匹配队列<br/>
     *     * 代表匹配一个字符，# 匹配0个或多个字符<br/>
     * </p>
     */
    TOPIC,

    /**
     * 扇形交换机
     * <p>
     *     直接分发给所有绑定的队列，忽略routing-key，用于广播信息
     * </p>
     */
    FANOUT,

    /**
     * 头交换机
     * <p>
     * 类似直连交换机<br/>
     * 不同于直连交换机的路由规则建立在头属性上而不是routing-key
     * </p>
     */
    HEADERS

}
