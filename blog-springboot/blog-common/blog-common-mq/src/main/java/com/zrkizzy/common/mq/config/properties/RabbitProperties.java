package com.zrkizzy.common.mq.config.properties;

import com.zrkizzy.common.mq.enums.RabbitExchangeTypeEnum;
import lombok.Data;

import java.util.Map;

/**
 * RabbitMQ属性配置类
 * <p>
 *     对应application-mq.yml中配置属性
 * </p>
 *
 * @author zhangrongkang
 * @since 2023/10/24
 */
@Data
public class RabbitProperties {

    /**
     * 路由Key
     */
    private String routingKey;

    /**
     * 生产者
     */
    private String producer;

    /**
     * 消费者
     */
    private String consumer;

    /**
     * 重试机制
     */
    private String retry;

    /**
     * 自动确认
     */
    private Boolean autoAck = Boolean.TRUE;

    /**
     * 队列
     */
    private Queue queue;

    /**
     * 交换机
     */
    private Exchange exchange;

    /**
     * 交换机信息类
     */
    @Data
    public static class Exchange {

        /**
         * 交换机类型
         * <p>
         *   默认绑定主题交换机
         * </p>
         */
        private RabbitExchangeTypeEnum type = RabbitExchangeTypeEnum.TOPIC;

        /**
         * 交换机名名称
         */
        private String name;

        /**
         * 是否持久化
         * <p>
         *   默认true表示持久化，重启后信息不会消失
         * </p>
         */
        private Boolean durable = Boolean.TRUE;

        /**
         * 是否自动删除交换机
         * <p>
         *   当所有绑定的队列都不使用时是否自动删除交换机<br/>
         *   默认false表示不自动删除<br/>
         * </p>
         */
        private Boolean autoDelete = Boolean.FALSE;

        /**
         * 交换机其他参数
         */
        private Map<String, Object> arguments;

    }

    @Data
    public static class Queue {

        /**
         * 队列名称
         */
        private String name;

        /**
         * 是否持久化
         * <p>
         *   默认true表示持久化，重启后消息不会消失
         * </p>
         */
        private Boolean durable = Boolean.TRUE;

        /**
         * 是否具有排他性
         * <p>
         *   默认false表示不具备，允许多个消费者消费同一个队列
         * </p>
         */
        private Boolean exclusive = Boolean.FALSE;

        /**
         * 是否自动删除队列
         * <p>
         *   当消费者断开连接，是否自动删除队列<br/>
         *   默认false表示不自动删除，防止消费者断开队列丢弃消息<br/>
         * </p>
         */
        private Boolean autoDelete = Boolean.FALSE;

        /**
         * 绑定死信队列的交换机名称
         */
        private String deadLetterExchange;

        /**
         * 绑定死信队列的路由Key
         */
        private String deadLetterRoutingKey;

        /**
         * 队列其他参数
         */
        private Map<String, Object> arguments;

    }
}
