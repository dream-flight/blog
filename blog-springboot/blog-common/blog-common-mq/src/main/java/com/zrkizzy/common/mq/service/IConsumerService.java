package com.zrkizzy.common.mq.service;

import org.springframework.amqp.rabbit.listener.api.ChannelAwareMessageListener;

/**
 * RabbitMQ消费者接口
 *
 * @author zhangrongkang
 * @since 2023/10/24
 */
public interface IConsumerService extends ChannelAwareMessageListener {

}
