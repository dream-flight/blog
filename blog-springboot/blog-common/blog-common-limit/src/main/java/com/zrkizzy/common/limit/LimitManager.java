package com.zrkizzy.common.limit;

import com.zrkizzy.common.limit.annotation.AccessLimit;

/**
 * 限流接口
 *
 * @author zhangrongkang
 * @since 2024/2/18
 */
public interface LimitManager {

    /**
     * 请求限流
     *
     * @param accessLimit 自定义限流注解
     * @return 是否请求成功
     */
    boolean tryAccess(AccessLimit accessLimit);

}
