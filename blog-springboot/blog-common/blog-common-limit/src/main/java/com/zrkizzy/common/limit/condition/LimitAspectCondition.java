package com.zrkizzy.common.limit.condition;

import com.zrkizzy.common.limit.constant.LimitConst;
import jakarta.validation.constraints.NotNull;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.Condition;
import org.springframework.context.annotation.ConditionContext;
import org.springframework.core.type.AnnotatedTypeMetadata;

/**
 * 限流AOP条件选择器
 *
 * @author zhangrongkang
 * @since 2024/2/18
 */
@Slf4j
public class LimitAspectCondition implements Condition {

    @Override
    public boolean matches(ConditionContext conditionContext, @NotNull AnnotatedTypeMetadata metadata) {
//        log.info("开始检查是否包含 blog.limit.strategy 属性");
        // 检查配置文件是否包含blog.limit.strategy属性
        return conditionContext.getEnvironment().containsProperty(LimitConst.LIMIT_STRATEGY);
    }

}
