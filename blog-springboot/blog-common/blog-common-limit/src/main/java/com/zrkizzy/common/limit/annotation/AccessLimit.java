package com.zrkizzy.common.limit.annotation;

import java.lang.annotation.*;
import java.util.concurrent.TimeUnit;

/**
 * 接口限流自定义注解
 *
 * @author zhangrongkang
 * @since 2024/2/14
 */
@Documented
@Target({ ElementType.METHOD })
@Retention(RetentionPolicy.RUNTIME)
public @interface AccessLimit {

    /**
     * 限流资源Key（唯一）
     * <p>针对不同的接口进行不同的流量控制</p>
     *
     * @return 限流Key
     */
    String key() default "";

    /**
     * 每秒最大访问次数
     *
     * @return int
     */
    int max();

    /**
     * 获取令牌最大等待时间
     */
    long timeout();

    /**
     * 获取令牌最大等待时间，默认:秒
     */
    TimeUnit timeunit() default TimeUnit.SECONDS;

}
