package com.zrkizzy.common.limit.constant;

/**
 * 限流全局自定义常量
 *
 * @author zhangrongkang
 * @since 2024/2/18
 */
public class LimitConst {

    /**
     * 限流配置属性
     */
    public static final String LIMIT_STRATEGY = "blog.limit.strategy";
}
