package com.zrkizzy.common.limit.exception;

import com.zrkizzy.common.core.exception.ErrorCode;
import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * 限流自定义错误码
 *
 * @author zhangrongkang
 * @since 2024/2/18
 */
@Getter
@AllArgsConstructor
public enum LimitErrorCode implements ErrorCode {

    REQUEST_COUNT_EXCEED_LIMIT("A0501", "请求次数超出限制"),

    INTERFACE_KEY_NOT_NULL("A0507", "限流接口KEY不能为空");

    /**
     * 状态码
     */
    private final String code;

    /**
     * 状态码描述
     */
    private final String message;

    @Override
    public String message() {
        return this.message;
    }

    /**
     * 错误码
     */
    @Override
    public String code() {
        return this.code;
    }
}
