-- Lua脚本：限流
-- KEYS[1] 限流的键
-- ARGV[1] 最大访问次数
-- ARGV[2] 超时时间（秒）

-- 获取限流的Key
local key = KEYS[1]
-- 最大限制次数，tonumber函数用于将一个值转为数值类型
local max = tonumber(string.match(ARGV[1], '"([^"]*)"'))
-- 获取时间戳并去除引号
local timeout = string.match(ARGV[2], '"([^"]*)"')

-- 检查键是否存在
if redis.call("exists", key) == 0 then
    -- 如果键不存在，则设置初始值为0
    redis.call('SET', key, 0)
    -- 设置过期时间
    redis.call('EXPIRE', key, timeout)
end

-- 获取当前访问次数
local currentLimit = tonumber(redis.call("get", key))

if currentLimit + 1 > max then
    -- 超过最大访问次数，拒绝访问
    return 0
else
    -- 自增长1，调用INCR命令是直接自增长1，使用 INCRBY 命令则是增长指定步长
    redis.call('INCR', key)
    -- 返回当前访问次数 + 1
    return currentLimit + 1
end