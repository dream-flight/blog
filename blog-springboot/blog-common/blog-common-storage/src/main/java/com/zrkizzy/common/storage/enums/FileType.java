package com.zrkizzy.common.storage.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * Excel类型枚举
 *
 * @author zhangrongkang
 * @since 2023/7/17
 */
@Getter
@AllArgsConstructor
public enum FileType {

    /**
     * Excel文件
     */
    XLSX(".xlsx"),

    /**
     * Word文件
     */
    WORD(".word"),

    /**
     * HTML文件
     */
    HTML(".html"),

    /**
     * markdown文件
     */
    MARKDOWN(".md");

    /**
     * 后缀
     */
    private final String suffix;

}
