package com.zrkizzy.common.storage.factory;

import com.zrkizzy.common.storage.enums.UploadStrategy;
import com.zrkizzy.common.storage.template.AbstractFileUploadStrategy;
import com.zrkizzy.common.storage.template.impl.LocalFileUploadStrategy;
import com.zrkizzy.common.storage.template.impl.OssFileUploadStrategy;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * 文件上传工厂
 *
 * @author zhangrongkang
 * @since 2023/12/13
 */
@Component
public class FileUploadFactory {

    @Autowired
    private OssFileUploadStrategy ossFileUploadStrategy;

    @Autowired
    private LocalFileUploadStrategy localFileUploadStrategy;

    /**
     * 获取文件上传策略实现
     *
     * @param uploadStrategy 上传策略
     * @return 文件上传策略
     */
    public AbstractFileUploadStrategy getInstance(UploadStrategy uploadStrategy) {
        return switch (uploadStrategy) {
            case OSS -> ossFileUploadStrategy;
            case LOCAL -> localFileUploadStrategy;
        };
    }

}
