package com.zrkizzy.common.storage.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * 上传策略枚举
 *
 * @author zhangrongkang
 * @since 2023/12/13
 */
@Getter
@AllArgsConstructor
public enum UploadStrategy {

    /**
     * OSS策略
     */
    OSS("OSS策略", "OSS"),

    /**
     * 本地上传
     */
    LOCAL("本地策略", "LOCAL");

    /**
     * 上传策略
     */
    private final String strategy;

    /**
     * 策略标识
     */
    private final String mark;

    /**
     * 根据策略标识获取上传策略
     *
     * @param mark 策略标识
     * @return 上传策略
     */
    public static UploadStrategy getUploadStrategyByMark(String mark) {
        for (UploadStrategy value : UploadStrategy.values()) {
            if (value.getMark().equals(mark)) {
                return value;
            }
        }
        return null;
    }

}
