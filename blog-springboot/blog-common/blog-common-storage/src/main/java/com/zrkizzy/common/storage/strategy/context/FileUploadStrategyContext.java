package com.zrkizzy.common.storage.strategy.context;

import com.zrkizzy.common.storage.dto.FileUrlDTO;
import com.zrkizzy.common.storage.enums.UploadStrategy;
import com.zrkizzy.common.storage.factory.FileUploadFactory;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;

/**
 * 文件上传策略上下文
 *
 * @author zhangrongkang
 * @since 2023/12/13
 */
@Component
@Slf4j
public class FileUploadStrategyContext {

    @Autowired
    private FileUploadFactory fileUploadFactory;

    /**
     * 上传文件并返回文件访问路径
     *
     * @param file 文件
     * @param classify 文件分类
     * @param uploadStrategy 文件上传策略
     * @return 文件路径数据传输对象
     */
    public FileUrlDTO executeUploadStrategy(MultipartFile file, String classify, UploadStrategy uploadStrategy) {
        return fileUploadFactory.getInstance(uploadStrategy).uploadFile(file, classify);
    }


    /**
     * 批量删除文件
     *
     * @param filePaths 文件路径集合
     * @param uploadStrategy 上传策略
     * @return true--删除成功; false--删除失败
     */
    public Boolean executeDeleteStrategy(List<String> filePaths, UploadStrategy uploadStrategy) {
        return fileUploadFactory.getInstance(uploadStrategy).deleteFile(filePaths);
    }

}
