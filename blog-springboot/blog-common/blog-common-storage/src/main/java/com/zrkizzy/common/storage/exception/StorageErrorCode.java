package com.zrkizzy.common.storage.exception;

import com.zrkizzy.common.core.exception.ErrorCode;
import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * 文件存储异常枚举
 *
 * @author zhangrongkang
 * @since 2024/2/8
 */
@Getter
@AllArgsConstructor
public enum StorageErrorCode implements ErrorCode {

    FILE_UPLOAD_EXCEPTION("A0700", "用户文件上传异常"),

    REPEAT_UPLOAD_FILE("A0706", "用户重复上传文件"),

    FILE_MD5_GET_ERROR("A0707", "文件MD5获取失败"),

    FILE_DELETE_ERROR("A0709", "文件删除失败");

    /**
     * 状态码
     */
    private final String code;

    /**
     * 状态码描述
     */
    private final String message;

    @Override
    public String message() {
        return this.message;
    }

    /**
     * 错误码
     */
    @Override
    public String code() {
        return this.code;
    }
}
