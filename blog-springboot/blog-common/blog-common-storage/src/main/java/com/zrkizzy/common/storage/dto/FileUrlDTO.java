package com.zrkizzy.common.storage.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

/**
 * 文件路径数据传输对象
 *
 * @author zhangrongkang
 * @since 2023/12/13
 */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class FileUrlDTO implements Serializable {

    /**
     * 文件存储路径
     */
    private String path;

    /**
     * 文件访问路径
     */
    private String accessUrl;

    /**
     * 文件名称
     */
    private String fileName;

    /**
     * 文件MD5
     */
    private String md5;

    /**
     * 文件类型
     */
    private String type;

}
