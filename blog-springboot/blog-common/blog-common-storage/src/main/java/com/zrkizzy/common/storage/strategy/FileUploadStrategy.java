package com.zrkizzy.common.storage.strategy;

import com.zrkizzy.common.storage.dto.FileUrlDTO;
import org.springframework.web.multipart.MultipartFile;

/**
 * 文件上传策略接口
 *
 * @author zhangrongkang
 * @since 2023/12/13
 */
public interface FileUploadStrategy {

    /**
     * 上传文件
     *
     * @param file 文件数据
     * @param classify 文件分类
     * @return 文件路径数据传输对象
     */
    FileUrlDTO uploadFile(MultipartFile file, String classify);

}
