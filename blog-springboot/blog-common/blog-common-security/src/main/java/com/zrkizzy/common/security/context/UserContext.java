package com.zrkizzy.common.security.context;


import com.alibaba.ttl.TransmittableThreadLocal;
import com.zrkizzy.common.models.domain.system.core.User;

/**
 * 用户变量上下文
 *
 * @author zhangrongkang
 * @since 2023/8/30
 */
public class UserContext {

    /**
     * 记录登录用户属性
     */
    private static final TransmittableThreadLocal<User> USER_THREAD_LOCAL = new TransmittableThreadLocal<>();

    /**
     * 设置用户属性
     *
     * @param user 用户对象
     */
    public static void setUser(User user) {
        USER_THREAD_LOCAL.set(user);
    }

    /**
     * 获取系统变量中存储的用户对象
     *
     * @return 用户对象
     */
    public static User getUser() {
        return USER_THREAD_LOCAL.get();
    }

    /**
     * 清空当前线程中所有变量
     */
    public static void remove() {
        // 清除用户变量
        USER_THREAD_LOCAL.remove();
    }

}
