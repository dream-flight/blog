package com.zrkizzy.common.security.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * Token校验结果枚举
 *
 * @author zhangrongkang
 * @since 2023/12/5
 */
@Getter
@AllArgsConstructor
public enum TokenValidateEnum {

    /**
     * 校验通过
     */
    ACCESS,

    /**
     * 过期
     */
    EXPIRE,

    /**
     * 非法
     */
    ILLEGAL;
}
