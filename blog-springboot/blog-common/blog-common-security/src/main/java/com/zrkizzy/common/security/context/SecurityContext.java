package com.zrkizzy.common.security.context;

import com.alibaba.ttl.TransmittableThreadLocal;

/**
 * 系统权限上下文
 * 
 * @author zhangrongkang
 * @since 2023/7/10
 */
public class SecurityContext {
    
    /**
     * 存储当前请求用户的唯一值
     */
    private static final TransmittableThreadLocal<String> TRACE_ID_THREAD_LOCAL = new TransmittableThreadLocal<>();

    /**
     * 设置当前用户唯一标识
     *
     * @param traceId 用户唯一标识
     */
    public static void setTraceId(String traceId) {
        TRACE_ID_THREAD_LOCAL.set(traceId);
    }

    /**
     * 获取当前用户唯一标识
     *
     * @return 用户唯一标识
     */
    public static String getTraceId() {
        return TRACE_ID_THREAD_LOCAL.get();
    }

    /**
     * 清空当前线程中所有变量
     */
    public static void remove() {
        // 清除用户唯一标识
        TRACE_ID_THREAD_LOCAL.remove();
    }
    
}
