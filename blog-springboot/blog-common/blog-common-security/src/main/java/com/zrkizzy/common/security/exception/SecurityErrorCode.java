package com.zrkizzy.common.security.exception;

import com.zrkizzy.common.core.exception.ErrorCode;
import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * 权限异常枚举
 *
 * @author zhangrongkang
 * @since 2024/2/5
 */
@Getter
@AllArgsConstructor
public enum SecurityErrorCode implements ErrorCode {


    USER_LOGIN_EXPIRED("A0230", "用户登录已过期"),

    UNAUTHORIZED_ACCESS("A0301", "访问未授权"),

    AUTHORIZED_EXPIRED("A0311", "授权已过期"),

    UNAUTHORIZED_USE_API("A0312", "无权限使用 API"),

    USER_SIGN_EXCEPTION("A0340", "用户签名异常"),

    USER_NOT_ALLOW_LOGIN_MANAGE_SYSTEM("A0342", "当前用户不允许登录管理系统");

    /**
     * 状态码
     */
    @Getter
    private final String code;

    /**
     * 状态码描述
     */
    private final String message;

    /**
     * 错误信息描述
     */
    @Override
    public String message() {
        return this.message;
    }

    /**
     * 错误码
     */
    @Override
    public String code() {
        return this.code;
    }
}
