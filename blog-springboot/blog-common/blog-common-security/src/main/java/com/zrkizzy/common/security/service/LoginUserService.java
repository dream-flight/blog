package com.zrkizzy.common.security.service;

/**
 * 登录用户权限业务逻辑接口
 *
 * @author zhangrongkang
 * @since 2024/2/22
 */
public interface LoginUserService {

    /**
     * 获取当前登录用户唯一标识
     *
     * @return 登录用户唯一标识
     */
    String getLoginUserTraceId();
}
