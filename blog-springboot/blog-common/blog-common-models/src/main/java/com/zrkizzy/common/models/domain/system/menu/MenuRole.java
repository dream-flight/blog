package com.zrkizzy.common.models.domain.system.menu;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;

import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * 菜单角色实体类
 *
 * @author zhangrongkang
 * @since 2023/4/17
 */
@Data
@Builder
@AllArgsConstructor
@TableName("tb_menu_role")
@Schema(name = "菜单角色关联对象")
public class MenuRole implements Serializable {

    /**
     * 主键
     */
    @TableId
    @Schema(description = "主键")
    private Long id;

    /**
     * 菜单ID
     */
    @Schema(description = "菜单ID")
    private Long menuId;

    /**
     * 角色ID
     */
    @Schema(description = "角色ID")
    private Long roleId;

    /**
     * 创建时间
     */
    @Schema(description = "创建时间")
    private LocalDateTime createTime;

}
