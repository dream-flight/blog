package com.zrkizzy.common.models.enums.system.common.login;

import com.fasterxml.jackson.annotation.JsonCreator;
import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * 登录目标枚举
 *
 * @author zhangrongkang
 * @since 2024/2/22
 */
@Getter
@AllArgsConstructor
public enum LoginTarget {

    /**
     * 前台
     */
    BLOG,

    /**
     * 后台
     */
    ADMIN;

    /**
     * 根据登录目标值获取对应登录目标枚举
     *
     * @param value 登录目标值
     * @return null--登录目标不正确；登录目标枚举
     */
    @JsonCreator
    public static LoginTarget fromValue(String value) {
        for (LoginTarget loginTarget : values()) {
            if (loginTarget.name().equals(value)) {
                return loginTarget;
            }
        }
        // 返回空的登录目标
        return null;
    }
}
