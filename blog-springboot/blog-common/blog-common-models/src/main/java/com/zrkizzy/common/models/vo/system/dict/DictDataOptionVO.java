package com.zrkizzy.common.models.vo.system.dict;

import lombok.Data;

/**
 * 字典数据选项
 *
 * @author zhangrongkang
 * @since 2024/1/30
 */
@Data
public class DictDataOptionVO {

    /**
     * 字典标签
     */
    private String dictLabel;

    /**
     * 字典键值
     */
    private String dictValue;

}
