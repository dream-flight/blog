package com.zrkizzy.common.models.enums.system.message;

import com.fasterxml.jackson.annotation.JsonCreator;
import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * 消息发送渠道枚举
 *
 * @author zhangrongkang
 * @since 2023/11/20
 */
@Getter
@AllArgsConstructor
public enum MessageSenderChannel {

    /**
     * 邮件
     */
    EMAIL("EMAIL"),

    /**
     * 短信
     */
    SMS("SMS");

    /**
     * 渠道名称
     */
    private final String channel;

    /**
     * 根据消息发送渠道值获取对应消息发送渠道枚举
     *
     * @param value 消息发送渠道值
     * @return null 消息发送渠道不正确；消息发送渠道枚举
     */
    @JsonCreator
    public static MessageSenderChannel fromValue(String value) {
        for (MessageSenderChannel messageSenderChannel : values()) {
            if (messageSenderChannel.channel.equals(value)) {
                return messageSenderChannel;
            }
        }
        // 返回空的消息发送渠道
        return null;
    }

}
