package com.zrkizzy.common.models.domain.system.config;

import com.baomidou.mybatisplus.annotation.TableName;
import com.zrkizzy.common.datasource.domain.BaseEntity;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serial;

/**
 * 系统配置实体类
 *
 * @author zhangrongkang
 * @since 2023/7/22
 */
@Data
@TableName("tb_system_config")
@Schema(name = "系统配置对象")
@EqualsAndHashCode(callSuper = false)
public class SystemConfig extends BaseEntity {

    @Serial
    private static final long serialVersionUID = 1L;

    /**
     * 用户默认头像
     */
    @Schema(description = "用户默认头像")
    private String avatar;

    /**
     * 系统通知
     */
    @Schema(description = "系统通知")
    private String notice;

    /**
     * 上传策略
     */
    @Schema(description = "上传策略")
    private String uploadStrategy;

}