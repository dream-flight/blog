package com.zrkizzy.common.models.vo.system.menu;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;

import java.io.Serializable;
import java.util.List;

/**
 * 菜单树型返回数据
 *
 * @author zhangrongkang
 * @since 2023/12/19
 */
@Data
public class MenuTreeVO implements Serializable {

    /**
     * 菜单ID
     */
    @JsonFormat(shape =JsonFormat.Shape.STRING)
    private Long id;

    /**
     * 菜单名称
     */
    private String label;

    /**
     * 子菜单
     */
    private List<MenuTreeVO> children;

}
