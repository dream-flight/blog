package com.zrkizzy.common.models.enums.system.log;

import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * 操作类型枚举
 *
 * @author zhangrongkang
 * @since 2023/12/14
 */
@Getter
@AllArgsConstructor
public enum OperateType {

    /**
     * 其他
     */
    OTHER,

    /**
     * 新增
     */
    ADD,

    /**
     * 更新
     */
    UPDATE,

    /**
     * 删除
     */
    DELETE,

    /**
     * 查询
     */
    QUERY;

}
