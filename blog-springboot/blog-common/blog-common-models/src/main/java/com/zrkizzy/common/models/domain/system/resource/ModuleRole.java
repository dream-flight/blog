package com.zrkizzy.common.models.domain.system.resource;

import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * 模块资源实体类
 *
 * @author zhangrongkang
 * @since 2023/3/16
 */
@Data
@Builder
@AllArgsConstructor
@Schema(name = "模块资源对象")
@TableName("tb_module_role")
@EqualsAndHashCode(callSuper = false)
public class ModuleRole implements Serializable {

    /**
     * 主键
     */
    @Schema(description = "主键")
    private Long id;

    /**
     * 模块ID
     */
    @Schema(description = "模块ID")
    private Long moduleId;

    /**
     * 角色ID
     */
    @Schema(description = "角色ID")
    private Long roleId;

    /**
     * 创建时间
     */
    @Schema(description = "创建时间")
    private LocalDateTime createTime;

}
