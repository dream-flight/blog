package com.zrkizzy.common.models.domain.system.scheduler;

import com.baomidou.mybatisplus.annotation.TableName;
import com.zrkizzy.common.datasource.domain.BaseEntity;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serial;

/**
 * 定时任务调度日志实体类
 *
 * @author zhangrongkang
 * @since 2024/3/6
 */
@Data
@TableName("tb_scheduler_job_log")
@Schema(name = "定时任务调度日志对象")
@EqualsAndHashCode(callSuper = false)
public class SchedulerJobLog extends BaseEntity {

    @Serial
    private static final long serialVersionUID = 1L;

    /**
     * 任务编号
     */
    @Schema(description = "任务编号")
    private Long jobId;

    /**
     * 任务名称
     */
    @Schema(description = "任务名称")
    private String jobName;

    /**
     * 任务分组
     */
    @Schema(description = "任务分组")
    private String jobGroup;

    /**
     * 调用字符
     */
    @Schema(description = "调用字符")
    private String invokeTarget;

    /**
     * 任务信息
     */
    @Schema(description = "任务信息")
    private String jobMessage;

    /**
     * 执行状态： 1 成功， 0 失败
     */
    @Schema(description = "执行状态")
    private Boolean status;

}