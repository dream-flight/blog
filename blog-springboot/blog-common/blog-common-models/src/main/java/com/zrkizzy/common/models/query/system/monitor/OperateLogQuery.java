package com.zrkizzy.common.models.query.system.monitor;

import com.zrkizzy.common.core.domain.request.BasePage;
import com.zrkizzy.common.models.enums.system.log.OperateType;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

import java.util.List;

/**
 * 操作日志信息查询对象
 *
 * @author zhangrongkang
 * @since 2023/7/3
 */
@Data
@ToString(callSuper = true)
@EqualsAndHashCode(callSuper = false)
public class OperateLogQuery extends BasePage {

    /**
     * 操作类型
     * <p>
     *     OTHER--其他操作<br/>
     *     ADD--新增<br/>
     *     UPDATE--修改<br/>
     *     DELETE--删除<br/>
     *     QUERY--查询<br/>
     * </p>
     * @see OperateType
     */
    private String type;

    /**
     * 请求方式
     */
    private String requestMethod;

    /**
     * 操作用户ID
     */
    private Long userId;

    /**
     * 操作状态 0 失败 1 成功 
     */
    private Boolean status;

    /**
     * 时间范围
     */
    private List<String> dataRange;

}