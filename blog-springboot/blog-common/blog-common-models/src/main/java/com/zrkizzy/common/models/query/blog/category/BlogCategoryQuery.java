package com.zrkizzy.common.models.query.blog.category;

import com.zrkizzy.common.core.domain.request.BasePage;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

import java.util.List;

/**
 * 博客分类信息查询对象
 *
 * @author zhangrongkang
 * @since 2023/9/4
 */
@Data
@ToString(callSuper = true)
@EqualsAndHashCode(callSuper = false)
public class BlogCategoryQuery extends BasePage {

    /**
     * 分类名称
     */
    private String name;

    /**
     * 可见类型：0 自己可见 1 全部可见
     */
    private Byte visibility;

    /**
     * 时间范围
     */
    private List<String> dataRange;

}