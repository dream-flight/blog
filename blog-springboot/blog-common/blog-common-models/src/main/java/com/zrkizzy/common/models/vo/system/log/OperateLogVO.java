package com.zrkizzy.common.models.vo.system.log;

import com.zrkizzy.common.core.domain.response.BaseVO;
import com.zrkizzy.common.models.enums.system.log.OperateType;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;


/**
 * 操作日志数据返回对象
 *
 * @author zhangrongkang
 * @since 2023/7/3
 */
@Data
@ToString(callSuper = true)
@EqualsAndHashCode(callSuper = false)
public class OperateLogVO extends BaseVO {

    /**
     * 唯一追踪值
     */
    private String traceId;

    /**
     * 操作内容
     */
    private String operateContent;

    /**
     * 操作类型
     * <p>
     *     OTHER--其他操作<br/>
     *     ADD--新增<br/>
     *     UPDATE--修改<br/>
     *     DELETE--删除<br/>
     *     QUERY--查询<br/>
     * </p>
     * @see OperateType
     */
    private String type;

    /**
     * 操作方法名称
     */
    private String methodName;

    /**
     * 请求方式
     */
    private String requestMethod;

    /**
     * 操作IP
     */
    private String operateIp;

    /**
     * 操作地址
     */
    private String operateLocation;

    /**
     * 操作参数
     */
    private String operateParam;

    /**
     * 操作结果描述
     */
    private String operateResult;

    /**
     * 操作状态 0 失败 1 成功 
     */
    private Boolean status;

    /**
     * 操作消耗时间
     */
    private Long costTime;

    /**
     * 用户昵称
     */
    private String nickname;

}
