package com.zrkizzy.common.models.vo.system.dict;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.zrkizzy.common.core.domain.response.BaseVO;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;


/**
 * 字典数据数据返回对象
 *
 * @author zhangrongkang
 * @since 2024/1/18
 */
@Data
@ToString(callSuper = true)
@EqualsAndHashCode(callSuper = false)
public class DictDataVO extends BaseVO {

    /**
     * 字典标签
     */
    private String dictLabel;

    /**
     * 字典类型ID
     */
    @JsonFormat(shape = JsonFormat.Shape.STRING)
    private Long dictTypeId;

    /**
     * 字典键值
     */
    private String dictValue;

    /**
     * 表格回显样式
     */
    private String listClass;

    /**
     * 样式属性（其他样式扩展）
     */
    private String cssClass;

    /**
     * 备注
     */
    private String remark;

    /**
     * 字典排序
     */
    private Integer sort;

    /**
     * 状态（1 正常 0 停用）
     */
    private Boolean status;

}
