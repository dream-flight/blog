package com.zrkizzy.common.models.domain.system.resource;

import com.baomidou.mybatisplus.annotation.TableName;
import com.zrkizzy.common.datasource.domain.BaseEntity;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * 模块实体类
 *
 * @author zhangrongkang
 * @since 2023/3/15
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Schema(name = "模块对象")
@TableName("tb_module")
public class Module extends BaseEntity {

    /**
     * 模块名称
     */
    @Schema(description = "模块名称")
    private String name;

    /**
     * 模块描述
     */
    @Schema(description = "模块描述")
    private String description;

}
