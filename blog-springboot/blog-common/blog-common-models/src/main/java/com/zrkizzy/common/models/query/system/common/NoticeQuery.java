package com.zrkizzy.common.models.query.system.common;

import com.zrkizzy.common.core.domain.request.BasePage;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

import java.util.List;

/**
 * 通知公告信息查询对象
 *
 * @author zhangrongkang
 * @since 2024/3/29
 */
@Data
@ToString(callSuper = true)
@EqualsAndHashCode(callSuper = false)
public class NoticeQuery extends BasePage {

    /**
     * 公告标题
     */
    private String title;

    /**
     * 公告类型（1通知 2公告）
     */
    private Byte type;

    /**
     * 时间范围
     */
    private List<String> dataRange;

}