package com.zrkizzy.common.models.dto.blog.tag;

import com.zrkizzy.common.core.domain.BaseDTO;
import jakarta.validation.constraints.NotBlank;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;


/**
 * 博客标签数据传输对象
 *
 * @author zhangrongkang
 * @since 2023/9/5
 */
@Data
@ToString(callSuper = true)
@EqualsAndHashCode(callSuper = false)
public class BlogTagsDTO extends BaseDTO {


    /**
     * 标签名称
     */
    @NotBlank(message = "标签名称不能为空")
    private String name;

    /**
     * 标签排序
     */
    private Integer sort;

    /**
     * 文章数量
     */
    private Integer count;

}
