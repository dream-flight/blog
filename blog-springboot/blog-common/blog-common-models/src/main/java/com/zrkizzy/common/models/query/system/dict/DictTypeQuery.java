package com.zrkizzy.common.models.query.system.dict;

import com.zrkizzy.common.core.domain.request.BasePage;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

import java.util.List;

/**
 * 字典类型信息查询对象
 *
 * @author zhangrongkang
 * @since 2024/1/16
 */
@Data
@ToString(callSuper = true)
@EqualsAndHashCode(callSuper = false)
public class DictTypeQuery extends BasePage {

    /**
     * 字典名称
     */
    private String dictName;

    /**
     * 字典类型
     */
    private String dictType;

    /**
     * 状态（1 正常 0 停用）
     */
    private Boolean status;

    /**
     * 时间范围
     */
    private List<String> dataRange;

}