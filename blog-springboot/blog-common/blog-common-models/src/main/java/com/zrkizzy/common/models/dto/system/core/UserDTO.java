package com.zrkizzy.common.models.dto.system.core;

import com.zrkizzy.common.core.domain.BaseDTO;
import jakarta.validation.constraints.Email;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;


/**
 * 用户数据传输对象
 *
 * @author zhangrongkang
 * @since 2023/7/15
 */
@Data
@ToString(callSuper = true)
@EqualsAndHashCode(callSuper = false)
public class UserDTO extends BaseDTO {


    /**
     * 用户名
     */
    @NotBlank(message = "用户名不能为空")
    @Email(message = "请输入正确的Email格式")
    private String username;

    /**
     * 密码
     */
    private String password;

    /**
     * 昵称
     */
    @NotBlank(message = "昵称不能为空")
    private String nickname;

    /**
     * 头像
     */
    private String avatar;

    /**
     * 状态，0：禁用，1：启用
     */
    @NotNull(message = "用户状态不能为空")
    private Boolean status;

    /**
     * 备注
     */
    private String remark;

}
