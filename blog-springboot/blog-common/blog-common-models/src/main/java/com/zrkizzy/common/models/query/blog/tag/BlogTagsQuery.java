package com.zrkizzy.common.models.query.blog.tag;

import com.zrkizzy.common.core.domain.request.BasePage;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

import java.util.List;

/**
 * 博客标签信息查询对象
 *
 * @author zhangrongkang
 * @since 2023/9/5
 */
@Data
@ToString(callSuper = true)
@EqualsAndHashCode(callSuper = false)
public class BlogTagsQuery extends BasePage {

    /**
     * 标签名称
     */
    private String name;

    /**
     * 时间范围
     */
    private List<String> dataRange;

}