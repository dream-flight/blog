package com.zrkizzy.common.models.domain.system.scheduler;

import com.baomidou.mybatisplus.annotation.TableName;
import com.zrkizzy.common.datasource.domain.BaseEntity;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serial;

/**
 * 定时任务实体类
 *
 * @author zhangrongkang
 * @since 2024/2/23
 */
@Data
@TableName("tb_scheduler_job")
@Schema(name = "定时任务对象")
@EqualsAndHashCode(callSuper = false)
public class SchedulerJob extends BaseEntity {

    @Serial
    private static final long serialVersionUID = 1L;

    /**
     * 任务名称
     */
    @Schema(description = "任务名称")
    private String jobName;

    /**
     * 任务分组
     */
    @Schema(description = "任务分组")
    private String jobGroup;

    /**
     * 任务状态： 1 正常，0 暂停
     */
    @Schema(description = "任务状态： 1 正常，0 暂停")
    private Boolean status;

    /**
     * 是否并发执行： 1 允许， 0 禁止
     */
    @Schema(description = "是否并发执行： 1 允许， 0 禁止")
    private Boolean concurrent;

    /**
     * 调用目标字符串
     */
    @Schema(description = "调用目标字符串")
    private String invokeTarget;

    /**
     * cron执行表达式
     */
    @Schema(description = "cron执行表达式")
    private String cron;

    /**
     * 执行策略： 1 立即执行，2执行一次， 3 放弃执行
     */
    @Schema(description = "执行策略： 1 立即执行，2执行一次， 3 放弃执行")
    private Byte executeStrategy;

    /**
     * 备注信息
     */
    @Schema(description = "备注信息")
    private String remark;

    /**
     * 创建用户
     */
    @Schema(description = "创建用户")
    private String createBy;

    /**
     * 更新用户
     */
    @Schema(description = "更新用户")
    private String updateBy;

}