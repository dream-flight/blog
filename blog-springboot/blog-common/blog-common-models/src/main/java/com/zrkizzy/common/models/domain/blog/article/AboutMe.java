package com.zrkizzy.common.models.domain.blog.article;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * 关于我实体类
 *
 * @author zhangrongkang
 * @since 2023/9/8
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@Accessors(chain = true)
@Schema(name = "关于我实体类")
@Document(collection = "contents")
public class AboutMe implements Serializable {

    /**
     * 主键
     */
    @Id
    private String id;

    /**
     * 标题
     */
    private String title;

    /**
     * 作者头像
     */
    private String avatar;

    /**
     * 内容
     */
    private String content;

    /**
     * 评论ID
     */
    private String commentId;

    /**
     * 更新时间
     */
    private LocalDateTime updateTime;

}
