package com.zrkizzy.common.models.dto.system.config;

import lombok.Data;

import java.io.Serializable;

/**
 * 博客配置数据传输对象
 *
 * @author zhangrongkang
 * @since 2023/8/24
 */
@Data
public class BlogConfigDTO implements Serializable {

    /**
     * ID
     */
    private String id;

    /**
     * 是否展示Banner
     */
    private Boolean showBanner;

    /**
     * Banner背景图片
     */
    private String bannerImage;

    /**
     * 页脚背景图片
     */
    private String footerImage;

    /**
     * 亮色主题背景图片
     */
    private String lightImage;

    /**
     * 暗色主题背景图片
     */
    private String darkImage;

    /**
     * <p>评论是否需要审核</p>
     * true：需要，false：不需要
     */
    private Boolean commentApproval;

    /**
     * <p>留言是否需要审核</p>
     * true：需要，false：不需要
     */
    private Boolean messageApproval;

}
