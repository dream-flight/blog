package com.zrkizzy.common.models.query.system.resource;

import com.zrkizzy.common.core.domain.request.BasePage;
import jakarta.validation.constraints.NotNull;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

/**
 * 模块资源关联查询对象
 *
 * @author zhangrongkang
 * @since 2023/7/28
 */
@Data
@ToString(callSuper = true)
@EqualsAndHashCode(callSuper = false)
public class ModuleResourceQuery extends BasePage {

    /**
     * 模块ID
     */
    @NotNull(message = "模块ID不能为空")
    private Long moduleId;

    /**
     * 请求名称
     */
    private String name;

    /**
     * 请求方式
     */
    private String method;

    /**
     * 请求路径
     */
    private String url;

}
