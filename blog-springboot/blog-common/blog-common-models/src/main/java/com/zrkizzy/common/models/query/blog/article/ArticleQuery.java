package com.zrkizzy.common.models.query.blog.article;

import com.zrkizzy.common.core.domain.request.BasePage;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

import java.util.List;

/**
 * 文章信息查询对象
 *
 * @author zhangrongkang
 * @since 2023/9/18
 */
@Data
@ToString(callSuper = true)
@EqualsAndHashCode(callSuper = false)
public class ArticleQuery extends BasePage {

    /**
     * 文章名称
     */
    private String title;

    /**
     * 文章类型
     */
    private Byte type;

    /**
     * 文章状态：0 草稿箱 1 已发布 2 待发布
     */
    private Byte status;

    /**
     * 可见范围
     */
    private Byte visibility;

    /**
     * 时间范围
     */
    private List<String> dataRange;

}