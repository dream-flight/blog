package com.zrkizzy.common.models.domain.system.log;

import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serial;
import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * 用户登录日志实体类
 *
 * @author zhangrongkang
 * @since 2023/6/20
 */
@Data
@TableName("tb_login_log")
@Schema(name = "用户登录日志对象")
@EqualsAndHashCode(callSuper = false)
public class LoginLog implements Serializable {

    @Serial
    private static final long serialVersionUID = 1L;

    /**
     * 主键
     */
    @Schema(description = "主键")
    private Long id;

    /**
     * 登录用户名称
     */
    @Schema(description = "登录用户名称")
    private String username;

    /**
     * 登录IP
     */
    @Schema(description = "登录IP")
    private String loginIp;

    /**
     * 登录位置
     */
    @Schema(description = "登录位置")
    private String loginLocation;

    /**
     * 浏览器版本
     */
    @Schema(description = "浏览器版本")
    private String browser;

    /**
     * 操作系统
     */
    @Schema(description = "操作系统")
    private String os;

    /**
     * 登录状态：0 失败; 1 成功
     */
    @Schema(description = "登录状态：0 失败; 1 成功")
    private Boolean status;

    /**
     * 登录消息提示
     */
    @Schema(description = "登录消息提示")
    private String message;

    /**
     * 登录时间
     */
    @Schema(description = "登录时间")
    private LocalDateTime loginTime;

}