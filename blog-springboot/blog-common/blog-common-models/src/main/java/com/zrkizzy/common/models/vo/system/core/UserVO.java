package com.zrkizzy.common.models.vo.system.core;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.zrkizzy.common.core.domain.response.BaseVO;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

import java.time.LocalDateTime;

/**
 * 用户数据返回对象
 *
 * @author zhangrongkang
 * @since 2023/3/7
 */
@Data
@ToString(callSuper = true)
@EqualsAndHashCode(callSuper = false)
public class UserVO extends BaseVO {

    /**
     * 用户名
     */
    private String username;

    /**
     * 昵称
     */
    private String nickname;

    /**
     * 头像
     */
    private String avatar;

    /**
     * 状态，0：禁用，1：启用
     */
    private Boolean status;

    /**
     * 角色ID
     */
    @JsonFormat(shape =JsonFormat.Shape.STRING)
    private Long roleId;

    /**
     * 用户角色
     */
    private String roles;


    /**
     * 登录IP
     */
    private String ipAddress;

    /**
     * IP属地
     */
    private String ipLocation;

    /**
     * 登录时间
     */
    private LocalDateTime loginTime;

    /**
     * 备注
     */
    private String remark;

}
