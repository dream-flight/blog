package com.zrkizzy.common.models.query.system.scheduler;

import com.zrkizzy.common.core.domain.request.BasePage;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

import java.util.List;

/**
 * 定时任务信息查询对象
 *
 * @author zhangrongkang
 * @since 2024/2/23
 */
@Data
@ToString(callSuper = true)
@EqualsAndHashCode(callSuper = false)
public class SchedulerJobQuery extends BasePage {

    /**
     * 任务编号
     */
    private Long id;

    /**
     * 任务名称
     */
    private String jobName;

    /**
     * 任务分组
     */
    private String jobGroup;

    /**
     * 任务状态： 1 正常，0 暂停
     */
    private Boolean status;

    /**
     * 调用目标字符串
     */
    private String invokeTarget;

    /**
     * 时间范围
     */
    private List<String> dataRange;

}