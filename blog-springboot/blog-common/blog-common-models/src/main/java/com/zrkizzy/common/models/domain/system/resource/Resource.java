package com.zrkizzy.common.models.domain.system.resource;

import com.baomidou.mybatisplus.annotation.TableName;
import com.zrkizzy.common.datasource.domain.BaseEntity;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serial;

/**
 * 资源实体类
 *
 * @author zhangrongkang
 * @since 2023/7/27
 */
@Data
@TableName("tb_resource")
@Schema(name = "资源对象")
@EqualsAndHashCode(callSuper = false)
public class Resource extends BaseEntity {

    @Serial
    private static final long serialVersionUID = 1L;

    /**
     * 资源名称
     */
    @Schema(description = "资源名称")
    private String name;

    /**
     * 资源描述
     */
    @Schema(description = "资源描述")
    private String description;

    /**
     * 资源请求方式
     */
    @Schema(description = "资源请求方式")
    private String method;

    /**
     * 资源请求路径
     */
    @Schema(description = "资源请求路径")
    private String url;

}