package com.zrkizzy.common.models.dto.system.dict;

import com.zrkizzy.common.core.domain.BaseDTO;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;


/**
 * 字典数据数据传输对象
 *
 * @author zhangrongkang
 * @since 2024/1/18
 */
@Data
@ToString(callSuper = true)
@EqualsAndHashCode(callSuper = false)
public class DictDataDTO extends BaseDTO {

    /**
     * 字典标签
     */
    @NotBlank(message = "数据标签不能为空")
    private String dictLabel;

    /**
     * 字典类型ID
     */
    private Long dictTypeId;

    /**
     * 字典键值
     */
    @NotBlank(message = "数据键值不能为空")
    private String dictValue;

    /**
     * 表格回显样式
     */
    private String listClass;

    /**
     * 样式属性（其他样式扩展）
     */
    private String cssClass;

    /**
     * 备注
     */
    private String remark;

    /**
     * 字典排序
     */
    private Integer sort;

    /**
     * 状态（1 正常 0 停用）
     */
    @NotNull(message = "字典状态不能为空")
    private Boolean status;

}
