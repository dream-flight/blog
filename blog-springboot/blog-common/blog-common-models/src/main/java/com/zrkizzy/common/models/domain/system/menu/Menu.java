package com.zrkizzy.common.models.domain.system.menu;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import com.zrkizzy.common.datasource.domain.BaseEntity;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

import java.util.List;

/**
 * 菜单实体类
 *
 * @author zhangrongkang
 * @since 2023/4/17
 */
@Data
@ToString(callSuper = true)
@EqualsAndHashCode(callSuper = false)
@Schema(name = "菜单对象")
@TableName("tb_menu")
public class Menu extends BaseEntity {

    /**
     * 菜单名称
     */
    @Schema(description = "菜单名称")
    private String name;

    /**
     * 菜单类型
     */
    @Schema(description = "菜单类型")
    private Character type;

    /**
     * 父菜单ID
     */
    @Schema(description = "父菜单ID")
    private Long parentId;

    /**
     * 访问路径
     */
    @Schema(description = "访问路径")
    private String path;

    /**
     * 激活菜单
     */
    @Schema(description = "激活菜单")
    private String activeMenu;

    /**
     * 组件路径
     */
    @Schema(description = "组件路径")
    private String component;

    /**
     * 是否缓存：0: 不缓存，1: 缓存
     */
    @Schema(description = "是否缓存")
    private Boolean isCache;

    /**
     * 是否外链：0: 不是外链， 1: 外链
     */
    @Schema(description = "是否外链")
    private Boolean isLink;

    /**
     * 是否隐藏：0: 不隐藏， 1: 隐藏
     */
    @Schema(description = "是否隐藏")
    private Boolean visible;

    /**
     * 状态，0：禁用，1：正常
     */
    @Schema(description = "菜单状态")
    private Boolean status;

    /**
     * 图标
     */
    @Schema(description = "图标")
    private String icon;

    /**
     * 菜单顺序
     */
    @Schema(description = "菜单顺序")
    private Integer sort;

    /**
     * 子菜单
     */
    @TableField(exist = false)
    @Schema(description = "子菜单")
    private List<Menu> children;

}
