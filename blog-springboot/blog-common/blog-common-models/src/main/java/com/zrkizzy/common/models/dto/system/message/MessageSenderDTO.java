package com.zrkizzy.common.models.dto.system.message;

import com.zrkizzy.common.models.enums.system.message.MessageSenderChannel;
import com.zrkizzy.common.models.enums.system.message.MessageSenderType;
import jakarta.validation.constraints.NotNull;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

/**
 * 消息发送数据传输对象
 *
 * @author zhangrongkang
 * @since 2023/5/5
 */
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class MessageSenderDTO implements Serializable {

    /**
     * 接收人
     */
    private String senderTo;

    /**
     * 接收人昵称
     */
    private String nickname;

    /**
     * 消息主题
     */
    private String subject;

    /**
     * 消息发送类型
     * @see MessageSenderType
     */
    @NotNull(message = "请选择正确的发送类型")
    private MessageSenderType type;

    /**
     * 消息发送渠道
     * <p>
     *     EMAIL: 邮件 SMS：短信
     * </p>
     * @see MessageSenderChannel
     *
     */
    @NotNull(message = "请选择正确的发送渠道")
    private MessageSenderChannel channel;

}
