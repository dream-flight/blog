package com.zrkizzy.common.models.vo.system.menu;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.util.List;

/**
 * 菜单角色关联返回对象
 *
 * @author zhangrongkang
 * @since 2023/12/18
 */
@Data
@AllArgsConstructor
public class MenuRoleVO {

    /**
     * 当前角色选择ID
     */
    private List<String> checkIds;

    /**
     * 菜单树型数据
     */
    private List<MenuTreeVO> menuTree;

}
