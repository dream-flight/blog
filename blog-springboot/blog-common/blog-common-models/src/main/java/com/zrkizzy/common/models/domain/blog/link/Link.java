package com.zrkizzy.common.models.domain.blog.link;

import com.baomidou.mybatisplus.annotation.TableName;
import com.zrkizzy.common.datasource.domain.BaseEntity;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serial;

/**
 * 友情链接实体类
 *
 * @author zhangrongkang
 * @since 2023/6/19
 */
@Data
@TableName("tb_link")
@Schema(name = "友情链接对象")
@EqualsAndHashCode(callSuper = false)
public class Link extends BaseEntity {

    @Serial
    private static final long serialVersionUID = 1L;

    /**
     * 网站名称
     */
    @Schema(description = "网站名称")
    private String name;

    /**
     * 网站域名
     */
    @Schema(description = "网站域名")
    private String website;

    /**
     * 网站Logo
     */
    @Schema(description = "网站Logo")
    private String logo;

    /**
     * 网站介绍
     */
    @Schema(description = "网站介绍")
    private String introduce;

}