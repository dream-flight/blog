package com.zrkizzy.common.models.domain.blog.category;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

/**
 * 文章分类关联对象
 *
 * @author zhangrongkang
 * @since 2023/10/6
 */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Schema(name = "文章分类关联对象")
@TableName("tb_article_category")
public class ArticleCategory implements Serializable {

    /**
     * 主键ID
     */
    @TableId
    @Schema(description = "主键")
    private Long id;

    /**
     * 文章ID
     */
    @Schema(description = "文章ID")
    private Long articleId;

    /**
     * 分类ID
     */
    @Schema(description = "分类ID")
    private Long categoryId;

}
