package com.zrkizzy.common.models.domain.blog.tag;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;

import java.io.Serializable;

/**
 * 文章标签关联对象
 *
 * @author zhangrongkang
 * @since 2023/10/6
 */
@Data
@Builder
@AllArgsConstructor
@TableName("tb_article_tags")
@Schema(name = "文章标签关联对象")
public class ArticleTags implements Serializable {

    /**
     * 主键
     */
    @TableId
    @Schema(description = "主键")
    private Long id;

    /**
     * 文章ID
     */
    @Schema(description = "文章ID")
    private Long articleId;

    /**
     * 标签ID
     */
    @Schema(description = "标签ID")
    private Long tagId;

}
