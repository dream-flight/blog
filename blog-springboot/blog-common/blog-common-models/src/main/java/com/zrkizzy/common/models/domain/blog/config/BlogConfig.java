package com.zrkizzy.common.models.domain.blog.config;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.io.Serializable;

/**
 * 博客配置实体类
 *
 * @author zhangrongkang
 * @since 2023/8/24
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@Schema(name = "博客配置对象")
@Document(collection = "configurations")
public class BlogConfig implements Serializable {

    /**
     * ID
     */
    @Id
    private String id;

    /**
     * 是否展示Banner
     */
    private Boolean showBanner;

    /**
     * Banner背景图片
     */
    private String bannerImage;

    /**
     * 页脚背景图片
     */
    private String footerImage;

    /**
     * 亮色主题背景图片
     */
    private String lightImage;

    /**
     * 暗色主题背景图片
     */
    private String darkImage;

    /**
     * <p>评论是否需要审核</p>
     * true：需要，false：不需要
     */
    private Boolean commentApproval;

    /**
     * <p>留言是否需要审核</p>
     * true：需要，false：不需要
     */
    private Boolean messageApproval;

}
