package com.zrkizzy.common.models.dto.blog.article;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.zrkizzy.common.core.domain.BaseDTO;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotEmpty;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Size;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

import java.util.List;

/**
 * 文章数据传输对象
 *
 * @author zhangrongkang
 * @since 2023/9/18
 */
@Data
@ToString(callSuper = true)
@EqualsAndHashCode(callSuper = false)
public class ArticleDTO extends BaseDTO {

    /**
     * 文章名称
     */
    @NotBlank(message = "文章标题不能为空")
    private String title;

    /**
     * 文章封面
     */
    private String cover;

    /**
     * 文章类型：0 原创 1 转载 2 翻译
     */
    @NotNull(message = "请选择文章类型")
    private Byte type;

    /**
     * 文章摘要
     */
    @NotBlank(message = "文章摘要不能为空")
    @Size(max = 255)
    private String summary;

    /**
     * 文章状态：0 草稿箱 1 已发布 2 待发布
     */
    private Byte status;

    /**
     * 文章内容
     */
    private String content;

    /**
     * 原文链接
     */
    private String originalLink;

    /**
     * 是否允许评论：1 允许 0 不允许
     */
    private Boolean allowComment;

    /**
     * 是否置顶：1 置顶 0 不置顶
     */
    private Boolean isSticky;

    /**
     * 是否开启打赏：1 开启 0 不开启
     */
    private Boolean allowDonation;

    /**
     * 可见范围：0 自己可见 1 全部可见
     */
    private Byte visibility;

    /**
     * 评论数量
     */
    @JsonIgnore
    private Integer commentCount;

    /**
     * 浏览数量
     */
    @JsonIgnore
    private Integer visitCount;

    /**
     * 发布IP
     */
    @JsonIgnore
    private String createAddress;

    /**
     * 发布地址
     */
    @JsonIgnore
    private String createLocation;

    /**
     * 博客分类
     */
    @NotNull(message = "请选择博客分类")
    private Long categoryId;

    /**
     * 博客标签
     */
    @NotEmpty(message = "请选择博客标签")
    private List<Long> tags;

    /**
     * 定时发布文章时间
     */
    private String publishTime;

}
