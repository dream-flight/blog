package com.zrkizzy.common.models.query.blog.link;

import com.zrkizzy.common.core.domain.request.BasePage;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

import java.util.List;

/**
 * 友情链接信息查询对象
 *
 * @author zhangrongkang
 * @since 2023/6/19
 */
@Data
@ToString(callSuper = true)
@EqualsAndHashCode(callSuper = false)
public class LinkQuery extends BasePage {

    /**
     * 网站名称
     */
    private String name;

    /**
     * 时间范围
     */
    private List<String> dataRange;

}