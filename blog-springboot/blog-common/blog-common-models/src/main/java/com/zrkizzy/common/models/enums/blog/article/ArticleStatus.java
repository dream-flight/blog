package com.zrkizzy.common.models.enums.blog.article;

import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * 文章状态字段枚举
 *
 * @author zhangrongkang
 * @since 2023/9/22
 */
@Getter
@AllArgsConstructor
public enum ArticleStatus {

    /**
     * 是否允许评论
     */
    COMMENT("COMMENT", "allow_comment"),

    /**
     * 是否置顶
     */
    STICKY("STICKY", "is_sticky"),

    /**
     * 是否开启打赏
     */
    DONATION("DONATION", "allow_donation");


    /**
     * 状态名
     */
    private final String status;

    /**
     * 列名
     */
    private final String field;

    /**
     * 根据状态名称获取列名
     *
     * @param status 状态名称
     * @return 对应列名
     */
    public static String getFieldByStatus(String status) {
        for (ArticleStatus articleStatus : ArticleStatus.values()) {
            if (status.equals(articleStatus.getStatus())) {
                return articleStatus.getField();
            }
        }
        return null;
    }
}
