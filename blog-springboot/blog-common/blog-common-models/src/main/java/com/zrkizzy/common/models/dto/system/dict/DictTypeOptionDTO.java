package com.zrkizzy.common.models.dto.system.dict;

import lombok.Data;

import java.io.Serializable;

/**
 * 字典类型选项数据传输对象
 *
 * @author zhangrongkang
 * @since 2024/2/1
 */
@Data
public class DictTypeOptionDTO implements Serializable {

    /**
     * 字典类型ID
     */
    private Long dictTypeId;

    /**
     * 字典类型
     */
    private String dictType;

}
