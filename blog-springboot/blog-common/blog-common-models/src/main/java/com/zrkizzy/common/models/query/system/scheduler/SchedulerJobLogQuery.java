package com.zrkizzy.common.models.query.system.scheduler;

import com.zrkizzy.common.core.domain.request.BasePage;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

import java.util.List;

/**
 * 定时任务调度日志信息查询对象
 *
 * @author zhangrongkang
 * @since 2024/3/6
 */
@Data
@ToString(callSuper = true)
@EqualsAndHashCode(callSuper = false)
public class SchedulerJobLogQuery extends BasePage {

    /**
     * 任务编号
     */
    private Long jobId;

    /**
     * 任务名称
     */
    private String jobName;

    /**
     * 任务分组
     */
    private String jobGroup;

    /**
     * 执行状态： 1 成功， 0 失败
     */
    private Boolean status;

    /**
     * 时间范围
     */
    private List<String> dataRange;

}