package com.zrkizzy.common.models.vo.system.scheduler;

import com.zrkizzy.common.core.domain.response.BaseVO;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;


/**
 * 定时任务调度日志数据返回对象
 *
 * @author zhangrongkang
 * @since 2024/3/6
 */
@Data
@ToString(callSuper = true)
@EqualsAndHashCode(callSuper = false)
public class SchedulerJobLogVO extends BaseVO {

    /**
     * 任务编号
     */
    private Long jobId;

    /**
     * 任务名称
     */
    private String jobName;

    /**
     * 任务分组
     */
    private String jobGroup;

    /**
     * 调用字符
     */
    private String invokeTarget;

    /**
     * 任务信息
     */
    private String jobMessage;

    /**
     * 执行状态： 1 成功， 0 失败
     */
    private Boolean status;

}
