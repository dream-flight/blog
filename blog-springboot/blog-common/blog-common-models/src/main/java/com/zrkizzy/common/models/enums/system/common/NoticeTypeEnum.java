package com.zrkizzy.common.models.enums.system.common;

import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * 公告类型
 *
 * @author zhangrongkang
 * @since 2024/3/29
 */
@Getter
@AllArgsConstructor
public enum NoticeTypeEnum {

    /**
     * 通知
     */
    NOTICE((byte) 1),

    /**
     * 公告
     */
    ANNOUNCEMENT((byte) 2);

    /**
     * 通知类型
     */
    private final Byte type;

}
