package com.zrkizzy.common.models.vo.system.dict;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;

/**
 * 字典类型选项数据返回对象
 *
 * @author zhangrongkang
 * @since 2024/1/20
 */
@Data
public class DictTypeOptionVO {

    /**
     * 字典类型
     */
    @JsonFormat(shape = JsonFormat.Shape.STRING)
    private Long dictTypeId;

    /**
     * 字典名称
     */
    private String dictName;

}
