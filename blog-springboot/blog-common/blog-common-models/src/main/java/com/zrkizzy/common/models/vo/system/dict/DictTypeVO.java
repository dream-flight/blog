package com.zrkizzy.common.models.vo.system.dict;

import com.zrkizzy.common.core.domain.response.BaseVO;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;


/**
 * 字典类型数据返回对象
 *
 * @author zhangrongkang
 * @since 2024/1/16
 */
@Data
@ToString(callSuper = true)
@EqualsAndHashCode(callSuper = false)
public class DictTypeVO extends BaseVO {

    /**
     * 字典名称
     */
    private String dictName;

    /**
     * 字典类型
     */
    private String dictType;

    /**
     * 备注
     */
    private String remark;

    /**
     * 状态（1 正常 0 停用）
     */
    private Boolean status;

}
