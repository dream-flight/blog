package com.zrkizzy.common.models.domain.blog.article;

import com.baomidou.mybatisplus.annotation.TableName;
import com.zrkizzy.common.datasource.domain.BaseEntity;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serial;

/**
 * 文章实体类
 *
 * @author zhangrongkang
 * @since 2023/9/18
 */
@Data
@TableName("tb_article")
@Schema(name = "文章对象")
@EqualsAndHashCode(callSuper = false)
public class Article extends BaseEntity {

    @Serial
    private static final long serialVersionUID = 1L;

    /**
     * 文章名称
     */
    @Schema(description = "文章名称")
    private String title;

    /**
     * 文章封面
     */
    @Schema(description = "文章封面")
    private String cover;

    /**
     * 文章类型：0 原创 1 转载 2 翻译
     */
    @Schema(description = "文章类型")
    private Byte type;

    /**
     * 文章状态：0 草稿箱 1 已发布 2 待发布
     */
    @Schema(description = "文章状态")
    private Byte status;

    /**
     * 文章摘要
     */
    @Schema(description = "文章摘要")
    private String summary;

    /**
     * 文章内容
     */
    @Schema(description = "文章内容")
    private String content;

    /**
     * 原文链接
     */
    @Schema(description = "原文链接")
    private String originalLink;

    /**
     * 是否允许评论：1 允许 0 不允许
     */
    @Schema(description = "是否允许评论：1 允许 0 不允许")
    private Boolean allowComment;

    /**
     * 是否置顶：1 置顶 0 不置顶
     */
    @Schema(description = "是否置顶：1 置顶 0 不置顶")
    private Boolean isSticky;

    /**
     * 是否开启打赏：1 开启
     */
    @Schema(description = "是否开启打赏：1 开启")
    private Boolean allowDonation;

    /**
     * 可见范围：0 自己可见 1 全部可见
     */
    @Schema(description = "可见范围：0 自己可见 1 全部可见")
    private Byte visibility;

    /**
     * 浏览数量
     */
    @Schema(description = "浏览数量")
    private Integer visitCount;

    /**
     * 评论数量
     */
    @Schema(description = "评论数量")
    private Integer commentCount;

    /**
     * 发布IP
     */
    @Schema(description = "发布IP")
    private String createAddress;

    /**
     * 发布地址
     */
    @Schema(description = "发布地址")
    private String createLocation;

}