package com.zrkizzy.common.models.vo.blog.article;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.zrkizzy.common.core.domain.response.BaseVO;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

import java.util.List;

/**
 * 文章数据返回对象
 *
 * @author zhangrongkang
 * @since 2023/9/18
 */
@Data
@ToString(callSuper = true)
@EqualsAndHashCode(callSuper = false)
public class ArticleVO extends BaseVO {

    /**
     * 文章名称
     */
    private String title;

    /**
     * 文章封面
     */
    private String cover;

    /**
     * 文章类型：0 原创 1 转载 2 翻译
     */
    @JsonFormat(shape = JsonFormat.Shape.STRING)
    private Byte type;

    /**
     * 文章摘要
     */
    private String summary;

    /**
     * 文章内容
     */
    private String content;

    /**
     * 文章状态：0 草稿箱 1 已发布 2 待发布
     */
    private Byte status;

    /**
     * 原文链接
     */
    private String originalLink;

    /**
     * 是否允许评论：1 允许 0 不允许
     */
    private Boolean allowComment;

    /**
     * 是否置顶：1 置顶 0 不置顶
     */
    private Boolean isSticky;

    /**
     * 是否开启打赏：1 开启 0 不开启
     */
    private Boolean allowDonation;

    /**
     * 可见范围：0 自己可见 1 全部可见
     */
    @JsonFormat(shape = JsonFormat.Shape.STRING)
    private Byte visibility;

    /**
     * 浏览数量
     */
    private Integer visitCount;

    /**
     * 评论数量
     */
    private Integer commentCount;

    /**
     * 发布IP
     */
    private String createAddress;

    /**
     * 发布地址
     */
    private String createLocation;

    /**
     * 文章标签
     */
    private List<String> tags;

    /**
     * 文章分类ID
     */
    @JsonFormat(shape = JsonFormat.Shape.STRING)
    private Long categoryId;

}
