package com.zrkizzy.common.models.domain.system.resource;

import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;

import java.io.Serial;
import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * 资源模块关联实体类
 *
 * @author zhangrongkang
 * @since 2023/7/27
 */
@Data
@TableName("tb_module_resource")
@Schema(name = "资源模块关联对象")
public class ModuleResource implements Serializable {

    @Serial
    private static final long serialVersionUID = 1L;

    /**
     * 主键
     */
    @Schema(description = "主键")
    private Long id;

    /**
     * 模块主键
     */
    @Schema(description = "模块主键")
    private Long moduleId;

    /**
     * 资源主键
     */
    @Schema(description = "资源主键")
    private Long resourceId;

    /**
     * 添加时间
     */
    @Schema(description = "添加时间")
    private LocalDateTime createTime;

}