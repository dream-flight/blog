package com.zrkizzy.common.models.vo.blog.article;

import lombok.Data;

import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * 关于我数据返回对象
 *
 * @author zhangrongkang
 * @since 2023/9/8
 */
@Data
public class AboutMeVO implements Serializable {

    /**
     * 主键
     */
    private String id;

    /**
     * 标题
     */
    private String title;

    /**
     * 作者头像
     */
    private String avatar;

    /**
     * 内容
     */
    private String content;

    /**
     * 评论ID
     */
    private String commentId;

    /**
     * 更新时间
     */
    private LocalDateTime updateTime;

}
