package com.zrkizzy.common.models.vo.system.scheduler;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.zrkizzy.common.core.domain.response.BaseVO;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;


/**
 * 定时任务数据返回对象
 *
 * @author zhangrongkang
 * @since 2024/2/23
 */
@Data
@ToString(callSuper = true)
@EqualsAndHashCode(callSuper = false)
public class SchedulerJobVO extends BaseVO {

    /**
     * 任务名称
     */
    private String jobName;

    /**
     * 任务分组
     */
    private String jobGroup;

    /**
     * 是否并发执行： 1 允许， 0 禁止
     */
    private Boolean concurrent;

    /**
     * corn执行表达式
     */
    private String cron;

    /**
     * 执行策略： 1 立即执行，2执行一次， 3 放弃执行
     */
    private Byte executeStrategy;

    /**
     * 调用目标字符串
     */
    private String invokeTarget;

    /**
     * 备注信息
     */
    private String remark;

    /**
     * 任务状态： 1 正常，0 暂停
     */
    @JsonFormat(shape = JsonFormat.Shape.STRING)
    private Boolean status;

    /**
     * 创建用户
     */
    private String createBy;

    /**
     * 更新用户
     */
    private String updateBy;

}
