package com.zrkizzy.common.models.dto.system.file;

import lombok.Data;
import org.springframework.web.multipart.MultipartFile;

import jakarta.validation.constraints.NotNull;
import java.io.Serializable;

/**
 * 文件上传数据传输对象
 *
 * @author zhangrongkang
 * @since 2023/5/29
 */
@Data
public class FileUploadDTO implements Serializable {

    /**
     * 文件分类ID
     */
    @NotNull(message = "文件分类不能为空")
    private Long fileTypeId;

    /**
     * 上传文件
     */
    @NotNull(message = "文件不能为空")
    private MultipartFile file;

}
