package com.zrkizzy.common.models.dto.system.scheduler;

import lombok.Data;

import java.io.Serializable;

/**
 * 定时任务运行数据传输对象
 *
 * @author zhangrongkang
 * @since 2024/3/5
 */
@Data
public class SchedulerJobRunDTO implements Serializable {

    /**
     * 定时任务ID
     */
    private Long id;

    /**
     * 任务分组
     */
    private String jobGroup;

}
