package com.zrkizzy.common.models.domain.system.core;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;

import java.io.Serializable;

/**
 * 用户角色关联实体类
 *
 * @author zhangrongkang
 * @since 2023/3/15
 */
@Data
@Builder
@AllArgsConstructor
@TableName("tb_user_role")
@Schema(name = "用户角色关联对象")
public class UserRole implements Serializable {

    /**
     * 主键
     */
    @TableId
    @Schema(description = "主键")
    private Long id;

    /**
     * 用户主键
     */
    @Schema(description = "用户主键")
    private Long userId;

    /**
     * 角色主键
     */
    @Schema(description = "角色主键")
    private Long roleId;

}
