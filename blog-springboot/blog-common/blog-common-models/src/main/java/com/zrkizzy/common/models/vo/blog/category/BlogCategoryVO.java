package com.zrkizzy.common.models.vo.blog.category;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.zrkizzy.common.core.domain.response.BaseVO;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;


/**
 * 博客分类数据返回对象
 *
 * @author zhangrongkang
 * @since 2023/9/4
 */
@Data
@ToString(callSuper = true)
@EqualsAndHashCode(callSuper = false)
public class BlogCategoryVO extends BaseVO {

    /**
     * 分类名称
     */
    private String name;

    /**
     * 分类描述
     */
    private String description;

    /**
     * 分类排序
     */
    private Integer sort;

    /**
     * 分类图标
     */
    private String icon;

    /**
     * 博客分类访问类型：0 自己可见 1 全部可见
     */
    @JsonFormat(shape = JsonFormat.Shape.STRING)
    private Byte visibility;

    /**
     * 文章数量
     */
    private Integer count;

}
