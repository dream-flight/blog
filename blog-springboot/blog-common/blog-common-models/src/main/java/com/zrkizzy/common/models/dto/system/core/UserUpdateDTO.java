package com.zrkizzy.common.models.dto.system.core;

import com.zrkizzy.common.core.domain.BaseDTO;
import jakarta.validation.constraints.Email;
import jakarta.validation.constraints.NotBlank;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

/**
 * 用户个人信息数据传输对象
 *
 * @author zhangrongkang
 * @since 2023/5/3
 */
@Data
@ToString(callSuper = true)
@EqualsAndHashCode(callSuper = false)
public class UserUpdateDTO extends BaseDTO {

    /**
     * 头像
     */
    private String avatar;

    /**
     * 用户名
     */
    @NotBlank(message = "用户名不能为空")
    @Email(message = "请输入正确的邮箱格式")
    private String username;

    /**
     * 昵称
     */
    @NotBlank(message = "昵称不能为空")
    private String nickname;

    /**
     * 手机号码
     */
    private String phone;

    /**
     * 备注
     */
    private String remark;

}
