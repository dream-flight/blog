package com.zrkizzy.common.models.vo.system.file;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;

import java.io.Serializable;

/**
 * 文件上传策略数据返回对象
 *
 * @author zhangrongkang
 * @since 2023/12/13
 */
@Data
@Builder
@AllArgsConstructor
public class FileUploadStrategyVO implements Serializable {


    /**
     * 上传策略
     */
    private String strategy;

    /**
     * 策略标识
     */
    private String mark;
}
