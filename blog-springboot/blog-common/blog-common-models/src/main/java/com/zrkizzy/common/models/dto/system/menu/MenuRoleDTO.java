package com.zrkizzy.common.models.dto.system.menu;

import jakarta.validation.constraints.NotEmpty;
import jakarta.validation.constraints.NotNull;
import lombok.Data;

import java.io.Serializable;
import java.util.List;

/**
 * 菜单角色关联数据传输对象
 *
 * @author zhangrongkang
 * @since 2023/7/31
 */
@Data
public class MenuRoleDTO implements Serializable {

    /**
     * 角色ID
     */
    @NotNull(message = "角色Id不能为空")
    private Long roleId;

    /**
     * 菜单ID
     */
    @NotEmpty(message = "菜单ID不能为空")
    private List<Long> menuIds;

}