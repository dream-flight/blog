package com.zrkizzy.common.models.dto.blog.article;

import lombok.Data;

import java.io.Serializable;

/**
 * 文章设置属性数据传输对象
 *
 * @author zhangrongkang
 * @since 2023/9/21
 */
@Data
public class ArticleSettingsDTO implements Serializable {

    /**
     * 文章ID
     */
    private Long id;

    /**
     * 修改字段
     */
    private String status;
}
