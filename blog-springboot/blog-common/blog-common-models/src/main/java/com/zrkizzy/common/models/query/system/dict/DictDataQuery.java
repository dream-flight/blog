package com.zrkizzy.common.models.query.system.dict;

import com.zrkizzy.common.core.domain.request.BasePage;
import jakarta.validation.constraints.NotBlank;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

import java.util.List;

/**
 * 字典数据信息查询对象
 *
 * @author zhangrongkang
 * @since 2024/1/18
 */
@Data
@ToString(callSuper = true)
@EqualsAndHashCode(callSuper = false)
public class DictDataQuery extends BasePage {

    /**
     * 字典标签
     */
    private String dictLabel;

    /**
     * 字典类型ID
     */
    @NotBlank(message = "字典类型ID不能为空")
    private String dictTypeId;

    /**
     * 状态（1 正常 0 停用）
     */
    private Boolean status;

    /**
     * 时间范围
     */
    private List<String> dataRange;

}