package com.zrkizzy.common.models.domain.system.core;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.zrkizzy.common.datasource.domain.BaseEntity;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

import java.time.LocalDateTime;
import java.util.List;

/**
 * 用户实体类
 *
 * @author zhangrongkang
 * @since 2023/3/7
 */
@Data
@ToString(callSuper = true)
@EqualsAndHashCode(callSuper = false)
@Schema(name = "用户对象")
@TableName("tb_user")
public class User extends BaseEntity {

    /**
     * 用户名（使用邮箱作为唯一替代）
     */
    @Schema(description = "用户名")
    private String username;

    /**
     * 密码
     */
    @JsonIgnore
    @Schema(description = "密码")
    private String password;

    /**
     * 昵称
     */
    @Schema(description = "昵称")
    private String nickname;

    /**
     * 头像
     */
    @Schema(description = "头像")
    private String avatar;

    /**
     * 状态，0：禁用，1：启用
     */
    @Schema(description = "状态，0：禁用，1：启用")
    private Boolean status;

    /**
     * 用户角色
     */
    @Schema(description = "用户角色")
    @TableField(exist = false)
    private List<Role> roles;

    /**
     * 备注
     */
    @Schema(description = "备注")
    private String remark;

    /**
     * 登录唯一标识
     */
    @Schema(description = "登录唯一标识")
    @TableField(exist = false)
    private String traceId;

    /**
     * 登录IP
     */
    @Schema(description = "登录IP")
    @TableField(exist = false)
    private String ipAddress;

    /**
     * IP属地
     */
    @Schema(description = "IP属地")
    @TableField(exist = false)
    private String ipLocation;

    /**
     * 登录时间
     */
    @Schema(description = "登录时间")
    @TableField(exist = false)
    private LocalDateTime loginTime;

    /**
     * 操作系统
     */
    @Schema(description = "操作系统")
    @TableField(exist = false)
    private String os;

    /**
     * 浏览器
     */
    @Schema(description = "浏览器")
    @TableField(exist = false)
    private String browser;

    /**
     * 获取用户角色
     *
     * @return 用户角色
     */
    public Role getRole() {
        return this.roles.getFirst();
    }

}
