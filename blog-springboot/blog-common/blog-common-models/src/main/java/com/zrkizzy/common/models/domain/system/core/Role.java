package com.zrkizzy.common.models.domain.system.core;

import com.baomidou.mybatisplus.annotation.TableName;
import com.zrkizzy.common.datasource.domain.BaseEntity;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

/**
 * 角色实体类
 *
 * @author zhangrongkang
 * @since 2023/3/8
 */
@Data
@ToString(callSuper = true)
@EqualsAndHashCode(callSuper = false)
@Schema(name = "角色对象")
@TableName("tb_role")
public class Role extends BaseEntity {

    /**
     * 角色名称
     */
    @Schema(description = "角色名称")
    private String name;

    /**
     * 角色标识
     */
    @Schema(description = "角色标识")
    private String mark;

    /**
     * 角色描述
     */
    @Schema(description = "角色描述")
    private String description;

}
