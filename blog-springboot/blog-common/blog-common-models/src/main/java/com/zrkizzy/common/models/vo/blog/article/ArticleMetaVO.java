package com.zrkizzy.common.models.vo.blog.article;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;

import java.time.LocalDateTime;

/**
 * 文章基本数据返回对象
 * <p>用于文章管理页面将基本信息进行展示，无需将所有内容全部返回</p>
 *
 * @author zhangrongkang
 * @since 2023/9/23
 */
@Data
public class ArticleMetaVO {

    /**
     * 文章主键
     */
    @JsonFormat(shape =JsonFormat.Shape.STRING)
    private Long id;

    /**
     * 文章名称
     */
    private String title;

    /**
     * 文章封面
     */
    private String cover;

    /**
     * 文章类型：0 原创 1 转载 2 翻译
     */
    private Byte type;

    /**
     * 文章状态：0 草稿箱 1 已发布 2 待发布
     */
    private Byte status;

    /**
     * 是否允许评论：1 允许 0 不允许
     */
    private Boolean allowComment;

    /**
     * 是否置顶：1 置顶 0 不置顶
     */
    private Boolean isSticky;

    /**
     * 是否开启打赏：1 开启
     */
    private Boolean allowDonation;

    /**
     * 可见范围：0 自己可见 1 全部可见
     */
    private Byte visibility;

    /**
     * 浏览数量
     */
    private Integer visitCount;

    /**
     * 评论数量
     */
    private Integer commentCount;

    /**
     * 文章发布时间
     */
    private LocalDateTime createTime;

}
