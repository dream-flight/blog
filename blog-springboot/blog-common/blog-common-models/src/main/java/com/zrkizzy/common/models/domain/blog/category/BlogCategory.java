package com.zrkizzy.common.models.domain.blog.category;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import com.zrkizzy.common.datasource.domain.BaseEntity;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serial;

/**
 * 博客分类实体类
 *
 * @author zhangrongkang
 * @since 2023/9/4
 */
@Data
@TableName("tb_blog_category")
@Schema(name = "博客分类对象")
@EqualsAndHashCode(callSuper = false)
public class BlogCategory extends BaseEntity {

    @Serial
    private static final long serialVersionUID = 1L;

    /**
     * 分类名称
     */
    @Schema(description = "分类名称")
    private String name;

    /**
     * 分类描述
     */
    @Schema(description = "分类描述")
    private String description;

    /**
     * 分类排序
     */
    @Schema(description = "分类排序")
    private Integer sort;

    /**
     * 分类图标
     */
    @Schema(description = "分类图标")
    private String icon;

    /**
     * 博客分类访问类型：0 自己可见 1 全部可见
     */
    @Schema(description = "访问类型")
    private Byte visibility;

    /**
     * 文章数量
     */
    @TableField(exist = false)
    @Schema(description = "文章数量")
    private Integer count;

}