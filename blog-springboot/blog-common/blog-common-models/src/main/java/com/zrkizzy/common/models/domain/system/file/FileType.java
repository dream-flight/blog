package com.zrkizzy.common.models.domain.system.file;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import com.zrkizzy.common.datasource.domain.BaseEntity;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.List;

/**
 * 文件分类实体类
 *
 * @author zhangrongkang
 * @since 2023/5/11
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Schema(name = "文件分类对象")
@TableName("tb_file_type")
public class FileType extends BaseEntity {

    /**
     * 文件分类名称
     */
    @Schema(description = "文件分类名称")
    private String name;

    /**
     * 文件分类描述
     */
    @Schema(description = "文件分类描述")
    private String description;

    /**
     * 文件分类标识
     */
    @Schema(description = "文件分类标识")
    private String mark;

    /**
     * 文件分类排序
     */
    @Schema(description = "文件分类排序")
    private Integer sort;

    /**
     * 当前文件分类对应文件
     */
    @Schema(description = "分类对应文件")
    @TableField(exist = false)
    private List<File> fileList;

}
