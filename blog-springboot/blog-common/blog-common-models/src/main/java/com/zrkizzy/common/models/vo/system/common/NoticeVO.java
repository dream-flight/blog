package com.zrkizzy.common.models.vo.system.common;

import com.zrkizzy.common.core.domain.response.BaseVO;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;


/**
 * 通知公告数据返回对象
 *
 * @author zhangrongkang
 * @since 2024/3/29
 */
@Data
@ToString(callSuper = true)
@EqualsAndHashCode(callSuper = false)
public class NoticeVO extends BaseVO {

    /**
     * 公告标题
     */
    private String title;

    /**
     * 公告类型（1通知 2公告）
     */
    private Byte type;

    /**
     * 公告状态（0正常 1关闭）
     */
    private Boolean status;

    /**
     * 公告内容
     */
    private String content;

    /**
     * 创建者
     */
    private String createBy;

    /**
     * 备注
     */
    private String remark;

    /**
     * 更新者
     */
    private String updateBy;

}
