package com.zrkizzy.common.models.domain.system.log;

import com.baomidou.mybatisplus.annotation.TableName;
import com.zrkizzy.common.datasource.domain.BaseEntity;
import com.zrkizzy.common.models.enums.system.log.OperateType;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serial;

/**
 * 操作日志实体类
 *
 * @author zhangrongkang
 * @since 2023/7/3
 */
@Data
@TableName("tb_operate_log")
@Schema(name = "操作日志对象")
@EqualsAndHashCode(callSuper = false)
public class OperateLog extends BaseEntity {

    @Serial
    private static final long serialVersionUID = 1L;

    /**
     * 唯一追踪值
     */
    @Schema(description = "唯一追踪值")
    private Long traceId;

    /**
     * 操作内容
     */
    @Schema(description = "操作内容")
    private String operateContent;

    /**
     * 操作类型
     * <p>
     *     OTHER--其他操作<br/>
     *     ADD--新增<br/>
     *     UPDATE--修改<br/>
     *     DELETE--删除<br/>
     *     QUERY--查询<br/>
     * </p>
     * @see OperateType
     */
    @Schema(description = "操作类型")
    private String type;

    /**
     * 操作方法名称
     */
    @Schema(description = "操作方法名称")
    private String methodName;

    /**
     * 请求方式
     */
    @Schema(description = "请求方式")
    private String requestMethod;

    /**
     * 操作用户ID
     */
    @Schema(description = "操作用户ID")
    private Long userId;

    /**
     * 操作IP
     */
    @Schema(description = "操作IP")
    private String operateIp;

    /**
     * 操作地址
     */
    @Schema(description = "操作地址")
    private String operateLocation;

    /**
     * 操作参数
     */
    @Schema(description = "操作参数")
    private String operateParam;

    /**
     * 操作结果描述
     */
    @Schema(description = "操作结果描述")
    private String operateResult;

    /**
     * 操作状态 0 失败 1 成功 
     */
    @Schema(description = "操作状态 0 失败 1 成功 ")
    private Boolean status;

    /**
     * 操作消耗时间
     */
    @Schema(description = "操作消耗时间")
    private Long costTime;

}