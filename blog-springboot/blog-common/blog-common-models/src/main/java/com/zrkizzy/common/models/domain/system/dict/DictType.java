package com.zrkizzy.common.models.domain.system.dict;

import com.baomidou.mybatisplus.annotation.TableName;
import com.zrkizzy.common.datasource.domain.BaseEntity;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serial;

/**
 * 字典类型实体类
 *
 * @author zhangrongkang
 * @since 2024/1/16
 */
@Data
@TableName("tb_dict_type")
@Schema(name = "字典类型对象")
@EqualsAndHashCode(callSuper = false)
public class DictType extends BaseEntity {

    @Serial
    private static final long serialVersionUID = 1L;

    /**
     * 字典名称
     */
    @Schema(description = "字典名称")
    private String dictName;

    /**
     * 字典类型
     */
    @Schema(description = "字典类型")
    private String dictType;

    /**
     * 备注
     */
    @Schema(description = "备注")
    private String remark;

    /**
     * 状态（1 正常 0 停用）
     */
    @Schema(description = "状态（1 正常 0 停用）")
    private Boolean status;

}