package com.zrkizzy.common.models.domain.system.common;

import com.baomidou.mybatisplus.annotation.TableName;
import com.zrkizzy.common.datasource.domain.BaseEntity;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serial;

/**
 * 通知公告实体类
 *
 * @author zhangrongkang
 * @since 2024/3/29
 */
@Data
@TableName("tb_notice")
@Schema(name = "通知公告对象")
@EqualsAndHashCode(callSuper = false)
public class Notice extends BaseEntity {

    @Serial
    private static final long serialVersionUID = 1L;

    /**
     * 公告内容
     */
    @Schema(description = "公告内容")
    private String content;

    /**
     * 创建者
     */
    @Schema(description = "创建者")
    private String createBy;

    /**
     * 备注
     */
    @Schema(description = "备注")
    private String remark;

    /**
     * 公告状态（0正常 1关闭）
     */
    @Schema(description = "公告状态（0正常 1关闭）")
    private Boolean status;

    /**
     * 公告标题
     */
    @Schema(description = "公告标题")
    private String title;

    /**
     * 公告类型（1通知 2公告）
     */
    @Schema(description = "公告类型（1通知 2公告）")
    private Byte type;

    /**
     * 更新者
     */
    @Schema(description = "更新者")
    private String updateBy;

}