package com.zrkizzy.common.models.domain.system.core;

import com.baomidou.mybatisplus.annotation.TableName;
import com.zrkizzy.common.datasource.domain.BaseEntity;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

/**
 * 用户信息实体类
 *
 * @author zhangrongkang
 * @since 2023/5/4
 */
@Data
@ToString(callSuper = true)
@EqualsAndHashCode(callSuper = false)
@Schema(name = "用户信息对象")
@TableName("tb_user_info")
public class UserInfo extends BaseEntity {

    /**
     * 手机号码
     */
    @Schema(description = "手机号码")
    public String phone;

}
