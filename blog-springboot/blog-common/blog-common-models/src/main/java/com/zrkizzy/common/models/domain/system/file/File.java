package com.zrkizzy.common.models.domain.system.file;


import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import com.zrkizzy.common.datasource.domain.BaseEntity;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.*;

/**
 * 文件实体类
 *
 * @author zhangrongkang
 * @since 2023/5/11
 */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@TableName("tb_file")
@Schema(name = "文件对象")
@EqualsAndHashCode(callSuper = false)
public class File extends BaseEntity {

    /**
     * 文件名称
     */
    @Schema(description = "文件名称")
    private String name;

    /**
     * 文件存储路径
     */
    @Schema(description = "文件存储路径")
    private String path;

    /**
     * 文件访问路径
     */
    @Schema(description = "文件访问路径")
    private String src;

    /**
     * 文件大小
     */
    @Schema(description = "文件大小")
    private Long size;

    /**
     * 文件类型
     */
    @Schema(description = "文件类型")
    private String type;

    /**
     * 文件MD5哈希值
     */
    @Schema(description = "文件MD5哈希值")
    private String md5;

    /**
     * 文件分类ID（在查询时不返回前端）
     */
    @Schema(description = "文件分类ID")
    private Long fileTypeId;

    /**
     * 上传用户ID
     */
    @Schema(description = "上传用户ID")
    private Long userId;

    /**
     * 上传策略
     */
    @Schema(description = "上传策略")
    private String strategy;

    /**
     * 上传用户昵称
     */
    @Schema(description = "上传用户昵称")
    @TableField(exist = false)
    private String nickname;

}
