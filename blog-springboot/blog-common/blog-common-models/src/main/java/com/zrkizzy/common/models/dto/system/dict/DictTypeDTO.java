package com.zrkizzy.common.models.dto.system.dict;

import com.zrkizzy.common.core.domain.BaseDTO;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;


/**
 * 字典类型数据传输对象
 *
 * @author zhangrongkang
 * @since 2024/1/16
 */
@Data
@ToString(callSuper = true)
@EqualsAndHashCode(callSuper = false)
public class DictTypeDTO extends BaseDTO {

    /**
     * 字典名称
     */
    private String dictName;

    /**
     * 字典类型
     */
    private String dictType;

    /**
     * 备注
     */
    private String remark;

    /**
     * 状态（1 正常 0 停用）
     */
    private Boolean status;

}
