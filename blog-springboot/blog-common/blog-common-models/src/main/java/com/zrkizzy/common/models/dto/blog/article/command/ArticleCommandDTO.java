package com.zrkizzy.common.models.dto.blog.article.command;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;

import java.io.Serializable;
import java.util.List;

/**
 * 文章命令数据传输对象
 *
 * @author zhangrongkang
 * @since 2023/10/9
 */
@Data
@Builder
@AllArgsConstructor
public class ArticleCommandDTO implements Serializable {

    /**
     * 文章ID
     */
    private Long articleId;

    /**
     * 分类ID
     */
    private Long categoryId;

    /**
     * 标签ID集合
     */
    private List<Long> tagIds;

}
