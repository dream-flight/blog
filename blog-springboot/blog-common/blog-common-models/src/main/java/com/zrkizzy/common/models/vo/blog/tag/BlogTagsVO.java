package com.zrkizzy.common.models.vo.blog.tag;

import com.zrkizzy.common.core.domain.response.BaseVO;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;


/**
 * 博客标签数据返回对象
 *
 * @author zhangrongkang
 * @since 2023/9/5
 */
@Data
@ToString(callSuper = true)
@EqualsAndHashCode(callSuper = false)
public class BlogTagsVO extends BaseVO {

    /**
     * 标签名称
     */
    private String name;

    /**
     * 标签排序
     */
    private Integer sort;

    /**
     * 文章数量
     */
    private Integer count;

}
