package com.zrkizzy.common.models.domain.blog.tag;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import com.zrkizzy.common.datasource.domain.BaseEntity;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serial;

/**
 * 博客标签实体类
 *
 * @author zhangrongkang
 * @since 2023/9/5
 */
@Data
@TableName("tb_blog_tags")
@Schema(name = "博客标签对象")
@EqualsAndHashCode(callSuper = false)
public class BlogTags extends BaseEntity {

    @Serial
    private static final long serialVersionUID = 1L;

    /**
     * 标签名称
     */
    @Schema(description = "标签名称")
    private String name;

    /**
     * 标签排序
     */
    @Schema(description = "标签排序")
    private Integer sort;

    /**
     * 文章数量
     */
    @TableField(exist = false)
    @Schema(description = "文章数量")
    private Integer count;

}