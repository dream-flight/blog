package com.zrkizzy.common.models.dto.system.common;

import com.zrkizzy.common.models.enums.system.common.login.LoginChannel;
import com.zrkizzy.common.models.enums.system.common.login.LoginTarget;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;
import lombok.Data;

import java.io.Serializable;

/**
 * 用户登录数据传输对象
 *
 * @author zhangrongkang
 * @since 2023/4/12
 */
@Data
public class LoginDTO implements Serializable {

    /**
     * 用户名
     */
    private String username;

    /**
     * 密码
     */
    private String password;

    /**
     * 验证码
     */
    private String code;

    /**
     * 验证码追踪值
     *
     * <p>用于标识是否为同一用户</p>
     */
    @NotBlank(message = "验证码追踪值不能为空")
    private String traceId;

    /**
     * 登录渠道：QQ，WeiBo，Gitee，Mobile，System
     *
     * <p>使用System表示使用系统默认验证码</p>
     * @see LoginChannel
     */
    @NotNull(message = "请选择正确的登录渠道")
    private LoginChannel channel;

    /**
     * 登录目标
     * <p>登录目标：前台--BLOG，后台--ADMIN</p>
     */
    @NotNull(message = "登录目标不能为空")
    private LoginTarget target;

    /**
     * 电话号码
     *
     * <p>当登录渠道为Mobile时当前值不能为空</p>
     */
    private String mobile;

}
