package com.zrkizzy.common.models.dto.blog.category;

import com.zrkizzy.common.core.domain.BaseDTO;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;


/**
 * 博客分类数据传输对象
 *
 * @author zhangrongkang
 * @since 2023/9/4
 */
@Data
@ToString(callSuper = true)
@EqualsAndHashCode(callSuper = false)
public class BlogCategoryDTO extends BaseDTO {


    /**
     * 分类名称
     */
    private String name;

    /**
     * 分类描述
     */
    private String description;

    /**
     * 分类排序
     */
    private Integer sort;

    /**
     * 分类图标
     */
    private String icon;

    /**
     * 博客分类访问类型：0 所有人不可见； 1 所有人可见； 2 仅自己可见
     */
    private Byte visibility;

    /**
     * 文章数量
     */
    private Integer count;

}
