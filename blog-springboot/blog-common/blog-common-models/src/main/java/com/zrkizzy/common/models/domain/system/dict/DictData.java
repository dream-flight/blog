package com.zrkizzy.common.models.domain.system.dict;

import com.baomidou.mybatisplus.annotation.TableName;
import com.zrkizzy.common.datasource.domain.BaseEntity;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serial;

/**
 * 字典数据实体类
 *
 * @author zhangrongkang
 * @since 2024/1/18
 */
@Data
@TableName("tb_dict_data")
@Schema(name = "字典数据对象")
@EqualsAndHashCode(callSuper = false)
public class DictData extends BaseEntity {

    @Serial
    private static final long serialVersionUID = 1L;

    /**
     * 字典标签
     */
    @Schema(description = "字典标签")
    private String dictLabel;

    /**
     * 字典类型
     */
    @Schema(description = "字典类型ID")
    private Long dictTypeId;

    /**
     * 字典键值
     */
    @Schema(description = "字典键值")
    private String dictValue;

    /**
     * 表格回显样式
     */
    @Schema(description = "表格回显样式")
    private String listClass;

    /**
     * 样式属性（其他样式扩展）
     */
    @Schema(description = "样式属性（其他样式扩展）")
    private String cssClass;

    /**
     * 备注
     */
    @Schema(description = "备注")
    private String remark;

    /**
     * 字典排序
     */
    @Schema(description = "字典排序")
    private Integer sort;

    /**
     * 状态（true--正常 false--停用）
     */
    @Schema(description = "状态")
    private Boolean status;

}