package com.zrkizzy.common.models.enums.system.common.login;

import com.fasterxml.jackson.annotation.JsonCreator;
import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * 登录渠道枚举
 *
 * @author zhangrongkang
 * @since 2023/8/31
 */
@Getter
@AllArgsConstructor
public enum LoginChannel {

    /**
     * QQ
     */
    QQ,

    /**
     * Gitee
     */
    GIE_EE,

    /**
     * 手机
     */
    MOBILE,

    /**
     * 微博
     */
    WEI_BO,

    /**
     * 系统
     */
    SYSTEM;

    /**
     * 根据登录渠道值获取对应登录渠道枚举
     *
     * @param value 登录渠道值
     * @return null--登录渠道不正确；登录渠道枚举
     */
    @JsonCreator
    public static LoginChannel fromValue(String value) {
        for (LoginChannel loginChannel : values()) {
            if (loginChannel.name().equals(value)) {
                return loginChannel;
            }
        }
        // 返回空的登录渠道
        return null;
    }

}
