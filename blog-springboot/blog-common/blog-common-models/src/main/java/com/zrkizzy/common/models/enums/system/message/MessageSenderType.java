package com.zrkizzy.common.models.enums.system.message;

import com.fasterxml.jackson.annotation.JsonCreator;
import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * 消息发送类型枚举
 *
 * @author zhangrongkang
 * @since 2023/11/20
 */
@Getter
@AllArgsConstructor
public enum MessageSenderType {

    /**
     * 修改密码
     */
    CHANGE_PASSWORD("CHANGE_PASSWORD"),

    /**
     * 审核评论
     */
    REVIEW_COMMENT("REVIEW_COMMENT"),

    /**
     * 审核留言
     */
    REVIEW_MESSAGE("REVIEW_MESSAGE"),

    /**
     * 用户回复评论
     */
    REPLY_COMMENT("REPLY_COMMENT");

    /**
     * 消息发送类型值
     */
    private final String type;

    /**
     * 根据消息发送类型值获取对应消息发送类型枚举
     *
     * @param value 消息发送类型值
     * @return null 消息发送类型不正确；消息发送类型枚举
     */
    @JsonCreator
    public static MessageSenderType fromValue(String value) {
        for (MessageSenderType messageSenderType : values()) {
            if (messageSenderType.type.equals(value)) {
                return messageSenderType;
            }
        }
        // 返回空的消息发送类型
        return null;
    }

}
