package com.zrkizzy.common.mongo.service.impl;

import com.zrkizzy.common.mongo.service.IMongoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * MongoDB操作接口实现类
 *
 * @author zhangrongkang
 * @since 2023/8/24
 */
@Service
public class MongoServiceImpl<T> implements IMongoService<T> {

    @Autowired
    private MongoTemplate mongoTemplate;

    /**
     * 向集合中插入一个新的文档
     *
     * @param document 文档
     * @return 文档内容
     */
    @Override
    public  T insert(T document) {
        return mongoTemplate.insert(document);
    }

    /**
     * 向集合中插入一个新的文档
     *
     * @param document 文档
     * @param collectionName 集合名称
     * @return 文档内容
     */
    @Override
    public T insert(T document, String collectionName) {
        return mongoTemplate.insert(document, collectionName);
    }

    /**
     * 更新指定文档
     *
     * @param document 文档对象
     * @return 更新后数据
     */
    @Override
    public T save(T document) {
        return mongoTemplate.save(document);
    }

    /**
     * 通过文档ID获得配置对象
     *
     * @param id    文档ID
     * @param clazz 配置对象类型
     * @return 配置对象
     */
    @Override
    public T findById(String id, Class<T> clazz) {
        return mongoTemplate.findById(id, clazz);
    }

    /**
     * 获取指定类的所有文档
     *
     * @param clazz 指定类
     * @return 指定文档集合
     */
    @Override
    public List<T> findAll(Class<T> clazz) {
        return mongoTemplate.findAll(clazz);
    }


}
