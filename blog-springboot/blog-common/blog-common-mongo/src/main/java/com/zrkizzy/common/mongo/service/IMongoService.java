package com.zrkizzy.common.mongo.service;

import java.util.List;

/**
 * MongoDB操作接口
 *
 * @author zhangrongkang
 * @since 2023/8/24
 */
public interface IMongoService<T> {

    /**
     * 向集合中插入一个新的文档
     *
     * @param document 文档
     * @return 文档内容
     */
    T insert(T document);

    /**
     * 向集合中插入一个新的文档
     *
     * @param document 文档
     * @param collectionName 文档名称
     * @return 文档内容
     */
    T insert(T document, String collectionName);

    T save(T document);

    /**
     * 通过文档ID获得配置对象
     *
     * @param id 文档ID
     * @param clazz 配置对象类型
     * @return 配置对象
     */
    T findById(String id, Class<T> clazz);

    /**
     * 获取指定类的所有文档
     *
     * @param clazz 指定类
     * @return 指定文档集合
     */
    List<T> findAll(Class<T> clazz);
}
