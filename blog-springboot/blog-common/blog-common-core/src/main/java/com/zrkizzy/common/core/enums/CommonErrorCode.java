package com.zrkizzy.common.core.enums;

import com.zrkizzy.common.core.exception.ErrorCode;
import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * 前端返回对象状态枚举
 * <table>
 *     <tr>
 *         <td><b>状态码</b></td>
 *         <td><b>作用范围</b></td>
 *     </tr>
 *     <tr>
 *         <td>00000</td>
 *         <td>一切OK</td>
 *     </tr>
 *     <tr>
 *         <td>A0001</td>
 *         <td>用户端错误</td>
 *     </tr>
 *     <tr>
 *         <td>B0001</td>
 *         <td>系统执行出错</td>
 *     </tr>
 *     <tr>
 *        <td>C0001</td>
 *         <td>调用第三方服务出错</td>
 *     </tr>
 * </table>
 *
 * @author zhangrongkang
 * @since 2023/3/7
 */
@Getter
@AllArgsConstructor
public enum CommonErrorCode implements ErrorCode {

    /**
     * 成功状态码
     */
    SUCCESS("00000", "一切OK"),

    /**
     * 用户端错误
     */
    BAD_REQUEST("400", "请求参数错误"),

    /**
     * 系统执行出错
     */
    INTERNAL_SERVER_ERROR("B0001", "系统执行出错"),
    SYSTEM_RESOURCE_EXCEPTION("B0300", "系统资源异常"),
    CONFIGURATION_ERROR("B0410", "系统配置出错"),

    /**
     * 调用第三方服务出错
     */
    MESSAGE_SERVICE_ERROR("C0120", "消息服务出错");

    /**
     * 状态码
     */
    private final String code;

    /**
     * 状态码描述
     */
    private final String message;

    @Override
    public String message() {
        return this.message;
    }

    /**
     * 错误码
     */
    @Override
    public String code() {
        return this.code;
    }

}
