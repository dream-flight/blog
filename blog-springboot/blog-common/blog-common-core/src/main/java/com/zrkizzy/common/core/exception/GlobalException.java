package com.zrkizzy.common.core.exception;

import com.zrkizzy.common.core.enums.CommonErrorCode;
import lombok.Getter;
import lombok.NoArgsConstructor;

/**
 * 自定义全局异常
 *
 * @author zhangrongkang
 * @since 2024/2/4
 */
@Getter
@NoArgsConstructor
public class GlobalException extends RuntimeException {

    /**
     * 默认错误码
     */
    private ErrorCode errorCode = CommonErrorCode.INTERNAL_SERVER_ERROR;

    /**
     * 自定义错误描述
     */
    private Object[] args;

    public GlobalException(ErrorCode errorCode, Object ...args) {
        this.errorCode = errorCode;
        this.args = args;
    }

    public GlobalException(ErrorCode errorCode) {
        this.errorCode = errorCode;
    }
}
