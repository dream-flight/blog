package com.zrkizzy.common.core.domain.response;

import com.zrkizzy.common.core.enums.CommonErrorCode;
import com.zrkizzy.common.core.exception.ErrorCode;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

/**
 * 前端返回对象
 *
 * @author zhangrongkang
 * @since 2023/3/7
 */
@Data
@NoArgsConstructor
public class Result<T> implements Serializable {

    /**
     * 状态码
     */
    private String code;

    /**
     * 描述
     */
    private String message;

    /**
     * 返回数据
     */
    private T data;

    /**
     * 时间戳
     */
    private long timestamp = System.currentTimeMillis();

    public Result(ErrorCode errorCode) {
        this.code = errorCode.code();
        this.message = errorCode.message();
        this.data = null;
    }

    public Result(ErrorCode errorCode, T data) {
        this.code = errorCode.code();
        this.message = errorCode.message();
        this.data = data;
    }

    public Result(ErrorCode errorCode, String message) {
        this.code = errorCode.code();
        this.message = message;
        this.data = null;
    }

    public Result(ErrorCode errorCode, String message, T data) {
        this.code = errorCode.code();
        this.message = message;
        this.data = data;
    }

    /**
     * 响应成功
     *
     * @return 对应的前端返回对象
     * @param <T> 返回参数类型
     */
    public static <T> Result<T> ok() {
        return new Result<T>(CommonErrorCode.SUCCESS);
    }

    /**
     * 响应成功
     *
     * @param message 响应信息
     * @param data 响应数据
     * @return 对应的前端返回对象
     * @param <T> 返回参数类型
     */
    public static <T> Result<T> ok(String message, T data) {
        return new Result<>(CommonErrorCode.SUCCESS, message, data);
    }

    /**
     * 响应成功
     *
     * @param errorCode 前端返回对象状态枚举
     * @return 对应的前端返回对象
     * @param <T> 返回参数类型
     */
    public static <T> Result<T> ok(ErrorCode errorCode) {
        return new Result<T>(errorCode);
    }

    /**
     * 响应成功
     *
     * @param data 传递数据
     * @return 返回对应对象
     * @param <T> 返回参数类型
     */
    public static <T> Result<T> ok(T data) {
        return new Result<>(CommonErrorCode.SUCCESS, data);
    }

    /**
     * 响应成功
     *
     * @param errorCode 前端返回对象状态枚举
     * @param data 传递数据
     * @return 对应的前端返回对象
     * @param <T> 返回参数类型
     */
    public static <T> Result<T> ok(ErrorCode errorCode, T data) {
        return new Result<>(errorCode, data);
    }

    /**
     * 响应失败
     *
     * @return 对应的前端返回对象
     * @param <T> 返回参数类型
     */
    public static <T> Result<T> failure() {
        return new Result<T>(CommonErrorCode.INTERNAL_SERVER_ERROR);
    }

    /**
     * 响应失败
     *
     * @param errorCode 前端返回对象状态枚举
     * @return 对应的前端返回对象
     * @param <T> 返回参数类型
     */
    public static <T> Result<T> failure(ErrorCode errorCode) {
        return new Result<T>(errorCode);
    }

    /**
     * 响应失败
     *
     * @param message 响应信息
     * @return 对应的前端返回对象
     * @param <T> 返回参数类型
     */
    public static <T> Result<T> failure(String message) {
        return new Result<T>(CommonErrorCode.INTERNAL_SERVER_ERROR, message);
    }

    /**
     * 响应失败
     *
     * @param errorCode 前端返回对象状态枚举
     * @param message 响应信息
     * @return 对应的前端返回对象
     * @param <T> 返回参数类型
     */
    public static <T> Result<T> failure(ErrorCode errorCode, String message) {
        return new Result<T>(errorCode, message);
    }

    /**
     * 响应失败
     *
     * @param errorCode 前端返回对象状态枚举
     * @param data 传递数据
     * @return 对应的前端返回对象
     * @param <T> 返回参数类型
     */
    public static <T> Result<T> failure(ErrorCode errorCode, T data) {
        return new Result<>(errorCode, data);
    }
    
}
