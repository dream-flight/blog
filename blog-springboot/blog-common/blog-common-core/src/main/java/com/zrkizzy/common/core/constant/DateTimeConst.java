package com.zrkizzy.common.core.constant;

/**
 * 时间类型全局常量
 *
 * @author zhangrongkang
 * @since 2023/11/30
 */
public class DateTimeConst {

    /**
     * 默认格式化日期
     */
    public static final String DATE_FORMAT = "yyyy-MM-dd";

    /**
     * 默认日期时间格式化
     */
    public static final String DATE_TIME_FORMAT = "yyyy-MM-dd HH:mm:ss";

    /**
     * 默认时间格式化
     */
    public static final String TIME_FORMAT = "HH:mm:ss";

}
