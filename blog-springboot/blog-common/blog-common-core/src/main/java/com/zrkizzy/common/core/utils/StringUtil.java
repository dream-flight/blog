package com.zrkizzy.common.core.utils;

import cn.hutool.core.util.StrUtil;
import org.apache.commons.lang3.StringUtils;

import java.util.Objects;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * 字符串工具类
 *
 * @author zhangrongkang
 * @since 2023/11/30
 */
public class StringUtil extends StringUtils {

    /**
     * 判断一个对象数组是否为空
     *
     * @param objects 要判断的对象数组
     ** @return true：空 false：非空
     */
    public static boolean isEmpty(Object[] objects) {
        return Objects.isNull(objects) || (objects.length == 0);
    }

    /**
     * 格式化字符串
     *
     * @param message 字符串模板
     * @param params 对象参数
     * @return 格式化后的字符串
     */
    public static String messageFormat(String message, Object... params) {
        if (isBlank(message) || isEmpty(params)) {
            return message;
        }
        return StrUtil.format(message, params);
    }

    /**
     * 下划线转大驼峰
     *
     * @param str 带有下划线的字符
     * @return 驼峰规则实体类名称
     */
    public static String lineToUpHump(String str) {
        String line = lineToHump(str, "_");
        // 将首字母大写
        return line.substring(0, 1).toUpperCase() + line.substring(1);
    }

    /**
     * 减号分隔符转大驼峰
     *
     * @param str 带有减号分隔符的字符
     * @return 驼峰规则实体类名称
     */
    public static String minusToUpHump(String str) {
        String line = lineToHump(str, "-");
        // 将首字母大写
        return line.substring(0, 1).toUpperCase() + line.substring(1);
    }

    /**
     * 指定字符转驼峰
     *
     * @param str 含有指定字符的字符
     * @param symbol 指定符号
     * @return 驼峰规则字段名称
     */
    public static String lineToHump(String str, String symbol) {
        StringBuilder result = new StringBuilder();
        // 创建正则表达式，寻找下划线字符
        Pattern linePattern = Pattern.compile(symbol + "(\\w)");
        // 将字符串全部转换为小写
        str = str.toLowerCase();
        // 将正则表达式应用到字符串并创建匹配器对象
        Matcher matcher = linePattern.matcher(str);
        while (matcher.find()) {
            // 把下划线后一位字符替换为对应的大写字符
            matcher.appendReplacement(result, matcher.group(1).toUpperCase());
        }
        // 查找结束后，将未匹配的部分追加到头部
        matcher.appendTail(result);
        // 返回拼接结果
        return result.toString();
    }

}
