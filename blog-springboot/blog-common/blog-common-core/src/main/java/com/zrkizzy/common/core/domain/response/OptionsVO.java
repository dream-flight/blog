package com.zrkizzy.common.core.domain.response;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * 选项返回对象
 *
 * @author zhangrongkang
 * @since 2023/7/3
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class OptionsVO {

    /**
     * 选项ID
     */
    @JsonFormat(shape =JsonFormat.Shape.STRING)
    private Long value;

    /**
     * 选项名称
     */
    private String label;

}
