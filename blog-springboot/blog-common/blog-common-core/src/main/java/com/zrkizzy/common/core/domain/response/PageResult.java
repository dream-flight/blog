package com.zrkizzy.common.core.domain.response;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

/**
 * 前端分页返回对象
 *
 * @author zhangrongkang
 * @since 2023/4/14
 */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Schema(name = "分页返回对象")
public class PageResult<T> {

    /**
     * 数据集合
     */
    @Schema(description = "数据集合")
    private List<T> list;

    /**
     * 数据总数
     */
    @Schema(description = "数据总数")
    private Long total;

}
