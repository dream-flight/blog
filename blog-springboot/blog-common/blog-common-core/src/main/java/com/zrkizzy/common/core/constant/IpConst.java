package com.zrkizzy.common.core.constant;

/**
 * IP全局静态常量
 *
 * @author zhangrongkang
 * @since 2023/3/9
 */
public class IpConst {

    /**
     * 本地登录
     */
    public static final String LOCAL_NAME = "内网IP";

    /**
     * 中国IP
     */
    public static final String CHINA_LOCATION = "中国";

    /**
     * 未知主机名
     */
    public static final String UNKNOWN_HOST_NAME = "未知主机名";

    /**
     * 本地IP地址
     */
    public static final String LOCAL_HOST = "127.0.0.1";

    /**
     * 字符0
     */
    public static final String ZERO = "0";

    /**
     * 环回地址
     */
    public static final String LOOP_BACK_HOST = "0:0:0:0:0:0:0:1";

}
