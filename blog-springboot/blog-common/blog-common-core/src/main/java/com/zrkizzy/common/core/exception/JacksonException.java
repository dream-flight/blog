package com.zrkizzy.common.core.exception;

import java.io.Serial;

/**
 * Jackson自定义异常
 *
 * @author zhangrongkang
 * @since 2023/11/30
 */
public class JacksonException extends RuntimeException {
	
	@Serial
    private static final long serialVersionUID = 1L;

	public JacksonException() {
        super();
    }
    
    public JacksonException(String errMsg) {
        super(errMsg);
    }
    
    public JacksonException(Throwable throwable) {
        super(throwable);
    }
    
    public JacksonException(String errMsg, Throwable throwable) {
        super(errMsg, throwable);
    }

}