package com.zrkizzy.common.core.config.properties;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;

/**
 * 项目相关配置参数
 *
 * @author zhangrongkang
 * @since 2023/11/30
 */
@Data
@ConfigurationProperties(prefix = "blog")
public class BlogProperties {

    /**
     * 项目名称
     */
    private String name;

    /**
     * 版本
     */
    private String version;

    /**
     * 版权年份
     */
    private String copyrightYear;

    /**
     * 网站地址
     */
    private String website;

}
