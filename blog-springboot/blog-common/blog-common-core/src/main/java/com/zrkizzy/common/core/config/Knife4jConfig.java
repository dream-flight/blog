package com.zrkizzy.common.core.config;

import io.swagger.v3.oas.models.Components;
import io.swagger.v3.oas.models.OpenAPI;
import io.swagger.v3.oas.models.info.Contact;
import io.swagger.v3.oas.models.info.Info;
import io.swagger.v3.oas.models.security.SecurityRequirement;
import io.swagger.v3.oas.models.security.SecurityScheme;
import org.springdoc.core.models.GroupedOpenApi;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.Collections;

import static com.zrkizzy.common.core.constant.HttpConst.*;

/**
 * Swagger配置类
 *
 * @author zhangrongkang
 * @since 2023/3/6
 */
@Configuration
public class Knife4jConfig {

    @Bean
    public OpenAPI apiInfo() {
        return new OpenAPI()
                .info(new Info()
                        .title("个人博客接口文档")
                        .description("基于SpringBoot3+Vue开发的前后端分离的个人博客，数据库使用MySQL和MongoDB，使用Redis做缓存，RabbitMQ作为消息队列")
                        .version("1.0.0")
                        .termsOfService("https://www.zrkizzy.com")
                        .contact(new Contact().name("世纪末的架构师").url("1072876976@qq.com")))
                .components(new Components()
                        .addSecuritySchemes(AUTHORIZATION, new SecurityScheme()
                                // API密钥身份验证方案
                                .type(SecurityScheme.Type.APIKEY)
                                // 请求会自动在token前添加"Bearer" -- Authorization: Bearer xxx.xxx.xxx
                                .scheme(BEARER)
                                // Http请求头中名称
                                .name(AUTHORIZATION)
                                .bearerFormat(JWT)
                                .description("API认证描述")
                                // 在请求头中
                                .in(SecurityScheme.In.HEADER)));
    }

    /**
     * 系统管理分组接口文档
     * <p>
     *     类似于Swagger规范中的Docket
     * </p>
     *
     * @return OpenAPI分组
     */
    @Bean
    public GroupedOpenApi groupedAdminOpenApi() {
        return GroupedOpenApi.builder()
                .group("系统管理接口")
                // 带有"/admin"的请求都需要进行认证
                .pathsToMatch("/admin/**")
                .addOperationCustomizer((operation, handleMethod) -> operation
                        .security(Collections.singletonList(new SecurityRequirement()
                                .addList(AUTHORIZATION)))).build();
    }

    /**
     * 博客前台分组接口文档
     *
     * @return OpenAPI分组
     */
    @Bean
    public GroupedOpenApi groupedCommonOpenApi() {
        return GroupedOpenApi.builder()
                .group("博客开放接口")
                .pathsToMatch("/common/**").build();
    }
}