package com.zrkizzy.common.core.exception;

/**
 * 自定义错误编码
 *
 * @author zhangrongkang
 * @since 2024/2/4
 */
public interface ErrorCode {

    /**
     * 错误信息描述
     */
    String message();

    /**
     * 错误码
     */
    String code();

    default GlobalException exception() {
        return new GlobalException(this);
    }

    default GlobalException exception(Object...args) {
        return new GlobalException(this, args);
    }

}
