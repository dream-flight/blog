package com.zrkizzy.common.core.constant;

/**
 * HTTP全局静态常量
 *
 * @author zhangrongkang
 * @since 2023/11/30
 */
public class HttpConst {

    /**
     * 中文编码格式
     */
    public static final String UTF_8 = "UTF-8";

    /**
     * JSON传递格式
     */
    public static final String APPLICATION_JSON = "application/json";

    /**
     * TEXT传递格式
     */
    public static final String TEXT_PLAIN = "text/plain";

    /**
     * JWT鉴权类型
     */
    public static final String JWT = "JWT";

    /**
     * token请求头
     */
    public static final String AUTHORIZATION = "Authorization";

    /**
     * Jwt负载开头
     */
    public static final String BEARER = "Bearer";

    /**
     * 图片格式
     */
    public static final String IMAGE_JPEG = "image/jpeg";

    /**
     * Http / 1.1之前版本缓存控制
     */
    public static final String PRAGMA = "Pragma";

    /**
     * 缓存控制
     */
    public static final String CACHE_CONTROL = "Cache-Control";

    /**
     * 禁止缓存
     */
    public static final String NO_CACHE = "no-cache";

    /**
     * 失效时间
     */
    public static final String EXPIRES = "Expires";

    /**
     * 布局组件
     */
    public static final String LAYOUT = "Layout";

    /**
     * 默认父ID
     */
    public static final Long PARENT_ID = 0L;

    /**
     * http请求前缀
     */
    public static final String HTTP_PREFIX = "http://";

    /**
     * https请求前缀
     */
    public static final String HTTPS_PREFIX = "https://";

    /**
     * 无需重定向
     */
    public static final String NO_REDIRECT = "noRedirect";

    /**
     * 用户解析信息
     */
    public static final String USER_AGENT = "User-Agent";

}
