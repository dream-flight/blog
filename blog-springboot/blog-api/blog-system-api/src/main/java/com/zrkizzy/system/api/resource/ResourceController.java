package com.zrkizzy.system.api.resource;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.zrkizzy.common.core.domain.response.PageResult;
import com.zrkizzy.common.core.domain.response.Result;
import com.zrkizzy.common.core.utils.bean.BeanCopyUtil;
import com.zrkizzy.common.models.domain.system.resource.Resource;
import com.zrkizzy.common.models.dto.system.resource.ResourceDTO;
import com.zrkizzy.common.models.enums.system.log.OperateType;
import com.zrkizzy.common.models.query.system.resource.ResourceQuery;
import com.zrkizzy.common.models.vo.system.resource.ResourceVO;
import com.zrkizzy.system.facade.annotation.OperationLog;
import com.zrkizzy.system.facade.service.resource.IResourceService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * 资源数据返回对象
 *
 * @author zhangrongkang
 * @since 2023/7/27
 */
@Tag(name = "资源控制器")
@RestController
@RequestMapping("/admin/resource")
public class ResourceController {

     @Autowired
     private IResourceService resourceService;

    @Operation(summary = "分页获取所有资源")
    @PostMapping("/list")
    public Result<PageResult<ResourceVO>> listResources(@RequestBody ResourceQuery resourceQuery) {
        // 查询到对应的资源集合
        Page<Resource> resourcePage = resourceService.listResources(resourceQuery);
        // 处理对应数据集合并返回数据
        return Result.ok(PageResult.<ResourceVO>builder().total(resourcePage.getTotal())
            // 资源集合
            .list(BeanCopyUtil.copyList(resourcePage.getRecords(), ResourceVO.class)).build());
    }
            
    @Operation(summary = "更新指定请求资源")
    @PutMapping("/save")
    public Result<Boolean> saveResource(@RequestBody ResourceDTO resourceDTO) {
        // 保存资源数据
        return Result.ok(resourceService.saveResource(resourceDTO));
    }

    @Operation(summary = "获取指定资源信息")
    @GetMapping("/getResourceById/{id}")
    public Result<ResourceVO> getResourceById (@PathVariable Long id) {
        return Result.ok(BeanCopyUtil.copy(resourceService.getResourceById(id), ResourceVO.class));
    }

    @Operation(summary = "批量删除请求资源")
    @DeleteMapping("/delete")
    @OperationLog(type = OperateType.DELETE)
    public Result<Boolean> delete(@RequestBody List<Long> ids) {
        return Result.ok(resourceService.deleteBatch(ids));
    }

}
