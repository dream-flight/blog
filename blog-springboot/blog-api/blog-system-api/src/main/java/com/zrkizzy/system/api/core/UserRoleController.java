package com.zrkizzy.system.api.core;

import com.zrkizzy.common.core.domain.response.Result;
import com.zrkizzy.common.models.dto.system.core.UserRoleDTO;
import com.zrkizzy.common.models.enums.system.log.OperateType;
import com.zrkizzy.system.facade.annotation.OperationLog;
import com.zrkizzy.system.facade.service.core.IUserRoleService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * 用户角色关联控制器
 *
 * @author zhangrongkang
 * @since 2023/7/26
 */
@Tag(name = "用户角色关联控制器")
@RestController
@RequestMapping("/admin/user-role")
public class UserRoleController {

    @Autowired
    private IUserRoleService userRoleService;

    @Operation(summary = "更新用户角色")
    @PutMapping("/update")
    @OperationLog(type = OperateType.UPDATE)
    public Result<Boolean> update(@Validated @RequestBody UserRoleDTO userRoleDTO) {
        // 更新用户角色
        return Result.ok(userRoleService.update(userRoleDTO));
    }
}
