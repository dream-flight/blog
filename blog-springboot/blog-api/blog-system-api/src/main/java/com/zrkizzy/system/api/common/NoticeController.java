package com.zrkizzy.system.api.common;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.zrkizzy.common.core.domain.response.PageResult;
import com.zrkizzy.common.core.domain.response.Result;
import com.zrkizzy.common.core.utils.bean.BeanCopyUtil;
import com.zrkizzy.common.models.domain.system.common.Notice;
import com.zrkizzy.common.models.query.system.common.NoticeQuery;
import com.zrkizzy.common.models.vo.system.common.NoticeVO;
import com.zrkizzy.system.facade.service.common.INoticeService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * 通知公告获取控制器
 *
 * @author zhangrongkang
 * @since 2024/3/29
 */
@Tag(name = "通知公告获取控制器")
@RestController
@RequestMapping("/common/notice")
public class NoticeController {

    @Autowired
    private INoticeService noticeService;

    @Operation(summary = "分页查询通知公告")
    @PostMapping("/list")
    public Result<PageResult<NoticeVO>> listNotices(@RequestBody NoticeQuery noticeQuery) {
        // 查询到对应的通知公告集合
        Page<Notice> noticePage = noticeService.listNotices(noticeQuery);
        // 处理对应数据集合并返回数据
        return Result.ok(PageResult.<NoticeVO>builder().total(noticePage.getTotal())
                .list(BeanCopyUtil.copyList(noticePage.getRecords(), NoticeVO.class)).build());
    }
}
