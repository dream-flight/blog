package com.zrkizzy.system.api.menu;

import com.zrkizzy.common.core.domain.response.Result;
import com.zrkizzy.common.models.dto.system.menu.MenuRoleDTO;
import com.zrkizzy.common.models.vo.system.menu.MenuRoleVO;
import com.zrkizzy.common.models.vo.system.menu.MenuTreeVO;
import com.zrkizzy.system.facade.service.menu.IMenuRoleService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import jakarta.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * 菜单角色关联控制器
 *
 * @author zhangrongkang
 * @since 2023/12/18
 */
@Tag(name = "菜单角色关联控制器")
@RestController
@RequestMapping("/admin/menu-role")
public class MenuRoleController {

    @Autowired
    private IMenuRoleService menuRoleService;

    @Operation(summary = "获取角色对应菜单权限")
    @GetMapping("/listByRoleId/{id}")
    public Result<MenuRoleVO> listByRoleId(@PathVariable Long id) {
        // 获取所有菜单树型数据
        List<MenuTreeVO> menuTrees = menuRoleService.listAllMenuTree();
        // 获取当前角色已有的菜单权限
        List<String> ids = menuRoleService.listMenuIdByRoleId(id);
        return Result.ok(new MenuRoleVO(ids, menuTrees));
    }

    @Operation(summary = "保存角色菜单权限")
    @PostMapping("/save")
    public Result<Boolean> save(@Valid @RequestBody MenuRoleDTO menuRoleDTO) {
        return menuRoleService.save(menuRoleDTO) ? Result.ok() : Result.failure() ;
    }

}
