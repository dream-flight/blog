package com.zrkizzy.system.api.log;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.zrkizzy.common.core.domain.response.PageResult;
import com.zrkizzy.common.core.domain.response.Result;
import com.zrkizzy.common.core.enums.CommonErrorCode;
import com.zrkizzy.common.models.query.system.monitor.LoginLogQuery;
import com.zrkizzy.common.models.vo.system.log.LoginLogVO;
import com.zrkizzy.system.facade.service.log.ILoginLogService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * 用户登录日志控制器
 *
 * @author zhangrongkang
 * @since 2023/7/4
 */
@Tag(name = "用户登录日志控制器")
@RestController
@RequestMapping("/admin/login-log")
public class LoginLogController {

     @Autowired
     private ILoginLogService loginLogService;

    @Operation(summary = "获取所有用户登录日志")
    @PostMapping("/list")
    public Result<PageResult<LoginLogVO>> listLoginInfos(@RequestBody LoginLogQuery loginLogQuery) {
        // 查询到对应的用户登录日志集合
        Page<LoginLogVO> loginInfos = loginLogService.listLoginLogs(loginLogQuery);
        // 处理对应数据集合并返回数据
        return Result.ok(PageResult.<LoginLogVO>builder().total(loginInfos.getTotal())
            // 用户登录日志集合
            .list(loginInfos.getRecords()).build());
    }

    @Operation(summary = "批量删除用户登录日志数据")
    @DeleteMapping("/delete")
    public Result<Boolean> delete(@RequestBody List<Long> ids) {
        if (loginLogService.deleteBatch(ids)) {
            return Result.ok();
        }
        return Result.failure(CommonErrorCode.INTERNAL_SERVER_ERROR, "用户登录日志数据删除失败");
    }

    @Operation(summary = "清空所有登录日志")
    @GetMapping("/clear")
    public Result<Boolean> clear() {
        // 清空登录日志
        loginLogService.clear();
        return Result.ok();
    }

}
