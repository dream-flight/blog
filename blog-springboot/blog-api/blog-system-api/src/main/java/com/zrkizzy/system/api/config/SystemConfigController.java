package com.zrkizzy.system.api.config;

import com.zrkizzy.common.core.domain.response.Result;
import com.zrkizzy.common.core.utils.bean.BeanCopyUtil;
import com.zrkizzy.common.models.dto.system.config.SystemConfigDTO;
import com.zrkizzy.common.models.enums.system.log.OperateType;
import com.zrkizzy.common.models.vo.system.config.SystemConfigVO;
import com.zrkizzy.system.facade.annotation.OperationLog;
import com.zrkizzy.system.facade.service.config.ISystemConfigService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * 系统配置数据返回对象
 *
 * @author zhangrongkang
 * @since 2023/7/21
 */
@Tag(name = "系统配置控制器")
@RestController
@RequestMapping("/admin/system-config")
public class SystemConfigController {

     @Autowired
     private ISystemConfigService systemConfigService;

    @Operation(summary = "获取所有系统配置")
    @GetMapping("/getSystemConfig")
    public Result<SystemConfigVO> getSystemConfig() {
        // 处理对应数据集合并返回数据
        SystemConfigVO configVO = BeanCopyUtil.copy(systemConfigService.getSystemConfig(), SystemConfigVO.class);
        return Result.ok(configVO);
    }
            
    @OperationLog(type = OperateType.UPDATE)
    @Operation(summary = "更新系统配置")
    @PostMapping("/save")
    public Result<Boolean> saveConfig(@RequestBody SystemConfigDTO systemConfigDTO) {
        // 保存系统配置数据
        return Result.ok(systemConfigService.saveConfig(systemConfigDTO));
    }

}
