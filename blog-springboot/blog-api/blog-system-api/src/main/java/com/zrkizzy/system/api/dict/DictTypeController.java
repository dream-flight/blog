package com.zrkizzy.system.api.dict;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.zrkizzy.common.core.domain.response.PageResult;
import com.zrkizzy.common.core.domain.response.Result;
import com.zrkizzy.common.core.enums.CommonErrorCode;
import com.zrkizzy.common.core.utils.bean.BeanCopyUtil;
import com.zrkizzy.common.models.domain.system.dict.DictType;
import com.zrkizzy.common.models.dto.system.dict.DictTypeDTO;
import com.zrkizzy.common.models.enums.system.log.OperateType;
import com.zrkizzy.common.models.query.system.dict.DictTypeQuery;
import com.zrkizzy.common.models.vo.system.dict.DictTypeOptionVO;
import com.zrkizzy.common.models.vo.system.dict.DictTypeVO;
import com.zrkizzy.system.facade.annotation.OperationLog;
import com.zrkizzy.system.facade.service.dict.IDictTypeService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * 字典类型数据返回对象
 *
 * @author zhangrongkang
 * @since 2024/1/16
 */
@Tag(name = "字典类型控制器")
@RestController
@RequestMapping("/admin/dict-type")
public class DictTypeController {

    @Autowired
    private IDictTypeService dictTypeService;

    @Operation(summary = "分页查询字典类型")
    @PostMapping("/list")
    public Result<PageResult<DictTypeVO>> listDictTypes(@RequestBody DictTypeQuery dictTypeQuery) {
        // 查询到对应的字典类型集合
        Page<DictType> dictTypePage = dictTypeService.listDictTypes(dictTypeQuery);
        // 处理对应数据集合并返回数据
        return Result.ok(PageResult.<DictTypeVO>builder().total(dictTypePage.getTotal())
            .list(BeanCopyUtil.copyList(dictTypePage.getRecords(), DictTypeVO.class)).build());
    }
            
    @Operation(summary = "编辑字典类型")
    @PostMapping("/save")
    @OperationLog(type = OperateType.UPDATE)
    public Result<Boolean> saveDictType(@RequestBody DictTypeDTO dictTypeDTO) {
        // 保存字典类型数据
        return Result.ok(dictTypeService.saveDictType(dictTypeDTO));
    }

    @Operation(summary = "获取指定字典类型信息")
    @GetMapping("/getDictTypeById/{dictTypeId}")
    public Result<DictTypeVO> getDictTypeById (@PathVariable Long dictTypeId) {
        return Result.ok(BeanCopyUtil.copy(dictTypeService.getDictTypeById(dictTypeId), DictTypeVO.class));
    }

    @Operation(summary = "批量删除字典类型数据")
    @DeleteMapping("/delete")
    public Result<Boolean> delete(@RequestBody List<Long> ids) {
        return dictTypeService.deleteBatch(ids) ? Result.ok() : Result.failure(CommonErrorCode.INTERNAL_SERVER_ERROR, "字典类型数据删除失败");
    }

    @Operation(summary = "获取字典类型选项")
    @GetMapping("/listOption")
    public Result<List<DictTypeOptionVO>> listOption() {
        List<DictTypeOptionVO> result = dictTypeService.listOption();
        return Result.ok(result);
    }

    @Operation(summary = "刷新字典数据缓存")
    @OperationLog(type = OperateType.UPDATE)
    @PutMapping("/refresh")
    public Result<Boolean> refresh() {
        dictTypeService.reloadDictCache();
        return Result.ok();
    }

}
