package com.zrkizzy.system.api.common;

import com.zrkizzy.common.core.domain.response.Result;
import com.zrkizzy.common.models.dto.system.message.MessageSenderDTO;
import com.zrkizzy.common.mq.service.IRabbitService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * 邮件控制器
 *
 * @author zhangrongkang
 * @since 2023/5/4
 */
@Tag(name = "消息发送控制器")
@RestController
@RequestMapping("/common/message")
public class MessageSenderController {

    @Autowired
    @Qualifier("messageSenderProducer")
    private IRabbitService rabbitService;

    @Operation(summary = "发送邮件/短信消息")
    @PostMapping("/sendMessage")
    public Result<Boolean> sendMessage(@Validated @RequestBody MessageSenderDTO messageSenderDTO) {
        // 直接调用发送消息方法
        rabbitService.sendMessage(messageSenderDTO);
        return Result.ok();
    }

}
