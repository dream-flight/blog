package com.zrkizzy.system.api.core;

import com.zrkizzy.common.core.domain.response.Result;
import com.zrkizzy.common.core.utils.bean.BeanCopyUtil;
import com.zrkizzy.common.models.domain.system.core.UserInfo;
import com.zrkizzy.common.models.vo.system.core.UserInfoVO;
import com.zrkizzy.common.models.vo.system.core.UserVO;
import com.zrkizzy.system.facade.service.core.IUserService;
import com.zrkizzy.system.facade.service.core.IUserInfoService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * 用户信息控制器
 *
 * @author zhangrongkang
 * @since 2023/7/18
 */
@Tag(name = "用户信息控制器")
@RestController
@RequestMapping("/admin/user-info")
public class UserInfoController {
    @Autowired
    private IUserService userService;
    @Autowired
    private IUserInfoService userInfoService;

    @Operation(summary = "用户个人信息")
    @GetMapping("/getUserInfo")
    public Result<UserInfoVO> getUserInfo () {
        // 获取用户信息
        return Result.ok(userInfoService.getUserInfo());
    }

    @Operation(summary = "获取指定用户信息")
    @GetMapping("/getUserInfoById/{id}")
    public Result<UserInfoVO> getUserInfoById(@PathVariable Long id) {
        // 拿到用户的具体信息
        UserVO userVO = userService.getUserById(id);
        UserInfoVO userInfoVO = BeanCopyUtil.copy(userVO, UserInfoVO.class);
        // 获取当前用户信息对象
        UserInfo userInfo = userInfoService.getUserInfoById(id);
        BeanCopyUtil.copy(userInfo, userInfoVO);
        return Result.ok(userInfoVO);
    }

}
