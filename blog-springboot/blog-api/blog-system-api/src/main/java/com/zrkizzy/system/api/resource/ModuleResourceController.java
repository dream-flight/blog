package com.zrkizzy.system.api.resource;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.zrkizzy.common.core.domain.response.PageResult;
import com.zrkizzy.common.core.domain.response.Result;
import com.zrkizzy.common.models.dto.system.resource.ModuleResourceDTO;
import com.zrkizzy.common.models.enums.system.log.OperateType;
import com.zrkizzy.common.models.query.system.resource.ModuleResourceQuery;
import com.zrkizzy.common.models.vo.system.resource.ModuleResourceVO;
import com.zrkizzy.common.models.vo.system.resource.ResourceTreeVO;
import com.zrkizzy.common.models.vo.system.resource.ResourceVO;
import com.zrkizzy.system.facade.annotation.OperationLog;
import com.zrkizzy.system.facade.service.resource.IModuleResourceService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

/**
 * 资源模块关联数据返回对象
 *
 * @author zhangrongkang
 * @since 2023/7/27
 */
@Tag(name = "资源模块关联控制器")
@RestController
@RequestMapping("/admin/module-resource")
public class ModuleResourceController {

     @Autowired
     private IModuleResourceService moduleResourceService;

     @Operation(summary = "分页获取指定模块请求资源")
     @PostMapping ("/list")
     public Result<PageResult<ResourceVO>> list(@Validated @RequestBody ModuleResourceQuery moduleResourceQuery) {
          Page<ResourceVO> resourcePage = moduleResourceService.listByModuleId(moduleResourceQuery);
          return Result.ok(PageResult.<ResourceVO>builder()
                  .total(resourcePage.getTotal())
                  .list(resourcePage.getRecords()).build());
     }

     @Operation(summary = "获取指定模块可以添加的接口")
     @GetMapping("/listResourceById/{id}")
     public Result<ModuleResourceVO> listById(@PathVariable Long id) {
          // 获取当前模块中可以选择的资源
          List<ResourceTreeVO> resourceTree = moduleResourceService.listResourceById(id);
          // 获取当前模块中已有的资源ID集合
          List<Long> ids = moduleResourceService.listCheckById(id);
          List<String> checkIds = new ArrayList<>();
          for (Long resourceId : ids) {
               checkIds.add(String.valueOf(resourceId));
          }
          return Result.ok(new ModuleResourceVO(resourceTree, checkIds));
     }

     @Operation(summary = "为指定模块分配资源请求")
     @PostMapping("/save")
     @OperationLog(type = OperateType.ADD)
     public Result<Boolean> save(@Validated @RequestBody ModuleResourceDTO moduleResourceDTO) {
          return Result.ok(moduleResourceService.save(moduleResourceDTO));
     }

     @Operation(summary = "批量删除模块对应请求资源")
     @DeleteMapping("/delete")
     @OperationLog(type = OperateType.DELETE)
     public Result<Boolean> delete(@RequestBody List<Long> ids) {
          // 删除对应数据并返回结果
          return Result.ok(moduleResourceService.delete(ids));
     }

}
