package com.zrkizzy.system.api.dict;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.zrkizzy.common.core.domain.response.PageResult;
import com.zrkizzy.common.core.domain.response.Result;
import com.zrkizzy.common.core.utils.bean.BeanCopyUtil;
import com.zrkizzy.common.models.domain.system.dict.DictData;
import com.zrkizzy.common.models.dto.system.dict.DictDataDTO;
import com.zrkizzy.common.models.enums.system.log.OperateType;
import com.zrkizzy.common.models.query.system.dict.DictDataQuery;
import com.zrkizzy.common.models.vo.system.dict.DictDataOptionVO;
import com.zrkizzy.common.models.vo.system.dict.DictDataVO;
import com.zrkizzy.system.facade.annotation.OperationLog;
import com.zrkizzy.system.facade.service.dict.IDictDataService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * 字典数据数据返回对象
 *
 * @author zhangrongkang
 * @since 2024/1/18
 */
@Tag(name = "字典数据控制器")
@RestController
@RequestMapping("/admin/dict-data")
public class DictDataController {

    @Autowired
    private IDictDataService dictDataService;

    @Operation(summary = "分页查询字典数据")
    @PostMapping("/list")
    public Result<PageResult<DictDataVO>> listDictData(@Validated @RequestBody DictDataQuery dictDataQuery) {
        // 查询到对应的字典数据集合
        Page<DictData> dictDataPage = dictDataService.listDictData(dictDataQuery);
        // 处理对应数据集合并返回数据
        return Result.ok(PageResult.<DictDataVO>builder().total(dictDataPage.getTotal())
            .list(BeanCopyUtil.copyList(dictDataPage.getRecords(), DictDataVO.class)).build());
    }

    @Operation(summary = "编辑字典数据")
    @PostMapping("/save")
    @OperationLog(type = OperateType.UPDATE)
    public Result<Boolean> saveDictData(@Validated @RequestBody DictDataDTO dictDataDTO) {
        // 保存字典数据数据
        return Result.ok(dictDataService.saveDictData(dictDataDTO));
    }

    @Operation(summary = "获取指定字典数据信息")
    @GetMapping("/getDictDataById/{dictDataId}")
    public Result<DictDataVO> getDictDataById (@PathVariable Long dictDataId) {
        return Result.ok(BeanCopyUtil.copy(dictDataService.getDictDataById(dictDataId), DictDataVO.class));
    }

    @Operation(summary = "批量删除字典数据")
    @OperationLog(type = OperateType.DELETE)
    @DeleteMapping("/delete")
    public Result<Boolean> delete(@RequestBody List<Long> ids) {
        return Result.ok(dictDataService.deleteBatch(ids));
    }

    @Operation(summary = "获取字典类型对应数据")
    @GetMapping("/type/{dictType}")
    public Result<List<DictDataOptionVO>> getDictDataByType(@PathVariable String dictType) {
        List<DictDataOptionVO> result = dictDataService.listDictDataByType(dictType);
        return Result.ok(result);
    }
}
