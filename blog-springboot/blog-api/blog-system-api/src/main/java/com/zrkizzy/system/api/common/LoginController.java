package com.zrkizzy.system.api.common;

import com.zrkizzy.common.core.domain.response.Result;
import com.zrkizzy.common.models.dto.system.common.LoginDTO;
import com.zrkizzy.system.facade.annotation.LoginLog;
import com.zrkizzy.system.facade.service.common.ILoginService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * 用户登录控制器
 *
 * @author zhangrongkang
 * @since 2023/4/12
 */
@Tag(name = "用户登录控制器")
@RestController
@RequestMapping("/common/login")
public class LoginController {

    @Autowired
    private ILoginService loginService;

    /**
     * 用户登录返回Token
     *
     * @param loginDTO 用户登录数据传输对象
     * @return 公共返回对象（Token）
     */
    @LoginLog
    @Operation(summary = "后台用户登录")
    @PostMapping("/user")
    public Result<String> login(@RequestBody @Validated LoginDTO loginDTO) {
        return Result.ok(loginService.processLogin(loginDTO));
    }
}
