package com.zrkizzy.system.api;

import com.zrkizzy.common.core.domain.response.Result;
import com.zrkizzy.common.core.utils.bean.BeanCopyUtil;
import com.zrkizzy.common.models.dto.system.common.NoticeDTO;
import com.zrkizzy.common.models.vo.system.common.NoticeVO;
import com.zrkizzy.system.facade.service.common.INoticeService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * 通知公告管理控制器
 *
 * @author zhangrongkang
 * @since 2024/3/29
 */
@Tag(name = "通知公告管理控制器")
@RestController
@RequestMapping("/admin/notice")
public class NoticeManageController {

    @Autowired
    private INoticeService noticeService;

    @Operation(summary = "编辑通知公告")
    @PostMapping("/save")
    public Result<Boolean> saveNotice(@RequestBody NoticeDTO noticeDTO) {
        // 保存通知公告数据
        return Result.ok(noticeService.saveNotice(noticeDTO));
    }

    @Operation(summary = "获取指定通知公告信息")
    @GetMapping("/getNoticeById/{noticeId}")
    public Result<NoticeVO> getNoticeById (@PathVariable Long noticeId) {
        return Result.ok(BeanCopyUtil.copy(noticeService.getNoticeById(noticeId), NoticeVO.class));
    }

    @Operation(summary = "批量删除通知公告数据")
    @DeleteMapping("/delete")
    public Result<Boolean> delete(@RequestBody List<Long> ids) {
        return Result.ok(noticeService.deleteBatch(ids));
    }

}
