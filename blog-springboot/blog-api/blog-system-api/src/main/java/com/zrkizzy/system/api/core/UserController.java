package com.zrkizzy.system.api.core;

import com.zrkizzy.common.core.domain.response.OptionsVO;
import com.zrkizzy.common.core.domain.response.PageResult;
import com.zrkizzy.common.core.domain.response.Result;
import com.zrkizzy.common.models.dto.system.core.AvatarDTO;
import com.zrkizzy.common.models.dto.system.core.PasswordDTO;
import com.zrkizzy.common.models.dto.system.core.UserDTO;
import com.zrkizzy.common.models.dto.system.core.UserUpdateDTO;
import com.zrkizzy.common.models.dto.system.message.MessageSenderDTO;
import com.zrkizzy.common.models.enums.system.log.OperateType;
import com.zrkizzy.common.models.enums.system.message.MessageSenderChannel;
import com.zrkizzy.common.models.enums.system.message.MessageSenderType;
import com.zrkizzy.common.models.query.system.core.UserQuery;
import com.zrkizzy.common.models.vo.system.core.UserVO;
import com.zrkizzy.common.mq.service.IRabbitService;
import com.zrkizzy.module.security.utils.SecurityUtil;
import com.zrkizzy.system.facade.annotation.OperationLog;
import com.zrkizzy.system.facade.service.core.IUserService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * 用户管理控制器
 *
 * @author zhangrongkang
 * @since 2023/3/7
 */
@Tag(name = "用户管理控制器")
@RestController
@RequestMapping("/admin/user")
public class UserController {
    @Autowired
    private SecurityUtil securityUtil;

    @Autowired
    @Qualifier("messageSenderProducer")
    private IRabbitService rabbitService;

    @Autowired
    private IUserService userService;

    @Operation(summary = "获取当前登录用户")
    @GetMapping("/getLoginUser")
    public Result<UserVO> getLoginUser() {
        // 获取用户ID
        Long userId = securityUtil.getLoginUser().getId();
        // 返回数据
        return Result.ok(userService.getUserById(userId));
    }

    @Operation(summary = "更新用户个人信息")
    @PutMapping("/updateLoginUser")
    @OperationLog(type = OperateType.UPDATE)
    public Result<Integer> updateLoginUser(@RequestBody @Validated UserUpdateDTO userUpdateDTO) {
        return Result.ok(userService.updateLoginUser(userUpdateDTO));
    }

    @Operation(summary = "更新登录用户头像")
    @PutMapping("/updateLoginUserAvatar")
    public Result<String> updateLoginUserAvatar(@RequestBody @Validated AvatarDTO avatarDTO) {
        return Result.ok(userService.updateLoginUserAvatar(avatarDTO));
    }

    @Operation(summary = "更新用户密码")
    @PutMapping("/updatePassword")
    public Result<Integer> updatePassword(@RequestBody @Validated PasswordDTO passwordDTO) {
        // 更新用户密码
        return Result.ok(userService.updatePassword(passwordDTO));
    }

    @Operation(summary = "获取所有用户")
    @PostMapping("/list")
    public Result<PageResult<UserVO>> listUsers(@RequestBody UserQuery userQuery) {
        return Result.ok(userService.listUsers(userQuery));
    }

    @Operation(summary = "获取用户选项")
    @GetMapping("/listUserOptions")
    public Result<List<OptionsVO>> listUserOptions() {
        return Result.ok(userService.listUserOptions());
    }

    @Operation(summary = "新增用户")
    @PostMapping("/insert")
    @OperationLog(type = OperateType.ADD)
    public Result<Boolean> insert(@Validated @RequestBody UserDTO userDTO) {
        // 新增用户
        return Result.ok(userService.insert(userDTO));
    }

    @Operation(summary = "更新指定用户")
    @PutMapping("/updateUser")
    @OperationLog(type = OperateType.UPDATE)
    public Result<Boolean> updateUser(@RequestBody UserUpdateDTO userUpdateDTO) {
        // 更新用户并返回结果
        return Result.ok(userService.updateUser(userUpdateDTO));
    }

    @Operation(summary = "更新用户状态")
    @GetMapping("/updateUserStatus/{id}")
    @OperationLog(type = OperateType.UPDATE)
    public Result<Boolean> updateUserStatus(@PathVariable Long id) {
        return Result.ok(userService.updateUserStatus(id));
    }

    @Operation(summary = "重置指定用户密码")
    @GetMapping("/resetPassword/{id}")
    @OperationLog(type = OperateType.UPDATE)
    public Result<Boolean> resetPassword(@PathVariable Long id) {
        return Result.ok(userService.resetPassword(id));
    }

    @Operation(summary = "批量删除用户")
    @DeleteMapping("/delete")
    @OperationLog(type = OperateType.DELETE)
    public Result<Boolean> delete(@RequestBody List<Long> ids) {
        return Result.ok(userService.delete(ids));
    }

    @Operation(summary = "修改密码验证码")
    @GetMapping("/password")
    public Result<Boolean> password() {
        // 定义邮件发送对象
        MessageSenderDTO messageSenderDTO = MessageSenderDTO.builder()
                // 发送方式
                .channel(MessageSenderChannel.EMAIL)
                // 收件人
                .senderTo(securityUtil.getLoginUsername())
                // 收件人昵称
                .nickname(securityUtil.getLoginUserNickname())
                // 邮件类型标识
                .type(MessageSenderType.CHANGE_PASSWORD)
                // 邮件主题
                .subject("验证身份").build();
        // 将邮件数据传输对象发送到RabbitMQ中
        rabbitService.sendMessage(messageSenderDTO);
        // 返回成功结果，如果失败则会被全局异常处理捕获
        return Result.ok();
    }

}
