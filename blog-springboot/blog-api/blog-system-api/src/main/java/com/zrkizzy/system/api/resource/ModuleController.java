package com.zrkizzy.system.api.resource;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.zrkizzy.common.core.domain.response.OptionsVO;
import com.zrkizzy.common.core.domain.response.PageResult;
import com.zrkizzy.common.core.domain.response.Result;
import com.zrkizzy.common.core.utils.bean.BeanCopyUtil;
import com.zrkizzy.common.models.domain.system.resource.Module;
import com.zrkizzy.common.models.dto.system.resource.ModuleDTO;
import com.zrkizzy.common.models.enums.system.log.OperateType;
import com.zrkizzy.common.models.query.system.resource.ModuleQuery;
import com.zrkizzy.common.models.vo.system.resource.ModuleVO;
import com.zrkizzy.module.system.exception.SystemErrorCode;
import com.zrkizzy.system.facade.annotation.OperationLog;
import com.zrkizzy.system.facade.service.resource.IModuleService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * 请求模块控制器
 *
 * @author zhangrongkang
 * @since 2023/7/3
 */
@Tag(name = "请求模块控制器")
@RestController
@RequestMapping("/admin/module")
public class ModuleController {
    @Autowired
    private IModuleService moduleService;

    @Operation(summary = "获取请求模块选项")
    @GetMapping("/listModuleOptions")
    public Result<List<OptionsVO>> listModuleOptions() {
        return Result.ok(moduleService.listModuleOptions());
    }

    @Operation(summary = "分页查询资源模块")
    @PostMapping("/list")
    public Result<PageResult<ModuleVO>> listModules(@RequestBody ModuleQuery moduleQuery) {
        Page<Module> modulePage = moduleService.listModules(moduleQuery);
        return Result.ok(PageResult.<ModuleVO>builder()
                .list(BeanCopyUtil.copyList(modulePage.getRecords(), ModuleVO.class))
                .total(modulePage.getTotal())
                .build());
    }

    @Operation(summary = "添加-更新资源模块")
    @PostMapping("/save")
    public Result<Boolean> saveModule(@RequestBody ModuleDTO moduleDTO) {
        // 保存资源模块数据
        return Result.ok(moduleService.saveModule(moduleDTO));
    }

    @Operation(summary = "获取指定资源模块信息")
    @GetMapping("/getModuleById/{moduleId}")
    public Result<ModuleVO> getModuleById (@PathVariable Long moduleId) {
        return Result.ok(BeanCopyUtil.copy(moduleService.getModuleById(moduleId), ModuleVO.class));
    }

    @Operation(summary = "批量删除资源模块数据")
    @DeleteMapping("/delete")
    @OperationLog(type = OperateType.DELETE)
    public Result<Boolean> delete(@RequestBody List<Long> ids) {
        return moduleService.deleteBatch(ids) ? Result.ok() : Result.failure(SystemErrorCode.MODULE_RESOURCE_DELETE);
    }

}
