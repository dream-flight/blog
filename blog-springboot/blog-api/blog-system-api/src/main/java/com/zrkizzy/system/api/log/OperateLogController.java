package com.zrkizzy.system.api.log;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.zrkizzy.common.core.domain.response.PageResult;
import com.zrkizzy.common.core.domain.response.Result;
import com.zrkizzy.common.core.enums.CommonErrorCode;
import com.zrkizzy.common.models.query.system.monitor.OperateLogQuery;
import com.zrkizzy.common.models.vo.system.log.OperateLogVO;
import com.zrkizzy.system.facade.service.log.IOperateLogService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * 操作日志数据返回对象
 *
 * @author zhangrongkang
 * @since 2023/7/3
 */
@Tag(name = "操作日志控制器")
@RestController
@RequestMapping("/admin/operate-log")
public class OperateLogController {

     @Autowired
     private IOperateLogService operateLogService;

    @Operation(summary = "获取所有操作日志")
    @PostMapping("/list")
    public Result<PageResult<OperateLogVO>> listOperateLogs(@RequestBody OperateLogQuery operateLogQuery) {
        // 查询到对应的操作日志集合
        Page<OperateLogVO> operateLogPage = operateLogService.listOperateLogs(operateLogQuery);
        // 处理对应数据集合并返回数据
        return Result.ok(PageResult.<OperateLogVO>builder().total(operateLogPage.getTotal())
            // 操作日志集合
            .list(operateLogPage.getRecords()).build());
    }

    @Operation(summary = "清空日志操作信息")
    @GetMapping("/clear")
    public Result<Boolean> clearOperateLogs () {
        // 清空请求日志
        operateLogService.clearOperateLogs();
        return Result.ok();
    }

    @Operation(summary = "批量删除操作日志数据")
    @DeleteMapping("/delete")
    public Result<Boolean> delete(@RequestBody List<Long> ids) {
        if (operateLogService.deleteBatch(ids)) {
            return Result.ok();
        }
        return Result.failure(CommonErrorCode.INTERNAL_SERVER_ERROR, "操作日志数据删除失败");
    }

}
