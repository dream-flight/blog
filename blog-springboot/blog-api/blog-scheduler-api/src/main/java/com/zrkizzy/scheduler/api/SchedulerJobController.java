package com.zrkizzy.scheduler.api;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.zrkizzy.common.core.domain.response.PageResult;
import com.zrkizzy.common.core.domain.response.Result;
import com.zrkizzy.common.core.utils.bean.BeanCopyUtil;
import com.zrkizzy.common.models.domain.system.scheduler.SchedulerJob;
import com.zrkizzy.common.models.dto.system.scheduler.SchedulerJobDTO;
import com.zrkizzy.common.models.dto.system.scheduler.SchedulerJobStatusDTO;
import com.zrkizzy.common.models.enums.system.log.OperateType;
import com.zrkizzy.common.models.query.system.scheduler.SchedulerJobQuery;
import com.zrkizzy.common.models.vo.system.scheduler.SchedulerJobVO;
import com.zrkizzy.scheduler.facade.service.ISchedulerJobService;
import com.zrkizzy.system.facade.annotation.OperationLog;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * 定时任务数据返回对象
 *
 * @author zhangrongkang
 * @since 2024/2/23
 */
@Tag(name = "定时任务控制器")
@RestController
@RequestMapping("/admin/scheduler-job")
public class SchedulerJobController {

    @Autowired
    private ISchedulerJobService schedulerJobService;

    @Operation(summary = "分页查询定时任务")
    @PostMapping("/list")
    public Result<PageResult<SchedulerJobVO>> listSchedulerJob(@RequestBody SchedulerJobQuery schedulerJobQuery) {
        // 查询到对应的定时任务集合
        Page<SchedulerJob> schedulerJobPage = schedulerJobService.listSchedulerJob(schedulerJobQuery);
        // 处理对应数据集合并返回数据
        return Result.ok(PageResult.<SchedulerJobVO>builder().total(schedulerJobPage.getTotal())
            .list(BeanCopyUtil.copyList(schedulerJobPage.getRecords(), SchedulerJobVO.class)).build());
    }

    @Operation(summary = "编辑定时任务")
    @PostMapping("/save")
    @OperationLog(type = OperateType.UPDATE)
    public Result<Boolean> saveSchedulerJob(@RequestBody SchedulerJobDTO schedulerJobDTO) {
        // 保存定时任务数据
        return Result.ok(schedulerJobService.saveSchedulerJob(schedulerJobDTO));
    }

    @Operation(summary = "获取指定定时任务信息")
    @GetMapping("/getSchedulerJobById/{schedulerJobId}")
    public Result<SchedulerJobVO> getSchedulerJobById (@PathVariable Long schedulerJobId) {
        return Result.ok(BeanCopyUtil.copy(schedulerJobService.getSchedulerJobById(schedulerJobId), SchedulerJobVO.class));
    }

    @Operation(summary = "批量删除定时任务数据")
    @DeleteMapping("/delete")
    @OperationLog(type = OperateType.DELETE)
    public Result<Boolean> delete(@RequestBody List<Long> ids) {
        schedulerJobService.deleteBatch(ids);
        return Result.ok();
    }

    @Operation(summary = "修改定时任务状态")
    @PutMapping("/changeStatus")
    @OperationLog(type = OperateType.UPDATE)
    public Result<Boolean> updateStatus(@RequestBody SchedulerJobStatusDTO schedulerJobStatusDTO) {
        return Result.ok(schedulerJobService.changeJobStatus(schedulerJobStatusDTO));
    }

    @Operation(summary = "执行一次定时任务")
    @PutMapping("/run/{jobId}")
    @OperationLog(type = OperateType.UPDATE)
    public Result<Boolean> run(@PathVariable Long jobId) {
        return Result.ok(schedulerJobService.run(jobId));
    }

}
