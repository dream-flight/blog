package com.zrkizzy.scheduler.api;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.zrkizzy.common.core.domain.response.PageResult;
import com.zrkizzy.common.core.domain.response.Result;
import com.zrkizzy.common.core.utils.bean.BeanCopyUtil;
import com.zrkizzy.common.models.domain.system.scheduler.SchedulerJobLog;
import com.zrkizzy.common.models.query.system.scheduler.SchedulerJobLogQuery;
import com.zrkizzy.scheduler.facade.service.ISchedulerJobLogService;
import com.zrkizzy.common.models.vo.system.scheduler.SchedulerJobLogVO;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * 定时任务调度日志数据返回对象
 *
 * @author zhangrongkang
 * @since 2024/3/6
 */
@Tag(name = "定时任务调度日志控制器")
@RestController
@RequestMapping("/admin/scheduler-job-log")
public class SchedulerJobLogController {

    @Autowired
    private ISchedulerJobLogService schedulerJobLogService;

    @Operation(summary = "分页查询定时任务调度日志")
    @PostMapping("/list")
    public Result<PageResult<SchedulerJobLogVO>> listSchedulerJobLog(@RequestBody SchedulerJobLogQuery schedulerJobLogQuery) {
        // 查询到对应的定时任务调度日志集合
        Page<SchedulerJobLog> schedulerJobLogPage = schedulerJobLogService.listSchedulerJobLogs(schedulerJobLogQuery);
        // 处理对应数据集合并返回数据
        return Result.ok(PageResult.<SchedulerJobLogVO>builder().total(schedulerJobLogPage.getTotal())
            .list(BeanCopyUtil.copyList(schedulerJobLogPage.getRecords(), SchedulerJobLogVO.class)).build());
    }

    @Operation(summary = "获取指定定时任务调度日志信息")
    @GetMapping("/getSchedulerJobLogById/{schedulerJobLogId}")
    public Result<SchedulerJobLogVO> getSchedulerJobLogById (@PathVariable Long schedulerJobLogId) {
        return Result.ok(BeanCopyUtil.copy(schedulerJobLogService.getSchedulerJobLogById(schedulerJobLogId), SchedulerJobLogVO.class));
    }

    @Operation(summary = "批量删除定时任务调度日志数据")
    @DeleteMapping("/delete")
    public Result<Boolean> delete(@RequestBody List<Long> ids) {
        return Result.ok(schedulerJobLogService.deleteBatch(ids));
    }

}
