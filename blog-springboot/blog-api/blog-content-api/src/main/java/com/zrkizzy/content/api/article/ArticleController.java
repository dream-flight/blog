package com.zrkizzy.content.api.article;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.zrkizzy.common.core.domain.response.PageResult;
import com.zrkizzy.common.core.domain.response.Result;
import com.zrkizzy.common.core.utils.bean.BeanCopyUtil;
import com.zrkizzy.common.models.domain.blog.article.Article;
import com.zrkizzy.common.models.dto.blog.article.ArticleDTO;
import com.zrkizzy.common.models.dto.blog.article.ArticleSettingsDTO;
import com.zrkizzy.common.models.query.blog.article.ArticleQuery;
import com.zrkizzy.common.models.vo.blog.article.ArticleMetaVO;
import com.zrkizzy.common.models.vo.blog.article.ArticleVO;
import com.zrkizzy.content.facade.service.article.IArticleCategoryService;
import com.zrkizzy.content.facade.service.article.IArticleService;
import com.zrkizzy.content.facade.service.article.IArticleTagsService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * 文章数据返回对象
 *
 * @author zhangrongkang
 * @since 2023/9/18
 */
@Tag(name = "文章控制器")
@RestController
@RequestMapping("/admin/article")
public class ArticleController {

     @Autowired
     private IArticleService articleService;

     @Autowired
     private IArticleCategoryService articleCategoryService;

     @Autowired
     private IArticleTagsService articleTagsService;

    @Operation(summary = "获取所有文章")
    @PostMapping("/list")
    public Result<PageResult<ArticleMetaVO>> listArticles(@RequestBody ArticleQuery articleQuery) {
        // 查询到对应的文章集合
        Page<Article> articlePage = articleService.listArticles(articleQuery);
        // 处理对应数据集合并返回数据
        return Result.ok(PageResult.<ArticleMetaVO>builder().total(articlePage.getTotal())
            // 文章集合
            .list(BeanCopyUtil.copyList(articlePage.getRecords(), ArticleMetaVO.class)).build());
    }

    @Operation(summary = "编辑文章")
    @PostMapping("/save")
    public Result<String> saveArticle(@Validated @RequestBody ArticleDTO articleDTO) {
        // 保存文章数据
        return Result.ok(articleService.saveArticle(articleDTO));
    }

    @Operation(summary = "获取指定文章信息")
    @GetMapping("/getArticleById/{articleId}")
    public Result<ArticleVO> getArticleById (@PathVariable Long articleId) {
        // 获取转换文章数据返回对象
        ArticleVO articleVO = BeanCopyUtil.copy(articleService.getArticleById(articleId), ArticleVO.class);
        // 获取当前文章对应标签和分类
        articleVO.setCategoryId(articleCategoryService.getCategoryIdByArticleId(articleId));
        articleVO.setTags(articleTagsService.listTagIdsByArticleId(articleId));
        return Result.ok(articleVO);
    }

    @Operation(summary = "批量删除文章")
    @DeleteMapping("/delete")
    public Result<Boolean> delete(@RequestBody List<Long> ids) {
        return Result.ok(articleService.deleteBatch(ids));
    }

    @Operation(summary = "修改文章设置属性")
    @PostMapping("/updateSettings")
    public Result<Boolean> updateSettings(@RequestBody ArticleSettingsDTO articleSettingsDTO) {
        return Result.ok(articleService.updateSettings(articleSettingsDTO));
    }

}
