package com.zrkizzy.content.api.index;

import com.zrkizzy.common.core.domain.response.Result;
import com.zrkizzy.common.models.vo.blog.dashboard.HomeVO;
import com.zrkizzy.content.facade.service.index.IHomeService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * 仪表盘控制器
 *
 * @author zhangrongkang
 * @since 2023/7/23
 */
@Tag(name = "仪表盘控制器")
@RestController
@RequestMapping("/admin/index")
public class IndexController {
    @Autowired
    private IHomeService homeService;

    @Operation(summary = "获取首页信息")
    @GetMapping("/getHomeInfo")
    public Result<HomeVO> getHomeInfo() {
        return Result.ok(homeService.getHomeInfo());
    }

}
