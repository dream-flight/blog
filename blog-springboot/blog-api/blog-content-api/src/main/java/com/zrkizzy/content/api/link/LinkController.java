package com.zrkizzy.content.api.link;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.zrkizzy.common.core.domain.response.PageResult;
import com.zrkizzy.common.core.domain.response.Result;
import com.zrkizzy.common.core.utils.bean.BeanCopyUtil;
import com.zrkizzy.common.models.domain.blog.link.Link;
import com.zrkizzy.common.models.dto.blog.link.LinkDTO;
import com.zrkizzy.common.models.query.blog.link.LinkQuery;
import com.zrkizzy.common.models.vo.blog.link.LinkVO;
import com.zrkizzy.content.facade.service.link.ILinkService;
import com.zrkizzy.system.facade.annotation.OperationLog;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * 友情链接数据返回对象
 *
 * @author zhangrongkang
 * @since 2023/6/19
 */
@Tag(name = "友情链接控制器")
@RestController
@RequestMapping("/admin/link")
public class LinkController {

     @Autowired
     private ILinkService linkService;

    @OperationLog
    @Operation(summary = "获取所有友情链接")
    @PostMapping("/list")
    public Result<PageResult<LinkVO>> listLinks(@RequestBody LinkQuery linkQuery) {
        // 查询到对应的友情链接集合
        Page<Link> linkPage = linkService.listLinks(linkQuery);
        // 处理对应数据集合并返回数据
        return Result.ok(PageResult.<LinkVO>builder().total(linkPage.getTotal())
            // 友情链接集合
            .list(BeanCopyUtil.copyList(linkPage.getRecords(), LinkVO.class)).build());
    }
            
    @Operation(summary = "添加-更新友情链接") 
    @PostMapping("/save")
    public Result<Boolean> saveLink(@RequestBody LinkDTO linkDTO) {
        // 保存友情链接数据
        return Result.ok(linkService.saveLink(linkDTO));
    }

    @Operation(summary = "获取指定友情链接信息")
    @GetMapping("/getLinkById/{linkId}")
    public Result<LinkVO> getLinkById (@PathVariable Long linkId) {
        return Result.ok(BeanCopyUtil.copy(linkService.getLinkById(linkId), LinkVO.class));
    }

    @Operation(summary = "批量删除友情链接数据")
    @DeleteMapping("/delete")
    public Result<Boolean> delete(@RequestBody List<Long> ids) {
        // 批量删除友情链接并根据删除结果返回相应提示信息
        return Result.ok(linkService.deleteBatch(ids));
    }

}
