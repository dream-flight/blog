package com.zrkizzy.content.api.category;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.zrkizzy.common.core.domain.response.OptionsVO;
import com.zrkizzy.common.core.domain.response.PageResult;
import com.zrkizzy.common.core.domain.response.Result;
import com.zrkizzy.common.core.utils.bean.BeanCopyUtil;
import com.zrkizzy.common.models.domain.blog.category.BlogCategory;
import com.zrkizzy.common.models.dto.blog.category.BlogCategoryDTO;
import com.zrkizzy.common.models.query.blog.category.BlogCategoryQuery;
import com.zrkizzy.common.models.vo.blog.category.BlogCategoryVO;
import com.zrkizzy.content.facade.service.category.IBlogCategoryService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * 博客分类数据返回对象
 *
 * @author zhangrongkang
 * @since 2023/9/4
 */
@Tag(name = "博客分类控制器")
@RestController
@RequestMapping("/admin/blog-category")
public class BlogCategoryController {

     @Autowired
     private IBlogCategoryService categoryService;

    @Operation(summary = "获取所有博客分类")
    @PostMapping("/list")
    public Result<PageResult<BlogCategoryVO>> listBlogCategories(@RequestBody BlogCategoryQuery categoryQuery) {
        // 查询到对应的分类集合
        Page<BlogCategory> categoryPage = categoryService.listBlogCategories(categoryQuery);
        // 处理对应数据集合并返回数据
        return Result.ok(PageResult.<BlogCategoryVO>builder().total(categoryPage.getTotal())
            // 分类集合
            .list(BeanCopyUtil.copyList(categoryPage.getRecords(), BlogCategoryVO.class)).build());
    }
            
    @Operation(summary = "编辑博客分类")
    @PostMapping("/save")
    public Result<Boolean> saveBlogCategory(@RequestBody BlogCategoryDTO categoryDTO) {
        // 保存分类数据
        return Result.ok(categoryService.saveBlogCategory(categoryDTO));
    }

    @Operation(summary = "获取指定博客分类")
    @GetMapping("/getCategoryById/{categoryId}")
    public Result<BlogCategoryVO> getBlogCategoryById (@PathVariable Long categoryId) {
        return Result.ok(BeanCopyUtil.copy(categoryService.getBlogCategoryById(categoryId), BlogCategoryVO.class));
    }

    @Operation(summary = "批量删除博客分类")
    @DeleteMapping("/delete")
    public Result<Boolean> delete(@RequestBody List<Long> ids) {
        // 根据删除结果返回相应信息
        return Result.ok(categoryService.deleteBatch(ids));
    }

    @Operation(summary = "获取博客分类选项")
    @GetMapping("/listOptions")
    public Result<List<OptionsVO>> listOptions() {
        // 获取所有博客分类
        return Result.ok(categoryService.listOptions());
    }

}
