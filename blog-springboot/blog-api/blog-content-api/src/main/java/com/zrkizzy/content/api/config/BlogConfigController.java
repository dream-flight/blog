package com.zrkizzy.content.api.config;

import com.zrkizzy.common.core.domain.response.Result;
import com.zrkizzy.common.core.utils.bean.BeanCopyUtil;
import com.zrkizzy.common.models.dto.system.config.BlogConfigDTO;
import com.zrkizzy.common.models.vo.system.config.BlogConfigVO;
import com.zrkizzy.content.facade.service.config.IBlogConfigService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * 博客配置控制器
 *
 * @author zhangrongkang
 * @since 2023/8/24
 */
@Tag(name = "博客配置控制器")
@RestController
@RequestMapping("/admin/blog-config")
public class BlogConfigController {

    @Autowired
    private IBlogConfigService blogConfigService;

    @Operation(summary = "获取博客配置")
    @GetMapping("/getBlogConfig")
    public Result<BlogConfigVO> getBlogConfig() {
        // 获取博客配置信息
        return Result.ok(BeanCopyUtil.copy(blogConfigService.getBlogConfig(), BlogConfigVO.class));
    }

    @Operation(summary = "更新博客配置")
    @PostMapping("/save")
    public Result<BlogConfigVO> save(@RequestBody BlogConfigDTO blogConfigDTO) {
        // 更新博客配置
        BlogConfigVO blogConfigVO = BeanCopyUtil.copy(blogConfigService.save(blogConfigDTO), BlogConfigVO.class);
        // 返回更新结果
        return Result.ok(blogConfigVO);
    }

}
