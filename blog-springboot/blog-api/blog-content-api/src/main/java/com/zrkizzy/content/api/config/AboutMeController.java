package com.zrkizzy.content.api.config;

import com.zrkizzy.common.core.domain.response.Result;
import com.zrkizzy.common.core.utils.bean.BeanCopyUtil;
import com.zrkizzy.common.models.domain.blog.article.AboutMe;
import com.zrkizzy.common.models.dto.blog.article.AboutMeDTO;
import com.zrkizzy.common.models.vo.blog.article.AboutMeVO;
import com.zrkizzy.content.facade.service.config.IAboutMeService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * 关于我控制器
 *
 * @author zhangrongkang
 * @since 2023/9/8
 */
@Tag(name = "关于我控制器")
@RestController
@RequestMapping("/admin/about-me")
public class AboutMeController {

    @Autowired
    private IAboutMeService aboutMeService;

    @Operation(summary = "获取关于我内容")
    @GetMapping("/getAboutMe")
    public Result<AboutMeVO> getAboutMe() {
        AboutMe aboutMe = aboutMeService.getAboutMe();
        return Result.ok(BeanCopyUtil.copy(aboutMe, AboutMeVO.class));
    }

    @Operation(summary = "更新关于我内容")
    @PostMapping("/update")
    public Result<Boolean> update(@RequestBody AboutMeDTO aboutMeDTO) {
        return Result.ok(aboutMeService.update(aboutMeDTO));
    }
}
