package com.zrkizzy.content.api.tag;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.zrkizzy.common.core.domain.response.OptionsVO;
import com.zrkizzy.common.core.domain.response.PageResult;
import com.zrkizzy.common.core.domain.response.Result;
import com.zrkizzy.common.core.utils.bean.BeanCopyUtil;
import com.zrkizzy.common.models.domain.blog.tag.BlogTags;
import com.zrkizzy.common.models.dto.blog.tag.BlogTagsDTO;
import com.zrkizzy.common.models.query.blog.tag.BlogTagsQuery;
import com.zrkizzy.common.models.vo.blog.tag.BlogTagsVO;
import com.zrkizzy.content.facade.service.tag.IBlogTagsService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * 博客标签数据返回对象
 *
 * @author zhangrongkang
 * @since 2023/9/5
 */
@Tag(name = "博客标签控制器")
@RestController
@RequestMapping("/admin/blog-tags")
public class BlogTagsController {

     @Autowired
     private IBlogTagsService blogTagsService;

    @Operation(summary = "获取所有博客标签")
    @PostMapping("/list")
    public Result<PageResult<BlogTagsVO>> listBlogTags(@RequestBody BlogTagsQuery blogTagsQuery) {
        // 查询到对应的博客标签集合
        Page<BlogTags> blogTagsPage = blogTagsService.listBlogTags(blogTagsQuery);
        // 处理对应数据集合并返回数据
        return Result.ok(PageResult.<BlogTagsVO>builder().total(blogTagsPage.getTotal())
            // 博客标签集合
            .list(BeanCopyUtil.copyList(blogTagsPage.getRecords(), BlogTagsVO.class)).build());
    }
            
    @Operation(summary = "添加-更新博客标签") 
    @PostMapping("/save")
    public Result<Boolean> saveBlogTags(@Validated @RequestBody BlogTagsDTO blogTagsDTO) {
        // 保存博客标签数据
        return Result.ok(blogTagsService.saveBlogTags(blogTagsDTO));
    }

    @Operation(summary = "获取指定博客标签信息")
    @GetMapping("/getBlogTagsById/{blogTagsId}")
    public Result<BlogTagsVO> getBlogTagsById (@PathVariable Long blogTagsId) {
        return Result.ok(BeanCopyUtil.copy(blogTagsService.getBlogTagsById(blogTagsId), BlogTagsVO.class));
    }

    @Operation(summary = "批量删除博客标签数据")
    @DeleteMapping("/delete")
    public Result<Boolean> delete(@RequestBody List<Long> ids) {
        return Result.ok(blogTagsService.deleteBatch(ids));
    }

    @Operation(summary = "获取博客所有标签选项")
    @GetMapping("/listOptions")
    public Result<List<OptionsVO>> listOptions() {
        return Result.ok(blogTagsService.listOptions());
    }

}
