package com.zrkizzy.monitor.api.cache;

import com.zrkizzy.common.core.domain.response.Result;
import com.zrkizzy.common.models.enums.system.log.OperateType;
import com.zrkizzy.common.models.vo.system.monitor.CacheInfoVO;
import com.zrkizzy.common.models.vo.system.monitor.CacheKeyVO;
import com.zrkizzy.common.models.vo.system.monitor.CacheTypeVO;
import com.zrkizzy.common.redis.service.IRedisService;
import com.zrkizzy.monitor.facade.service.cache.ICacheService;
import com.zrkizzy.system.facade.annotation.OperationLog;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;

/**
 * 缓存管理控制器
 *
 * @author zhangrongkang
 * @since 2023/7/13
 */
@Tag(name = "缓存管理控制器")
@RestController
@RequestMapping("/admin/cache")
public class CacheController {
    @Autowired
    private IRedisService redisService;
    @Autowired
    private ICacheService cacheService;

    @Operation(summary = "获取所有缓存键前缀")
    @GetMapping("/listCacheType")
    public Result<List<CacheTypeVO>> listRedisKeyType() {
        return Result.ok(cacheService.listRedisKeyType());
    }

    @Operation(summary = "获取所有缓存键")
    @GetMapping("/listCacheKeys/{type}")
    public Result<List<CacheKeyVO>> listCacheKeys(@PathVariable String type) {
        return Result.ok(cacheService.listCacheKeys(type));
    }

    @Operation(summary = "获取指定缓存")
    @GetMapping("/getCacheInfoByKey/{key}")
    public Result<CacheInfoVO> getCacheInfoByKey(@PathVariable String key) {
        return Result.ok(cacheService.getCacheInfoByKey(key));
    }

    @Operation(summary = "清理缓存列表")
    @OperationLog(type = OperateType.DELETE)
    @DeleteMapping("/clearCacheKeys/{type}")
    public Result<Boolean> clearCacheKeys(@PathVariable String type) {
        cacheService.clearCacheKeys(type);
        return Result.ok();
    }

    @Operation(summary = "删除指定缓存")
    @OperationLog(type = OperateType.DELETE)
    @DeleteMapping("/deleteCacheKey/{key}")
    public Result<Boolean> deleteCacheKey(@PathVariable String key) {
        redisService.del(key);
        return Result.ok();
    }

    @Operation(summary = "获取缓存监控信息")
    @GetMapping("/getMonitorInfo")
    public Result<Map<String, Object>> getMonitorInfo() {
        Map<String ,Object> result = cacheService.getMonitorInfo();
        return Result.ok(result);
    }

}
