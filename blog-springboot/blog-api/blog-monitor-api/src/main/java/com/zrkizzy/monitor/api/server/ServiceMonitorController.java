package com.zrkizzy.monitor.api.server;

import com.zrkizzy.common.core.domain.response.Result;
import com.zrkizzy.common.models.vo.system.monitor.ServiceMonitorVO;
import com.zrkizzy.monitor.facade.service.server.IServiceMonitorService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.net.UnknownHostException;

/**
 * 服务监控控制器
 *
 * @author zhangrongkang
 * @since 2023/7/7
 */
@Tag(name = "服务监控控制器")
@RestController
@RequestMapping("/admin/service-monitor")
public class ServiceMonitorController {

    @Autowired
    private IServiceMonitorService serviceMonitorService;

    @Operation(summary = "获取服务监控信息")
    @GetMapping("/getMonitorInfo")
    public Result<ServiceMonitorVO> getMonitorInfo() throws UnknownHostException {
        // 获取系统监控信息
        return Result.ok(serviceMonitorService.getMonitorInfo());
    }

}
