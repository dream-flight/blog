package com.zrkizzy.monitor.api.user;

import com.zrkizzy.common.core.domain.response.PageResult;
import com.zrkizzy.common.core.domain.response.Result;
import com.zrkizzy.common.models.enums.system.log.OperateType;
import com.zrkizzy.common.models.query.system.monitor.OnlineQuery;
import com.zrkizzy.common.models.vo.system.monitor.OnlineUserVO;
import com.zrkizzy.common.redis.service.IRedisService;
import com.zrkizzy.monitor.facade.service.user.IOnlineService;
import com.zrkizzy.system.facade.annotation.OperationLog;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import static com.zrkizzy.common.redis.enums.RedisKey.USER_KEY;


/**
 * 在线用户控制器
 *
 * @author zhangrongkang
 * @since 2023/7/11
 */
@Tag(name = "在线用户控制器")
@RestController
@RequestMapping("/admin/online")
public class OnlineController {

    @Autowired
    private IRedisService redisService;
    @Autowired
    private IOnlineService onlineService;

    @Operation(summary = "分页获取在线用户")
    @PostMapping("/list")
    public Result<PageResult<OnlineUserVO>> listOnline(@RequestBody OnlineQuery onlineQuery) {
        return Result.ok(onlineService.listOnlineUsers(onlineQuery));
    }

    @Operation(summary = "下线指定用户")
    @OperationLog(type = OperateType.DELETE)
    @DeleteMapping("/offline/{traceId}")
    public Result<Boolean> offlineUser(@PathVariable String traceId) {
        // 下线指定用户
        redisService.del(USER_KEY.getKey() + traceId);
        return Result.ok();
    }
}
