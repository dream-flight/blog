package com.zrkizzy.storage.api;

import com.zrkizzy.common.core.domain.response.Result;
import com.zrkizzy.common.models.dto.system.file.FileDTO;
import com.zrkizzy.common.models.dto.system.file.FileUploadDTO;
import com.zrkizzy.common.models.vo.system.file.FileUploadStrategyVO;
import com.zrkizzy.storage.facade.service.IFileService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * 文件控制器
 *
 * @author zhangrongkang
 * @since 2023/5/11
 */
@Tag(name = "文件控制器")
@RestController
@RequestMapping("/admin/file")
public class FileController {
    @Autowired
    private IFileService fileService;

    @Operation(summary = "获取文件上传策略")
    @GetMapping("/listUploadStrategy")
    public Result<List<FileUploadStrategyVO>> listUploadStrategy() {
        return Result.ok(fileService.listUploadStrategy());
    }

    @Operation(summary = "上传文件")
    @PostMapping("/upload")
    public Result<String> upload(@Validated @ModelAttribute FileUploadDTO fileUploadDTO) {
        // 上传文件并返回结果
        return Result.ok(fileService.upload(fileUploadDTO));
    }

    @Operation(summary = "批量删除文件")
    @DeleteMapping("/delete")
    public Result<Boolean> delete(@RequestBody List<FileDTO> fileList) {
        // 批量删除文件并返回删除结果
        return Result.ok(fileService.delete(fileList));
    }

    @Operation(summary = "编辑器上传图片")
    @PostMapping("/addImage")
    public Result<String> addImage(@Validated @ModelAttribute FileUploadDTO fileUploadDTO) {
        return Result.ok(fileService.addImage(fileUploadDTO));
    }
}
