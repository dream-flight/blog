package com.zrkizzy.storage.api;

import com.zrkizzy.common.core.domain.response.Result;
import com.zrkizzy.common.core.utils.bean.BeanCopyUtil;
import com.zrkizzy.common.models.dto.system.file.FileTypeDTO;
import com.zrkizzy.common.models.vo.system.file.FileTypeVO;
import com.zrkizzy.storage.facade.service.IFileTypeService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * 文件分类控制器
 *
 * @author zhangrongkang
 * @since 2023/6/1
 */

@Tag(name = "文件分类控制器")
@RestController
@RequestMapping("/admin/file-type")
public class FileTypeController {
    @Autowired
    private IFileTypeService fileTypeService;

    @Operation(summary = "获取文件数据")
    @GetMapping("/list")
    public Result<List<FileTypeVO>> listFiles() {
        return Result.ok(fileTypeService.listFileTypes());
    }

    @Operation(summary = "添加-更新文件分类")
    @PostMapping("/save")
    public Result<Boolean> save(@Validated @RequestBody FileTypeDTO fileTypeDTO) {
        return fileTypeService.save(fileTypeDTO) > 0 ? Result.ok() : Result.failure();
    }

    @Operation(summary = "获取指定文件分类")
    @GetMapping("/getFileTypeById/{id}")
    public Result<FileTypeVO> getFileTypeById(@PathVariable Long id) {
        return Result.ok(BeanCopyUtil.copy(fileTypeService.getById(id), FileTypeVO.class));
    }

    @Operation(summary = "删除指定文件分类")
    @DeleteMapping("/delete/{id}")
    public Result<Boolean> delete(@PathVariable Long id) {
        return fileTypeService.delete(id) ? Result.ok() : Result.failure();
    }

}
