package com.zrkizzy.security.api.test;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.security.Principal;

/**
 * 测试Oauth2控制器
 *
 * @author zhangrongkang
 * @since 2023/12/26
 */
@RestController
public class HelloController {

    @GetMapping("/hello")
    public String hello(Principal principal) {
        return "Hello," + principal.getName();
    }
}
