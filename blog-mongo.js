/*
 Navicat Premium Data Transfer

 Source Server         : MongoDB@aliyun
 Source Server Type    : MongoDB
 Source Server Version : 40223
 Source Host           : dds-bp1af35f50ae53541398-pub.mongodb.rds.aliyuncs.com:3717
 Source Schema         : blog

 Target Server Type    : MongoDB
 Target Server Version : 40223
 File Encoding         : 65001

 Date: 23/10/2023 09:57:02
*/


// ----------------------------
// Collection structure for configurations
// ----------------------------
db.getCollection("configurations").drop();
db.createCollection("configurations");

// ----------------------------
// Documents of configurations
// ----------------------------
session = db.getMongo().startSession();
session.startTransaction();
db = session.getDatabase("blog");
session.commitTransaction(); session.endSession();

// ----------------------------
// Collection structure for contents
// ----------------------------
db.getCollection("contents").drop();
db.createCollection("contents");

// ----------------------------
// Documents of contents
// ----------------------------
session = db.getMongo().startSession();
session.startTransaction();
db = session.getDatabase("blog");
db.getCollection("contents").insert([ {
    _id: ObjectId("6506eaea1796f169fb838595"),
    title: "关于我",
    content: "测试一下内容",
    updateTime: ISODate("2023-09-23T08:19:32.322Z"),
    _class: "com.zrkizzy.data.domain.blog.AboutMe"
} ]);
session.commitTransaction(); session.endSession();