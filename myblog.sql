/*
 Navicat Premium Data Transfer

 Source Server         : blog@117.72.37.52
 Source Server Type    : MySQL
 Source Server Version : 80035 (8.0.35)
 Source Host           : 117.72.37.52:3306
 Source Schema         : myblog

 Target Server Type    : MySQL
 Target Server Version : 80035 (8.0.35)
 File Encoding         : 65001

 Date: 15/06/2024 13:14:02
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for flyway_schema_history
-- ----------------------------
DROP TABLE IF EXISTS `flyway_schema_history`;
CREATE TABLE `flyway_schema_history` (
  `installed_rank` int NOT NULL,
  `version` varchar(50) DEFAULT NULL,
  `description` varchar(200) NOT NULL,
  `type` varchar(20) NOT NULL,
  `script` varchar(1000) NOT NULL,
  `checksum` int DEFAULT NULL,
  `installed_by` varchar(100) NOT NULL,
  `installed_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `execution_time` int NOT NULL,
  `success` tinyint(1) NOT NULL,
  PRIMARY KEY (`installed_rank`),
  KEY `flyway_schema_history_s_idx` (`success`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- ----------------------------
-- Records of flyway_schema_history
-- ----------------------------
BEGIN;
INSERT INTO `flyway_schema_history` (`installed_rank`, `version`, `description`, `type`, `script`, `checksum`, `installed_by`, `installed_on`, `execution_time`, `success`) VALUES (1, '1.0.0', 'init project', 'SQL', 'V1.0.0__init_project.sql', -1136206108, 'root', '2024-01-09 10:23:47', 4787, 1);
INSERT INTO `flyway_schema_history` (`installed_rank`, `version`, `description`, `type`, `script`, `checksum`, `installed_by`, `installed_on`, `execution_time`, `success`) VALUES (2, '1.0.1', 'add dict', 'SQL', 'V1.0.1__add_dict.sql', 85140519, 'root', '2024-01-16 15:00:29', 4787, 1);
COMMIT;

-- ----------------------------
-- Table structure for tb_app_list
-- ----------------------------
DROP TABLE IF EXISTS `tb_app_list`;
CREATE TABLE `tb_app_list` (
  `id` bigint NOT NULL COMMENT '主键',
  `name` varchar(50) NOT NULL COMMENT '应用名称',
  `url` varchar(255) NOT NULL COMMENT '访问路径',
  `description` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL COMMENT '应用描述',
  `logo` varchar(255) DEFAULT NULL COMMENT '应用logo',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `update_time` datetime DEFAULT NULL COMMENT '更新时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci COMMENT='应用表';

-- ----------------------------
-- Records of tb_app_list
-- ----------------------------
BEGIN;
COMMIT;

-- ----------------------------
-- Table structure for tb_article
-- ----------------------------
DROP TABLE IF EXISTS `tb_article`;
CREATE TABLE `tb_article` (
  `id` bigint NOT NULL COMMENT '主键',
  `title` varchar(100) NOT NULL COMMENT '文章名称',
  `cover` varchar(255) DEFAULT NULL COMMENT '文章封面',
  `type` tinyint NOT NULL COMMENT '文章类型：0 原创，1 转载，2翻译',
  `status` tinyint DEFAULT NULL COMMENT '文章状态：0 草稿箱 1 已发布 2 待发布',
  `summary` varchar(255) DEFAULT NULL COMMENT '文章摘要',
  `content` longtext COMMENT '文章内容',
  `original_link` varchar(120) DEFAULT NULL COMMENT '原文链接',
  `allow_comment` tinyint DEFAULT NULL COMMENT '是否允许评论：1 允许 0 不允许',
  `is_sticky` tinyint DEFAULT NULL COMMENT '是否置顶：1 置顶 0 不置顶',
  `allow_donation` tinyint DEFAULT NULL COMMENT '是否开启打赏：1 开启',
  `visibility` tinyint DEFAULT NULL COMMENT '可见范围 0 自己可见 1 全部可见',
  `visit_count` int DEFAULT NULL COMMENT '浏览数量',
  `comment_count` int DEFAULT NULL COMMENT '评论数量',
  `create_address` varchar(50) DEFAULT NULL COMMENT '发布IP',
  `create_location` varchar(50) DEFAULT NULL COMMENT '发布地址',
  `create_time` datetime DEFAULT NULL COMMENT '发布时间',
  `update_time` datetime DEFAULT NULL COMMENT '更新时间',
  PRIMARY KEY (`id`) USING BTREE,
  KEY `idx_title` (`title`) USING BTREE COMMENT '文章标题索引'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci COMMENT='文章表';

-- ----------------------------
-- Records of tb_article
-- ----------------------------
BEGIN;
INSERT INTO `tb_article` (`id`, `title`, `cover`, `type`, `status`, `summary`, `content`, `original_link`, `allow_comment`, `is_sticky`, `allow_donation`, `visibility`, `visit_count`, `comment_count`, `create_address`, `create_location`, `create_time`, `update_time`) VALUES (1716112808115961856, '测试文章标题', 'https://blog-yk0504.oss-cn-hangzhou.aliyuncs.com/cover/20231018235505.jpg', 0, 1, '测试零妹妹作为封面', '开始编辑...\nasdfsdaf', NULL, 1, 0, 1, 1, 0, 0, '127.0.0.1', '内网IP', '2023-10-22 23:22:32', '2023-12-21 10:39:08');
INSERT INTO `tb_article` (`id`, `title`, `cover`, `type`, `status`, `summary`, `content`, `original_link`, `allow_comment`, `is_sticky`, `allow_donation`, `visibility`, `visit_count`, `comment_count`, `create_address`, `create_location`, `create_time`, `update_time`) VALUES (1728431663593029632, 'iohhj ', 'https://blog-yk0504.oss-cn-hangzhou.aliyuncs.com/cover/20230530223559.png', 0, 1, 'nijhiu hu', 'jiojio jioj ', NULL, 1, 1, 1, 0, 0, 0, '127.0.0.1', '内网IP', '2023-11-25 23:13:16', '2024-01-31 23:17:21');
INSERT INTO `tb_article` (`id`, `title`, `cover`, `type`, `status`, `summary`, `content`, `original_link`, `allow_comment`, `is_sticky`, `allow_donation`, `visibility`, `visit_count`, `comment_count`, `create_address`, `create_location`, `create_time`, `update_time`) VALUES (1753072682204659712, '测试发布文章更新时间', 'https://blog-yk0504.oss-cn-hangzhou.aliyuncs.com/cover/20231018235424.jpg', 0, 1, '测试摘要1', '测试能不能发布文章，看看时间\n```java\npublic static void main(String[] args) {\n    System.out.println(\"Hello, World!\");\n}\n```', NULL, 0, 1, 0, 0, 0, 0, '127.0.0.1', '内网IP', '2024-02-01 23:07:53', '2024-02-04 14:37:12');
INSERT INTO `tb_article` (`id`, `title`, `cover`, `type`, `status`, `summary`, `content`, `original_link`, `allow_comment`, `is_sticky`, `allow_donation`, `visibility`, `visit_count`, `comment_count`, `create_address`, `create_location`, `create_time`, `update_time`) VALUES (1753345645655097344, '测试新文章发布UI', 'https://blog-yk0504.oss-cn-hangzhou.aliyuncs.com/cover/20231018235451.jpg', 0, 1, '测试文章发布UI', '测试新文章发布UI', NULL, 1, 1, 1, 1, 0, 0, '127.0.0.1', '内网IP', '2024-02-02 17:12:32', '2024-03-10 21:21:36');
INSERT INTO `tb_article` (`id`, `title`, `cover`, `type`, `status`, `summary`, `content`, `original_link`, `allow_comment`, `is_sticky`, `allow_donation`, `visibility`, `visit_count`, `comment_count`, `create_address`, `create_location`, `create_time`, `update_time`) VALUES (1754032333746339840, '测试文章2', 'https://blog-yk0504.oss-cn-hangzhou.aliyuncs.com/cover/20230530223559.png', 0, 0, '111', '喝上一篇文章一样\n# hjhj\njsdhjfhjasd', NULL, 1, 0, 1, 1, 0, 0, '127.0.0.1', '内网IP', '2024-02-04 14:41:11', '2024-03-10 21:27:10');
INSERT INTO `tb_article` (`id`, `title`, `cover`, `type`, `status`, `summary`, `content`, `original_link`, `allow_comment`, `is_sticky`, `allow_donation`, `visibility`, `visit_count`, `comment_count`, `create_address`, `create_location`, `create_time`, `update_time`) VALUES (1769738153108701184, '测试定时发布文章', 'https://blog-yk0504.oss-cn-hangzhou.aliyuncs.com/cover/20230604215201.jpeg', 0, 1, '值啊摇', '测试定时发布', NULL, 1, 0, 1, 1, 0, 0, '127.0.0.1', '内网IP', '2024-03-18 22:50:31', '2024-03-18 22:52:58');
COMMIT;

-- ----------------------------
-- Table structure for tb_article_category
-- ----------------------------
DROP TABLE IF EXISTS `tb_article_category`;
CREATE TABLE `tb_article_category` (
  `id` bigint NOT NULL COMMENT '主键',
  `article_id` bigint DEFAULT NULL COMMENT '文章ID',
  `category_id` bigint DEFAULT NULL COMMENT '分类ID',
  PRIMARY KEY (`id`) USING BTREE,
  KEY `idx_article_id` (`article_id`) USING BTREE COMMENT '文章ID索引',
  KEY `idx_category_id` (`category_id`) USING BTREE COMMENT '文章分类索引'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci COMMENT='文章分类关联表';

-- ----------------------------
-- Records of tb_article_category
-- ----------------------------
BEGIN;
INSERT INTO `tb_article_category` (`id`, `article_id`, `category_id`) VALUES (1716112809571385344, 1716112808115961856, 1699078935502913536);
INSERT INTO `tb_article_category` (`id`, `article_id`, `category_id`) VALUES (1728431663689498624, 1728431663593029632, 1699085088492355584);
INSERT INTO `tb_article_category` (`id`, `article_id`, `category_id`) VALUES (1753072682829611008, 1753072682204659712, 1698725448252391424);
INSERT INTO `tb_article_category` (`id`, `article_id`, `category_id`) VALUES (1753345646305214464, 1753345645655097344, 1698725448252391424);
INSERT INTO `tb_article_category` (`id`, `article_id`, `category_id`) VALUES (1754032334337736704, 1754032333746339840, 1699078935502913536);
INSERT INTO `tb_article_category` (`id`, `article_id`, `category_id`) VALUES (1769738153947561984, 1769738153108701184, 1699078935502913536);
COMMIT;

-- ----------------------------
-- Table structure for tb_article_tags
-- ----------------------------
DROP TABLE IF EXISTS `tb_article_tags`;
CREATE TABLE `tb_article_tags` (
  `id` bigint NOT NULL COMMENT '主键',
  `article_id` bigint DEFAULT NULL COMMENT '文章ID',
  `tag_id` bigint DEFAULT NULL COMMENT '标签ID',
  PRIMARY KEY (`id`) USING BTREE,
  KEY `idx_article_id` (`article_id`) USING BTREE COMMENT '文章ID索引',
  KEY `idx_tag_id` (`tag_id`) USING BTREE COMMENT '博客标签索引'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci COMMENT='文章标签关联表';

-- ----------------------------
-- Records of tb_article_tags
-- ----------------------------
BEGIN;
INSERT INTO `tb_article_tags` (`id`, `article_id`, `tag_id`) VALUES (1737663965284007936, 1716112808115961856, 1712125141502132224);
INSERT INTO `tb_article_tags` (`id`, `article_id`, `tag_id`) VALUES (1737663965284007937, 1716112808115961856, 1714668306494914560);
INSERT INTO `tb_article_tags` (`id`, `article_id`, `tag_id`) VALUES (1737663965284007938, 1716112808115961856, 1714668325264424960);
INSERT INTO `tb_article_tags` (`id`, `article_id`, `tag_id`) VALUES (1752712677290934272, 1728431663593029632, 1714668325264424960);
INSERT INTO `tb_article_tags` (`id`, `article_id`, `tag_id`) VALUES (1754031329457995776, 1753072682204659712, 1714668325264424960);
INSERT INTO `tb_article_tags` (`id`, `article_id`, `tag_id`) VALUES (1754031329457995777, 1753072682204659712, 1712125141502132224);
INSERT INTO `tb_article_tags` (`id`, `article_id`, `tag_id`) VALUES (1766816674385756160, 1753345645655097344, 1714668325264424960);
INSERT INTO `tb_article_tags` (`id`, `article_id`, `tag_id`) VALUES (1766816674385756161, 1753345645655097344, 1712125141502132224);
INSERT INTO `tb_article_tags` (`id`, `article_id`, `tag_id`) VALUES (1766816674385756162, 1753345645655097344, 1714668306494914560);
INSERT INTO `tb_article_tags` (`id`, `article_id`, `tag_id`) VALUES (1766818077791485952, 1754032333746339840, 1714668325264424960);
INSERT INTO `tb_article_tags` (`id`, `article_id`, `tag_id`) VALUES (1769738771256836096, 1769738153108701184, 1714668325264424960);
COMMIT;

-- ----------------------------
-- Table structure for tb_blog_category
-- ----------------------------
DROP TABLE IF EXISTS `tb_blog_category`;
CREATE TABLE `tb_blog_category` (
  `id` bigint NOT NULL COMMENT '主键',
  `name` varchar(50) NOT NULL COMMENT '分类名称',
  `description` varchar(255) DEFAULT NULL COMMENT '分类描述',
  `icon` varchar(255) DEFAULT NULL COMMENT '分类图标',
  `sort` int DEFAULT '0' COMMENT '分类排序',
  `visibility` tinyint NOT NULL COMMENT '可见范围：0 自己可见 1 全部可见',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `update_time` datetime DEFAULT NULL COMMENT '更新时间',
  PRIMARY KEY (`id`) USING BTREE,
  KEY `idx_privacy_setting` (`visibility`) USING BTREE COMMENT '分类状态索引',
  KEY `idx_name` (`name`) USING BTREE COMMENT '分类名称索引'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci COMMENT='博客分类表';

-- ----------------------------
-- Records of tb_blog_category
-- ----------------------------
BEGIN;
INSERT INTO `tb_blog_category` (`id`, `name`, `description`, `icon`, `sort`, `visibility`, `create_time`, `update_time`) VALUES (1698725448252391424, '测试分类', '测试分类', 'bug', 0, 0, '2023-09-04 23:51:22', '2023-11-25 22:38:54');
INSERT INTO `tb_blog_category` (`id`, `name`, `description`, `icon`, `sort`, `visibility`, `create_time`, `update_time`) VALUES (1699078935502913536, '测试', '测试哈哈哈', 'clipboard', 0, 0, '2023-09-05 23:16:00', '2024-01-31 21:54:37');
INSERT INTO `tb_blog_category` (`id`, `name`, `description`, `icon`, `sort`, `visibility`, `create_time`, `update_time`) VALUES (1699085088492355584, '测试2', '测试', 'article-ranking', 0, 1, '2023-09-05 23:40:27', '2023-11-25 22:38:45');
INSERT INTO `tb_blog_category` (`id`, `name`, `description`, `icon`, `sort`, `visibility`, `create_time`, `update_time`) VALUES (1752691143692255232, '12312', '12', 'analysis', 0, 1, '2024-01-31 21:51:47', NULL);
COMMIT;

-- ----------------------------
-- Table structure for tb_blog_tags
-- ----------------------------
DROP TABLE IF EXISTS `tb_blog_tags`;
CREATE TABLE `tb_blog_tags` (
  `id` bigint NOT NULL COMMENT '主键',
  `name` varchar(50) NOT NULL COMMENT '标签名称',
  `sort` int DEFAULT '0' COMMENT '标签排序',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `update_time` datetime DEFAULT NULL COMMENT '更新时间',
  PRIMARY KEY (`id`) USING BTREE,
  KEY `idx_name` (`name`) USING BTREE COMMENT '标签名称索引'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci COMMENT='博客标签表';

-- ----------------------------
-- Records of tb_blog_tags
-- ----------------------------
BEGIN;
INSERT INTO `tb_blog_tags` (`id`, `name`, `sort`, `create_time`, `update_time`) VALUES (1712125141502132224, '测试标签', 0, '2023-10-11 23:16:58', NULL);
INSERT INTO `tb_blog_tags` (`id`, `name`, `sort`, `create_time`, `update_time`) VALUES (1714668306494914560, '测试标签2', 0, '2023-10-18 23:42:36', NULL);
INSERT INTO `tb_blog_tags` (`id`, `name`, `sort`, `create_time`, `update_time`) VALUES (1714668325264424960, '哈哈哈哈', 0, '2023-10-18 23:42:40', NULL);
COMMIT;

-- ----------------------------
-- Table structure for tb_dict_data
-- ----------------------------
DROP TABLE IF EXISTS `tb_dict_data`;
CREATE TABLE `tb_dict_data` (
  `id` bigint NOT NULL COMMENT '主键',
  `dict_label` varchar(100) NOT NULL DEFAULT '' COMMENT '字典标签',
  `dict_value` varchar(100) DEFAULT '' COMMENT '字典键值',
  `dict_type_id` bigint NOT NULL COMMENT '字典类型ID',
  `sort` int DEFAULT '0' COMMENT '字典排序',
  `status` tinyint NOT NULL DEFAULT '1' COMMENT '状态（1 正常 0 停用）',
  `css_class` varchar(100) DEFAULT NULL COMMENT '样式属性（其他样式扩展）',
  `list_class` varchar(100) DEFAULT NULL COMMENT '表格回显样式',
  `remark` varchar(255) DEFAULT NULL COMMENT '备注',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `update_time` datetime DEFAULT NULL COMMENT '更新时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci COMMENT='字典数据表';

-- ----------------------------
-- Records of tb_dict_data
-- ----------------------------
BEGIN;
INSERT INTO `tb_dict_data` (`id`, `dict_label`, `dict_value`, `dict_type_id`, `sort`, `status`, `css_class`, `list_class`, `remark`, `create_time`, `update_time`) VALUES (1751303580737863680, '男', '0', 1747512431925526528, 1, 1, NULL, 'default', '性别男', '2024-01-28 01:58:06', '2024-01-28 02:05:34');
INSERT INTO `tb_dict_data` (`id`, `dict_label`, `dict_value`, `dict_type_id`, `sort`, `status`, `css_class`, `list_class`, `remark`, `create_time`, `update_time`) VALUES (1751305435337457664, '女', '1', 1747512431925526528, 2, 1, NULL, 'default', '性别女', '2024-01-28 02:05:28', NULL);
INSERT INTO `tb_dict_data` (`id`, `dict_label`, `dict_value`, `dict_type_id`, `sort`, `status`, `css_class`, `list_class`, `remark`, `create_time`, `update_time`) VALUES (1751311402544398336, '未知性别', '2', 1747512431925526528, 3, 1, NULL, 'info', '未知性别', '2024-01-28 02:29:11', '2024-01-28 23:15:43');
INSERT INTO `tb_dict_data` (`id`, `dict_label`, `dict_value`, `dict_type_id`, `sort`, `status`, `css_class`, `list_class`, `remark`, `create_time`, `update_time`) VALUES (1751625335033561088, '正常', 'true', 1751622547662700544, 1, 1, NULL, 'success', '正常状态', '2024-01-28 23:16:38', '2024-01-31 09:08:40');
INSERT INTO `tb_dict_data` (`id`, `dict_label`, `dict_value`, `dict_type_id`, `sort`, `status`, `css_class`, `list_class`, `remark`, `create_time`, `update_time`) VALUES (1751625524507049984, '停用', 'false', 1751622547662700544, 2, 1, NULL, 'danger', '停用状态', '2024-01-28 23:17:23', '2024-01-31 14:36:55');
INSERT INTO `tb_dict_data` (`id`, `dict_label`, `dict_value`, `dict_type_id`, `sort`, `status`, `css_class`, `list_class`, `remark`, `create_time`, `update_time`) VALUES (1752593315175333888, '成功', 'true', 1752593210292568064, 1, 1, NULL, 'success', '操作成功', '2024-01-31 15:23:03', '2024-01-31 15:23:10');
INSERT INTO `tb_dict_data` (`id`, `dict_label`, `dict_value`, `dict_type_id`, `sort`, `status`, `css_class`, `list_class`, `remark`, `create_time`, `update_time`) VALUES (1752593441163837440, '失败', 'false', 1752593210292568064, 2, 1, NULL, 'danger', '操作失败', '2024-01-31 15:23:33', NULL);
INSERT INTO `tb_dict_data` (`id`, `dict_label`, `dict_value`, `dict_type_id`, `sort`, `status`, `css_class`, `list_class`, `remark`, `create_time`, `update_time`) VALUES (1752595490026815488, '新增', 'ADD', 1752595310229585920, 1, 1, NULL, 'default', '新增数据操作', '2024-01-31 15:31:41', NULL);
INSERT INTO `tb_dict_data` (`id`, `dict_label`, `dict_value`, `dict_type_id`, `sort`, `status`, `css_class`, `list_class`, `remark`, `create_time`, `update_time`) VALUES (1752595625267953664, '修改', 'UPDATE', 1752595310229585920, 2, 1, NULL, 'success', '更新数据操作', '2024-01-31 15:32:13', '2024-01-31 15:38:11');
INSERT INTO `tb_dict_data` (`id`, `dict_label`, `dict_value`, `dict_type_id`, `sort`, `status`, `css_class`, `list_class`, `remark`, `create_time`, `update_time`) VALUES (1752595730654035968, '删除', 'DELETE', 1752595310229585920, 3, 1, NULL, 'danger', '删除数据操作', '2024-01-31 15:32:38', NULL);
INSERT INTO `tb_dict_data` (`id`, `dict_label`, `dict_value`, `dict_type_id`, `sort`, `status`, `css_class`, `list_class`, `remark`, `create_time`, `update_time`) VALUES (1752596361154396160, '查询', 'QUERY', 1752595310229585920, 4, 1, NULL, 'warning', '查询数据操作', '2024-01-31 15:35:09', NULL);
INSERT INTO `tb_dict_data` (`id`, `dict_label`, `dict_value`, `dict_type_id`, `sort`, `status`, `css_class`, `list_class`, `remark`, `create_time`, `update_time`) VALUES (1752596697541771264, '其他', 'OTHER', 1752595310229585920, 5, 1, NULL, 'info', '用户其他操作', '2024-01-31 15:36:29', NULL);
INSERT INTO `tb_dict_data` (`id`, `dict_label`, `dict_value`, `dict_type_id`, `sort`, `status`, `css_class`, `list_class`, `remark`, `create_time`, `update_time`) VALUES (1752597727457312768, 'GET', 'GET', 1752597492312047616, 1, 1, NULL, 'success', 'GET请求', '2024-01-31 15:40:35', NULL);
INSERT INTO `tb_dict_data` (`id`, `dict_label`, `dict_value`, `dict_type_id`, `sort`, `status`, `css_class`, `list_class`, `remark`, `create_time`, `update_time`) VALUES (1752597818016530432, 'POST', 'POST', 1752597492312047616, 2, 1, NULL, 'default', 'POST请求', '2024-01-31 15:40:56', NULL);
INSERT INTO `tb_dict_data` (`id`, `dict_label`, `dict_value`, `dict_type_id`, `sort`, `status`, `css_class`, `list_class`, `remark`, `create_time`, `update_time`) VALUES (1752597899734155264, 'PUT', 'PUT', 1752597492312047616, 3, 1, NULL, 'warning', 'PUT请求', '2024-01-31 15:41:16', NULL);
INSERT INTO `tb_dict_data` (`id`, `dict_label`, `dict_value`, `dict_type_id`, `sort`, `status`, `css_class`, `list_class`, `remark`, `create_time`, `update_time`) VALUES (1752597967379890176, 'DELETE', 'DELETE', 1752597492312047616, 4, 1, NULL, 'danger', 'DELETE请求', '2024-01-31 15:41:32', NULL);
INSERT INTO `tb_dict_data` (`id`, `dict_label`, `dict_value`, `dict_type_id`, `sort`, `status`, `css_class`, `list_class`, `remark`, `create_time`, `update_time`) VALUES (1752610595477127168, '开启', 'true', 1752609109540077568, 1, 1, NULL, 'success', '开关开启', '2024-01-31 16:31:43', '2024-01-31 16:31:48');
INSERT INTO `tb_dict_data` (`id`, `dict_label`, `dict_value`, `dict_type_id`, `sort`, `status`, `css_class`, `list_class`, `remark`, `create_time`, `update_time`) VALUES (1752610693049221120, '关闭', 'false', 1752609109540077568, 2, 1, NULL, 'danger', '开关关闭', '2024-01-31 16:32:06', NULL);
INSERT INTO `tb_dict_data` (`id`, `dict_label`, `dict_value`, `dict_type_id`, `sort`, `status`, `css_class`, `list_class`, `remark`, `create_time`, `update_time`) VALUES (1752617051253374976, '原创', '0', 1752611764672921600, 1, 1, NULL, 'primary', '原创文章', '2024-01-31 16:57:22', NULL);
INSERT INTO `tb_dict_data` (`id`, `dict_label`, `dict_value`, `dict_type_id`, `sort`, `status`, `css_class`, `list_class`, `remark`, `create_time`, `update_time`) VALUES (1752617156945641472, '转载', '1', 1752611764672921600, 2, 1, NULL, 'danger', '转载文章', '2024-01-31 16:57:47', NULL);
INSERT INTO `tb_dict_data` (`id`, `dict_label`, `dict_value`, `dict_type_id`, `sort`, `status`, `css_class`, `list_class`, `remark`, `create_time`, `update_time`) VALUES (1752617245911023616, '翻译', '2', 1752611764672921600, 3, 1, NULL, 'success', '翻译文章', '2024-01-31 16:58:08', NULL);
INSERT INTO `tb_dict_data` (`id`, `dict_label`, `dict_value`, `dict_type_id`, `sort`, `status`, `css_class`, `list_class`, `remark`, `create_time`, `update_time`) VALUES (1752621309843472384, '私密', '0', 1752621077168652288, 1, 1, NULL, 'primary', '自己可见', '2024-01-31 17:14:17', '2024-02-02 13:37:47');
INSERT INTO `tb_dict_data` (`id`, `dict_label`, `dict_value`, `dict_type_id`, `sort`, `status`, `css_class`, `list_class`, `remark`, `create_time`, `update_time`) VALUES (1752621366785343488, '公开', '1', 1752621077168652288, 2, 1, NULL, 'success', '全部可见', '2024-01-31 17:14:31', '2024-02-02 13:38:01');
INSERT INTO `tb_dict_data` (`id`, `dict_label`, `dict_value`, `dict_type_id`, `sort`, `status`, `css_class`, `list_class`, `remark`, `create_time`, `update_time`) VALUES (1762049518171324416, '正常', 'true', 1762049325271089152, 1, 1, NULL, 'success', '任务正常', '2024-02-26 17:38:37', '2024-02-26 17:39:13');
INSERT INTO `tb_dict_data` (`id`, `dict_label`, `dict_value`, `dict_type_id`, `sort`, `status`, `css_class`, `list_class`, `remark`, `create_time`, `update_time`) VALUES (1762049750665789440, '暂停', 'false', 1762049325271089152, 2, 1, NULL, 'danger', '任务暂停', '2024-02-26 17:39:33', NULL);
COMMIT;

-- ----------------------------
-- Table structure for tb_dict_type
-- ----------------------------
DROP TABLE IF EXISTS `tb_dict_type`;
CREATE TABLE `tb_dict_type` (
  `id` bigint NOT NULL COMMENT '主键',
  `dict_name` varchar(100) NOT NULL DEFAULT '' COMMENT '字典名称',
  `dict_type` varchar(100) NOT NULL DEFAULT '' COMMENT '字典类型',
  `status` tinyint NOT NULL DEFAULT '1' COMMENT '状态（1 正常 0 停用）',
  `remark` varchar(255) DEFAULT NULL COMMENT '备注',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `update_time` datetime DEFAULT NULL COMMENT '更新时间',
  PRIMARY KEY (`id`),
  UNIQUE KEY `dict_type` (`dict_type`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci COMMENT='字典类型表';

-- ----------------------------
-- Records of tb_dict_type
-- ----------------------------
BEGIN;
INSERT INTO `tb_dict_type` (`id`, `dict_name`, `dict_type`, `status`, `remark`, `create_time`, `update_time`) VALUES (1747512431925526528, '用户性别', 'sys_user_sex', 1, '系统用户性别', '2024-01-17 14:53:26', '2024-01-31 15:05:02');
INSERT INTO `tb_dict_type` (`id`, `dict_name`, `dict_type`, `status`, `remark`, `create_time`, `update_time`) VALUES (1751622547662700544, '激活状态', 'sys_active_state', 1, '系统激活状态', '2024-01-28 23:05:34', '2024-02-05 16:29:05');
INSERT INTO `tb_dict_type` (`id`, `dict_name`, `dict_type`, `status`, `remark`, `create_time`, `update_time`) VALUES (1752593210292568064, '结果状态', 'sys_result_state', 1, '操作结果状态', '2024-01-31 15:22:38', NULL);
INSERT INTO `tb_dict_type` (`id`, `dict_name`, `dict_type`, `status`, `remark`, `create_time`, `update_time`) VALUES (1752595310229585920, '操作类型', 'sys_operate_type', 1, '用户操作类型', '2024-01-31 15:30:58', NULL);
INSERT INTO `tb_dict_type` (`id`, `dict_name`, `dict_type`, `status`, `remark`, `create_time`, `update_time`) VALUES (1752597492312047616, '请求类型', 'sys_request_type', 1, '接口请求类型', '2024-01-31 15:39:39', NULL);
INSERT INTO `tb_dict_type` (`id`, `dict_name`, `dict_type`, `status`, `remark`, `create_time`, `update_time`) VALUES (1752609109540077568, '系统开关', 'sys_switch_enable', 1, '系统开关列表', '2024-01-31 16:25:48', NULL);
INSERT INTO `tb_dict_type` (`id`, `dict_name`, `dict_type`, `status`, `remark`, `create_time`, `update_time`) VALUES (1752611764672921600, '文章类型', 'blog_article_type', 1, '文章类型列表', '2024-01-31 16:36:21', NULL);
INSERT INTO `tb_dict_type` (`id`, `dict_name`, `dict_type`, `status`, `remark`, `create_time`, `update_time`) VALUES (1752621077168652288, '可见类型', 'blog_visible_type', 1, '可见类型列表', '2024-01-31 17:13:22', NULL);
INSERT INTO `tb_dict_type` (`id`, `dict_name`, `dict_type`, `status`, `remark`, `create_time`, `update_time`) VALUES (1762049325271089152, '任务状态', 'sys_job_status', 1, '定时任务状态', '2024-02-26 17:37:51', NULL);
COMMIT;

-- ----------------------------
-- Table structure for tb_file
-- ----------------------------
DROP TABLE IF EXISTS `tb_file`;
CREATE TABLE `tb_file` (
  `id` bigint NOT NULL COMMENT '主键',
  `name` varchar(50) DEFAULT NULL COMMENT '文件名称',
  `path` varchar(255) NOT NULL COMMENT '文件存储位置',
  `src` varchar(100) DEFAULT NULL COMMENT '文件访问路径',
  `size` bigint DEFAULT NULL COMMENT '文件大小',
  `type` varchar(20) DEFAULT NULL COMMENT '文件类型',
  `strategy` varchar(50) NOT NULL COMMENT '上传策略',
  `user_id` bigint DEFAULT NULL COMMENT '上传用户ID',
  `file_type_id` bigint DEFAULT NULL COMMENT '文件分类ID',
  `md5` varchar(50) DEFAULT NULL COMMENT '文件唯一标识（MD5哈希值）',
  `create_time` datetime NOT NULL COMMENT '创建时间',
  `update_time` datetime DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci COMMENT='文件表';

-- ----------------------------
-- Records of tb_file
-- ----------------------------
BEGIN;
INSERT INTO `tb_file` (`id`, `name`, `path`, `src`, `size`, `type`, `strategy`, `user_id`, `file_type_id`, `md5`, `create_time`, `update_time`) VALUES (1663144125785964544, '20230529192353.jpg', 'avatar/20230529192353.jpg', 'https://blog-yk0504.oss-cn-hangzhou.aliyuncs.com/avatar/20230529192353.jpg', 72708, '.jpg', 'oss', 1653794265890816000, 1656676089927303170, 'bee1e765d72ab1eb20e3cdfb3de68abb', '2023-05-29 19:23:54', '2023-05-30 09:36:07');
INSERT INTO `tb_file` (`id`, `name`, `path`, `src`, `size`, `type`, `strategy`, `user_id`, `file_type_id`, `md5`, `create_time`, `update_time`) VALUES (1663144536764841984, '20230529192531.jpg', 'avatar/20230529192531.jpg', 'https://blog-yk0504.oss-cn-hangzhou.aliyuncs.com/avatar/20230529192531.jpg', 152031, '.jpg', 'oss', 1653794265890816000, 1656676089927303170, 'fd43ed5490db1894cb5c46cb300e1cf2', '2023-05-29 19:25:32', '2023-05-30 09:36:07');
INSERT INTO `tb_file` (`id`, `name`, `path`, `src`, `size`, `type`, `strategy`, `user_id`, `file_type_id`, `md5`, `create_time`, `update_time`) VALUES (1663144640041189376, '20230529192556.jpg', 'avatar/20230529192556.jpg', 'https://blog-yk0504.oss-cn-hangzhou.aliyuncs.com/avatar/20230529192556.jpg', 149077, '.jpg', 'oss', 1653794265890816000, 1656676089927303170, '8a8aa84284c227bad891e840561cc2a4', '2023-05-29 19:25:57', '2023-05-30 09:36:07');
INSERT INTO `tb_file` (`id`, `name`, `path`, `src`, `size`, `type`, `strategy`, `user_id`, `file_type_id`, `md5`, `create_time`, `update_time`) VALUES (1663196287383633920, '20230529225103.jpg', 'avatar/20230529225103.jpg', 'https://blog-yk0504.oss-cn-hangzhou.aliyuncs.com/avatar/20230529225103.jpg', 97766, '.jpg', 'oss', 1653794265890816000, 1656676089927303170, 'e960ac942f4cd2c10af9bcbacba5f4e6', '2023-05-29 22:51:10', '2023-05-30 09:36:07');
INSERT INTO `tb_file` (`id`, `name`, `path`, `src`, `size`, `type`, `strategy`, `user_id`, `file_type_id`, `md5`, `create_time`, `update_time`) VALUES (1663554855068762112, '20230530223559.png', 'cover/20230530223559.png', 'https://blog-yk0504.oss-cn-hangzhou.aliyuncs.com/cover/20230530223559.png', 181272, '.png', 'oss', 1653794265890816000, 1656676089927303168, 'fa74451429d178e2682a3cf6e833c019', '2023-05-30 22:35:59', '2023-05-30 22:35:59');
INSERT INTO `tb_file` (`id`, `name`, `path`, `src`, `size`, `type`, `strategy`, `user_id`, `file_type_id`, `md5`, `create_time`, `update_time`) VALUES (1663556688776527872, '20230530224316.jpg', 'cover/20230530224316.jpg', 'https://blog-yk0504.oss-cn-hangzhou.aliyuncs.com/cover/20230530224316.jpg', 50746, '.jpg', 'oss', 1653794265890816000, 1656676089927303168, '8c2eb375053690cb1ae6e9cdc2253a23', '2023-05-30 22:43:17', '2023-05-30 22:43:17');
INSERT INTO `tb_file` (`id`, `name`, `path`, `src`, `size`, `type`, `strategy`, `user_id`, `file_type_id`, `md5`, `create_time`, `update_time`) VALUES (1665355748768481280, '20230604215201.jpeg', 'cover/20230604215201.jpeg', 'https://blog-yk0504.oss-cn-hangzhou.aliyuncs.com/cover/20230604215201.jpeg', 604332, '.jpeg', 'oss', 1653794265890816000, 1656676089927303168, '67bd2983a12ea08bffa358d68e1169f7', '2023-06-04 21:52:06', '2023-06-04 21:52:06');
INSERT INTO `tb_file` (`id`, `name`, `path`, `src`, `size`, `type`, `strategy`, `user_id`, `file_type_id`, `md5`, `create_time`, `update_time`) VALUES (1667556674283503616, '20230610233746.jpg', 'article/20230610233746.jpg', 'https://blog-yk0504.oss-cn-hangzhou.aliyuncs.com/article/20230610233746.jpg', 1284390, '.jpg', 'oss', 1653794265890816000, 1656676089927303169, 'e8afdebb8420ad7822f75df100c8cc99', '2023-06-10 23:37:48', '2023-06-10 23:37:48');
INSERT INTO `tb_file` (`id`, `name`, `path`, `src`, `size`, `type`, `strategy`, `user_id`, `file_type_id`, `md5`, `create_time`, `update_time`) VALUES (1695768706853896192, '20230827200219.jpg', 'article/20230827200219.jpg', 'https://blog-yk0504.oss-cn-hangzhou.aliyuncs.com/article/20230827200219.jpg', 152031, '.jpg', 'oss', 1653794265890816000, 1656676089927303169, 'fd43ed5490db1894cb5c46cb300e1cf2', '2023-08-27 20:02:20', '2023-08-27 20:02:20');
INSERT INTO `tb_file` (`id`, `name`, `path`, `src`, `size`, `type`, `strategy`, `user_id`, `file_type_id`, `md5`, `create_time`, `update_time`) VALUES (1714671281158225920, '20231018235424.jpg', 'cover/20231018235424.jpg', 'https://blog-yk0504.oss-cn-hangzhou.aliyuncs.com/cover/20231018235424.jpg', 1050254, '.jpg', 'oss', 1653794265890816000, 1656676089927303168, 'ad45e0ac4d6bc255aa005ce91582e4f0', '2023-10-18 23:54:25', '2023-10-18 23:54:25');
INSERT INTO `tb_file` (`id`, `name`, `path`, `src`, `size`, `type`, `strategy`, `user_id`, `file_type_id`, `md5`, `create_time`, `update_time`) VALUES (1714671393318109184, '20231018235451.jpg', 'cover/20231018235451.jpg', 'https://blog-yk0504.oss-cn-hangzhou.aliyuncs.com/cover/20231018235451.jpg', 647894, '.jpg', 'oss', 1653794265890816000, 1656676089927303168, 'bc37d5dedcc333168b6f2664c73456a2', '2023-10-18 23:54:52', '2023-10-18 23:54:52');
INSERT INTO `tb_file` (`id`, `name`, `path`, `src`, `size`, `type`, `strategy`, `user_id`, `file_type_id`, `md5`, `create_time`, `update_time`) VALUES (1714671450125762560, '20231018235505.jpg', 'cover/20231018235505.jpg', 'https://blog-yk0504.oss-cn-hangzhou.aliyuncs.com/cover/20231018235505.jpg', 615846, '.jpg', 'oss', 1653794265890816000, 1656676089927303168, 'd211834e55c9a61007a57a1aa0c2dd90', '2023-10-18 23:55:05', '2023-10-18 23:55:05');
INSERT INTO `tb_file` (`id`, `name`, `path`, `src`, `size`, `type`, `strategy`, `user_id`, `file_type_id`, `md5`, `create_time`, `update_time`) VALUES (1735568476707225600, '6f1887b84ab079a9d7b6f2e8947f2580.jpg', 'https://blog-yk0504.oss-cn-hangzhou.aliyuncs.com/article/6f1887b84ab079a9d7b6f2e8947f2580.jpg', 'https://blog-yk0504.oss-cn-hangzhou.aliyuncs.com/article/6f1887b84ab079a9d7b6f2e8947f2580.jpg', 1660299, '.jpg', 'OSS', 1653794265890816000, 1656676089927303169, '6f1887b84ab079a9d7b6f2e8947f2580', '2023-12-15 15:52:25', NULL);
INSERT INTO `tb_file` (`id`, `name`, `path`, `src`, `size`, `type`, `strategy`, `user_id`, `file_type_id`, `md5`, `create_time`, `update_time`) VALUES (1747296374073851904, '06bc0e1731caf6e88e8ec580ac0d5ea2.jpg', 'https://blog-yk0504.oss-cn-hangzhou.aliyuncs.com/avatar/06bc0e1731caf6e88e8ec580ac0d5ea2.jpg', 'https://blog-yk0504.oss-cn-hangzhou.aliyuncs.com/avatar/06bc0e1731caf6e88e8ec580ac0d5ea2.jpg', 232390, '.jpg', 'OSS', 1653794265890816000, 1656676089927303170, '06bc0e1731caf6e88e8ec580ac0d5ea2', '2024-01-17 00:34:53', NULL);
INSERT INTO `tb_file` (`id`, `name`, `path`, `src`, `size`, `type`, `strategy`, `user_id`, `file_type_id`, `md5`, `create_time`, `update_time`) VALUES (1752860823346216960, '6dc38e172d06528a3b4a00014028075b.jpg', 'https://blog-yk0504.oss-cn-hangzhou.aliyuncs.com/avatar/6dc38e172d06528a3b4a00014028075b.jpg', 'https://blog-yk0504.oss-cn-hangzhou.aliyuncs.com/avatar/6dc38e172d06528a3b4a00014028075b.jpg', 120593, '.jpg', 'OSS', 1653794265890816000, 1656676089927303170, '6dc38e172d06528a3b4a00014028075b', '2024-02-01 09:06:02', NULL);
INSERT INTO `tb_file` (`id`, `name`, `path`, `src`, `size`, `type`, `strategy`, `user_id`, `file_type_id`, `md5`, `create_time`, `update_time`) VALUES (1752860862231609344, 'e18231c13d6700424b067e6382e13170.jpg', 'https://blog-yk0504.oss-cn-hangzhou.aliyuncs.com/avatar/e18231c13d6700424b067e6382e13170.jpg', 'https://blog-yk0504.oss-cn-hangzhou.aliyuncs.com/avatar/e18231c13d6700424b067e6382e13170.jpg', 198426, '.jpg', 'OSS', 1653794265890816000, 1656676089927303170, 'e18231c13d6700424b067e6382e13170', '2024-02-01 09:06:11', NULL);
INSERT INTO `tb_file` (`id`, `name`, `path`, `src`, `size`, `type`, `strategy`, `user_id`, `file_type_id`, `md5`, `create_time`, `update_time`) VALUES (1767219539436634112, '57d013a79861905156d885c48c99cba6.jpg', 'https://blog-yk0504.oss-cn-hangzhou.aliyuncs.com/avatar/57d013a79861905156d885c48c99cba6.jpg', 'https://blog-yk0504.oss-cn-hangzhou.aliyuncs.com/avatar/57d013a79861905156d885c48c99cba6.jpg', 281233, '.jpg', 'OSS', 1653794265890816000, 1656676089927303170, '57d013a79861905156d885c48c99cba6', '2024-03-12 00:02:26', NULL);
INSERT INTO `tb_file` (`id`, `name`, `path`, `src`, `size`, `type`, `strategy`, `user_id`, `file_type_id`, `md5`, `create_time`, `update_time`) VALUES (1767219582692491264, 'e8889e4556c4b9612c118bc6c2af7c9e.jpg', 'https://blog-yk0504.oss-cn-hangzhou.aliyuncs.com/avatar/e8889e4556c4b9612c118bc6c2af7c9e.jpg', 'https://blog-yk0504.oss-cn-hangzhou.aliyuncs.com/avatar/e8889e4556c4b9612c118bc6c2af7c9e.jpg', 161959, '.jpg', 'OSS', 1653794265890816000, 1656676089927303170, 'e8889e4556c4b9612c118bc6c2af7c9e', '2024-03-12 00:02:37', NULL);
INSERT INTO `tb_file` (`id`, `name`, `path`, `src`, `size`, `type`, `strategy`, `user_id`, `file_type_id`, `md5`, `create_time`, `update_time`) VALUES (1767219626938204160, '5b34a81e2261e3cdb0770b475605cd53.jpg', 'https://blog-yk0504.oss-cn-hangzhou.aliyuncs.com/avatar/5b34a81e2261e3cdb0770b475605cd53.jpg', 'https://blog-yk0504.oss-cn-hangzhou.aliyuncs.com/avatar/5b34a81e2261e3cdb0770b475605cd53.jpg', 143042, '.jpg', 'OSS', 1653794265890816000, 1656676089927303170, '5b34a81e2261e3cdb0770b475605cd53', '2024-03-12 00:02:47', NULL);
INSERT INTO `tb_file` (`id`, `name`, `path`, `src`, `size`, `type`, `strategy`, `user_id`, `file_type_id`, `md5`, `create_time`, `update_time`) VALUES (1767219661570572288, '0a5ecab9d8beaf4f5790718c324b8129.jpg', 'https://blog-yk0504.oss-cn-hangzhou.aliyuncs.com/avatar/0a5ecab9d8beaf4f5790718c324b8129.jpg', 'https://blog-yk0504.oss-cn-hangzhou.aliyuncs.com/avatar/0a5ecab9d8beaf4f5790718c324b8129.jpg', 224388, '.jpg', 'OSS', 1653794265890816000, 1656676089927303170, '0a5ecab9d8beaf4f5790718c324b8129', '2024-03-12 00:02:55', NULL);
INSERT INTO `tb_file` (`id`, `name`, `path`, `src`, `size`, `type`, `strategy`, `user_id`, `file_type_id`, `md5`, `create_time`, `update_time`) VALUES (1767219709368860672, '4cf442a34b9bc1d6bfd6a249c041b628.jpg', 'https://blog-yk0504.oss-cn-hangzhou.aliyuncs.com/avatar/4cf442a34b9bc1d6bfd6a249c041b628.jpg', 'https://blog-yk0504.oss-cn-hangzhou.aliyuncs.com/avatar/4cf442a34b9bc1d6bfd6a249c041b628.jpg', 366793, '.jpg', 'OSS', 1653794265890816000, 1656676089927303170, '4cf442a34b9bc1d6bfd6a249c041b628', '2024-03-12 00:03:07', NULL);
COMMIT;

-- ----------------------------
-- Table structure for tb_file_type
-- ----------------------------
DROP TABLE IF EXISTS `tb_file_type`;
CREATE TABLE `tb_file_type` (
  `id` bigint NOT NULL COMMENT '主键',
  `name` varchar(50) NOT NULL COMMENT '文件分类名称',
  `mark` varchar(50) DEFAULT NULL COMMENT '文件分类标识',
  `description` varchar(255) DEFAULT NULL COMMENT '文件分类描述',
  `sort` int DEFAULT NULL COMMENT '文件分类排序',
  `create_time` datetime NOT NULL COMMENT '创建时间',
  `update_time` datetime DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci COMMENT='文件分类表';

-- ----------------------------
-- Records of tb_file_type
-- ----------------------------
BEGIN;
INSERT INTO `tb_file_type` (`id`, `name`, `mark`, `description`, `sort`, `create_time`, `update_time`) VALUES (1656676089927303168, '封面图片', 'cover', '文章封面图片', 1, '2023-05-11 23:02:37', '2023-05-23 16:02:28');
INSERT INTO `tb_file_type` (`id`, `name`, `mark`, `description`, `sort`, `create_time`, `update_time`) VALUES (1656676089927303169, '文章图片', 'article', '文章中的图片', 2, '2023-05-11 23:03:05', '2023-05-23 16:02:50');
INSERT INTO `tb_file_type` (`id`, `name`, `mark`, `description`, `sort`, `create_time`, `update_time`) VALUES (1656676089927303170, '头像文件', 'avatar', '分类图片', 3, '2023-05-11 23:03:24', '2023-05-23 16:06:41');
INSERT INTO `tb_file_type` (`id`, `name`, `mark`, `description`, `sort`, `create_time`, `update_time`) VALUES (1656676089927303171, '其他文件', 'other', '其他类型文件', 4, '2023-05-11 23:03:45', '2023-05-23 16:03:01');
COMMIT;

-- ----------------------------
-- Table structure for tb_generate_log
-- ----------------------------
DROP TABLE IF EXISTS `tb_generate_log`;
CREATE TABLE `tb_generate_log` (
  `id` bigint NOT NULL COMMENT '主键',
  `table_name` varchar(100) NOT NULL DEFAULT '' COMMENT '生成表名',
  `module_name` varchar(100) DEFAULT NULL COMMENT '模块名称',
  `package_name` varchar(100) DEFAULT NULL COMMENT '包名称',
  `enable_cache` tinyint NOT NULL DEFAULT '0' COMMENT '开启缓存',
  `enable_chain` tinyint NOT NULL DEFAULT '0' COMMENT '链式编程',
  `create_time` datetime DEFAULT NULL COMMENT '生成时间',
  `update_time` datetime DEFAULT NULL COMMENT '更新时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci COMMENT='代码生成日志表';

-- ----------------------------
-- Records of tb_generate_log
-- ----------------------------
BEGIN;
INSERT INTO `tb_generate_log` (`id`, `table_name`, `module_name`, `package_name`, `enable_cache`, `enable_chain`, `create_time`, `update_time`) VALUES (1747152512785317888, 'tb_dict_type', 'system', 'dict', 0, 0, '2024-01-16 15:03:14', NULL);
INSERT INTO `tb_generate_log` (`id`, `table_name`, `module_name`, `package_name`, `enable_cache`, `enable_chain`, `create_time`, `update_time`) VALUES (1760911906832384000, 'tb_scheduler_job', 'scheduler', 'scheduler', 0, 0, '2024-02-23 14:18:09', NULL);
INSERT INTO `tb_generate_log` (`id`, `table_name`, `module_name`, `package_name`, `enable_cache`, `enable_chain`, `create_time`, `update_time`) VALUES (1765365164607012864, 'tb_scheduler_job_log', 'scheduler', 'scheduler', 0, 0, '2024-03-06 21:13:49', NULL);
INSERT INTO `tb_generate_log` (`id`, `table_name`, `module_name`, `package_name`, `enable_cache`, `enable_chain`, `create_time`, `update_time`) VALUES (1771513643679088640, 'tb_user_role', '1', '1', 0, 0, '2024-03-23 20:25:40', NULL);
COMMIT;

-- ----------------------------
-- Table structure for tb_link
-- ----------------------------
DROP TABLE IF EXISTS `tb_link`;
CREATE TABLE `tb_link` (
  `id` bigint NOT NULL COMMENT '主键',
  `name` varchar(60) NOT NULL COMMENT '网站名称',
  `website` varchar(255) DEFAULT NULL COMMENT '网站域名',
  `logo` varchar(255) DEFAULT NULL COMMENT '网站Logo',
  `introduce` varchar(255) DEFAULT NULL COMMENT '网站介绍',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `update_time` datetime DEFAULT NULL COMMENT '更新时间',
  PRIMARY KEY (`id`) USING BTREE,
  KEY `index_name` (`name`) USING BTREE COMMENT '网站名称索引'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci COMMENT='友情链接表';

-- ----------------------------
-- Records of tb_link
-- ----------------------------
BEGIN;
INSERT INTO `tb_link` (`id`, `name`, `website`, `logo`, `introduce`, `create_time`, `update_time`) VALUES (1671411053646315520, '半晴Miko', 'https://banq.ink/', 'https://www.static.banq.ink/sunnyBlog/avatar/fcc72d35fc928185c3e70773a29a310f.jpg', 'Semisunny', '2023-06-21 14:53:43', '2023-06-21 15:17:31');
COMMIT;

-- ----------------------------
-- Table structure for tb_login_log
-- ----------------------------
DROP TABLE IF EXISTS `tb_login_log`;
CREATE TABLE `tb_login_log` (
  `id` bigint NOT NULL COMMENT '主键',
  `username` varchar(255) NOT NULL COMMENT '登录用户名称',
  `login_ip` varchar(255) DEFAULT NULL COMMENT '登录IP',
  `login_location` varchar(255) DEFAULT NULL COMMENT '登录位置',
  `browser` varchar(255) DEFAULT NULL COMMENT '浏览器版本',
  `os` varchar(255) DEFAULT NULL COMMENT '操作系统',
  `status` tinyint NOT NULL COMMENT '登录状态：0 失败; 1 成功',
  `message` varchar(255) DEFAULT NULL COMMENT '登录消息提示',
  `login_time` datetime NOT NULL COMMENT '登录时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci COMMENT='用户登录信息表';

-- ----------------------------
-- Records of tb_login_log
-- ----------------------------
BEGIN;
INSERT INTO `tb_login_log` (`id`, `username`, `login_ip`, `login_location`, `browser`, `os`, `status`, `message`, `login_time`) VALUES (1767219167754190848, 'admin@qq.com', '180.108.32.190', '江苏省苏州市', 'Chrome 12', 'Mac OS X', 1, '一切OK', '2024-03-12 00:01:40');
INSERT INTO `tb_login_log` (`id`, `username`, `login_ip`, `login_location`, `browser`, `os`, `status`, `message`, `login_time`) VALUES (1767219396683497472, 'admin@qq.com', '180.108.32.190', '江苏省苏州市', 'Chrome 12', 'Mac OS X', 1, '一切OK', '2024-03-12 00:01:59');
INSERT INTO `tb_login_log` (`id`, `username`, `login_ip`, `login_location`, `browser`, `os`, `status`, `message`, `login_time`) VALUES (1770366250812178432, 'test@qq.com', '27.39.88.209', '广东省东莞市', 'Chrome 12', 'Windows 10', 1, '一切OK', '2024-03-20 16:31:09');
INSERT INTO `tb_login_log` (`id`, `username`, `login_ip`, `login_location`, `browser`, `os`, `status`, `message`, `login_time`) VALUES (1770370700884836352, 'test@qq.com', '180.108.32.91', '江苏省苏州市', 'Chrome Mobile', 'Mac OS X (iPhone)', 1, '一切OK', '2024-03-20 16:44:27');
INSERT INTO `tb_login_log` (`id`, `username`, `login_ip`, `login_location`, `browser`, `os`, `status`, `message`, `login_time`) VALUES (1770371370459332608, 'test@qq.com', '35.212.237.202', '美国俄勒冈', 'Chrome 12', 'Windows 10', 1, '一切OK', '2024-03-20 16:46:49');
INSERT INTO `tb_login_log` (`id`, `username`, `login_ip`, `login_location`, `browser`, `os`, `status`, `message`, `login_time`) VALUES (1771115692729303040, 'admin@qq.com', '180.108.32.91', '江苏省苏州市', 'Chrome 12', 'Mac OS X', 1, '一切OK', '2024-03-22 18:04:31');
INSERT INTO `tb_login_log` (`id`, `username`, `login_ip`, `login_location`, `browser`, `os`, `status`, `message`, `login_time`) VALUES (1771156816424599552, 'test@qq.com', '112.94.173.170', '广东省广州市', 'Chrome 12', 'Windows 10', 1, '一切OK', '2024-03-22 20:47:56');
INSERT INTO `tb_login_log` (`id`, `username`, `login_ip`, `login_location`, `browser`, `os`, `status`, `message`, `login_time`) VALUES (1771450846593155072, 'admin@qq.com', '180.108.32.91', '江苏省苏州市', 'Chrome 12', 'Mac OS X', 1, '一切OK', '2024-03-23 16:16:18');
INSERT INTO `tb_login_log` (`id`, `username`, `login_ip`, `login_location`, `browser`, `os`, `status`, `message`, `login_time`) VALUES (1771513036348063744, 'test@qq.com', '27.39.88.209', '广东省东莞市', 'Chrome 12', 'Windows 10', 1, '一切OK', '2024-03-23 20:23:28');
INSERT INTO `tb_login_log` (`id`, `username`, `login_ip`, `login_location`, `browser`, `os`, `status`, `message`, `login_time`) VALUES (1771834871652548608, 'admin@qq.com', '180.108.32.91', '江苏省苏州市', 'Chrome 12', 'Mac OS X', 1, '一切OK', '2024-03-24 17:42:29');
INSERT INTO `tb_login_log` (`id`, `username`, `login_ip`, `login_location`, `browser`, `os`, `status`, `message`, `login_time`) VALUES (1772127719111262208, 'test@qq.com', '39.144.95.33', '00', 'Chrome Mobile', 'Android 1.x', 1, '一切OK', '2024-03-25 13:06:04');
INSERT INTO `tb_login_log` (`id`, `username`, `login_ip`, `login_location`, `browser`, `os`, `status`, `message`, `login_time`) VALUES (1772565378900164608, 'test@qq.com', '180.106.237.207', '江苏省苏州市', 'Safari', 'Mac OS X', 1, '一切OK', '2024-03-26 18:05:08');
INSERT INTO `tb_login_log` (`id`, `username`, `login_ip`, `login_location`, `browser`, `os`, `status`, `message`, `login_time`) VALUES (1775010076747104256, 'test@qq.com', '36.112.206.38', '北京北京市', 'Chrome 12', 'Windows 10', 1, '一切OK', '2024-04-02 11:59:28');
INSERT INTO `tb_login_log` (`id`, `username`, `login_ip`, `login_location`, `browser`, `os`, `status`, `message`, `login_time`) VALUES (1777265320331837440, 'admin@qq.com', '117.83.27.253', '江苏省苏州市', 'Chrome 12', 'Windows 10', 0, '用户验证码已过期', '2024-04-08 17:20:47');
INSERT INTO `tb_login_log` (`id`, `username`, `login_ip`, `login_location`, `browser`, `os`, `status`, `message`, `login_time`) VALUES (1777265325234978816, 'admin@qq.com', '117.83.27.253', '江苏省苏州市', 'Chrome 12', 'Windows 10', 1, '一切OK', '2024-04-08 17:20:51');
INSERT INTO `tb_login_log` (`id`, `username`, `login_ip`, `login_location`, `browser`, `os`, `status`, `message`, `login_time`) VALUES (1777305807503753216, 'admin@qq.com', '117.83.101.17', '江苏省苏州市', 'Chrome 12', 'Windows 10', 1, '一切OK', '2024-04-08 20:01:50');
INSERT INTO `tb_login_log` (`id`, `username`, `login_ip`, `login_location`, `browser`, `os`, `status`, `message`, `login_time`) VALUES (1777355231965741056, 'admin@qq.com', '117.83.101.17', '江苏省苏州市', 'Chrome 12', 'Windows 10', 1, '一切OK', '2024-04-08 23:18:06');
INSERT INTO `tb_login_log` (`id`, `username`, `login_ip`, `login_location`, `browser`, `os`, `status`, `message`, `login_time`) VALUES (1777962143786729472, 'admin@qq.com', '49.65.220.62', '江苏省南京市', 'Chrome 12', 'Mac OS X', 1, '一切OK', '2024-04-10 15:29:50');
INSERT INTO `tb_login_log` (`id`, `username`, `login_ip`, `login_location`, `browser`, `os`, `status`, `message`, `login_time`) VALUES (1778317639467139072, 'test@qq.com', '223.104.204.122', '陕西省0', 'Chrome Mobile', 'Android 1.x', 1, '一切OK', '2024-04-11 15:02:29');
INSERT INTO `tb_login_log` (`id`, `username`, `login_ip`, `login_location`, `browser`, `os`, `status`, `message`, `login_time`) VALUES (1778340569433505792, 'test@qq.com', '124.115.169.86', '陕西省西安市', 'Firefox 12', 'Windows 10', 1, '一切OK', '2024-04-11 16:33:48');
INSERT INTO `tb_login_log` (`id`, `username`, `login_ip`, `login_location`, `browser`, `os`, `status`, `message`, `login_time`) VALUES (1779173311549276160, 'admin@qq.com', '127.0.0.1', '内网IP', 'Chrome 12', 'Mac OS X', 1, '一切OK', '2024-04-13 23:42:31');
INSERT INTO `tb_login_log` (`id`, `username`, `login_ip`, `login_location`, `browser`, `os`, `status`, `message`, `login_time`) VALUES (1779478464840073216, 'test@qq.com', '111.52.92.88', '山西省0', 'Chrome Mobile', 'Android 1.x', 1, '一切OK', '2024-04-14 19:55:57');
INSERT INTO `tb_login_log` (`id`, `username`, `login_ip`, `login_location`, `browser`, `os`, `status`, `message`, `login_time`) VALUES (1779478673389256704, 'admin@qq.com', '49.65.220.62', '江苏省南京市', 'Apple WebKit', 'Mac OS X (iPhone)', 1, '一切OK', '2024-04-14 19:56:05');
INSERT INTO `tb_login_log` (`id`, `username`, `login_ip`, `login_location`, `browser`, `os`, `status`, `message`, `login_time`) VALUES (1779868002037006336, 'test@qq.com', '27.39.88.91', '广东省东莞市', 'Chrome 12', 'Windows 10', 1, '一切OK', '2024-04-15 21:43:09');
INSERT INTO `tb_login_log` (`id`, `username`, `login_ip`, `login_location`, `browser`, `os`, `status`, `message`, `login_time`) VALUES (1780155940553621504, 'admin@qq.com', '58.208.182.73', '江苏省苏州市', 'Chrome 12', 'Windows 10', 1, '一切OK', '2024-04-16 16:47:07');
INSERT INTO `tb_login_log` (`id`, `username`, `login_ip`, `login_location`, `browser`, `os`, `status`, `message`, `login_time`) VALUES (1782324422649053184, 'test@qq.com', '121.229.82.109', '江苏省南京市', 'Chrome 12', 'Windows 10', 1, '一切OK', '2024-04-22 16:23:59');
INSERT INTO `tb_login_log` (`id`, `username`, `login_ip`, `login_location`, `browser`, `os`, `status`, `message`, `login_time`) VALUES (1782668255979110400, 'admin@qq.com', '180.108.32.225', '江苏省苏州市', 'Apple WebKit', 'Mac OS X (iPhone)', 1, '一切OK', '2024-04-23 15:10:33');
INSERT INTO `tb_login_log` (`id`, `username`, `login_ip`, `login_location`, `browser`, `os`, `status`, `message`, `login_time`) VALUES (1782669407068094464, 'admin@qq.com', '114.88.97.172', '上海上海市', 'Apple WebKit', 'Mac OS X (iPhone)', 1, '一切OK', '2024-04-23 15:15:21');
INSERT INTO `tb_login_log` (`id`, `username`, `login_ip`, `login_location`, `browser`, `os`, `status`, `message`, `login_time`) VALUES (1784418941238509568, 'test@qq.com', '117.80.143.123', '江苏省苏州市', 'Chrome 12', 'Windows 10', 1, '一切OK', '2024-04-28 11:07:06');
INSERT INTO `tb_login_log` (`id`, `username`, `login_ip`, `login_location`, `browser`, `os`, `status`, `message`, `login_time`) VALUES (1786577835721228288, 'test@qq.com', '121.229.82.109', '江苏省南京市', 'Chrome 12', 'Windows 10', 1, '一切OK', '2024-05-04 10:05:32');
INSERT INTO `tb_login_log` (`id`, `username`, `login_ip`, `login_location`, `browser`, `os`, `status`, `message`, `login_time`) VALUES (1786583881156132864, 'test@qq.com', '121.229.82.109', '江苏省南京市', 'Chrome 12', 'Windows 10', 1, '一切OK', '2024-05-04 10:29:33');
INSERT INTO `tb_login_log` (`id`, `username`, `login_ip`, `login_location`, `browser`, `os`, `status`, `message`, `login_time`) VALUES (1787048631442866176, 'test@qq.com', '113.65.95.87', '广东省广州市', 'Chrome 12', 'Windows 10', 1, '一切OK', '2024-05-05 17:16:25');
INSERT INTO `tb_login_log` (`id`, `username`, `login_ip`, `login_location`, `browser`, `os`, `status`, `message`, `login_time`) VALUES (1788031860626948096, 'test@qq.com', '113.87.82.125', '广东省深圳市', 'Chrome 12', 'Windows 10', 1, '一切OK', '2024-05-08 10:23:40');
INSERT INTO `tb_login_log` (`id`, `username`, `login_ip`, `login_location`, `browser`, `os`, `status`, `message`, `login_time`) VALUES (1788458612457209856, 'test@qq.com', '101.24.193.124', '河北省石家庄市', 'Chrome 12', 'Windows 10', 1, '一切OK', '2024-05-09 14:39:30');
INSERT INTO `tb_login_log` (`id`, `username`, `login_ip`, `login_location`, `browser`, `os`, `status`, `message`, `login_time`) VALUES (1788908057015615488, 'test@163.com', '58.20.26.242', '湖南省长沙市', 'Chrome 12', 'Windows 10', 0, '用户账户不存在', '2024-05-10 20:24:42');
INSERT INTO `tb_login_log` (`id`, `username`, `login_ip`, `login_location`, `browser`, `os`, `status`, `message`, `login_time`) VALUES (1790908996954095616, 'admin@qq.com', '223.104.67.200', '广东省东莞市', 'Chrome 12', 'Mac OS X', 1, '一切OK', '2024-05-16 08:56:03');
INSERT INTO `tb_login_log` (`id`, `username`, `login_ip`, `login_location`, `browser`, `os`, `status`, `message`, `login_time`) VALUES (1790909021289447424, 'admin@qq.com', '223.104.67.200', '广东省东莞市', 'Chrome 12', 'Mac OS X', 0, '用户验证码错误', '2024-05-16 08:55:59');
INSERT INTO `tb_login_log` (`id`, `username`, `login_ip`, `login_location`, `browser`, `os`, `status`, `message`, `login_time`) VALUES (1790945882351337472, 'admin@qq.com', '120.85.105.96', '广东省广州市', 'Chrome 12', 'Mac OS X', 1, '一切OK', '2024-05-16 11:22:34');
INSERT INTO `tb_login_log` (`id`, `username`, `login_ip`, `login_location`, `browser`, `os`, `status`, `message`, `login_time`) VALUES (1791343965077766144, 'admin@qq.com', '120.85.105.96', '广东省广州市', 'Chrome 12', 'Mac OS X', 1, '一切OK', '2024-05-17 13:44:24');
INSERT INTO `tb_login_log` (`id`, `username`, `login_ip`, `login_location`, `browser`, `os`, `status`, `message`, `login_time`) VALUES (1791377569216462848, 'admin@qq.com', '120.85.105.96', '广东省广州市', 'Chrome 12', 'Mac OS X', 1, '一切OK', '2024-05-17 15:59:00');
INSERT INTO `tb_login_log` (`id`, `username`, `login_ip`, `login_location`, `browser`, `os`, `status`, `message`, `login_time`) VALUES (1792375066709721088, 'admin@qq.com', '120.85.105.96', '广东省广州市', 'Chrome 12', 'Mac OS X', 1, '一切OK', '2024-05-20 10:01:42');
INSERT INTO `tb_login_log` (`id`, `username`, `login_ip`, `login_location`, `browser`, `os`, `status`, `message`, `login_time`) VALUES (1792819526211993600, 'admin@qq.com', '223.104.67.151', '广东省东莞市', 'Chrome 12', 'Mac OS X', 1, '一切OK', '2024-05-21 15:27:53');
INSERT INTO `tb_login_log` (`id`, `username`, `login_ip`, `login_location`, `browser`, `os`, `status`, `message`, `login_time`) VALUES (1793165211973189632, 'admin@qq.com', '49.73.57.57', '江苏省苏州市', 'Chrome 12', 'Windows 10', 0, '用户验证码错误', '2024-05-22 14:21:17');
INSERT INTO `tb_login_log` (`id`, `username`, `login_ip`, `login_location`, `browser`, `os`, `status`, `message`, `login_time`) VALUES (1793165230126137344, 'admin@qq.com', '49.73.57.57', '江苏省苏州市', 'Chrome 12', 'Windows 10', 0, '用户验证码错误', '2024-05-22 14:21:21');
INSERT INTO `tb_login_log` (`id`, `username`, `login_ip`, `login_location`, `browser`, `os`, `status`, `message`, `login_time`) VALUES (1793170217870819328, 'test@qq.com', '120.85.104.236', '广东省广州市', 'Chrome 12', 'Windows 10', 1, '一切OK', '2024-05-22 14:41:22');
INSERT INTO `tb_login_log` (`id`, `username`, `login_ip`, `login_location`, `browser`, `os`, `status`, `message`, `login_time`) VALUES (1793170560771948544, 'admin@qq.com', '120.85.104.236', '广东省广州市', 'Chrome 12', 'Windows 10', 1, '一切OK', '2024-05-22 14:42:43');
INSERT INTO `tb_login_log` (`id`, `username`, `login_ip`, `login_location`, `browser`, `os`, `status`, `message`, `login_time`) VALUES (1793180919968825344, 'admin@qq.com', '120.85.104.236', '广东省广州市', 'Chrome 12', 'Mac OS X', 1, '一切OK', '2024-05-22 15:23:47');
INSERT INTO `tb_login_log` (`id`, `username`, `login_ip`, `login_location`, `browser`, `os`, `status`, `message`, `login_time`) VALUES (1793185882459602944, 'test@qq.com', '59.42.128.12', '广东省广州市', 'Chrome 12', 'Windows 10', 1, '一切OK', '2024-05-22 15:44:18');
INSERT INTO `tb_login_log` (`id`, `username`, `login_ip`, `login_location`, `browser`, `os`, `status`, `message`, `login_time`) VALUES (1794922640880697344, 'admin@qq.com', '120.85.104.236', '广东省广州市', 'Chrome 12', 'Mac OS X', 1, '一切OK', '2024-05-27 10:44:51');
INSERT INTO `tb_login_log` (`id`, `username`, `login_ip`, `login_location`, `browser`, `os`, `status`, `message`, `login_time`) VALUES (1795378946406088704, 'admin@qq.com', '120.85.104.236', '广东省广州市', 'Chrome 12', 'Mac OS X', 1, '一切OK', '2024-05-28 16:58:00');
INSERT INTO `tb_login_log` (`id`, `username`, `login_ip`, `login_location`, `browser`, `os`, `status`, `message`, `login_time`) VALUES (1797096478393499648, 'test@qq.com', '218.12.17.98', '河北省石家庄市', 'Chrome 12', 'Windows 10', 1, '一切OK', '2024-06-02 10:42:49');
INSERT INTO `tb_login_log` (`id`, `username`, `login_ip`, `login_location`, `browser`, `os`, `status`, `message`, `login_time`) VALUES (1800824217596329984, 'test@qq.com', '218.104.142.162', '福建省厦门市', 'Chrome 12', 'Mac OS X', 1, '一切OK', '2024-06-12 17:36:11');
INSERT INTO `tb_login_log` (`id`, `username`, `login_ip`, `login_location`, `browser`, `os`, `status`, `message`, `login_time`) VALUES (1800824271346335744, 'test@qq.com', '223.156.85.242', '湖南省湘潭市', 'Chrome 11', 'Windows 10', 1, '一切OK', '2024-06-12 17:35:41');
INSERT INTO `tb_login_log` (`id`, `username`, `login_ip`, `login_location`, `browser`, `os`, `status`, `message`, `login_time`) VALUES (1800824284122185728, 'test@qq.com', '115.197.186.161', '浙江省杭州市', 'Chrome 10', 'Mac OS X', 1, '一切OK', '2024-06-12 17:36:07');
INSERT INTO `tb_login_log` (`id`, `username`, `login_ip`, `login_location`, `browser`, `os`, `status`, `message`, `login_time`) VALUES (1800824301864091648, 'test@qq.com', '218.104.142.162', '福建省厦门市', 'Chrome 12', 'Mac OS X', 0, '用户验证码错误', '2024-06-12 17:35:46');
INSERT INTO `tb_login_log` (`id`, `username`, `login_ip`, `login_location`, `browser`, `os`, `status`, `message`, `login_time`) VALUES (1800824302568734720, 'test@qq.com', '110.53.183.135', '湖南省长沙市', 'Chrome 12', 'Windows 10', 1, '一切OK', '2024-06-12 17:36:16');
INSERT INTO `tb_login_log` (`id`, `username`, `login_ip`, `login_location`, `browser`, `os`, `status`, `message`, `login_time`) VALUES (1800824320302252032, 'test@qq.com', '182.139.162.96', '四川省成都市', 'Chrome 12', 'Windows 10', 1, '一切OK', '2024-06-12 17:35:58');
INSERT INTO `tb_login_log` (`id`, `username`, `login_ip`, `login_location`, `browser`, `os`, `status`, `message`, `login_time`) VALUES (1800824490901372928, 'test@qq.com', '223.71.99.11', '北京北京市', 'Chrome 12', 'Mac OS X', 1, '一切OK', '2024-06-12 17:36:52');
INSERT INTO `tb_login_log` (`id`, `username`, `login_ip`, `login_location`, `browser`, `os`, `status`, `message`, `login_time`) VALUES (1800824537919520768, 'test@qq.com', '223.71.99.11', '北京北京市', 'Chrome 12', 'Mac OS X', 0, '用户密码错误', '2024-06-12 17:36:42');
INSERT INTO `tb_login_log` (`id`, `username`, `login_ip`, `login_location`, `browser`, `os`, `status`, `message`, `login_time`) VALUES (1800824705502937088, 'test@qq.com', '183.220.61.219', '四川省德阳市', 'Chrome 12', 'Windows 10', 1, '一切OK', '2024-06-12 17:37:27');
INSERT INTO `tb_login_log` (`id`, `username`, `login_ip`, `login_location`, `browser`, `os`, `status`, `message`, `login_time`) VALUES (1800825863961313280, 'test@qq.com', '223.156.85.242', '湖南省湘潭市', 'Chrome 12', 'Windows 10', 0, '用户密码错误', '2024-06-12 17:41:58');
INSERT INTO `tb_login_log` (`id`, `username`, `login_ip`, `login_location`, `browser`, `os`, `status`, `message`, `login_time`) VALUES (1800825909675032576, 'test@qq.com', '223.156.85.242', '湖南省湘潭市', 'Chrome 12', 'Windows 10', 0, '用户验证码错误', '2024-06-12 17:42:09');
INSERT INTO `tb_login_log` (`id`, `username`, `login_ip`, `login_location`, `browser`, `os`, `status`, `message`, `login_time`) VALUES (1800825964737855488, 'test@qq.com', '223.156.85.242', '湖南省湘潭市', 'Chrome 12', 'Windows 10', 0, '用户验证码错误', '2024-06-12 17:42:22');
INSERT INTO `tb_login_log` (`id`, `username`, `login_ip`, `login_location`, `browser`, `os`, `status`, `message`, `login_time`) VALUES (1800825976226054144, 'test@qq.com', '223.156.85.242', '湖南省湘潭市', 'Chrome 12', 'Windows 10', 1, '一切OK', '2024-06-12 17:42:29');
INSERT INTO `tb_login_log` (`id`, `username`, `login_ip`, `login_location`, `browser`, `os`, `status`, `message`, `login_time`) VALUES (1801437866954326016, 'admin@qq.com', '223.104.67.16', '广东省东莞市', 'Chrome 12', 'Windows 10', 1, '一切OK', '2024-06-14 10:14:01');
INSERT INTO `tb_login_log` (`id`, `username`, `login_ip`, `login_location`, `browser`, `os`, `status`, `message`, `login_time`) VALUES (1801502600709799936, 'admin@qq.com', '121.224.113.144', '江苏省苏州市', 'Chrome 12', 'Windows 10', 0, '用户验证码错误', '2024-06-14 14:31:05');
INSERT INTO `tb_login_log` (`id`, `username`, `login_ip`, `login_location`, `browser`, `os`, `status`, `message`, `login_time`) VALUES (1801502612470628352, 'admin@qq.com', '121.224.113.144', '江苏省苏州市', 'Chrome 12', 'Windows 10', 1, '一切OK', '2024-06-14 14:31:14');
COMMIT;

-- ----------------------------
-- Table structure for tb_menu
-- ----------------------------
DROP TABLE IF EXISTS `tb_menu`;
CREATE TABLE `tb_menu` (
  `id` bigint NOT NULL COMMENT '主键',
  `name` varchar(50) DEFAULT NULL COMMENT '菜单名称',
  `type` char(1) DEFAULT NULL COMMENT '菜单类型：D：目录，P：页面',
  `parent_id` bigint DEFAULT NULL COMMENT '父菜单',
  `path` varchar(100) DEFAULT NULL COMMENT '访问路径',
  `component` varchar(100) DEFAULT NULL COMMENT '组件',
  `is_cache` tinyint DEFAULT NULL COMMENT '是否缓存 （0: 不缓存 1: 缓存）',
  `is_link` tinyint DEFAULT NULL COMMENT '是否外链 （0: 不是外链 1: 外链）',
  `visible` tinyint DEFAULT NULL COMMENT '是否隐藏 （0: 不隐藏 1: 隐藏）',
  `active_menu` varchar(100) DEFAULT NULL COMMENT '激活菜单',
  `status` tinyint DEFAULT NULL COMMENT '状态，0：禁用，1：正常',
  `icon` varchar(50) DEFAULT NULL COMMENT '图标',
  `sort` int DEFAULT NULL COMMENT '菜单顺序',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `update_time` datetime DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci COMMENT='菜单表';

-- ----------------------------
-- Records of tb_menu
-- ----------------------------
BEGIN;
INSERT INTO `tb_menu` (`id`, `name`, `type`, `parent_id`, `path`, `component`, `is_cache`, `is_link`, `visible`, `active_menu`, `status`, `icon`, `sort`, `create_time`, `update_time`) VALUES (1647951751875133440, '博客管理', 'D', 0, 'blog', NULL, 0, 0, 0, NULL, 1, 'content', 1, '2023-04-17 21:17:07', '2023-12-21 14:27:20');
INSERT INTO `tb_menu` (`id`, `name`, `type`, `parent_id`, `path`, `component`, `is_cache`, `is_link`, `visible`, `active_menu`, `status`, `icon`, `sort`, `create_time`, `update_time`) VALUES (1647951751875133441, '网站管理', 'D', 0, 'website', NULL, 0, 0, 0, NULL, 1, 'message', 2, '2023-04-17 21:17:46', '2023-08-03 19:34:56');
INSERT INTO `tb_menu` (`id`, `name`, `type`, `parent_id`, `path`, `component`, `is_cache`, `is_link`, `visible`, `active_menu`, `status`, `icon`, `sort`, `create_time`, `update_time`) VALUES (1647951751875133442, '项目文档', 'D', 1714673277053116418, 'https://www.baidu.com', NULL, 0, 1, 0, NULL, 1, 'guide', 3, '2023-04-17 21:19:30', '2023-12-21 14:20:52');
INSERT INTO `tb_menu` (`id`, `name`, `type`, `parent_id`, `path`, `component`, `is_cache`, `is_link`, `visible`, `active_menu`, `status`, `icon`, `sort`, `create_time`, `update_time`) VALUES (1647952968122630144, '文章管理', 'P', 1647951751875133440, 'article', 'blog/article/index', 0, 0, 0, NULL, 1, 'article-create', 1, '2023-04-17 21:22:42', '2023-12-21 14:28:03');
INSERT INTO `tb_menu` (`id`, `name`, `type`, `parent_id`, `path`, `component`, `is_cache`, `is_link`, `visible`, `active_menu`, `status`, `icon`, `sort`, `create_time`, `update_time`) VALUES (1648539108017963008, '评论管理', 'P', 1647951751875133440, 'comment', 'blog/comment/index', 0, 0, 0, NULL, 1, 'comments', 2, '2023-04-19 12:09:46', '2023-12-21 14:28:10');
INSERT INTO `tb_menu` (`id`, `name`, `type`, `parent_id`, `path`, `component`, `is_cache`, `is_link`, `visible`, `active_menu`, `status`, `icon`, `sort`, `create_time`, `update_time`) VALUES (1651040949146484736, '留言管理', 'P', 1647951751875133440, 'message', 'blog/message/index', 0, 0, 0, NULL, 1, 'message', 3, '2023-04-26 09:51:28', '2023-12-21 14:28:17');
INSERT INTO `tb_menu` (`id`, `name`, `type`, `parent_id`, `path`, `component`, `is_cache`, `is_link`, `visible`, `active_menu`, `status`, `icon`, `sort`, `create_time`, `update_time`) VALUES (1651040949146484737, '分类管理', 'P', 1647951751875133440, 'category', 'blog/category/index', 0, 0, 0, NULL, 1, 'category', 4, '2023-04-26 09:52:57', '2023-12-21 14:28:23');
INSERT INTO `tb_menu` (`id`, `name`, `type`, `parent_id`, `path`, `component`, `is_cache`, `is_link`, `visible`, `active_menu`, `status`, `icon`, `sort`, `create_time`, `update_time`) VALUES (1651042040491802625, '标签管理', 'P', 1647951751875133440, 'tags', 'blog/tags/index', 0, 0, 0, NULL, 1, 'tags', 5, '2023-04-26 09:53:51', '2023-12-21 14:28:28');
INSERT INTO `tb_menu` (`id`, `name`, `type`, `parent_id`, `path`, `component`, `is_cache`, `is_link`, `visible`, `active_menu`, `status`, `icon`, `sort`, `create_time`, `update_time`) VALUES (1651217423560343552, '文件管理', 'P', 1647951751875133441, 'file', 'website/file/index', 0, 0, 0, NULL, 1, 'article-ranking', 1, '2023-04-26 21:33:29', '2023-08-03 19:35:08');
INSERT INTO `tb_menu` (`id`, `name`, `type`, `parent_id`, `path`, `component`, `is_cache`, `is_link`, `visible`, `active_menu`, `status`, `icon`, `sort`, `create_time`, `update_time`) VALUES (1651217423560343553, '相册管理', 'P', 1647951751875133440, 'photo', 'blog/photo/index', 0, 0, 0, NULL, 1, 'album', 7, '2023-04-26 21:33:31', '2024-02-05 16:37:18');
INSERT INTO `tb_menu` (`id`, `name`, `type`, `parent_id`, `path`, `component`, `is_cache`, `is_link`, `visible`, `active_menu`, `status`, `icon`, `sort`, `create_time`, `update_time`) VALUES (1651217423560343554, '页面管理', 'P', 1647951751875133440, 'page', 'blog/page/index', 0, 0, 0, NULL, 1, 'documentation', 8, '2023-04-26 21:33:29', '2023-12-21 14:28:54');
INSERT INTO `tb_menu` (`id`, `name`, `type`, `parent_id`, `path`, `component`, `is_cache`, `is_link`, `visible`, `active_menu`, `status`, `icon`, `sort`, `create_time`, `update_time`) VALUES (1651217423560343555, '角色管理', 'P', 1652313423859417088, 'role', 'system/role/index', 0, 0, 0, NULL, 1, 'role', 2, '2023-04-26 21:33:29', '2023-12-21 14:20:20');
INSERT INTO `tb_menu` (`id`, `name`, `type`, `parent_id`, `path`, `component`, `is_cache`, `is_link`, `visible`, `active_menu`, `status`, `icon`, `sort`, `create_time`, `update_time`) VALUES (1651217423560343556, '友链管理', 'P', 1647951751875133440, 'link', 'blog/link/index', 0, 0, 0, NULL, 1, 'personnel', 6, '2023-04-26 21:33:29', '2023-12-21 14:28:37');
INSERT INTO `tb_menu` (`id`, `name`, `type`, `parent_id`, `path`, `component`, `is_cache`, `is_link`, `visible`, `active_menu`, `status`, `icon`, `sort`, `create_time`, `update_time`) VALUES (1651217423560343557, '用户管理', 'P', 1652313423859417088, 'user', 'system/user/index', 0, 0, 0, NULL, 1, 'personnel-manage', 1, '2023-04-26 21:33:29', '2023-12-21 14:20:16');
INSERT INTO `tb_menu` (`id`, `name`, `type`, `parent_id`, `path`, `component`, `is_cache`, `is_link`, `visible`, `active_menu`, `status`, `icon`, `sort`, `create_time`, `update_time`) VALUES (1651217423560343558, '公告管理', 'P', 1647951751875133441, 'notice', 'website/notice/index', 0, 0, 0, NULL, 1, 'email', 2, '2023-04-26 21:33:29', '2023-12-21 14:20:02');
INSERT INTO `tb_menu` (`id`, `name`, `type`, `parent_id`, `path`, `component`, `is_cache`, `is_link`, `visible`, `active_menu`, `status`, `icon`, `sort`, `create_time`, `update_time`) VALUES (1651217423560343559, '权限管理', 'P', 1652313423859417088, 'security', 'system/security/index', 0, 0, 0, NULL, 1, 'security', 3, '2023-04-26 21:33:29', '2024-01-16 23:39:03');
INSERT INTO `tb_menu` (`id`, `name`, `type`, `parent_id`, `path`, `component`, `is_cache`, `is_link`, `visible`, `active_menu`, `status`, `icon`, `sort`, `create_time`, `update_time`) VALUES (1651217423560343560, '菜单管理', 'P', 1652313423859417088, 'menu', 'system/menu/index', 0, 0, 0, NULL, 1, 'menu', 4, '2023-04-26 21:33:29', '2023-12-21 14:20:29');
INSERT INTO `tb_menu` (`id`, `name`, `type`, `parent_id`, `path`, `component`, `is_cache`, `is_link`, `visible`, `active_menu`, `status`, `icon`, `sort`, `create_time`, `update_time`) VALUES (1651217423560343561, '网站配置', 'P', 1647951751875133441, 'config', 'website/config/index', 0, 0, 0, NULL, 1, 'edit', 3, '2023-04-26 21:33:29', '2023-12-21 14:20:07');
INSERT INTO `tb_menu` (`id`, `name`, `type`, `parent_id`, `path`, `component`, `is_cache`, `is_link`, `visible`, `active_menu`, `status`, `icon`, `sort`, `create_time`, `update_time`) VALUES (1651227500568641536, '系统监控', 'D', 0, 'monitor', NULL, 0, 0, 0, NULL, 1, 'monitor', 4, '2023-04-26 22:15:11', '2023-08-03 19:35:16');
INSERT INTO `tb_menu` (`id`, `name`, `type`, `parent_id`, `path`, `component`, `is_cache`, `is_link`, `visible`, `active_menu`, `status`, `icon`, `sort`, `create_time`, `update_time`) VALUES (1651227500568641537, '在线用户', 'P', 1651227500568641536, 'online', 'monitor/online/index', 0, 0, 0, NULL, 1, 'online', 1, '2023-04-26 22:15:55', '2023-08-03 19:35:19');
INSERT INTO `tb_menu` (`id`, `name`, `type`, `parent_id`, `path`, `component`, `is_cache`, `is_link`, `visible`, `active_menu`, `status`, `icon`, `sort`, `create_time`, `update_time`) VALUES (1651227500568641538, '服务监控', 'P', 1651227500568641536, 'server', 'monitor/server/index', 0, 0, 0, NULL, 1, 'server', 2, '2023-04-26 22:16:24', '2023-08-03 19:35:20');
INSERT INTO `tb_menu` (`id`, `name`, `type`, `parent_id`, `path`, `component`, `is_cache`, `is_link`, `visible`, `active_menu`, `status`, `icon`, `sort`, `create_time`, `update_time`) VALUES (1651227500568641539, '缓存管理', 'P', 1651227500568641536, 'cache', 'monitor/cache/index', 0, 0, 0, NULL, 1, 'redis', 3, '2023-04-26 22:16:59', '2023-08-03 19:35:21');
INSERT INTO `tb_menu` (`id`, `name`, `type`, `parent_id`, `path`, `component`, `is_cache`, `is_link`, `visible`, `active_menu`, `status`, `icon`, `sort`, `create_time`, `update_time`) VALUES (1651227500568641540, '操作日志', 'P', 1751797702926106625, 'operate-log', 'logs/operate-log/index', 0, 0, 0, NULL, 1, 'form', 2, '2023-04-26 22:18:09', '2024-01-29 10:42:58');
INSERT INTO `tb_menu` (`id`, `name`, `type`, `parent_id`, `path`, `component`, `is_cache`, `is_link`, `visible`, `active_menu`, `status`, `icon`, `sort`, `create_time`, `update_time`) VALUES (1651227500568641541, '登录日志', 'P', 1751797702926106625, 'login-log', 'logs/login-log/index', 0, 0, 0, NULL, 1, 'logininfor', 1, '2023-04-26 22:18:32', '2024-01-29 10:42:52');
INSERT INTO `tb_menu` (`id`, `name`, `type`, `parent_id`, `path`, `component`, `is_cache`, `is_link`, `visible`, `active_menu`, `status`, `icon`, `sort`, `create_time`, `update_time`) VALUES (1652313423859417088, '系统管理', 'D', 0, 'system', NULL, 0, 0, 0, NULL, 1, 'system', 3, '2023-04-29 22:08:18', '2023-08-03 19:35:24');
INSERT INTO `tb_menu` (`id`, `name`, `type`, `parent_id`, `path`, `component`, `is_cache`, `is_link`, `visible`, `active_menu`, `status`, `icon`, `sort`, `create_time`, `update_time`) VALUES (1714673277053116418, '访问项目', 'D', 0, 'project', NULL, 0, 0, 0, NULL, 1, 'content', 6, '2023-10-19 00:02:21', '2024-01-29 10:41:03');
INSERT INTO `tb_menu` (`id`, `name`, `type`, `parent_id`, `path`, `component`, `is_cache`, `is_link`, `visible`, `active_menu`, `status`, `icon`, `sort`, `create_time`, `update_time`) VALUES (1714673499896487938, '相册纪念馆', 'P', 1714673277053116418, 'https://album.zrkizzy.com', NULL, 0, 0, 0, NULL, 1, 'permission', 2, '2023-10-19 00:03:14', '2023-12-21 14:20:42');
INSERT INTO `tb_menu` (`id`, `name`, `type`, `parent_id`, `path`, `component`, `is_cache`, `is_link`, `visible`, `active_menu`, `status`, `icon`, `sort`, `create_time`, `update_time`) VALUES (1714673667987415041, '博客首页', 'P', 1714673277053116418, 'https://www.zrkizzy.com', NULL, 0, 0, 0, NULL, 1, 'computer', 1, '2023-10-19 00:03:54', '2023-12-21 14:20:46');
INSERT INTO `tb_menu` (`id`, `name`, `type`, `parent_id`, `path`, `component`, `is_cache`, `is_link`, `visible`, `active_menu`, `status`, `icon`, `sort`, `create_time`, `update_time`) VALUES (1719375047478120449, '发布文章', 'P', 1737661101677502466, 'article-create', 'blog/article/components/article-create/index', 1, 0, 1, '/blog/article', 1, 'article-create', 0, '2023-10-31 23:25:30', '2024-01-29 16:05:09');
INSERT INTO `tb_menu` (`id`, `name`, `type`, `parent_id`, `path`, `component`, `is_cache`, `is_link`, `visible`, `active_menu`, `status`, `icon`, `sort`, `create_time`, `update_time`) VALUES (1719376052710182914, '文章详情', 'P', 1737661101677502466, 'article-edit/:id', 'blog/article/components/article-detail/index', 1, 0, 1, '/blog/article', 1, 'article', 0, '2023-10-31 23:29:30', '2024-01-29 16:05:16');
INSERT INTO `tb_menu` (`id`, `name`, `type`, `parent_id`, `path`, `component`, `is_cache`, `is_link`, `visible`, `active_menu`, `status`, `icon`, `sort`, `create_time`, `update_time`) VALUES (1726889420524638209, '发布成功', 'P', 1737661101677502466, 'article-success/:articleId', 'blog/article/components/article-success/index', 0, 0, 1, '/blog/article', 1, 'computer', 0, '2023-11-21 17:04:57', '2024-02-04 14:31:55');
INSERT INTO `tb_menu` (`id`, `name`, `type`, `parent_id`, `path`, `component`, `is_cache`, `is_link`, `visible`, `active_menu`, `status`, `icon`, `sort`, `create_time`, `update_time`) VALUES (1737661101677502466, '文章组件', 'D', 0, 'article-content', NULL, 0, 0, 1, NULL, 1, 'article-create', 100, '2023-12-21 10:27:45', '2024-01-29 10:40:28');
INSERT INTO `tb_menu` (`id`, `name`, `type`, `parent_id`, `path`, `component`, `is_cache`, `is_link`, `visible`, `active_menu`, `status`, `icon`, `sort`, `create_time`, `update_time`) VALUES (1737664918557118466, '配置组件', 'D', 0, 'config-content', NULL, 0, 0, 1, NULL, 1, 'component', 101, '2023-12-21 10:42:55', '2024-01-29 10:52:01');
INSERT INTO `tb_menu` (`id`, `name`, `type`, `parent_id`, `path`, `component`, `is_cache`, `is_link`, `visible`, `active_menu`, `status`, `icon`, `sort`, `create_time`, `update_time`) VALUES (1737665649209069569, '系统配置', 'P', 1737664918557118466, 'system', 'website/config/components/system/index.vue', 0, 0, 1, '/website/config', 1, 'system', 0, '2023-12-21 10:45:50', '2024-03-08 19:28:27');
INSERT INTO `tb_menu` (`id`, `name`, `type`, `parent_id`, `path`, `component`, `is_cache`, `is_link`, `visible`, `active_menu`, `status`, `icon`, `sort`, `create_time`, `update_time`) VALUES (1737666447162822658, '关于我', 'P', 1737664918557118466, 'about-me', 'website/config/components/about-me/index.vue', 0, 0, 1, '/website/config', 1, 'personnel-manage', 0, '2023-12-21 10:49:00', '2024-01-28 00:39:04');
INSERT INTO `tb_menu` (`id`, `name`, `type`, `parent_id`, `path`, `component`, `is_cache`, `is_link`, `visible`, `active_menu`, `status`, `icon`, `sort`, `create_time`, `update_time`) VALUES (1737666885635362818, '权限组件', 'D', 0, 'security-content', NULL, 0, 0, 1, NULL, 1, 'swagger', 102, '2023-12-21 10:50:44', '2024-01-29 10:40:44');
INSERT INTO `tb_menu` (`id`, `name`, `type`, `parent_id`, `path`, `component`, `is_cache`, `is_link`, `visible`, `active_menu`, `status`, `icon`, `sort`, `create_time`, `update_time`) VALUES (1737668345395761153, '接口管理', 'P', 1737666885635362818, 'resource', 'system/security/components/resource/index.vue', 0, 0, 1, '/system/security', 1, 'article', 0, '2023-12-21 10:56:33', '2024-03-06 21:52:13');
INSERT INTO `tb_menu` (`id`, `name`, `type`, `parent_id`, `path`, `component`, `is_cache`, `is_link`, `visible`, `active_menu`, `status`, `icon`, `sort`, `create_time`, `update_time`) VALUES (1737668522479276033, '模块管理', 'P', 1737666885635362818, 'module/:id', 'system/security/components/module/index.vue', 0, 0, 1, '/system/security', 1, 'tree-table', 0, '2023-12-21 10:57:15', '2024-01-28 00:38:35');
INSERT INTO `tb_menu` (`id`, `name`, `type`, `parent_id`, `path`, `component`, `is_cache`, `is_link`, `visible`, `active_menu`, `status`, `icon`, `sort`, `create_time`, `update_time`) VALUES (1738931248316907521, '缓存监控', 'P', 1651227500568641536, 'cache-monitor', 'monitor/cache/Monitor.vue', 0, 0, 0, NULL, 1, 'monitor', 4, '2023-12-24 22:34:52', '2024-01-22 15:00:58');
INSERT INTO `tb_menu` (`id`, `name`, `type`, `parent_id`, `path`, `component`, `is_cache`, `is_link`, `visible`, `active_menu`, `status`, `icon`, `sort`, `create_time`, `update_time`) VALUES (1745696963082321920, '代码生成', 'P', 1647951751875133441, 'generator', 'website/generator/index', 0, 0, 0, NULL, 1, 'code', 4, '2024-01-10 16:23:39', '2024-01-10 16:23:39');
INSERT INTO `tb_menu` (`id`, `name`, `type`, `parent_id`, `path`, `component`, `is_cache`, `is_link`, `visible`, `active_menu`, `status`, `icon`, `sort`, `create_time`, `update_time`) VALUES (1747151945530867712, '系统组件', 'D', 0, 'system-content', NULL, 0, 0, 1, NULL, 1, 'tree-table', 103, '2024-01-16 00:05:45', '2024-01-29 10:40:54');
INSERT INTO `tb_menu` (`id`, `name`, `type`, `parent_id`, `path`, `component`, `is_cache`, `is_link`, `visible`, `active_menu`, `status`, `icon`, `sort`, `create_time`, `update_time`) VALUES (1747151945530867713, '代码生成日志', 'P', 1747151945530867712, 'generate-log', 'website/generator/components/generate-log/index.vue', 0, 0, 1, '/website/generator', 1, 'time', 0, '2024-01-16 00:07:17', '2024-01-28 00:39:37');
INSERT INTO `tb_menu` (`id`, `name`, `type`, `parent_id`, `path`, `component`, `is_cache`, `is_link`, `visible`, `active_menu`, `status`, `icon`, `sort`, `create_time`, `update_time`) VALUES (1747282513481969666, '字典管理', 'P', 1652313423859417088, 'dict-type', 'system/dict/index.vue', 0, 0, 0, NULL, 1, 'dict', 5, '2024-01-16 23:39:49', NULL);
INSERT INTO `tb_menu` (`id`, `name`, `type`, `parent_id`, `path`, `component`, `is_cache`, `is_link`, `visible`, `active_menu`, `status`, `icon`, `sort`, `create_time`, `update_time`) VALUES (1748712694704795650, '字典数据', 'P', 1747151945530867712, 'dict-data/:dictTypeId', 'system/dict/components/dict-data/index.vue', 0, 0, 1, '/system/dict-type', 1, 'table', 0, '2024-01-20 22:22:51', '2024-01-28 22:32:10');
INSERT INTO `tb_menu` (`id`, `name`, `type`, `parent_id`, `path`, `component`, `is_cache`, `is_link`, `visible`, `active_menu`, `status`, `icon`, `sort`, `create_time`, `update_time`) VALUES (1751797702926106625, '日志管理', 'D', 0, 'logs', NULL, 0, 0, 0, NULL, 1, 'log', 5, '2024-01-29 10:41:34', NULL);
INSERT INTO `tb_menu` (`id`, `name`, `type`, `parent_id`, `path`, `component`, `is_cache`, `is_link`, `visible`, `active_menu`, `status`, `icon`, `sort`, `create_time`, `update_time`) VALUES (1762030630404182017, '定时任务', 'P', 1651227500568641536, 'scheduler-job', 'monitor/scheduler/index.vue', 0, 0, 0, '', 1, 'job', 5, '2024-02-26 16:23:34', '2024-02-26 16:24:07');
INSERT INTO `tb_menu` (`id`, `name`, `type`, `parent_id`, `path`, `component`, `is_cache`, `is_link`, `visible`, `active_menu`, `status`, `icon`, `sort`, `create_time`, `update_time`) VALUES (1765414044511596546, '调度日志', 'P', 1747151945530867712, 'scheduler-job-log/:jobId', 'monitor/scheduler/components/SchedulerJobLog.vue', 0, 0, 0, '/monitor/scheduler-job', 1, 'log', 0, '2024-03-07 00:28:03', '2024-03-07 00:29:10');
INSERT INTO `tb_menu` (`id`, `name`, `type`, `parent_id`, `path`, `component`, `is_cache`, `is_link`, `visible`, `active_menu`, `status`, `icon`, `sort`, `create_time`, `update_time`) VALUES (1766811174702686209, '草稿箱', 'P', 1737661101677502466, 'article-drafts', 'blog/article/components/article-drafts/index', 0, 0, 0, '/blog/article', 1, 'build', 0, '2024-03-10 20:59:44', '2024-03-10 21:00:02');
COMMIT;

-- ----------------------------
-- Table structure for tb_menu_role
-- ----------------------------
DROP TABLE IF EXISTS `tb_menu_role`;
CREATE TABLE `tb_menu_role` (
  `id` bigint NOT NULL COMMENT '主键',
  `role_id` bigint DEFAULT NULL COMMENT '角色ID',
  `menu_id` bigint DEFAULT NULL COMMENT '菜单ID',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci COMMENT='菜单角色关联表';

-- ----------------------------
-- Records of tb_menu_role
-- ----------------------------
BEGIN;
INSERT INTO `tb_menu_role` (`id`, `role_id`, `menu_id`, `create_time`) VALUES (1753947610202243072, 1633657944153260032, 1647951751875133440, NULL);
INSERT INTO `tb_menu_role` (`id`, `role_id`, `menu_id`, `create_time`) VALUES (1753947610202243073, 1633657944153260032, 1647952968122630144, NULL);
INSERT INTO `tb_menu_role` (`id`, `role_id`, `menu_id`, `create_time`) VALUES (1753947610202243074, 1633657944153260032, 1648539108017963008, NULL);
INSERT INTO `tb_menu_role` (`id`, `role_id`, `menu_id`, `create_time`) VALUES (1753947610202243075, 1633657944153260032, 1651040949146484736, NULL);
INSERT INTO `tb_menu_role` (`id`, `role_id`, `menu_id`, `create_time`) VALUES (1753947610202243076, 1633657944153260032, 1651040949146484737, NULL);
INSERT INTO `tb_menu_role` (`id`, `role_id`, `menu_id`, `create_time`) VALUES (1753947610202243077, 1633657944153260032, 1651042040491802625, NULL);
INSERT INTO `tb_menu_role` (`id`, `role_id`, `menu_id`, `create_time`) VALUES (1753947610202243078, 1633657944153260032, 1651217423560343553, NULL);
INSERT INTO `tb_menu_role` (`id`, `role_id`, `menu_id`, `create_time`) VALUES (1753947610202243079, 1633657944153260032, 1651217423560343554, NULL);
INSERT INTO `tb_menu_role` (`id`, `role_id`, `menu_id`, `create_time`) VALUES (1753947610202243080, 1633657944153260032, 1651217423560343556, NULL);
INSERT INTO `tb_menu_role` (`id`, `role_id`, `menu_id`, `create_time`) VALUES (1753947610202243081, 1633657944153260032, 1647951751875133441, NULL);
INSERT INTO `tb_menu_role` (`id`, `role_id`, `menu_id`, `create_time`) VALUES (1753947610202243082, 1633657944153260032, 1651217423560343552, NULL);
INSERT INTO `tb_menu_role` (`id`, `role_id`, `menu_id`, `create_time`) VALUES (1753947610202243083, 1633657944153260032, 1745696963082321920, NULL);
INSERT INTO `tb_menu_role` (`id`, `role_id`, `menu_id`, `create_time`) VALUES (1753947610202243084, 1633657944153260032, 1651217423560343558, NULL);
INSERT INTO `tb_menu_role` (`id`, `role_id`, `menu_id`, `create_time`) VALUES (1753947610202243085, 1633657944153260032, 1651217423560343561, NULL);
INSERT INTO `tb_menu_role` (`id`, `role_id`, `menu_id`, `create_time`) VALUES (1753947610202243086, 1633657944153260032, 1652313423859417088, NULL);
INSERT INTO `tb_menu_role` (`id`, `role_id`, `menu_id`, `create_time`) VALUES (1753947610202243087, 1633657944153260032, 1651217423560343555, NULL);
INSERT INTO `tb_menu_role` (`id`, `role_id`, `menu_id`, `create_time`) VALUES (1753947610202243088, 1633657944153260032, 1747282513481969666, NULL);
INSERT INTO `tb_menu_role` (`id`, `role_id`, `menu_id`, `create_time`) VALUES (1753947610202243089, 1633657944153260032, 1651217423560343560, NULL);
INSERT INTO `tb_menu_role` (`id`, `role_id`, `menu_id`, `create_time`) VALUES (1753947610202243090, 1633657944153260032, 1651217423560343559, NULL);
INSERT INTO `tb_menu_role` (`id`, `role_id`, `menu_id`, `create_time`) VALUES (1753947610202243091, 1633657944153260032, 1651217423560343557, NULL);
INSERT INTO `tb_menu_role` (`id`, `role_id`, `menu_id`, `create_time`) VALUES (1753947610202243092, 1633657944153260032, 1651227500568641536, NULL);
INSERT INTO `tb_menu_role` (`id`, `role_id`, `menu_id`, `create_time`) VALUES (1753947610202243093, 1633657944153260032, 1651227500568641538, NULL);
INSERT INTO `tb_menu_role` (`id`, `role_id`, `menu_id`, `create_time`) VALUES (1753947610202243094, 1633657944153260032, 1738931248316907521, NULL);
INSERT INTO `tb_menu_role` (`id`, `role_id`, `menu_id`, `create_time`) VALUES (1753947610202243095, 1633657944153260032, 1651227500568641539, NULL);
INSERT INTO `tb_menu_role` (`id`, `role_id`, `menu_id`, `create_time`) VALUES (1753947610202243096, 1633657944153260032, 1651227500568641537, NULL);
INSERT INTO `tb_menu_role` (`id`, `role_id`, `menu_id`, `create_time`) VALUES (1753947610202243097, 1633657944153260032, 1751797702926106625, NULL);
INSERT INTO `tb_menu_role` (`id`, `role_id`, `menu_id`, `create_time`) VALUES (1753947610202243098, 1633657944153260032, 1651227500568641540, NULL);
INSERT INTO `tb_menu_role` (`id`, `role_id`, `menu_id`, `create_time`) VALUES (1753947610202243099, 1633657944153260032, 1651227500568641541, NULL);
INSERT INTO `tb_menu_role` (`id`, `role_id`, `menu_id`, `create_time`) VALUES (1753947610202243100, 1633657944153260032, 1714673277053116418, NULL);
INSERT INTO `tb_menu_role` (`id`, `role_id`, `menu_id`, `create_time`) VALUES (1753947610202243101, 1633657944153260032, 1714673499896487938, NULL);
INSERT INTO `tb_menu_role` (`id`, `role_id`, `menu_id`, `create_time`) VALUES (1753947610202243102, 1633657944153260032, 1714673667987415041, NULL);
INSERT INTO `tb_menu_role` (`id`, `role_id`, `menu_id`, `create_time`) VALUES (1753947610202243103, 1633657944153260032, 1647951751875133442, NULL);
INSERT INTO `tb_menu_role` (`id`, `role_id`, `menu_id`, `create_time`) VALUES (1753947610202243104, 1633657944153260032, 1737661101677502466, NULL);
INSERT INTO `tb_menu_role` (`id`, `role_id`, `menu_id`, `create_time`) VALUES (1753947610202243105, 1633657944153260032, 1719375047478120449, NULL);
INSERT INTO `tb_menu_role` (`id`, `role_id`, `menu_id`, `create_time`) VALUES (1753947610202243106, 1633657944153260032, 1719376052710182914, NULL);
INSERT INTO `tb_menu_role` (`id`, `role_id`, `menu_id`, `create_time`) VALUES (1753947610202243107, 1633657944153260032, 1726889420524638209, NULL);
INSERT INTO `tb_menu_role` (`id`, `role_id`, `menu_id`, `create_time`) VALUES (1753947610202243108, 1633657944153260032, 1737664918557118466, NULL);
INSERT INTO `tb_menu_role` (`id`, `role_id`, `menu_id`, `create_time`) VALUES (1753947610202243109, 1633657944153260032, 1737665649209069569, NULL);
INSERT INTO `tb_menu_role` (`id`, `role_id`, `menu_id`, `create_time`) VALUES (1753947610202243110, 1633657944153260032, 1737666447162822658, NULL);
INSERT INTO `tb_menu_role` (`id`, `role_id`, `menu_id`, `create_time`) VALUES (1753947610202243111, 1633657944153260032, 1737666885635362818, NULL);
INSERT INTO `tb_menu_role` (`id`, `role_id`, `menu_id`, `create_time`) VALUES (1753947610202243112, 1633657944153260032, 1737668345395761153, NULL);
INSERT INTO `tb_menu_role` (`id`, `role_id`, `menu_id`, `create_time`) VALUES (1753947610202243113, 1633657944153260032, 1737668522479276033, NULL);
INSERT INTO `tb_menu_role` (`id`, `role_id`, `menu_id`, `create_time`) VALUES (1753947610202243114, 1633657944153260032, 1747151945530867712, NULL);
INSERT INTO `tb_menu_role` (`id`, `role_id`, `menu_id`, `create_time`) VALUES (1753947610202243115, 1633657944153260032, 1747151945530867713, NULL);
INSERT INTO `tb_menu_role` (`id`, `role_id`, `menu_id`, `create_time`) VALUES (1753947610202243116, 1633657944153260032, 1748712694704795650, NULL);
INSERT INTO `tb_menu_role` (`id`, `role_id`, `menu_id`, `create_time`) VALUES (1764986440950743040, 1667607066451116032, 1647951751875133440, NULL);
INSERT INTO `tb_menu_role` (`id`, `role_id`, `menu_id`, `create_time`) VALUES (1764986440950743041, 1667607066451116032, 1647952968122630144, NULL);
INSERT INTO `tb_menu_role` (`id`, `role_id`, `menu_id`, `create_time`) VALUES (1764986440950743042, 1667607066451116032, 1648539108017963008, NULL);
INSERT INTO `tb_menu_role` (`id`, `role_id`, `menu_id`, `create_time`) VALUES (1764986440950743043, 1667607066451116032, 1651040949146484736, NULL);
INSERT INTO `tb_menu_role` (`id`, `role_id`, `menu_id`, `create_time`) VALUES (1764986440950743044, 1667607066451116032, 1651040949146484737, NULL);
INSERT INTO `tb_menu_role` (`id`, `role_id`, `menu_id`, `create_time`) VALUES (1764986440950743045, 1667607066451116032, 1651042040491802625, NULL);
INSERT INTO `tb_menu_role` (`id`, `role_id`, `menu_id`, `create_time`) VALUES (1764986440950743046, 1667607066451116032, 1651217423560343553, NULL);
INSERT INTO `tb_menu_role` (`id`, `role_id`, `menu_id`, `create_time`) VALUES (1764986440950743047, 1667607066451116032, 1651217423560343554, NULL);
INSERT INTO `tb_menu_role` (`id`, `role_id`, `menu_id`, `create_time`) VALUES (1764986440950743048, 1667607066451116032, 1651217423560343556, NULL);
INSERT INTO `tb_menu_role` (`id`, `role_id`, `menu_id`, `create_time`) VALUES (1764986440950743049, 1667607066451116032, 1647951751875133441, NULL);
INSERT INTO `tb_menu_role` (`id`, `role_id`, `menu_id`, `create_time`) VALUES (1764986440950743050, 1667607066451116032, 1651217423560343552, NULL);
INSERT INTO `tb_menu_role` (`id`, `role_id`, `menu_id`, `create_time`) VALUES (1764986440950743051, 1667607066451116032, 1745696963082321920, NULL);
INSERT INTO `tb_menu_role` (`id`, `role_id`, `menu_id`, `create_time`) VALUES (1764986440950743052, 1667607066451116032, 1651217423560343558, NULL);
INSERT INTO `tb_menu_role` (`id`, `role_id`, `menu_id`, `create_time`) VALUES (1764986440950743053, 1667607066451116032, 1651217423560343561, NULL);
INSERT INTO `tb_menu_role` (`id`, `role_id`, `menu_id`, `create_time`) VALUES (1764986440950743054, 1667607066451116032, 1652313423859417088, NULL);
INSERT INTO `tb_menu_role` (`id`, `role_id`, `menu_id`, `create_time`) VALUES (1764986440950743055, 1667607066451116032, 1651217423560343555, NULL);
INSERT INTO `tb_menu_role` (`id`, `role_id`, `menu_id`, `create_time`) VALUES (1764986440950743056, 1667607066451116032, 1747282513481969666, NULL);
INSERT INTO `tb_menu_role` (`id`, `role_id`, `menu_id`, `create_time`) VALUES (1764986440950743057, 1667607066451116032, 1651217423560343560, NULL);
INSERT INTO `tb_menu_role` (`id`, `role_id`, `menu_id`, `create_time`) VALUES (1764986440950743058, 1667607066451116032, 1651217423560343559, NULL);
INSERT INTO `tb_menu_role` (`id`, `role_id`, `menu_id`, `create_time`) VALUES (1764986440950743059, 1667607066451116032, 1651217423560343557, NULL);
INSERT INTO `tb_menu_role` (`id`, `role_id`, `menu_id`, `create_time`) VALUES (1764986440950743060, 1667607066451116032, 1651227500568641536, NULL);
INSERT INTO `tb_menu_role` (`id`, `role_id`, `menu_id`, `create_time`) VALUES (1764986440950743061, 1667607066451116032, 1762030630404182017, NULL);
INSERT INTO `tb_menu_role` (`id`, `role_id`, `menu_id`, `create_time`) VALUES (1764986440950743062, 1667607066451116032, 1738931248316907521, NULL);
INSERT INTO `tb_menu_role` (`id`, `role_id`, `menu_id`, `create_time`) VALUES (1764986440950743063, 1667607066451116032, 1651227500568641537, NULL);
INSERT INTO `tb_menu_role` (`id`, `role_id`, `menu_id`, `create_time`) VALUES (1764986440950743064, 1667607066451116032, 1651227500568641538, NULL);
INSERT INTO `tb_menu_role` (`id`, `role_id`, `menu_id`, `create_time`) VALUES (1764986440950743065, 1667607066451116032, 1651227500568641539, NULL);
INSERT INTO `tb_menu_role` (`id`, `role_id`, `menu_id`, `create_time`) VALUES (1764986440950743066, 1667607066451116032, 1751797702926106625, NULL);
INSERT INTO `tb_menu_role` (`id`, `role_id`, `menu_id`, `create_time`) VALUES (1764986440950743067, 1667607066451116032, 1651227500568641541, NULL);
INSERT INTO `tb_menu_role` (`id`, `role_id`, `menu_id`, `create_time`) VALUES (1764986440950743068, 1667607066451116032, 1651227500568641540, NULL);
INSERT INTO `tb_menu_role` (`id`, `role_id`, `menu_id`, `create_time`) VALUES (1764986440950743069, 1667607066451116032, 1714673277053116418, NULL);
INSERT INTO `tb_menu_role` (`id`, `role_id`, `menu_id`, `create_time`) VALUES (1764986440950743070, 1667607066451116032, 1647951751875133442, NULL);
INSERT INTO `tb_menu_role` (`id`, `role_id`, `menu_id`, `create_time`) VALUES (1764986440950743071, 1667607066451116032, 1714673667987415041, NULL);
INSERT INTO `tb_menu_role` (`id`, `role_id`, `menu_id`, `create_time`) VALUES (1764986440950743072, 1667607066451116032, 1714673499896487938, NULL);
INSERT INTO `tb_menu_role` (`id`, `role_id`, `menu_id`, `create_time`) VALUES (1764986440950743073, 1667607066451116032, 1737661101677502466, NULL);
INSERT INTO `tb_menu_role` (`id`, `role_id`, `menu_id`, `create_time`) VALUES (1764986440950743074, 1667607066451116032, 1726889420524638209, NULL);
INSERT INTO `tb_menu_role` (`id`, `role_id`, `menu_id`, `create_time`) VALUES (1764986440950743075, 1667607066451116032, 1719376052710182914, NULL);
INSERT INTO `tb_menu_role` (`id`, `role_id`, `menu_id`, `create_time`) VALUES (1764986440950743076, 1667607066451116032, 1719375047478120449, NULL);
INSERT INTO `tb_menu_role` (`id`, `role_id`, `menu_id`, `create_time`) VALUES (1764986440950743077, 1667607066451116032, 1737664918557118466, NULL);
INSERT INTO `tb_menu_role` (`id`, `role_id`, `menu_id`, `create_time`) VALUES (1764986440950743078, 1667607066451116032, 1737665649209069569, NULL);
INSERT INTO `tb_menu_role` (`id`, `role_id`, `menu_id`, `create_time`) VALUES (1764986440950743079, 1667607066451116032, 1737666447162822658, NULL);
INSERT INTO `tb_menu_role` (`id`, `role_id`, `menu_id`, `create_time`) VALUES (1764986440950743080, 1667607066451116032, 1737666885635362818, NULL);
INSERT INTO `tb_menu_role` (`id`, `role_id`, `menu_id`, `create_time`) VALUES (1764986440950743081, 1667607066451116032, 1737668345395761153, NULL);
INSERT INTO `tb_menu_role` (`id`, `role_id`, `menu_id`, `create_time`) VALUES (1764986440950743082, 1667607066451116032, 1737668522479276033, NULL);
INSERT INTO `tb_menu_role` (`id`, `role_id`, `menu_id`, `create_time`) VALUES (1764986440950743083, 1667607066451116032, 1747151945530867712, NULL);
INSERT INTO `tb_menu_role` (`id`, `role_id`, `menu_id`, `create_time`) VALUES (1764986440950743084, 1667607066451116032, 1747151945530867713, NULL);
INSERT INTO `tb_menu_role` (`id`, `role_id`, `menu_id`, `create_time`) VALUES (1764986440950743085, 1667607066451116032, 1748712694704795650, NULL);
COMMIT;

-- ----------------------------
-- Table structure for tb_module
-- ----------------------------
DROP TABLE IF EXISTS `tb_module`;
CREATE TABLE `tb_module` (
  `id` bigint NOT NULL COMMENT '主键',
  `name` varchar(50) NOT NULL COMMENT '模块名称',
  `description` varchar(255) DEFAULT NULL COMMENT '模块描述',
  `create_time` datetime NOT NULL COMMENT '创建时间',
  `update_time` datetime NOT NULL ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci COMMENT='资源模块表';

-- ----------------------------
-- Records of tb_module
-- ----------------------------
BEGIN;
INSERT INTO `tb_module` (`id`, `name`, `description`, `create_time`, `update_time`) VALUES (1636182933754609664, '博客管理模块', '博客管理模块集文章、评论、留言、分类、标签管理于一体，提供全面的博客内容控制与组织功能，助力用户高效管理和维护博客平台。', '2023-03-16 10:04:53', '2023-12-21 13:37:16');
INSERT INTO `tb_module` (`id`, `name`, `description`, `create_time`, `update_time`) VALUES (1636182933754609665, '系统管理模块', '系统管理模块综合处理菜单、文件、网站配置、公告、缓存等多方面功能，为用户提供全面而高效的系统操作和管理手段。', '2023-03-16 10:05:44', '2023-12-21 13:39:07');
INSERT INTO `tb_module` (`id`, `name`, `description`, `create_time`, `update_time`) VALUES (1686716084163444736, '项目测试模块', '该测试模块基于RBAC权限系统，具备全部资源请求权限，但在增删改方面受限。通过该模块，系统可以全面测试资源的访问情况，确保权限体系的完整性和稳定性，同时有效防范非授权修改操作。', '2023-08-02 20:30:27', '2023-12-21 13:39:16');
COMMIT;

-- ----------------------------
-- Table structure for tb_module_resource
-- ----------------------------
DROP TABLE IF EXISTS `tb_module_resource`;
CREATE TABLE `tb_module_resource` (
  `id` bigint NOT NULL COMMENT '主键',
  `module_id` bigint DEFAULT NULL COMMENT '模块主键',
  `resource_id` bigint DEFAULT NULL COMMENT '资源主键',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  PRIMARY KEY (`id`) USING BTREE,
  KEY `idx_module_id` (`module_id`) USING BTREE COMMENT '模块ID索引'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci COMMENT='资源模块关联表';

-- ----------------------------
-- Records of tb_module_resource
-- ----------------------------
BEGIN;
INSERT INTO `tb_module_resource` (`id`, `module_id`, `resource_id`, `create_time`) VALUES (1737720467269091328, 1636182933754609664, 1698715961391054848, '2023-12-21 14:23:39');
INSERT INTO `tb_module_resource` (`id`, `module_id`, `resource_id`, `create_time`) VALUES (1737720467269091329, 1636182933754609664, 1698715961395249152, '2023-12-21 14:23:39');
INSERT INTO `tb_module_resource` (`id`, `module_id`, `resource_id`, `create_time`) VALUES (1737720467269091330, 1636182933754609664, 1698715961395249153, '2023-12-21 14:23:39');
INSERT INTO `tb_module_resource` (`id`, `module_id`, `resource_id`, `create_time`) VALUES (1737720467269091331, 1636182933754609664, 1698715961395249154, '2023-12-21 14:23:39');
INSERT INTO `tb_module_resource` (`id`, `module_id`, `resource_id`, `create_time`) VALUES (1737720467269091332, 1636182933754609664, 1709566124468731904, '2023-12-21 14:23:39');
INSERT INTO `tb_module_resource` (`id`, `module_id`, `resource_id`, `create_time`) VALUES (1737720467269091333, 1636182933754609664, 1671394657394753536, '2023-12-21 14:23:39');
INSERT INTO `tb_module_resource` (`id`, `module_id`, `resource_id`, `create_time`) VALUES (1737720467269091334, 1636182933754609664, 1671394657394753537, '2023-12-21 14:23:39');
INSERT INTO `tb_module_resource` (`id`, `module_id`, `resource_id`, `create_time`) VALUES (1737720467269091335, 1636182933754609664, 1671394657394753538, '2023-12-21 14:23:39');
INSERT INTO `tb_module_resource` (`id`, `module_id`, `resource_id`, `create_time`) VALUES (1737720467269091336, 1636182933754609664, 1671394657394753539, '2023-12-21 14:23:39');
INSERT INTO `tb_module_resource` (`id`, `module_id`, `resource_id`, `create_time`) VALUES (1737720467269091337, 1636182933754609664, 1694706666026565632, '2023-12-21 14:23:39');
INSERT INTO `tb_module_resource` (`id`, `module_id`, `resource_id`, `create_time`) VALUES (1737720467269091338, 1636182933754609664, 1694706666030759936, '2023-12-21 14:23:39');
INSERT INTO `tb_module_resource` (`id`, `module_id`, `resource_id`, `create_time`) VALUES (1737720467269091339, 1636182933754609664, 1704132386586886144, '2023-12-21 14:23:39');
INSERT INTO `tb_module_resource` (`id`, `module_id`, `resource_id`, `create_time`) VALUES (1737720467269091340, 1636182933754609664, 1704132386586886145, '2023-12-21 14:23:39');
INSERT INTO `tb_module_resource` (`id`, `module_id`, `resource_id`, `create_time`) VALUES (1737720467269091341, 1636182933754609664, 1704132386586886146, '2023-12-21 14:23:39');
INSERT INTO `tb_module_resource` (`id`, `module_id`, `resource_id`, `create_time`) VALUES (1737720467269091342, 1636182933754609664, 1704132386586886147, '2023-12-21 14:23:39');
INSERT INTO `tb_module_resource` (`id`, `module_id`, `resource_id`, `create_time`) VALUES (1737720467269091343, 1636182933754609664, 1704894548829798400, '2023-12-21 14:23:39');
INSERT INTO `tb_module_resource` (`id`, `module_id`, `resource_id`, `create_time`) VALUES (1737720467269091344, 1636182933754609664, 1699069650102386688, '2023-12-21 14:23:39');
INSERT INTO `tb_module_resource` (`id`, `module_id`, `resource_id`, `create_time`) VALUES (1737720467269091345, 1636182933754609664, 1699069650102386689, '2023-12-21 14:23:39');
INSERT INTO `tb_module_resource` (`id`, `module_id`, `resource_id`, `create_time`) VALUES (1737720467269091346, 1636182933754609664, 1699069650102386690, '2023-12-21 14:23:39');
INSERT INTO `tb_module_resource` (`id`, `module_id`, `resource_id`, `create_time`) VALUES (1737720467269091347, 1636182933754609664, 1699069650102386691, '2023-12-21 14:23:39');
INSERT INTO `tb_module_resource` (`id`, `module_id`, `resource_id`, `create_time`) VALUES (1737720467269091348, 1636182933754609664, 1709846740854636544, '2023-12-21 14:23:39');
INSERT INTO `tb_module_resource` (`id`, `module_id`, `resource_id`, `create_time`) VALUES (1737720467269091349, 1636182933754609664, 1700868600539119616, '2023-12-21 14:23:39');
INSERT INTO `tb_module_resource` (`id`, `module_id`, `resource_id`, `create_time`) VALUES (1737720467269091350, 1636182933754609664, 1700868600543313920, '2023-12-21 14:23:39');
INSERT INTO `tb_module_resource` (`id`, `module_id`, `resource_id`, `create_time`) VALUES (1764986795797250048, 1686716084163444736, 1686025120788774912, '2024-03-05 20:10:19');
INSERT INTO `tb_module_resource` (`id`, `module_id`, `resource_id`, `create_time`) VALUES (1764986795797250049, 1686716084163444736, 1698715961391054848, '2024-03-05 20:10:19');
INSERT INTO `tb_module_resource` (`id`, `module_id`, `resource_id`, `create_time`) VALUES (1764986795797250050, 1686716084163444736, 1698715961395249153, '2024-03-05 20:10:19');
INSERT INTO `tb_module_resource` (`id`, `module_id`, `resource_id`, `create_time`) VALUES (1764986795797250051, 1686716084163444736, 1709566124468731904, '2024-03-05 20:10:19');
INSERT INTO `tb_module_resource` (`id`, `module_id`, `resource_id`, `create_time`) VALUES (1764986795797250052, 1686716084163444736, 1677234418973933568, '2024-03-05 20:10:19');
INSERT INTO `tb_module_resource` (`id`, `module_id`, `resource_id`, `create_time`) VALUES (1764986795797250053, 1686716084163444736, 1671394657394753536, '2024-03-05 20:10:19');
INSERT INTO `tb_module_resource` (`id`, `module_id`, `resource_id`, `create_time`) VALUES (1764986795797250054, 1686716084163444736, 1671394657394753538, '2024-03-05 20:10:19');
INSERT INTO `tb_module_resource` (`id`, `module_id`, `resource_id`, `create_time`) VALUES (1764986795797250055, 1686716084163444736, 1647952968122630145, '2024-03-05 20:10:19');
INSERT INTO `tb_module_resource` (`id`, `module_id`, `resource_id`, `create_time`) VALUES (1764986795797250056, 1686716084163444736, 1686722622882054144, '2024-03-05 20:10:19');
INSERT INTO `tb_module_resource` (`id`, `module_id`, `resource_id`, `create_time`) VALUES (1764986795797250057, 1686716084163444736, 1688037885271343104, '2024-03-05 20:10:19');
INSERT INTO `tb_module_resource` (`id`, `module_id`, `resource_id`, `create_time`) VALUES (1764986795797250058, 1686716084163444736, 1688049564466020352, '2024-03-05 20:10:19');
INSERT INTO `tb_module_resource` (`id`, `module_id`, `resource_id`, `create_time`) VALUES (1764986795797250059, 1686716084163444736, 1694706666030759936, '2024-03-05 20:10:19');
INSERT INTO `tb_module_resource` (`id`, `module_id`, `resource_id`, `create_time`) VALUES (1764986795797250060, 1686716084163444736, 1704132386586886144, '2024-03-05 20:10:19');
INSERT INTO `tb_module_resource` (`id`, `module_id`, `resource_id`, `create_time`) VALUES (1764986795797250061, 1686716084163444736, 1704132386586886146, '2024-03-05 20:10:19');
INSERT INTO `tb_module_resource` (`id`, `module_id`, `resource_id`, `create_time`) VALUES (1764986795797250062, 1686716084163444736, 1678704512069533696, '2024-03-05 20:10:19');
INSERT INTO `tb_module_resource` (`id`, `module_id`, `resource_id`, `create_time`) VALUES (1764986795797250063, 1686716084163444736, 1679512161577074688, '2024-03-05 20:10:19');
INSERT INTO `tb_module_resource` (`id`, `module_id`, `resource_id`, `create_time`) VALUES (1764986795797250064, 1686716084163444736, 1679675931226013696, '2024-03-05 20:10:19');
INSERT INTO `tb_module_resource` (`id`, `module_id`, `resource_id`, `create_time`) VALUES (1764986795797250065, 1686716084163444736, 1679732738346713088, '2024-03-05 20:10:19');
INSERT INTO `tb_module_resource` (`id`, `module_id`, `resource_id`, `create_time`) VALUES (1764986795797250066, 1686716084163444736, 1738021576667824128, '2024-03-05 20:10:19');
INSERT INTO `tb_module_resource` (`id`, `module_id`, `resource_id`, `create_time`) VALUES (1764986795797250067, 1686716084163444736, 1748725200716824577, '2024-03-05 20:10:19');
INSERT INTO `tb_module_resource` (`id`, `module_id`, `resource_id`, `create_time`) VALUES (1764986795797250068, 1686716084163444736, 1748725200716824579, '2024-03-05 20:10:19');
INSERT INTO `tb_module_resource` (`id`, `module_id`, `resource_id`, `create_time`) VALUES (1764986795797250069, 1686716084163444736, 1752353681161846784, '2024-03-05 20:10:19');
INSERT INTO `tb_module_resource` (`id`, `module_id`, `resource_id`, `create_time`) VALUES (1764986795797250070, 1686716084163444736, 1684832097144930304, '2024-03-05 20:10:19');
INSERT INTO `tb_module_resource` (`id`, `module_id`, `resource_id`, `create_time`) VALUES (1764986795797250071, 1686716084163444736, 1685590302728912896, '2024-03-05 20:10:19');
INSERT INTO `tb_module_resource` (`id`, `module_id`, `resource_id`, `create_time`) VALUES (1764986795797250072, 1686716084163444736, 1682422693888000001, '2024-03-05 20:10:19');
INSERT INTO `tb_module_resource` (`id`, `module_id`, `resource_id`, `create_time`) VALUES (1764986795797250073, 1686716084163444736, 1676421084687106050, '2024-03-05 20:10:19');
INSERT INTO `tb_module_resource` (`id`, `module_id`, `resource_id`, `create_time`) VALUES (1764986795797250074, 1686716084163444736, 1636187548919267329, '2024-03-05 20:10:19');
INSERT INTO `tb_module_resource` (`id`, `module_id`, `resource_id`, `create_time`) VALUES (1764986795797250075, 1686716084163444736, 1667578577488445440, '2024-03-05 20:10:19');
INSERT INTO `tb_module_resource` (`id`, `module_id`, `resource_id`, `create_time`) VALUES (1764986795797250076, 1686716084163444736, 1684082256563404800, '2024-03-05 20:10:19');
INSERT INTO `tb_module_resource` (`id`, `module_id`, `resource_id`, `create_time`) VALUES (1764986795797250077, 1686716084163444736, 1683131008930545664, '2024-03-05 20:10:19');
INSERT INTO `tb_module_resource` (`id`, `module_id`, `resource_id`, `create_time`) VALUES (1764986795797250078, 1686716084163444736, 1675800348175892480, '2024-03-05 20:10:19');
INSERT INTO `tb_module_resource` (`id`, `module_id`, `resource_id`, `create_time`) VALUES (1764986795797250079, 1686716084163444736, 1675800348175892482, '2024-03-05 20:10:19');
INSERT INTO `tb_module_resource` (`id`, `module_id`, `resource_id`, `create_time`) VALUES (1764986795797250080, 1686716084163444736, 1684541750435119104, '2024-03-05 20:10:19');
INSERT INTO `tb_module_resource` (`id`, `module_id`, `resource_id`, `create_time`) VALUES (1764986795797250081, 1686716084163444736, 1684541750435119105, '2024-03-05 20:10:19');
INSERT INTO `tb_module_resource` (`id`, `module_id`, `resource_id`, `create_time`) VALUES (1764986795797250082, 1686716084163444736, 1762024256301957120, '2024-03-05 20:10:19');
INSERT INTO `tb_module_resource` (`id`, `module_id`, `resource_id`, `create_time`) VALUES (1764986795797250083, 1686716084163444736, 1762024256301957122, '2024-03-05 20:10:19');
INSERT INTO `tb_module_resource` (`id`, `module_id`, `resource_id`, `create_time`) VALUES (1764986795797250084, 1686716084163444736, 1684568413176856576, '2024-03-05 20:10:19');
INSERT INTO `tb_module_resource` (`id`, `module_id`, `resource_id`, `create_time`) VALUES (1764986795797250085, 1686716084163444736, 1684568413176856578, '2024-03-05 20:10:19');
INSERT INTO `tb_module_resource` (`id`, `module_id`, `resource_id`, `create_time`) VALUES (1764986795797250086, 1686716084163444736, 1747078312749957120, '2024-03-05 20:10:19');
INSERT INTO `tb_module_resource` (`id`, `module_id`, `resource_id`, `create_time`) VALUES (1764986795797250087, 1686716084163444736, 1747177876639186944, '2024-03-05 20:10:19');
INSERT INTO `tb_module_resource` (`id`, `module_id`, `resource_id`, `create_time`) VALUES (1764986795797250088, 1686716084163444736, 1736776127176769536, '2024-03-05 20:10:19');
INSERT INTO `tb_module_resource` (`id`, `module_id`, `resource_id`, `create_time`) VALUES (1764986795797250089, 1686716084163444736, 1699069650102386688, '2024-03-05 20:10:19');
INSERT INTO `tb_module_resource` (`id`, `module_id`, `resource_id`, `create_time`) VALUES (1764986795797250090, 1686716084163444736, 1699069650102386690, '2024-03-05 20:10:19');
INSERT INTO `tb_module_resource` (`id`, `module_id`, `resource_id`, `create_time`) VALUES (1764986795797250091, 1686716084163444736, 1709846740854636544, '2024-03-05 20:10:19');
INSERT INTO `tb_module_resource` (`id`, `module_id`, `resource_id`, `create_time`) VALUES (1764986795797250092, 1686716084163444736, 1656679330433990656, '2024-03-05 20:10:19');
INSERT INTO `tb_module_resource` (`id`, `module_id`, `resource_id`, `create_time`) VALUES (1764986795797250093, 1686716084163444736, 1664296403540639744, '2024-03-05 20:10:19');
INSERT INTO `tb_module_resource` (`id`, `module_id`, `resource_id`, `create_time`) VALUES (1764986795797250094, 1686716084163444736, 1661030954518446080, '2024-03-05 20:10:19');
INSERT INTO `tb_module_resource` (`id`, `module_id`, `resource_id`, `create_time`) VALUES (1764986795797250095, 1686716084163444736, 1700868600539119616, '2024-03-05 20:10:19');
INSERT INTO `tb_module_resource` (`id`, `module_id`, `resource_id`, `create_time`) VALUES (1764986795797250096, 1686716084163444736, 1636187548919267328, '2024-03-05 20:10:19');
INSERT INTO `tb_module_resource` (`id`, `module_id`, `resource_id`, `create_time`) VALUES (1764986795797250097, 1686716084163444736, 1647860341062762496, '2024-03-05 20:10:19');
INSERT INTO `tb_module_resource` (`id`, `module_id`, `resource_id`, `create_time`) VALUES (1764986795797250098, 1686716084163444736, 1675800348175892483, '2024-03-05 20:10:19');
INSERT INTO `tb_module_resource` (`id`, `module_id`, `resource_id`, `create_time`) VALUES (1764986795797250099, 1686716084163444736, 1744994308420796416, '2024-03-05 20:10:19');
INSERT INTO `tb_module_resource` (`id`, `module_id`, `resource_id`, `create_time`) VALUES (1764986795797250100, 1686716084163444736, 1745641016649580544, '2024-03-05 20:10:19');
INSERT INTO `tb_module_resource` (`id`, `module_id`, `resource_id`, `create_time`) VALUES (1764986795797250101, 1686716084163444736, 1746734037193457664, '2024-03-05 20:10:19');
INSERT INTO `tb_module_resource` (`id`, `module_id`, `resource_id`, `create_time`) VALUES (1764986795797250102, 1686716084163444736, 1747282790946177024, '2024-03-05 20:10:19');
INSERT INTO `tb_module_resource` (`id`, `module_id`, `resource_id`, `create_time`) VALUES (1764986795797250103, 1686716084163444736, 1747282790950371328, '2024-03-05 20:10:19');
INSERT INTO `tb_module_resource` (`id`, `module_id`, `resource_id`, `create_time`) VALUES (1764986795797250104, 1686716084163444736, 1748725200716824576, '2024-03-05 20:10:19');
INSERT INTO `tb_module_resource` (`id`, `module_id`, `resource_id`, `create_time`) VALUES (1764986795797250105, 1686716084163444736, 1652715951180742656, '2024-03-05 20:10:19');
INSERT INTO `tb_module_resource` (`id`, `module_id`, `resource_id`, `create_time`) VALUES (1764986795797250106, 1686716084163444736, 1681231793782521856, '2024-03-05 20:10:19');
INSERT INTO `tb_module_resource` (`id`, `module_id`, `resource_id`, `create_time`) VALUES (1773603474752143360, 1636182933754609665, 1686025120788774912, '2024-03-29 14:49:55');
INSERT INTO `tb_module_resource` (`id`, `module_id`, `resource_id`, `create_time`) VALUES (1773603474752143361, 1636182933754609665, 1686370540320718848, '2024-03-29 14:49:55');
INSERT INTO `tb_module_resource` (`id`, `module_id`, `resource_id`, `create_time`) VALUES (1773603474752143362, 1636182933754609665, 1677234418973933568, '2024-03-29 14:49:55');
INSERT INTO `tb_module_resource` (`id`, `module_id`, `resource_id`, `create_time`) VALUES (1773603474752143363, 1636182933754609665, 1647952968122630145, '2024-03-29 14:49:55');
INSERT INTO `tb_module_resource` (`id`, `module_id`, `resource_id`, `create_time`) VALUES (1773603474752143364, 1636182933754609665, 1686722622882054144, '2024-03-29 14:49:55');
INSERT INTO `tb_module_resource` (`id`, `module_id`, `resource_id`, `create_time`) VALUES (1773603474752143365, 1636182933754609665, 1688037885271343104, '2024-03-29 14:49:55');
INSERT INTO `tb_module_resource` (`id`, `module_id`, `resource_id`, `create_time`) VALUES (1773603474752143366, 1636182933754609665, 1688049564466020352, '2024-03-29 14:49:55');
INSERT INTO `tb_module_resource` (`id`, `module_id`, `resource_id`, `create_time`) VALUES (1773603474752143367, 1636182933754609665, 1688106647613865984, '2024-03-29 14:49:55');
INSERT INTO `tb_module_resource` (`id`, `module_id`, `resource_id`, `create_time`) VALUES (1773603474752143368, 1636182933754609665, 1688106647613865993, '2024-03-29 14:49:55');
INSERT INTO `tb_module_resource` (`id`, `module_id`, `resource_id`, `create_time`) VALUES (1773603474752143369, 1636182933754609665, 1678704512069533696, '2024-03-29 14:49:55');
INSERT INTO `tb_module_resource` (`id`, `module_id`, `resource_id`, `create_time`) VALUES (1773603474752143370, 1636182933754609665, 1679151716034936832, '2024-03-29 14:49:55');
INSERT INTO `tb_module_resource` (`id`, `module_id`, `resource_id`, `create_time`) VALUES (1773603474752143371, 1636182933754609665, 1679512161577074688, '2024-03-29 14:49:55');
INSERT INTO `tb_module_resource` (`id`, `module_id`, `resource_id`, `create_time`) VALUES (1773603474752143372, 1636182933754609665, 1679675931226013696, '2024-03-29 14:49:55');
INSERT INTO `tb_module_resource` (`id`, `module_id`, `resource_id`, `create_time`) VALUES (1773603474752143373, 1636182933754609665, 1679732738346713088, '2024-03-29 14:49:55');
INSERT INTO `tb_module_resource` (`id`, `module_id`, `resource_id`, `create_time`) VALUES (1773603474752143374, 1636182933754609665, 1680179775546589184, '2024-03-29 14:49:55');
INSERT INTO `tb_module_resource` (`id`, `module_id`, `resource_id`, `create_time`) VALUES (1773603474752143375, 1636182933754609665, 1680179775546589185, '2024-03-29 14:49:55');
INSERT INTO `tb_module_resource` (`id`, `module_id`, `resource_id`, `create_time`) VALUES (1773603474752143376, 1636182933754609665, 1738021576667824128, '2024-03-29 14:49:55');
INSERT INTO `tb_module_resource` (`id`, `module_id`, `resource_id`, `create_time`) VALUES (1773603474752143377, 1636182933754609665, 1748725200716824577, '2024-03-29 14:49:55');
INSERT INTO `tb_module_resource` (`id`, `module_id`, `resource_id`, `create_time`) VALUES (1773603474752143378, 1636182933754609665, 1748725200716824578, '2024-03-29 14:49:55');
INSERT INTO `tb_module_resource` (`id`, `module_id`, `resource_id`, `create_time`) VALUES (1773603474752143379, 1636182933754609665, 1748725200716824579, '2024-03-29 14:49:55');
INSERT INTO `tb_module_resource` (`id`, `module_id`, `resource_id`, `create_time`) VALUES (1773603474752143380, 1636182933754609665, 1748725200716824580, '2024-03-29 14:49:55');
INSERT INTO `tb_module_resource` (`id`, `module_id`, `resource_id`, `create_time`) VALUES (1773603474752143381, 1636182933754609665, 1752353681161846784, '2024-03-29 14:49:55');
INSERT INTO `tb_module_resource` (`id`, `module_id`, `resource_id`, `create_time`) VALUES (1773603474756337664, 1636182933754609665, 1765388135291682816, '2024-03-29 14:49:55');
INSERT INTO `tb_module_resource` (`id`, `module_id`, `resource_id`, `create_time`) VALUES (1773603474756337665, 1636182933754609665, 1765388271610757120, '2024-03-29 14:49:55');
INSERT INTO `tb_module_resource` (`id`, `module_id`, `resource_id`, `create_time`) VALUES (1773603474756337666, 1636182933754609665, 1765388401852284928, '2024-03-29 14:49:55');
INSERT INTO `tb_module_resource` (`id`, `module_id`, `resource_id`, `create_time`) VALUES (1773603474756337667, 1636182933754609665, 1684832097144930304, '2024-03-29 14:49:55');
INSERT INTO `tb_module_resource` (`id`, `module_id`, `resource_id`, `create_time`) VALUES (1773603474756337668, 1636182933754609665, 1685590302728912896, '2024-03-29 14:49:55');
INSERT INTO `tb_module_resource` (`id`, `module_id`, `resource_id`, `create_time`) VALUES (1773603474756337669, 1636182933754609665, 1685641090620719104, '2024-03-29 14:49:55');
INSERT INTO `tb_module_resource` (`id`, `module_id`, `resource_id`, `create_time`) VALUES (1773603474756337670, 1636182933754609665, 1685664653591445512, '2024-03-29 14:49:55');
INSERT INTO `tb_module_resource` (`id`, `module_id`, `resource_id`, `create_time`) VALUES (1773603474756337671, 1636182933754609665, 1682422693888000001, '2024-03-29 14:49:55');
INSERT INTO `tb_module_resource` (`id`, `module_id`, `resource_id`, `create_time`) VALUES (1773603474756337672, 1636182933754609665, 1683094177929232384, '2024-03-29 14:49:55');
INSERT INTO `tb_module_resource` (`id`, `module_id`, `resource_id`, `create_time`) VALUES (1773603474756337673, 1636182933754609665, 1676421084687106050, '2024-03-29 14:49:55');
INSERT INTO `tb_module_resource` (`id`, `module_id`, `resource_id`, `create_time`) VALUES (1773603474756337674, 1636182933754609665, 1676421084687106051, '2024-03-29 14:49:55');
INSERT INTO `tb_module_resource` (`id`, `module_id`, `resource_id`, `create_time`) VALUES (1773603474756337675, 1636182933754609665, 1676421084687106052, '2024-03-29 14:49:55');
INSERT INTO `tb_module_resource` (`id`, `module_id`, `resource_id`, `create_time`) VALUES (1773603474756337676, 1636182933754609665, 1636187548919267329, '2024-03-29 14:49:55');
INSERT INTO `tb_module_resource` (`id`, `module_id`, `resource_id`, `create_time`) VALUES (1773603474756337677, 1636182933754609665, 1667050222783561728, '2024-03-29 14:49:55');
INSERT INTO `tb_module_resource` (`id`, `module_id`, `resource_id`, `create_time`) VALUES (1773603474756337678, 1636182933754609665, 1667578577488445440, '2024-03-29 14:49:55');
INSERT INTO `tb_module_resource` (`id`, `module_id`, `resource_id`, `create_time`) VALUES (1773603474756337679, 1636182933754609665, 1667598029198196736, '2024-03-29 14:49:55');
INSERT INTO `tb_module_resource` (`id`, `module_id`, `resource_id`, `create_time`) VALUES (1773603474756337680, 1636182933754609665, 1684082256563404800, '2024-03-29 14:49:55');
INSERT INTO `tb_module_resource` (`id`, `module_id`, `resource_id`, `create_time`) VALUES (1773603474756337681, 1636182933754609665, 1683131008930545664, '2024-03-29 14:49:55');
INSERT INTO `tb_module_resource` (`id`, `module_id`, `resource_id`, `create_time`) VALUES (1773603474756337682, 1636182933754609665, 1675800348175892480, '2024-03-29 14:49:55');
INSERT INTO `tb_module_resource` (`id`, `module_id`, `resource_id`, `create_time`) VALUES (1773603474756337683, 1636182933754609665, 1675800348175892481, '2024-03-29 14:49:55');
INSERT INTO `tb_module_resource` (`id`, `module_id`, `resource_id`, `create_time`) VALUES (1773603474756337684, 1636182933754609665, 1676236928191561728, '2024-03-29 14:49:55');
INSERT INTO `tb_module_resource` (`id`, `module_id`, `resource_id`, `create_time`) VALUES (1773603474756337685, 1636182933754609665, 1675800348175892482, '2024-03-29 14:49:55');
INSERT INTO `tb_module_resource` (`id`, `module_id`, `resource_id`, `create_time`) VALUES (1773603474756337686, 1636182933754609665, 1684541750435119104, '2024-03-29 14:49:55');
INSERT INTO `tb_module_resource` (`id`, `module_id`, `resource_id`, `create_time`) VALUES (1773603474756337687, 1636182933754609665, 1684541750435119105, '2024-03-29 14:49:55');
INSERT INTO `tb_module_resource` (`id`, `module_id`, `resource_id`, `create_time`) VALUES (1773603474756337688, 1636182933754609665, 1684548449283866624, '2024-03-29 14:49:55');
INSERT INTO `tb_module_resource` (`id`, `module_id`, `resource_id`, `create_time`) VALUES (1773603474756337689, 1636182933754609665, 1684548449283866625, '2024-03-29 14:49:55');
INSERT INTO `tb_module_resource` (`id`, `module_id`, `resource_id`, `create_time`) VALUES (1773603474756337690, 1636182933754609665, 1762024256301957120, '2024-03-29 14:49:55');
INSERT INTO `tb_module_resource` (`id`, `module_id`, `resource_id`, `create_time`) VALUES (1773603474756337691, 1636182933754609665, 1762024256301957121, '2024-03-29 14:49:55');
INSERT INTO `tb_module_resource` (`id`, `module_id`, `resource_id`, `create_time`) VALUES (1773603474756337692, 1636182933754609665, 1762024256301957122, '2024-03-29 14:49:55');
INSERT INTO `tb_module_resource` (`id`, `module_id`, `resource_id`, `create_time`) VALUES (1773603474756337693, 1636182933754609665, 1762024256301957123, '2024-03-29 14:49:55');
INSERT INTO `tb_module_resource` (`id`, `module_id`, `resource_id`, `create_time`) VALUES (1773603474756337694, 1636182933754609665, 1764672852402896896, '2024-03-29 14:49:55');
INSERT INTO `tb_module_resource` (`id`, `module_id`, `resource_id`, `create_time`) VALUES (1773603474756337695, 1636182933754609665, 1764690603565645824, '2024-03-29 14:49:55');
INSERT INTO `tb_module_resource` (`id`, `module_id`, `resource_id`, `create_time`) VALUES (1773603474756337696, 1636182933754609665, 1684568413176856576, '2024-03-29 14:49:55');
INSERT INTO `tb_module_resource` (`id`, `module_id`, `resource_id`, `create_time`) VALUES (1773603474756337697, 1636182933754609665, 1684568413176856577, '2024-03-29 14:49:55');
INSERT INTO `tb_module_resource` (`id`, `module_id`, `resource_id`, `create_time`) VALUES (1773603474756337698, 1636182933754609665, 1684568413176856578, '2024-03-29 14:49:55');
INSERT INTO `tb_module_resource` (`id`, `module_id`, `resource_id`, `create_time`) VALUES (1773603474756337699, 1636182933754609665, 1765384760454021120, '2024-03-29 14:49:55');
INSERT INTO `tb_module_resource` (`id`, `module_id`, `resource_id`, `create_time`) VALUES (1773603474756337700, 1636182933754609665, 1773603128537513984, '2024-03-29 14:49:55');
INSERT INTO `tb_module_resource` (`id`, `module_id`, `resource_id`, `create_time`) VALUES (1773603474756337701, 1636182933754609665, 1773603256224710656, '2024-03-29 14:49:55');
INSERT INTO `tb_module_resource` (`id`, `module_id`, `resource_id`, `create_time`) VALUES (1773603474756337702, 1636182933754609665, 1773603340328894464, '2024-03-29 14:49:55');
INSERT INTO `tb_module_resource` (`id`, `module_id`, `resource_id`, `create_time`) VALUES (1773603474756337703, 1636182933754609665, 1747078312749957120, '2024-03-29 14:49:55');
INSERT INTO `tb_module_resource` (`id`, `module_id`, `resource_id`, `create_time`) VALUES (1773603474756337704, 1636182933754609665, 1747078312754151424, '2024-03-29 14:49:55');
INSERT INTO `tb_module_resource` (`id`, `module_id`, `resource_id`, `create_time`) VALUES (1773603474756337705, 1636182933754609665, 1747177876639186944, '2024-03-29 14:49:55');
INSERT INTO `tb_module_resource` (`id`, `module_id`, `resource_id`, `create_time`) VALUES (1773603474756337706, 1636182933754609665, 1684128046551924736, '2024-03-29 14:49:55');
INSERT INTO `tb_module_resource` (`id`, `module_id`, `resource_id`, `create_time`) VALUES (1773603474756337707, 1636182933754609665, 1736776127176769536, '2024-03-29 14:49:55');
INSERT INTO `tb_module_resource` (`id`, `module_id`, `resource_id`, `create_time`) VALUES (1773603474756337708, 1636182933754609665, 1737312904681619456, '2024-03-29 14:49:55');
INSERT INTO `tb_module_resource` (`id`, `module_id`, `resource_id`, `create_time`) VALUES (1773603474756337709, 1636182933754609665, 1656679330433990656, '2024-03-29 14:49:55');
INSERT INTO `tb_module_resource` (`id`, `module_id`, `resource_id`, `create_time`) VALUES (1773603474756337710, 1636182933754609665, 1664296403540639744, '2024-03-29 14:49:55');
INSERT INTO `tb_module_resource` (`id`, `module_id`, `resource_id`, `create_time`) VALUES (1773603474756337711, 1636182933754609665, 1664914212519936000, '2024-03-29 14:49:55');
INSERT INTO `tb_module_resource` (`id`, `module_id`, `resource_id`, `create_time`) VALUES (1773603474756337712, 1636182933754609665, 1664914212519936001, '2024-03-29 14:49:55');
INSERT INTO `tb_module_resource` (`id`, `module_id`, `resource_id`, `create_time`) VALUES (1773603474756337713, 1636182933754609665, 1661030954518446080, '2024-03-29 14:49:55');
INSERT INTO `tb_module_resource` (`id`, `module_id`, `resource_id`, `create_time`) VALUES (1773603474756337714, 1636182933754609665, 1662978940790112256, '2024-03-29 14:49:55');
INSERT INTO `tb_module_resource` (`id`, `module_id`, `resource_id`, `create_time`) VALUES (1773603474756337715, 1636182933754609665, 1663469788107636736, '2024-03-29 14:49:55');
INSERT INTO `tb_module_resource` (`id`, `module_id`, `resource_id`, `create_time`) VALUES (1773603474756337716, 1636182933754609665, 1683471580354576384, '2024-03-29 14:49:55');
INSERT INTO `tb_module_resource` (`id`, `module_id`, `resource_id`, `create_time`) VALUES (1773603474756337717, 1636182933754609665, 1636187548919267328, '2024-03-29 14:49:55');
INSERT INTO `tb_module_resource` (`id`, `module_id`, `resource_id`, `create_time`) VALUES (1773603474756337718, 1636182933754609665, 1647860341062762496, '2024-03-29 14:49:55');
INSERT INTO `tb_module_resource` (`id`, `module_id`, `resource_id`, `create_time`) VALUES (1773603474756337719, 1636182933754609665, 1653929645323583488, '2024-03-29 14:49:55');
INSERT INTO `tb_module_resource` (`id`, `module_id`, `resource_id`, `create_time`) VALUES (1773603474756337720, 1636182933754609665, 1654642840078123008, '2024-03-29 14:49:55');
INSERT INTO `tb_module_resource` (`id`, `module_id`, `resource_id`, `create_time`) VALUES (1773603474756337721, 1636182933754609665, 1663579937522581504, '2024-03-29 14:49:55');
INSERT INTO `tb_module_resource` (`id`, `module_id`, `resource_id`, `create_time`) VALUES (1773603474756337722, 1636182933754609665, 1663579937522581505, '2024-03-29 14:49:55');
INSERT INTO `tb_module_resource` (`id`, `module_id`, `resource_id`, `create_time`) VALUES (1773603474756337723, 1636182933754609665, 1675800348175892483, '2024-03-29 14:49:55');
INSERT INTO `tb_module_resource` (`id`, `module_id`, `resource_id`, `create_time`) VALUES (1773603474756337724, 1636182933754609665, 1683471580354576385, '2024-03-29 14:49:55');
INSERT INTO `tb_module_resource` (`id`, `module_id`, `resource_id`, `create_time`) VALUES (1773603474756337725, 1636182933754609665, 1683841299993591808, '2024-03-29 14:49:55');
INSERT INTO `tb_module_resource` (`id`, `module_id`, `resource_id`, `create_time`) VALUES (1773603474756337726, 1636182933754609665, 1683841299993591809, '2024-03-29 14:49:55');
INSERT INTO `tb_module_resource` (`id`, `module_id`, `resource_id`, `create_time`) VALUES (1773603474756337727, 1636182933754609665, 1683841299993591810, '2024-03-29 14:49:55');
INSERT INTO `tb_module_resource` (`id`, `module_id`, `resource_id`, `create_time`) VALUES (1773603474756337728, 1636182933754609665, 1684216181440905216, '2024-03-29 14:49:55');
INSERT INTO `tb_module_resource` (`id`, `module_id`, `resource_id`, `create_time`) VALUES (1773603474756337729, 1636182933754609665, 1744994308420796416, '2024-03-29 14:49:55');
INSERT INTO `tb_module_resource` (`id`, `module_id`, `resource_id`, `create_time`) VALUES (1773603474756337730, 1636182933754609665, 1745641016649580544, '2024-03-29 14:49:55');
INSERT INTO `tb_module_resource` (`id`, `module_id`, `resource_id`, `create_time`) VALUES (1773603474756337731, 1636182933754609665, 1746734037193457664, '2024-03-29 14:49:55');
INSERT INTO `tb_module_resource` (`id`, `module_id`, `resource_id`, `create_time`) VALUES (1773603474756337732, 1636182933754609665, 1747282790946177024, '2024-03-29 14:49:55');
INSERT INTO `tb_module_resource` (`id`, `module_id`, `resource_id`, `create_time`) VALUES (1773603474756337733, 1636182933754609665, 1747282790946177025, '2024-03-29 14:49:55');
INSERT INTO `tb_module_resource` (`id`, `module_id`, `resource_id`, `create_time`) VALUES (1773603474756337734, 1636182933754609665, 1747282790950371328, '2024-03-29 14:49:55');
INSERT INTO `tb_module_resource` (`id`, `module_id`, `resource_id`, `create_time`) VALUES (1773603474756337735, 1636182933754609665, 1747282790950371329, '2024-03-29 14:49:55');
INSERT INTO `tb_module_resource` (`id`, `module_id`, `resource_id`, `create_time`) VALUES (1773603474756337736, 1636182933754609665, 1748725200716824576, '2024-03-29 14:49:55');
INSERT INTO `tb_module_resource` (`id`, `module_id`, `resource_id`, `create_time`) VALUES (1773603474756337737, 1636182933754609665, 1752976286965301248, '2024-03-29 14:49:55');
INSERT INTO `tb_module_resource` (`id`, `module_id`, `resource_id`, `create_time`) VALUES (1773603474756337738, 1636182933754609665, 1652715951180742656, '2024-03-29 14:49:55');
INSERT INTO `tb_module_resource` (`id`, `module_id`, `resource_id`, `create_time`) VALUES (1773603474756337739, 1636182933754609665, 1681231793782521856, '2024-03-29 14:49:55');
COMMIT;

-- ----------------------------
-- Table structure for tb_module_role
-- ----------------------------
DROP TABLE IF EXISTS `tb_module_role`;
CREATE TABLE `tb_module_role` (
  `id` bigint NOT NULL COMMENT '主键',
  `module_id` bigint NOT NULL COMMENT '模块ID',
  `role_id` bigint NOT NULL COMMENT '角色ID',
  `create_time` datetime NOT NULL COMMENT '创建时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci COMMENT='模块角色表';

-- ----------------------------
-- Records of tb_module_role
-- ----------------------------
BEGIN;
INSERT INTO `tb_module_role` (`id`, `module_id`, `role_id`, `create_time`) VALUES (1686716440331157504, 1686716084163444736, 1667607066451116032, '2023-08-02 20:31:52');
INSERT INTO `tb_module_role` (`id`, `module_id`, `role_id`, `create_time`) VALUES (1737710750782193664, 1636182933754609664, 1633657944153260032, '2023-12-21 13:45:03');
INSERT INTO `tb_module_role` (`id`, `module_id`, `role_id`, `create_time`) VALUES (1737710750782193665, 1636182933754609665, 1633657944153260032, '2023-12-21 13:45:03');
COMMIT;

-- ----------------------------
-- Table structure for tb_notice
-- ----------------------------
DROP TABLE IF EXISTS `tb_notice`;
CREATE TABLE `tb_notice` (
  `id` bigint NOT NULL COMMENT '主键',
  `title` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '公告标题',
  `type` tinyint NOT NULL COMMENT '公告类型（1通知 2公告）',
  `content` text COMMENT '公告内容',
  `status` tinyint DEFAULT '0' COMMENT '公告状态（0正常 1关闭）',
  `remark` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL COMMENT '备注',
  `create_by` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT '' COMMENT '创建者',
  `update_by` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT '' COMMENT '更新者',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `update_time` datetime DEFAULT NULL COMMENT '更新时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci COMMENT='通知公告表';

-- ----------------------------
-- Records of tb_notice
-- ----------------------------
BEGIN;
INSERT INTO `tb_notice` (`id`, `title`, `type`, `content`, `status`, `remark`, `create_by`, `update_by`, `create_time`, `update_time`) VALUES (1774034479677964288, '测试类型', 2, '测试内容', 0, '111', '111', '测啊', '2024-03-30 19:22:35', '2024-03-30 19:27:29');
COMMIT;

-- ----------------------------
-- Table structure for tb_operate_log
-- ----------------------------
DROP TABLE IF EXISTS `tb_operate_log`;
CREATE TABLE `tb_operate_log` (
  `id` bigint NOT NULL COMMENT '主键',
  `trace_id` bigint DEFAULT NULL COMMENT '请求追踪值',
  `operate_content` varchar(255) DEFAULT NULL COMMENT '操作内容',
  `type` varchar(10) NOT NULL COMMENT '操作类型 OTHER 其他操作，ADD 新增，UPDATE 修改， DELETE 删除， QUERY 查询',
  `method_name` varchar(100) DEFAULT NULL COMMENT '操作方法名称',
  `request_method` varchar(50) NOT NULL COMMENT '请求方式',
  `user_id` bigint NOT NULL COMMENT '操作用户ID',
  `operate_ip` varchar(255) DEFAULT NULL COMMENT '操作IP',
  `operate_location` varchar(255) DEFAULT NULL COMMENT '操作地址',
  `operate_param` longtext COMMENT '操作参数',
  `operate_result` longtext COMMENT '操作结果描述',
  `status` tinyint NOT NULL COMMENT '操作状态 0 失败 1 成功 ',
  `cost_time` bigint DEFAULT NULL COMMENT '操作消耗时间',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `update_time` datetime DEFAULT NULL COMMENT '更新时间',
  PRIMARY KEY (`id`) USING BTREE,
  KEY `idx_type` (`type`) USING BTREE COMMENT '操作类型索引',
  KEY `idx_request` (`request_method`) USING BTREE COMMENT '操作方法类型索引',
  KEY `idx_user_id` (`user_id`) USING BTREE COMMENT '用户主键索引',
  KEY `idx_status` (`status`) USING BTREE COMMENT '操作状态索引'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci COMMENT='操作日志表';

-- ----------------------------
-- Records of tb_operate_log
-- ----------------------------
BEGIN;
INSERT INTO `tb_operate_log` (`id`, `trace_id`, `operate_content`, `type`, `method_name`, `request_method`, `user_id`, `operate_ip`, `operate_location`, `operate_param`, `operate_result`, `status`, `cost_time`, `create_time`, `update_time`) VALUES (1770367807184175104, 1770366250812178432, '分页查询友链', 'QUERY', 'com.zrkizzy.content.api.link.LinkController.listLinks()', 'POST', 1684221352883519488, '27.39.88.209', '广东省东莞市', '[{\"dataRange\":[],\"currentPage\":1,\"pageSize\":10}]', 'Result(code=00000, message=一切OK, data={\"list\":[{\"name\":\"半晴Miko\",\"website\":\"https://banq.ink/\",\"logo\":\"https://www.static.banq.ink/sunnyBlog/avatar/fcc72d35fc928185c3e70773a29a310f.jpg\",\"introduce\":\"Semisunny\",\"id\":1671411053646315520,\"createTime\":1687330423000,\"updateTime\":1687331851000}],\"total\":1}, timestamp=1710923551721)', 1, 20, '2024-03-20 16:32:32', NULL);
INSERT INTO `tb_operate_log` (`id`, `trace_id`, `operate_content`, `type`, `method_name`, `request_method`, `user_id`, `operate_ip`, `operate_location`, `operate_param`, `operate_result`, `status`, `cost_time`, `create_time`, `update_time`) VALUES (1771156984192565248, 1771156816424599552, '分页查询友链', 'QUERY', 'com.zrkizzy.content.api.link.LinkController.listLinks()', 'POST', 1684221352883519488, '112.94.173.170', '广东省广州市', '[{\"dataRange\":[],\"currentPage\":1,\"pageSize\":10}]', 'Result(code=00000, message=一切OK, data={\"list\":[{\"name\":\"半晴Miko\",\"website\":\"https://banq.ink/\",\"logo\":\"https://www.static.banq.ink/sunnyBlog/avatar/fcc72d35fc928185c3e70773a29a310f.jpg\",\"introduce\":\"Semisunny\",\"id\":1671411053646315520,\"createTime\":1687330423000,\"updateTime\":1687331851000}],\"total\":1}, timestamp=1711111706192)', 1, 10, '2024-03-22 20:48:26', NULL);
INSERT INTO `tb_operate_log` (`id`, `trace_id`, `operate_content`, `type`, `method_name`, `request_method`, `user_id`, `operate_ip`, `operate_location`, `operate_param`, `operate_result`, `status`, `cost_time`, `create_time`, `update_time`) VALUES (1771513338048544768, 1771513036348063744, '分页查询友链', 'QUERY', 'com.zrkizzy.content.api.link.LinkController.listLinks()', 'POST', 1684221352883519488, '27.39.88.209', '广东省东莞市', '[{\"dataRange\":[],\"currentPage\":1,\"pageSize\":10}]', 'Result(code=00000, message=一切OK, data={\"list\":[{\"name\":\"半晴Miko\",\"website\":\"https://banq.ink/\",\"logo\":\"https://www.static.banq.ink/sunnyBlog/avatar/fcc72d35fc928185c3e70773a29a310f.jpg\",\"introduce\":\"Semisunny\",\"id\":1671411053646315520,\"createTime\":1687330423000,\"updateTime\":1687331851000}],\"total\":1}, timestamp=1711196667573)', 1, 9, '2024-03-23 20:24:28', NULL);
INSERT INTO `tb_operate_log` (`id`, `trace_id`, `operate_content`, `type`, `method_name`, `request_method`, `user_id`, `operate_ip`, `operate_location`, `operate_param`, `operate_result`, `status`, `cost_time`, `create_time`, `update_time`) VALUES (1777265543535919104, 1777265325234978816, '分页查询友链', 'QUERY', 'com.zrkizzy.content.api.link.LinkController.listLinks()', 'POST', 1653794265890816000, '117.83.27.253', '江苏省苏州市', '[{\"dataRange\":[],\"currentPage\":1,\"pageSize\":10}]', 'Result(code=00000, message=一切OK, data={\"list\":[{\"name\":\"半晴Miko\",\"website\":\"https://banq.ink/\",\"logo\":\"https://www.static.banq.ink/sunnyBlog/avatar/fcc72d35fc928185c3e70773a29a310f.jpg\",\"introduce\":\"Semisunny\",\"id\":1671411053646315520,\"createTime\":1687330423000,\"updateTime\":1687331851000}],\"total\":1}, timestamp=1712568100232)', 1, 8, '2024-04-08 17:21:40', NULL);
INSERT INTO `tb_operate_log` (`id`, `trace_id`, `operate_content`, `type`, `method_name`, `request_method`, `user_id`, `operate_ip`, `operate_location`, `operate_param`, `operate_result`, `status`, `cost_time`, `create_time`, `update_time`) VALUES (1778342051214000128, 1778340569433505792, '分页查询友链', 'QUERY', 'com.zrkizzy.content.api.link.LinkController.listLinks()', 'POST', 1684221352883519488, '124.115.169.86', '陕西省西安市', '[{\"dataRange\":[],\"currentPage\":1,\"pageSize\":10}]', 'Result(code=00000, message=一切OK, data={\"list\":[{\"name\":\"半晴Miko\",\"website\":\"https://banq.ink/\",\"logo\":\"https://www.static.banq.ink/sunnyBlog/avatar/fcc72d35fc928185c3e70773a29a310f.jpg\",\"introduce\":\"Semisunny\",\"id\":1671411053646315520,\"createTime\":1687330423000,\"updateTime\":1687331851000}],\"total\":1}, timestamp=1712824759663)', 1, 7, '2024-04-11 16:39:20', NULL);
INSERT INTO `tb_operate_log` (`id`, `trace_id`, `operate_content`, `type`, `method_name`, `request_method`, `user_id`, `operate_ip`, `operate_location`, `operate_param`, `operate_result`, `status`, `cost_time`, `create_time`, `update_time`) VALUES (1784419217240489984, 1784418941238509568, '分页查询友链', 'QUERY', 'com.zrkizzy.content.api.link.LinkController.listLinks()', 'POST', 1684221352883519488, '117.80.143.123', '江苏省苏州市', '[{\"dataRange\":[],\"currentPage\":1,\"pageSize\":10}]', 'Result(code=00000, message=一切OK, data={\"list\":[{\"name\":\"半晴Miko\",\"website\":\"https://banq.ink/\",\"logo\":\"https://www.static.banq.ink/sunnyBlog/avatar/fcc72d35fc928185c3e70773a29a310f.jpg\",\"introduce\":\"Semisunny\",\"id\":1671411053646315520,\"createTime\":1687330423000,\"updateTime\":1687331851000}],\"total\":1}, timestamp=1714273668951)', 1, 8, '2024-04-28 11:07:49', NULL);
INSERT INTO `tb_operate_log` (`id`, `trace_id`, `operate_content`, `type`, `method_name`, `request_method`, `user_id`, `operate_ip`, `operate_location`, `operate_param`, `operate_result`, `status`, `cost_time`, `create_time`, `update_time`) VALUES (1786578580126302208, 1786577835721228288, '分页查询友链', 'QUERY', 'com.zrkizzy.content.api.link.LinkController.listLinks()', 'POST', 1684221352883519488, '121.229.82.109', '江苏省南京市', '[{\"dataRange\":[],\"currentPage\":1,\"pageSize\":10}]', 'Result(code=00000, message=一切OK, data={\"list\":[{\"name\":\"半晴Miko\",\"website\":\"https://banq.ink/\",\"logo\":\"https://www.static.banq.ink/sunnyBlog/avatar/fcc72d35fc928185c3e70773a29a310f.jpg\",\"introduce\":\"Semisunny\",\"id\":1671411053646315520,\"createTime\":1687330423000,\"updateTime\":1687331851000}],\"total\":1}, timestamp=1714788501182)', 1, 5, '2024-05-04 10:08:21', NULL);
INSERT INTO `tb_operate_log` (`id`, `trace_id`, `operate_content`, `type`, `method_name`, `request_method`, `user_id`, `operate_ip`, `operate_location`, `operate_param`, `operate_result`, `status`, `cost_time`, `create_time`, `update_time`) VALUES (1786578636057346048, 1786577835721228288, '分页查询友链', 'QUERY', 'com.zrkizzy.content.api.link.LinkController.listLinks()', 'POST', 1684221352883519488, '121.229.82.109', '江苏省南京市', '[{\"dataRange\":[],\"currentPage\":1,\"pageSize\":10}]', 'Result(code=00000, message=一切OK, data={\"list\":[{\"name\":\"半晴Miko\",\"website\":\"https://banq.ink/\",\"logo\":\"https://www.static.banq.ink/sunnyBlog/avatar/fcc72d35fc928185c3e70773a29a310f.jpg\",\"introduce\":\"Semisunny\",\"id\":1671411053646315520,\"createTime\":1687330423000,\"updateTime\":1687331851000}],\"total\":1}, timestamp=1714788514518)', 1, 5, '2024-05-04 10:08:35', NULL);
INSERT INTO `tb_operate_log` (`id`, `trace_id`, `operate_content`, `type`, `method_name`, `request_method`, `user_id`, `operate_ip`, `operate_location`, `operate_param`, `operate_result`, `status`, `cost_time`, `create_time`, `update_time`) VALUES (1786583745139048448, 1786577835721228288, '分页查询友链', 'QUERY', 'com.zrkizzy.content.api.link.LinkController.listLinks()', 'POST', 1684221352883519488, '121.229.82.109', '江苏省南京市', '[{\"dataRange\":[],\"currentPage\":1,\"pageSize\":10}]', 'Result(code=00000, message=一切OK, data={\"list\":[{\"name\":\"半晴Miko\",\"website\":\"https://banq.ink/\",\"logo\":\"https://www.static.banq.ink/sunnyBlog/avatar/fcc72d35fc928185c3e70773a29a310f.jpg\",\"introduce\":\"Semisunny\",\"id\":1671411053646315520,\"createTime\":1687330423000,\"updateTime\":1687331851000}],\"total\":1}, timestamp=1714789732618)', 1, 6, '2024-05-04 10:28:53', NULL);
INSERT INTO `tb_operate_log` (`id`, `trace_id`, `operate_content`, `type`, `method_name`, `request_method`, `user_id`, `operate_ip`, `operate_location`, `operate_param`, `operate_result`, `status`, `cost_time`, `create_time`, `update_time`) VALUES (1788032465332338688, 1788031860626948096, '分页查询友链', 'QUERY', 'com.zrkizzy.content.api.link.LinkController.listLinks()', 'POST', 1684221352883519488, '113.87.82.125', '广东省深圳市', '[{\"dataRange\":[],\"currentPage\":1,\"pageSize\":10}]', 'Result(code=00000, message=一切OK, data={\"list\":[{\"name\":\"半晴Miko\",\"website\":\"https://banq.ink/\",\"logo\":\"https://www.static.banq.ink/sunnyBlog/avatar/fcc72d35fc928185c3e70773a29a310f.jpg\",\"introduce\":\"Semisunny\",\"id\":1671411053646315520,\"createTime\":1687330423000,\"updateTime\":1687331851000}],\"total\":1}, timestamp=1715135134427)', 1, 6, '2024-05-08 10:25:34', NULL);
INSERT INTO `tb_operate_log` (`id`, `trace_id`, `operate_content`, `type`, `method_name`, `request_method`, `user_id`, `operate_ip`, `operate_location`, `operate_param`, `operate_result`, `status`, `cost_time`, `create_time`, `update_time`) VALUES (1791344430716813312, 1791343965077766144, '分页查询友链', 'QUERY', 'com.zrkizzy.content.api.link.LinkController.listLinks()', 'POST', 1653794265890816000, '120.85.105.96', '广东省广州市', '[{\"dataRange\":[],\"currentPage\":1,\"pageSize\":10}]', 'Result(code=00000, message=一切OK, data={\"list\":[{\"name\":\"半晴Miko\",\"website\":\"https://banq.ink/\",\"logo\":\"https://www.static.banq.ink/sunnyBlog/avatar/fcc72d35fc928185c3e70773a29a310f.jpg\",\"introduce\":\"Semisunny\",\"id\":1671411053646315520,\"createTime\":1687330423000,\"updateTime\":1687331851000}],\"total\":1}, timestamp=1715924768509)', 1, 6, '2024-05-17 13:46:09', NULL);
INSERT INTO `tb_operate_log` (`id`, `trace_id`, `operate_content`, `type`, `method_name`, `request_method`, `user_id`, `operate_ip`, `operate_location`, `operate_param`, `operate_result`, `status`, `cost_time`, `create_time`, `update_time`) VALUES (1792375366224969728, 1792375066709721088, '分页查询友链', 'QUERY', 'com.zrkizzy.content.api.link.LinkController.listLinks()', 'POST', 1653794265890816000, '120.85.105.96', '广东省广州市', '[{\"dataRange\":[],\"currentPage\":1,\"pageSize\":10}]', 'Result(code=00000, message=一切OK, data={\"list\":[{\"name\":\"半晴Miko\",\"website\":\"https://banq.ink/\",\"logo\":\"https://www.static.banq.ink/sunnyBlog/avatar/fcc72d35fc928185c3e70773a29a310f.jpg\",\"introduce\":\"Semisunny\",\"id\":1671411053646315520,\"createTime\":1687330423000,\"updateTime\":1687331851000}],\"total\":1}, timestamp=1716170562688)', 1, 7, '2024-05-20 10:02:43', NULL);
INSERT INTO `tb_operate_log` (`id`, `trace_id`, `operate_content`, `type`, `method_name`, `request_method`, `user_id`, `operate_ip`, `operate_location`, `operate_param`, `operate_result`, `status`, `cost_time`, `create_time`, `update_time`) VALUES (1793170714673545216, 1793170560771948544, '分页查询友链', 'QUERY', 'com.zrkizzy.content.api.link.LinkController.listLinks()', 'POST', 1653794265890816000, '120.85.104.236', '广东省广州市', '[{\"dataRange\":[],\"currentPage\":1,\"pageSize\":10}]', 'Result(code=00000, message=一切OK, data={\"list\":[{\"name\":\"半晴Miko\",\"website\":\"https://banq.ink/\",\"logo\":\"https://www.static.banq.ink/sunnyBlog/avatar/fcc72d35fc928185c3e70773a29a310f.jpg\",\"introduce\":\"Semisunny\",\"id\":1671411053646315520,\"createTime\":1687330423000,\"updateTime\":1687331851000}],\"total\":1}, timestamp=1716360188535)', 1, 7, '2024-05-22 14:43:09', NULL);
INSERT INTO `tb_operate_log` (`id`, `trace_id`, `operate_content`, `type`, `method_name`, `request_method`, `user_id`, `operate_ip`, `operate_location`, `operate_param`, `operate_result`, `status`, `cost_time`, `create_time`, `update_time`) VALUES (1794924243033522176, 1794922640880697344, '分页查询友链', 'QUERY', 'com.zrkizzy.content.api.link.LinkController.listLinks()', 'POST', 1653794265890816000, '120.85.104.236', '广东省广州市', '[{\"dataRange\":[],\"currentPage\":1,\"pageSize\":10}]', 'Result(code=00000, message=一切OK, data={\"list\":[{\"name\":\"半晴Miko\",\"website\":\"https://banq.ink/\",\"logo\":\"https://www.static.banq.ink/sunnyBlog/avatar/fcc72d35fc928185c3e70773a29a310f.jpg\",\"introduce\":\"Semisunny\",\"id\":1671411053646315520,\"createTime\":1687330423000,\"updateTime\":1687331851000}],\"total\":1}, timestamp=1716778262275)', 1, 6, '2024-05-27 10:51:02', NULL);
INSERT INTO `tb_operate_log` (`id`, `trace_id`, `operate_content`, `type`, `method_name`, `request_method`, `user_id`, `operate_ip`, `operate_location`, `operate_param`, `operate_result`, `status`, `cost_time`, `create_time`, `update_time`) VALUES (1800824998122749952, 1800824705502937088, '分页查询友链', 'QUERY', 'com.zrkizzy.content.api.link.LinkController.listLinks()', 'POST', 1684221352883519488, '183.220.61.219', '四川省德阳市', '[{\"dataRange\":[],\"currentPage\":1,\"pageSize\":10}]', 'Result(code=00000, message=一切OK, data={\"list\":[{\"name\":\"半晴Miko\",\"website\":\"https://banq.ink/\",\"logo\":\"https://www.static.banq.ink/sunnyBlog/avatar/fcc72d35fc928185c3e70773a29a310f.jpg\",\"introduce\":\"Semisunny\",\"id\":1671411053646315520,\"createTime\":1687330423000,\"updateTime\":1687331851000}],\"total\":1}, timestamp=1718185111919)', 1, 5, '2024-06-12 17:38:32', NULL);
COMMIT;

-- ----------------------------
-- Table structure for tb_resource
-- ----------------------------
DROP TABLE IF EXISTS `tb_resource`;
CREATE TABLE `tb_resource` (
  `id` bigint NOT NULL COMMENT '主键',
  `name` varchar(255) NOT NULL COMMENT '资源名称',
  `description` varchar(255) DEFAULT NULL COMMENT '资源描述',
  `method` varchar(20) NOT NULL COMMENT '资源请求方式',
  `url` varchar(255) NOT NULL COMMENT '资源请求路径',
  `create_time` datetime NOT NULL COMMENT '创建时间',
  `update_time` datetime DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci COMMENT='资源表';

-- ----------------------------
-- Records of tb_resource
-- ----------------------------
BEGIN;
INSERT INTO `tb_resource` (`id`, `name`, `description`, `method`, `url`, `create_time`, `update_time`) VALUES (1636187548919267328, '获取所有用户', '获取当前系统中所有用户信息', 'POST', '/admin/user/list', '2023-03-16 10:17:39', '2023-04-13 07:06:14');
INSERT INTO `tb_resource` (`id`, `name`, `description`, `method`, `url`, `create_time`, `update_time`) VALUES (1636187548919267329, '获取所有角色', '获取当前系统中所有角色的信息', 'POST', '/admin/role/list', '2023-03-16 10:24:57', '2023-04-13 07:06:16');
INSERT INTO `tb_resource` (`id`, `name`, `description`, `method`, `url`, `create_time`, `update_time`) VALUES (1647860341062762496, '获取当前登录用户', '获取当前系统登录用户', 'GET', '/admin/user/getLoginUser', '2023-04-17 15:12:14', '2023-04-17 21:24:19');
INSERT INTO `tb_resource` (`id`, `name`, `description`, `method`, `url`, `create_time`, `update_time`) VALUES (1647952968122630145, '获取菜单列表', '获取菜单列表', 'GET', '/admin/menu/getRoutes', '2023-04-17 21:24:19', '2023-04-19 08:28:04');
INSERT INTO `tb_resource` (`id`, `name`, `description`, `method`, `url`, `create_time`, `update_time`) VALUES (1652715951180742656, '获取用户个人信息', '获取用户个人信息', 'GET', '/admin/user-info/getUserInfo', '2023-05-01 00:46:55', '2023-07-18 09:23:14');
INSERT INTO `tb_resource` (`id`, `name`, `description`, `method`, `url`, `create_time`, `update_time`) VALUES (1653929645323583488, '更新用户个人信息', '更新用户个人信息', 'PUT', '/admin/user/updateLoginUser', '2023-05-01 00:46:55', '2023-07-25 22:42:51');
INSERT INTO `tb_resource` (`id`, `name`, `description`, `method`, `url`, `create_time`, `update_time`) VALUES (1654642840078123008, '用户更新密码', '用户个人信息更新密码', 'PUT', '/admin/user/updatePassword', '2023-05-06 08:22:27', '2023-07-25 22:42:49');
INSERT INTO `tb_resource` (`id`, `name`, `description`, `method`, `url`, `create_time`, `update_time`) VALUES (1656679330433990656, '获取文件列表', '用户获取文件列表', 'GET', '/admin/file-type/list', '2023-05-11 23:15:37', '2023-06-01 13:44:33');
INSERT INTO `tb_resource` (`id`, `name`, `description`, `method`, `url`, `create_time`, `update_time`) VALUES (1661030954518446080, '获取文件上传策略', '用户获取文件上传策略', 'GET', '/admin/file/listUploadStrategy', '2023-05-23 23:26:23', '2023-12-13 09:25:18');
INSERT INTO `tb_resource` (`id`, `name`, `description`, `method`, `url`, `create_time`, `update_time`) VALUES (1662978940790112256, '上传文件', '用户进行文件上传', 'POST', '/admin/file/upload', '2023-05-29 08:28:17', '2023-05-29 00:28:26');
INSERT INTO `tb_resource` (`id`, `name`, `description`, `method`, `url`, `create_time`, `update_time`) VALUES (1663469788107636736, '批量删除文件', '用户进行批量删除文件操作', 'DELETE', '/admin/file/delete', '2023-05-30 16:58:49', '2023-05-30 08:59:01');
INSERT INTO `tb_resource` (`id`, `name`, `description`, `method`, `url`, `create_time`, `update_time`) VALUES (1663579937522581504, '更新用户头像', '更新用户头像', 'PUT', '/admin/user/updateLoginUserAvatar', '2023-05-31 00:16:36', '2023-07-25 22:42:57');
INSERT INTO `tb_resource` (`id`, `name`, `description`, `method`, `url`, `create_time`, `update_time`) VALUES (1663579937522581505, '发送修改密码验证码', '发送修改密码验证码', 'GET', '/admin/user/password', '2023-05-31 00:16:36', '2023-07-25 22:42:57');
INSERT INTO `tb_resource` (`id`, `name`, `description`, `method`, `url`, `create_time`, `update_time`) VALUES (1664296403540639744, '获取指定文件分类对象', '根据文件分类ID获取指定文件分类', 'GET', '/admin/file-type/getFileTypeById/**', '2023-06-01 23:46:32', '2023-06-01 15:47:52');
INSERT INTO `tb_resource` (`id`, `name`, `description`, `method`, `url`, `create_time`, `update_time`) VALUES (1664914212519936000, '新增或编辑文件分类', '用户新增或编辑文件分类', 'POST', '/admin/file-type/save', '2023-06-03 16:38:24', '2023-06-03 08:41:48');
INSERT INTO `tb_resource` (`id`, `name`, `description`, `method`, `url`, `create_time`, `update_time`) VALUES (1664914212519936001, '删除指定文件分类', '用户删除指定文件分类', 'DELETE', '/admin/file-type/delete/**', '2023-06-03 16:41:39', '2023-06-03 08:41:50');
INSERT INTO `tb_resource` (`id`, `name`, `description`, `method`, `url`, `create_time`, `update_time`) VALUES (1667050222783561728, '新增或编辑更新角色信息', '新增或编辑角色信息', 'POST', '/admin/role/save', '2023-06-09 14:05:59', '2023-06-09 06:06:11');
INSERT INTO `tb_resource` (`id`, `name`, `description`, `method`, `url`, `create_time`, `update_time`) VALUES (1667578577488445440, '获取指定角色信息', '根据角色ID获取指定角色', 'GET', '/admin/role/getRoleById/**', '2023-06-11 01:05:40', '2023-06-11 01:05:51');
INSERT INTO `tb_resource` (`id`, `name`, `description`, `method`, `url`, `create_time`, `update_time`) VALUES (1667598029198196736, '批量删除角色信息', '批量删除角色信息', 'DELETE', '/admin/role/delete', '2023-06-11 02:23:06', '2023-06-13 21:47:51');
INSERT INTO `tb_resource` (`id`, `name`, `description`, `method`, `url`, `create_time`, `update_time`) VALUES (1671394657394753536, '获取友链信息', '分页查询友链', 'POST', '/admin/link/list', '2023-06-21 13:49:53', '2023-06-21 05:52:34');
INSERT INTO `tb_resource` (`id`, `name`, `description`, `method`, `url`, `create_time`, `update_time`) VALUES (1671394657394753537, '添加-更新友情链接', '编辑友链信息', 'POST', '/admin/link/save', '2023-06-21 13:50:35', '2023-06-21 05:52:36');
INSERT INTO `tb_resource` (`id`, `name`, `description`, `method`, `url`, `create_time`, `update_time`) VALUES (1671394657394753538, '获取指定友情链接信息', '根据友情链接ID获取到指定友请链接', 'GET', '/admin/link/getLinkById/**', '2023-06-21 13:51:41', '2023-06-21 05:52:37');
INSERT INTO `tb_resource` (`id`, `name`, `description`, `method`, `url`, `create_time`, `update_time`) VALUES (1671394657394753539, '批量删除友情链接数据', '根据友情链接集合批量删除友情链接数据', 'DELETE', '/admin/link/delete', '2023-06-21 13:52:23', '2023-06-21 05:52:39');
INSERT INTO `tb_resource` (`id`, `name`, `description`, `method`, `url`, `create_time`, `update_time`) VALUES (1675800348175892480, '获取操作日志信息', '分页查询操作日志信息', 'POST', '/admin/operate-log/list', '2023-07-03 17:36:35', '2023-07-03 09:38:51');
INSERT INTO `tb_resource` (`id`, `name`, `description`, `method`, `url`, `create_time`, `update_time`) VALUES (1675800348175892481, '批量删除操作日志信息', '批量删除操作日志信息', 'DELETE', '/admin/operate-log/delete', '2023-06-21 13:50:35', '2023-06-21 05:52:36');
INSERT INTO `tb_resource` (`id`, `name`, `description`, `method`, `url`, `create_time`, `update_time`) VALUES (1675800348175892482, '获取模块选项数据', '获取模块选项数据', 'GET', '/admin/module/listModuleOptions', '2023-07-03 22:34:42', '2023-07-03 22:34:53');
INSERT INTO `tb_resource` (`id`, `name`, `description`, `method`, `url`, `create_time`, `update_time`) VALUES (1675800348175892483, '获取用户选项数据', '获取用户选项数据集', 'GET', '/admin/user/listUserOptions', '2023-07-03 22:55:11', '2023-07-03 22:55:27');
INSERT INTO `tb_resource` (`id`, `name`, `description`, `method`, `url`, `create_time`, `update_time`) VALUES (1676236928191561728, '清空操作日志', '清空操作日志中的所有数据', 'GET', '/admin/operate-log/clear', '2023-07-04 22:31:05', '2023-07-04 22:31:14');
INSERT INTO `tb_resource` (`id`, `name`, `description`, `method`, `url`, `create_time`, `update_time`) VALUES (1676421084687106050, '获取登录日志信息', '获取所有登录日志信息', 'POST', '/admin/login-log/list', '2023-07-05 10:43:50', '2023-11-21 08:26:24');
INSERT INTO `tb_resource` (`id`, `name`, `description`, `method`, `url`, `create_time`, `update_time`) VALUES (1676421084687106051, '批量删除登录日志信息', '批量删除用户登录日志信息', 'DELETE', '/admin/login-log/delete', '2023-07-05 10:45:47', '2023-11-21 08:27:03');
INSERT INTO `tb_resource` (`id`, `name`, `description`, `method`, `url`, `create_time`, `update_time`) VALUES (1676421084687106052, '清空登录日志信息', '清空所有用户登录日志信息', 'GET', '/admin/login-log/clear', '2023-07-05 10:46:30', '2023-11-21 08:27:08');
INSERT INTO `tb_resource` (`id`, `name`, `description`, `method`, `url`, `create_time`, `update_time`) VALUES (1677234418973933568, '获取服务监控信息', '获取系统服务监控信息', 'GET', '/admin/service-monitor/getMonitorInfo', '2023-07-07 16:34:54', '2023-07-07 08:35:04');
INSERT INTO `tb_resource` (`id`, `name`, `description`, `method`, `url`, `create_time`, `update_time`) VALUES (1678704512069533696, '获取所有在线用户', '获取当前所有在线用户', 'POST', '/admin/online/list', '2023-07-11 17:56:01', '2023-07-12 23:33:21');
INSERT INTO `tb_resource` (`id`, `name`, `description`, `method`, `url`, `create_time`, `update_time`) VALUES (1679151716034936832, '下线指定用户', '将指定用户下线', 'DELETE', '/admin/online/offline/**', '2023-07-12 23:33:03', '2023-07-12 23:33:14');
INSERT INTO `tb_resource` (`id`, `name`, `description`, `method`, `url`, `create_time`, `update_time`) VALUES (1679512161577074688, '获取所有缓存键类型', '获取所有Redis缓存键类型', 'GET', '/admin/cache/listCacheType', '2023-07-13 23:26:11', '2023-07-14 02:43:40');
INSERT INTO `tb_resource` (`id`, `name`, `description`, `method`, `url`, `create_time`, `update_time`) VALUES (1679675931226013696, '获取所有缓存键', '获取所有缓存键', 'GET', '/admin/cache/listCacheKeys/**', '2023-07-14 10:16:09', '2023-07-14 02:16:25');
INSERT INTO `tb_resource` (`id`, `name`, `description`, `method`, `url`, `create_time`, `update_time`) VALUES (1679732738346713088, '获取指定缓存', '获取指定缓存', 'GET', '/admin/cache/getCacheInfoByKey/**', '2023-07-14 14:01:49', '2023-07-14 06:01:59');
INSERT INTO `tb_resource` (`id`, `name`, `description`, `method`, `url`, `create_time`, `update_time`) VALUES (1680179775546589184, '清除缓存列表', '清除缓存列表', 'DELETE', '/admin/cache/clearCacheKeys/**', '2023-07-15 19:38:03', '2023-07-15 19:44:28');
INSERT INTO `tb_resource` (`id`, `name`, `description`, `method`, `url`, `create_time`, `update_time`) VALUES (1680179775546589185, '删除指定缓存', '删除指定缓存', 'DELETE', '/admin/cache/deleteCacheKey/**', '2023-07-15 21:39:56', '2023-07-15 21:40:07');
INSERT INTO `tb_resource` (`id`, `name`, `description`, `method`, `url`, `create_time`, `update_time`) VALUES (1681231793782521856, '获取指定用户信息', '根据用户ID获取指定用户信息', 'GET', '/admin/user-info/getUserInfoById/**', '2023-07-18 17:20:09', '2023-07-18 09:23:06');
INSERT INTO `tb_resource` (`id`, `name`, `description`, `method`, `url`, `create_time`, `update_time`) VALUES (1682422693888000001, '获取系统配置', '获取系统基本配置', 'GET', '/admin/system-config/getSystemConfig', '2023-07-22 22:05:04', '2023-12-13 22:52:31');
INSERT INTO `tb_resource` (`id`, `name`, `description`, `method`, `url`, `create_time`, `update_time`) VALUES (1683094177929232384, '保存系统基本配置', '更新系统基本配置', 'POST', '/admin/system-config/save', '2023-07-23 20:38:02', '2023-12-13 22:52:37');
INSERT INTO `tb_resource` (`id`, `name`, `description`, `method`, `url`, `create_time`, `update_time`) VALUES (1683131008930545664, '获取首页内容', '获取系统后台首页信息', 'GET', '/admin/index/getHomeInfo', '2023-07-23 23:04:49', '2023-07-23 23:04:52');
INSERT INTO `tb_resource` (`id`, `name`, `description`, `method`, `url`, `create_time`, `update_time`) VALUES (1683471580354576384, '富文本上传图片', '富文本上传图片，默认保存到文章图片中', 'POST', '/admin/file/addImage', '2023-07-24 21:38:56', '2023-07-24 21:39:05');
INSERT INTO `tb_resource` (`id`, `name`, `description`, `method`, `url`, `create_time`, `update_time`) VALUES (1683471580354576385, '新增用户', '新增用户', 'POST', '/admin/user/insert', '2023-07-24 22:47:55', '2023-07-24 22:48:04');
INSERT INTO `tb_resource` (`id`, `name`, `description`, `method`, `url`, `create_time`, `update_time`) VALUES (1683841299993591808, '更新指定用户信息', '更新指定用户信息', 'PUT', '/admin/user/updateUser', '2023-07-25 22:08:08', '2023-07-26 23:20:41');
INSERT INTO `tb_resource` (`id`, `name`, `description`, `method`, `url`, `create_time`, `update_time`) VALUES (1683841299993591809, '重置指定用户密码', '重置指定用户密码', 'GET', '/admin/user/resetPassword/**', '2023-07-25 22:28:23', '2023-07-25 23:20:56');
INSERT INTO `tb_resource` (`id`, `name`, `description`, `method`, `url`, `create_time`, `update_time`) VALUES (1683841299993591810, '更新指定用户状态', '更新指定用户状态', 'GET', '/admin/user/updateUserStatus/**', '2023-07-25 22:58:29', '2023-07-25 22:58:31');
INSERT INTO `tb_resource` (`id`, `name`, `description`, `method`, `url`, `create_time`, `update_time`) VALUES (1684082256563404800, '获取角色选项', '获取角色选项', 'GET', '/admin/role/listRoleOptions', '2023-07-26 14:03:37', '2023-07-26 14:03:40');
INSERT INTO `tb_resource` (`id`, `name`, `description`, `method`, `url`, `create_time`, `update_time`) VALUES (1684128046551924736, '更新用户角色', '更新用户角色', 'PUT', '/admin/user-role/update', '2023-07-26 17:06:18', '2023-07-26 17:06:21');
INSERT INTO `tb_resource` (`id`, `name`, `description`, `method`, `url`, `create_time`, `update_time`) VALUES (1684216181440905216, '批量删除用户', '批量删除指定用户', 'DELETE', '/admin/user/delete', '2023-07-26 22:57:17', '2023-07-26 22:57:26');
INSERT INTO `tb_resource` (`id`, `name`, `description`, `method`, `url`, `create_time`, `update_time`) VALUES (1684541750435119104, '分页查询资源模块', '分页查询资源模块', 'POST', '/admin/module/list', '2023-07-27 20:34:10', '2023-07-27 20:34:12');
INSERT INTO `tb_resource` (`id`, `name`, `description`, `method`, `url`, `create_time`, `update_time`) VALUES (1684541750435119105, '根据ID查询模块内容', '获取指定模块信息', 'GET', '/admin/module/getModuleById/**', '2023-07-27 20:51:12', '2023-07-27 20:51:14');
INSERT INTO `tb_resource` (`id`, `name`, `description`, `method`, `url`, `create_time`, `update_time`) VALUES (1684548449283866624, '添加-更新资源模块', '添加-更新资源模块', 'POST', '/admin/module/save', '2023-07-27 20:57:44', '2023-07-27 20:57:47');
INSERT INTO `tb_resource` (`id`, `name`, `description`, `method`, `url`, `create_time`, `update_time`) VALUES (1684548449283866625, '批量删除资源模块数据', '批量删除资源模块数据', 'DELETE', '/admin/module/delete', '2023-07-27 20:58:18', '2023-07-27 20:58:21');
INSERT INTO `tb_resource` (`id`, `name`, `description`, `method`, `url`, `create_time`, `update_time`) VALUES (1684568413176856576, '分页获取所有资源', '分页获取所有资源', 'POST', '/admin/resource/list', '2023-07-27 22:17:43', '2023-07-27 22:17:45');
INSERT INTO `tb_resource` (`id`, `name`, `description`, `method`, `url`, `create_time`, `update_time`) VALUES (1684568413176856577, '更新指定请求资源', '更新指定请求资源', 'PUT', '/admin/resource/save', '2023-07-27 22:38:18', '2023-07-27 22:43:58');
INSERT INTO `tb_resource` (`id`, `name`, `description`, `method`, `url`, `create_time`, `update_time`) VALUES (1684568413176856578, '获取指定资源信息', '根据ID获取指定资源信息', 'GET', '/admin/resource/getResourceById/**', '2023-07-27 22:39:15', '2023-07-27 22:39:17');
INSERT INTO `tb_resource` (`id`, `name`, `description`, `method`, `url`, `create_time`, `update_time`) VALUES (1684832097144930304, '分页获取指定模块请求资源', '分页获取指定模块请求资源', 'POST', '/admin/module-resource/list', '2023-07-28 15:44:41', '2023-07-28 15:44:44');
INSERT INTO `tb_resource` (`id`, `name`, `description`, `method`, `url`, `create_time`, `update_time`) VALUES (1685590302728912896, '获取指定模块可以添加的接口', '获取指定模块可以添加的接口', 'GET', '/admin/module-resource/listResourceById/**', '2023-07-30 17:57:37', '2023-07-30 17:57:39');
INSERT INTO `tb_resource` (`id`, `name`, `description`, `method`, `url`, `create_time`, `update_time`) VALUES (1685641090620719104, '批量删除模块对应请求资源', '批量删除模块对应请求资源', 'DELETE', '/admin/module-resource/delete', '2023-07-30 21:56:44', '2023-07-30 21:56:48');
INSERT INTO `tb_resource` (`id`, `name`, `description`, `method`, `url`, `create_time`, `update_time`) VALUES (1685664653591445512, '为指定模块分配资源请求', '为指定模块分配资源请求', 'POST', '/admin/module-resource/save', '2023-07-30 23:11:07', '2023-07-30 23:11:10');
INSERT INTO `tb_resource` (`id`, `name`, `description`, `method`, `url`, `create_time`, `update_time`) VALUES (1686025120788774912, '获取角色对应模块权限', '获取角色对应模块权限', 'GET', '/admin/module-role/listByRoleId/**', '2023-07-31 22:45:26', '2023-07-31 22:45:28');
INSERT INTO `tb_resource` (`id`, `name`, `description`, `method`, `url`, `create_time`, `update_time`) VALUES (1686370540320718848, '分配角色模块权限', '分配角色模块权限', 'POST', '/admin/module-role/save', '2023-08-01 21:37:38', '2023-08-01 21:38:41');
INSERT INTO `tb_resource` (`id`, `name`, `description`, `method`, `url`, `create_time`, `update_time`) VALUES (1686722622882054144, '获取菜单数据', '获取菜单数据', 'POST', '/admin/menu/list', '2023-08-02 20:57:14', '2023-08-02 21:33:42');
INSERT INTO `tb_resource` (`id`, `name`, `description`, `method`, `url`, `create_time`, `update_time`) VALUES (1688037885271343104, '获取菜单选项', '获取菜单选项', 'GET', '/admin/menu/listMenuOptions', '2023-08-06 12:03:44', '2023-08-06 12:03:47');
INSERT INTO `tb_resource` (`id`, `name`, `description`, `method`, `url`, `create_time`, `update_time`) VALUES (1688049564466020352, '获取指定菜单数据', '获取指定菜单数据', 'GET', '/admin/menu/getMenuById/**', '2023-08-06 12:49:57', '2023-08-06 12:50:00');
INSERT INTO `tb_resource` (`id`, `name`, `description`, `method`, `url`, `create_time`, `update_time`) VALUES (1688106647613865984, '保存菜单数据', '保存菜单数据', 'POST', '/admin/menu/save', '2023-08-06 16:36:41', '2023-08-06 16:36:44');
INSERT INTO `tb_resource` (`id`, `name`, `description`, `method`, `url`, `create_time`, `update_time`) VALUES (1688106647613865993, '删除指定菜单', '删除指定菜单', 'DELETE', '/admin/menu/delete/**', '2023-08-06 23:13:27', '2023-08-06 23:13:32');
INSERT INTO `tb_resource` (`id`, `name`, `description`, `method`, `url`, `create_time`, `update_time`) VALUES (1694706666026565632, '更新博客配置', '更新博客配置', 'POST', '/admin/blog-config/save', '2023-08-24 21:42:45', '2023-08-24 21:42:47');
INSERT INTO `tb_resource` (`id`, `name`, `description`, `method`, `url`, `create_time`, `update_time`) VALUES (1694706666030759936, '获取博客配置', '获取博客配置', 'GET', '/admin/blog-config/getBlogConfig', '2023-08-24 23:42:42', '2023-08-24 23:42:46');
INSERT INTO `tb_resource` (`id`, `name`, `description`, `method`, `url`, `create_time`, `update_time`) VALUES (1698715961391054848, '分页获取所有博客分类', '分页获取所有博客分类', 'POST', '/admin/blog-category/list', '2023-09-04 23:14:31', '2023-09-05 22:43:09');
INSERT INTO `tb_resource` (`id`, `name`, `description`, `method`, `url`, `create_time`, `update_time`) VALUES (1698715961395249152, '编辑博客分类', '编辑博客分类', 'POST', '/admin/blog-category/save', '2023-09-04 23:15:25', '2023-09-05 22:43:13');
INSERT INTO `tb_resource` (`id`, `name`, `description`, `method`, `url`, `create_time`, `update_time`) VALUES (1698715961395249153, '获取指定博客分类', '获取指定博客分类', 'GET', '/admin/blog-category/getCategoryById/**', '2023-09-04 23:16:34', '2023-09-05 22:43:15');
INSERT INTO `tb_resource` (`id`, `name`, `description`, `method`, `url`, `create_time`, `update_time`) VALUES (1698715961395249154, '批量删除博客分类', '批量删除博客分类', 'DELETE', '/admin/blog-category/delete', '2023-09-04 23:18:14', '2023-09-05 22:43:17');
INSERT INTO `tb_resource` (`id`, `name`, `description`, `method`, `url`, `create_time`, `update_time`) VALUES (1699069650102386688, '分页获取所有博客标签', '获取所有博客标签', 'POST', '/admin/blog-tags/list', '2023-09-05 22:42:29', '2023-09-05 22:42:32');
INSERT INTO `tb_resource` (`id`, `name`, `description`, `method`, `url`, `create_time`, `update_time`) VALUES (1699069650102386689, '编辑博客标签', '编辑博客标签', 'POST', '/admin/blog-tags/save', '2023-09-05 22:47:26', '2023-09-05 22:47:29');
INSERT INTO `tb_resource` (`id`, `name`, `description`, `method`, `url`, `create_time`, `update_time`) VALUES (1699069650102386690, '获取指定博客标签信息', '获取指定博客标签信息', 'GET', '/admin/blog-tags/getBlogTagsById/**', '2023-09-05 22:48:11', '2023-09-05 22:48:14');
INSERT INTO `tb_resource` (`id`, `name`, `description`, `method`, `url`, `create_time`, `update_time`) VALUES (1699069650102386691, '批量删除博客标签数据', '批量删除博客标签数据', 'DELETE', '/admin/blog-tags/delete', '2023-09-05 22:48:50', '2023-09-05 22:48:53');
INSERT INTO `tb_resource` (`id`, `name`, `description`, `method`, `url`, `create_time`, `update_time`) VALUES (1700868600539119616, '获取关于我内容', '获取关于我内容', 'GET', '/admin/about-me/getAboutMe', '2023-09-10 21:50:25', '2023-09-10 21:50:28');
INSERT INTO `tb_resource` (`id`, `name`, `description`, `method`, `url`, `create_time`, `update_time`) VALUES (1700868600543313920, '更新关于我内容', '更新关于我内容', 'POST', '/admin/about-me/update', '2023-09-10 21:50:55', '2023-09-10 21:50:57');
INSERT INTO `tb_resource` (`id`, `name`, `description`, `method`, `url`, `create_time`, `update_time`) VALUES (1704132386586886144, '获取所有文章', '获取所有文章', 'POST', '/admin/article/list', '2023-09-19 21:57:44', '2023-09-19 13:58:41');
INSERT INTO `tb_resource` (`id`, `name`, `description`, `method`, `url`, `create_time`, `update_time`) VALUES (1704132386586886145, '编辑文章', '编辑文章', 'POST', '/admin/article/save', '2023-09-19 21:58:30', '2023-09-19 21:58:32');
INSERT INTO `tb_resource` (`id`, `name`, `description`, `method`, `url`, `create_time`, `update_time`) VALUES (1704132386586886146, '根据ID获取指定文章', '获取指定文章信息', 'GET', '/admin/article/getArticleById/**', '2023-09-19 22:01:59', '2023-09-19 22:02:01');
INSERT INTO `tb_resource` (`id`, `name`, `description`, `method`, `url`, `create_time`, `update_time`) VALUES (1704132386586886147, '批量删除文章', '批量删除文章', 'DELETE', '/admin/article/delete', '2023-09-19 22:03:38', '2023-09-19 22:03:40');
INSERT INTO `tb_resource` (`id`, `name`, `description`, `method`, `url`, `create_time`, `update_time`) VALUES (1704894548829798400, '更新文章设置属性', '更新文章设置属性', 'POST', '/admin/article/updateSettings', '2023-09-22 00:26:30', '2024-03-10 20:34:04');
INSERT INTO `tb_resource` (`id`, `name`, `description`, `method`, `url`, `create_time`, `update_time`) VALUES (1709566124468731904, '获取博客分类选项', '获取博客分类选项', 'GET', '/admin/blog-category/listOptions', '2023-10-04 21:49:20', '2023-10-04 21:51:52');
INSERT INTO `tb_resource` (`id`, `name`, `description`, `method`, `url`, `create_time`, `update_time`) VALUES (1709846740854636544, '获取博客标签选项', '获取博客标签选项', 'GET', '/admin/blog-tags/listOptions', '2023-10-05 16:24:16', '2023-10-05 16:24:18');
INSERT INTO `tb_resource` (`id`, `name`, `description`, `method`, `url`, `create_time`, `update_time`) VALUES (1736776127176769536, '获取角色对应菜单权限', '获取角色对应菜单权限', 'GET', '/admin/menu-role/listByRoleId/**', '2023-12-18 23:53:11', '2023-12-18 23:53:21');
INSERT INTO `tb_resource` (`id`, `name`, `description`, `method`, `url`, `create_time`, `update_time`) VALUES (1737312904681619456, '保存角色菜单权限', '保存角色菜单权限', 'POST', '/admin/menu-role/save', '2023-12-20 11:24:59', '2023-12-20 11:25:04');
INSERT INTO `tb_resource` (`id`, `name`, `description`, `method`, `url`, `create_time`, `update_time`) VALUES (1738021576667824128, '获取缓存监控信息', '获取缓存监控信息', 'GET', '/admin/cache/getMonitorInfo', '2023-12-22 10:21:39', '2023-12-22 10:21:42');
INSERT INTO `tb_resource` (`id`, `name`, `description`, `method`, `url`, `create_time`, `update_time`) VALUES (1744994308420796416, '分页获取数据库表', '分页获取数据库表', 'POST', '/admin/generator/listTable', '2024-01-10 16:08:39', '2024-01-10 16:08:57');
INSERT INTO `tb_resource` (`id`, `name`, `description`, `method`, `url`, `create_time`, `update_time`) VALUES (1745641016649580544, '预览代码', '预览代码', 'POST', '/admin/generator/preview', '2024-01-10 16:08:39', '2024-01-10 16:08:57');
INSERT INTO `tb_resource` (`id`, `name`, `description`, `method`, `url`, `create_time`, `update_time`) VALUES (1746734037193457664, '下载代码', '下载代码', 'POST', '/admin/generator/download', '2024-01-15 11:21:39', '2024-01-15 11:21:39');
INSERT INTO `tb_resource` (`id`, `name`, `description`, `method`, `url`, `create_time`, `update_time`) VALUES (1747078312749957120, '分页查询代码生成日志', '分页查询代码生成日志', 'POST', '/admin/generate-log/list', '2024-01-16 10:00:39', '2024-01-16 10:00:39');
INSERT INTO `tb_resource` (`id`, `name`, `description`, `method`, `url`, `create_time`, `update_time`) VALUES (1747078312754151424, '批量删除代码生成日志', '批量删除代码生成日志', 'DELETE', '/admin/generate-log/delete', '2024-01-16 10:00:39', '2024-01-16 10:00:39');
INSERT INTO `tb_resource` (`id`, `name`, `description`, `method`, `url`, `create_time`, `update_time`) VALUES (1747177876639186944, '根据生成日志下载代码', '根据生成日志下载代码', 'GET', '/admin/generate-log/download/**', '2024-01-16 10:00:39', '2024-01-16 10:00:39');
INSERT INTO `tb_resource` (`id`, `name`, `description`, `method`, `url`, `create_time`, `update_time`) VALUES (1747282790946177024, '分页查询字典类型', '分页查询字典类型', 'POST', '/admin/dict-type/list', '2024-01-16 23:41:45', '2024-01-16 23:41:50');
INSERT INTO `tb_resource` (`id`, `name`, `description`, `method`, `url`, `create_time`, `update_time`) VALUES (1747282790946177025, '编辑字典类型', '添加-更新字典类型', 'POST', '/admin/dict-type/save', '2024-01-16 23:42:38', '2024-01-16 23:42:43');
INSERT INTO `tb_resource` (`id`, `name`, `description`, `method`, `url`, `create_time`, `update_time`) VALUES (1747282790950371328, '获取指定字典类型信息', '获取指定字典类型信息', 'GET', '/admin/dict-type/getDictTypeById/**', '2024-01-16 23:43:13', '2024-01-16 23:43:17');
INSERT INTO `tb_resource` (`id`, `name`, `description`, `method`, `url`, `create_time`, `update_time`) VALUES (1747282790950371329, '批量删除字典类型数据', '批量删除字典类型数据', 'DELETE', '/admin/dict-type/delete', '2024-01-16 23:43:45', '2024-01-16 23:43:48');
INSERT INTO `tb_resource` (`id`, `name`, `description`, `method`, `url`, `create_time`, `update_time`) VALUES (1748725200716824576, '获取字典类型选项', '获取字典类型选项', 'GET', '/admin/dict-type/listOption', '2024-01-20 22:22:51', '2024-01-20 22:22:51');
INSERT INTO `tb_resource` (`id`, `name`, `description`, `method`, `url`, `create_time`, `update_time`) VALUES (1748725200716824577, '分页查询字典数据', '分页查询字典类型对应的字典数据', 'GET', '/admin/dict-data/list', '2024-01-20 22:22:51', '2024-01-20 22:22:51');
INSERT INTO `tb_resource` (`id`, `name`, `description`, `method`, `url`, `create_time`, `update_time`) VALUES (1748725200716824578, '编辑字典数据', '新增-添加字典数据', 'POST', '/admin/dict-data/save', '2024-01-20 22:22:51', '2024-01-20 22:22:51');
INSERT INTO `tb_resource` (`id`, `name`, `description`, `method`, `url`, `create_time`, `update_time`) VALUES (1748725200716824579, '获取指定字典数据信息', '获取指定字典数据信息', 'GET', '/admin/dict-data/getDictDataById/**', '2024-01-20 22:22:51', '2024-01-20 22:22:51');
INSERT INTO `tb_resource` (`id`, `name`, `description`, `method`, `url`, `create_time`, `update_time`) VALUES (1748725200716824580, '批量删除字典数据', '批量删除字典数据', 'DELETE', '/admin/dict-data/delete', '2024-01-20 22:22:51', '2024-01-20 22:22:51');
INSERT INTO `tb_resource` (`id`, `name`, `description`, `method`, `url`, `create_time`, `update_time`) VALUES (1752353681161846784, '获取字典类型对应数据', '获取字典类型对应数据', 'GET', '/admin/dict-data/type/**', '2024-01-30 23:32:15', '2024-01-30 23:32:20');
INSERT INTO `tb_resource` (`id`, `name`, `description`, `method`, `url`, `create_time`, `update_time`) VALUES (1752976286965301248, '刷新字典数据缓存', '刷新字典数据缓存', 'PUT', '/admin/dict-type/refresh', '2024-02-01 16:46:45', '2024-02-01 16:46:53');
INSERT INTO `tb_resource` (`id`, `name`, `description`, `method`, `url`, `create_time`, `update_time`) VALUES (1762024256301957120, '分页查询定时任务', '分页查询定时任务', 'POST', '/admin/scheduler-job/list', '2024-02-26 15:59:31', '2024-02-26 15:59:39');
INSERT INTO `tb_resource` (`id`, `name`, `description`, `method`, `url`, `create_time`, `update_time`) VALUES (1762024256301957121, '编辑定时任务', '编辑定时任务', 'POST', '/admin/scheduler-job/save', '2024-02-26 16:00:06', '2024-02-26 16:00:11');
INSERT INTO `tb_resource` (`id`, `name`, `description`, `method`, `url`, `create_time`, `update_time`) VALUES (1762024256301957122, '获取指定定时任务信息', '获取指定定时任务信息', 'GET', '/admin/scheduler-job/getSchedulerJobById/**', '2024-02-26 16:00:57', '2024-02-26 16:01:02');
INSERT INTO `tb_resource` (`id`, `name`, `description`, `method`, `url`, `create_time`, `update_time`) VALUES (1762024256301957123, '批量删除定时任务数据', '批量删除定时任务数据', 'DELETE', '/admin/scheduler-job/delete', '2024-02-26 16:01:38', '2024-02-26 16:01:42');
INSERT INTO `tb_resource` (`id`, `name`, `description`, `method`, `url`, `create_time`, `update_time`) VALUES (1764672852402896896, '修改定时任务状态', '修改定时任务状态', 'PUT', '/admin/scheduler-job/changeStatus', '2024-03-04 23:24:09', '2024-03-04 23:24:14');
INSERT INTO `tb_resource` (`id`, `name`, `description`, `method`, `url`, `create_time`, `update_time`) VALUES (1764690603565645824, '执行一次定时任务', '执行一次定时任务', 'PUT', '/admin/scheduler-job/run/**', '2024-03-05 00:36:54', '2024-03-05 00:49:52');
INSERT INTO `tb_resource` (`id`, `name`, `description`, `method`, `url`, `create_time`, `update_time`) VALUES (1765384760454021120, '批量删除请求资源', '批量删除请求资源', 'DELETE', '/admin/resource/delete', '2024-03-06 22:31:41', NULL);
INSERT INTO `tb_resource` (`id`, `name`, `description`, `method`, `url`, `create_time`, `update_time`) VALUES (1765388135291682816, '分页查询定时任务调度日志', '分页查询定时任务调度日志', 'POST', '/admin/scheduler-job-log/list', '2024-03-06 22:45:05', NULL);
INSERT INTO `tb_resource` (`id`, `name`, `description`, `method`, `url`, `create_time`, `update_time`) VALUES (1765388271610757120, '获取指定定时任务调度日志信息', '获取指定定时任务调度日志信息', 'GET', '/admin/scheduler-job-log/getSchedulerJobLogById/**', '2024-03-06 22:45:38', NULL);
INSERT INTO `tb_resource` (`id`, `name`, `description`, `method`, `url`, `create_time`, `update_time`) VALUES (1765388401852284928, '批量删除定时任务调度日志数据', '批量删除定时任务调度日志数据', 'DELETE', '/admin/scheduler-job-log/delete', '2024-03-06 22:46:09', NULL);
INSERT INTO `tb_resource` (`id`, `name`, `description`, `method`, `url`, `create_time`, `update_time`) VALUES (1773603128537513984, '编辑通知公告', '编辑通知公告', 'POST', '/admin/notice/save', '2024-03-29 14:48:32', NULL);
INSERT INTO `tb_resource` (`id`, `name`, `description`, `method`, `url`, `create_time`, `update_time`) VALUES (1773603256224710656, '获取指定通知公告信息', '获取指定通知公告信息', 'GET', '/admin/notice/getNoticeById/**', '2024-03-29 14:49:03', NULL);
INSERT INTO `tb_resource` (`id`, `name`, `description`, `method`, `url`, `create_time`, `update_time`) VALUES (1773603340328894464, '批量删除通知公告数据', '批量删除通知公告数据', 'DELETE', '/admin/notice/delete', '2024-03-29 14:49:23', NULL);
COMMIT;

-- ----------------------------
-- Table structure for tb_role
-- ----------------------------
DROP TABLE IF EXISTS `tb_role`;
CREATE TABLE `tb_role` (
  `id` bigint NOT NULL COMMENT '角色ID',
  `name` varchar(50) NOT NULL COMMENT '角色名称',
  `mark` varchar(50) DEFAULT NULL COMMENT '角色标识',
  `description` varchar(255) DEFAULT NULL COMMENT '角色描述',
  `create_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_time` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
  PRIMARY KEY (`id`) USING BTREE,
  KEY `index_01` (`name`,`mark`,`create_time`) USING BTREE,
  KEY `idx_mark_id` (`mark`,`id`) USING BTREE COMMENT '角色标识、ID联合索引',
  KEY `idx_name_id` (`name`,`id`) USING BTREE COMMENT '角色名称、ID联合索引'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci COMMENT='角色表';

-- ----------------------------
-- Records of tb_role
-- ----------------------------
BEGIN;
INSERT INTO `tb_role` (`id`, `name`, `mark`, `description`, `create_time`, `update_time`) VALUES (1000000000000000000, '超级管理员', 'ROLE_ADMIN', '超级管理员', '2023-03-09 09:50:27', '2023-06-11 02:31:37');
INSERT INTO `tb_role` (`id`, `name`, `mark`, `description`, `create_time`, `update_time`) VALUES (1633657944153260032, '系统管理员', 'ROLE_SYSTEM', '系统管理员负责维护系统安全、配置管理、日志监控，确保顺畅运行。专业管理权限、解决技术问题，保障系统高效、可靠运行。', '2023-03-09 10:36:20', '2023-12-21 13:44:28');
INSERT INTO `tb_role` (`id`, `name`, `mark`, `description`, `create_time`, `update_time`) VALUES (1667607066451116032, '系统测试员', 'ROLE_TEST', '系统测试员专注于测试模块，确保系统功能完备性。负责发现和报告潜在问题，保障软件质量，促进系统稳定性和用户满意度。', '2023-06-11 02:58:02', '2023-12-21 13:45:55');
COMMIT;

-- ----------------------------
-- Table structure for tb_scheduler_job
-- ----------------------------
DROP TABLE IF EXISTS `tb_scheduler_job`;
CREATE TABLE `tb_scheduler_job` (
  `id` bigint NOT NULL COMMENT '主键',
  `job_name` varchar(64) NOT NULL COMMENT '任务名称',
  `job_group` varchar(64) DEFAULT NULL COMMENT '任务分组',
  `invoke_target` varchar(500) NOT NULL COMMENT '调用目标字符串',
  `cron` varchar(255) DEFAULT NULL COMMENT 'corn执行表达式',
  `execute_strategy` tinyint DEFAULT '3' COMMENT '执行策略： 1 立即执行，2执行一次， 3 放弃执行',
  `concurrent` tinyint DEFAULT '0' COMMENT '是否并发执行： 1 允许， 0 禁止',
  `status` tinyint DEFAULT NULL COMMENT '任务状态： 1 正常，0 暂停',
  `remark` varchar(255) DEFAULT NULL COMMENT '备注信息',
  `create_by` varchar(255) DEFAULT NULL COMMENT '创建用户',
  `update_by` varchar(255) DEFAULT NULL COMMENT '更新用户',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `update_time` datetime DEFAULT NULL COMMENT '更新时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci COMMENT='定时任务表';

-- ----------------------------
-- Records of tb_scheduler_job
-- ----------------------------
BEGIN;
INSERT INTO `tb_scheduler_job` (`id`, `job_name`, `job_group`, `invoke_target`, `cron`, `execute_strategy`, `concurrent`, `status`, `remark`, `create_by`, `update_by`, `create_time`, `update_time`) VALUES (1763570215796342784, '测试任务', '默认', 'systemTask.noParamsMethod()', '30,40,50,20 18,19 22 9 3 ? 2024', 1, 0, 0, '测试定时任务', 'admin@qq.com', 'admin@qq.com', '2024-03-01 22:21:20', '2024-03-09 22:18:24');
INSERT INTO `tb_scheduler_job` (`id`, `job_name`, `job_group`, `invoke_target`, `cron`, `execute_strategy`, `concurrent`, `status`, `remark`, `create_by`, `update_by`, `create_time`, `update_time`) VALUES (1764611061379497984, '测试有参任务', '默认', 'systemTask.paramsMethod(\'String类型任务参数\')', '10 * 1 5 3 ? 2024', 2, 0, 0, '有参任务', 'admin@qq.com', 'admin@qq.com', '2024-03-04 19:17:17', '2024-03-05 01:04:50');
INSERT INTO `tb_scheduler_job` (`id`, `job_name`, `job_group`, `invoke_target`, `cron`, `execute_strategy`, `concurrent`, `status`, `remark`, `create_by`, `update_by`, `create_time`, `update_time`) VALUES (1769738153117089792, '定时发布文章 测试定时发布文章', 'ARTICLE', 'blogArticleTask.publishArticle(1769738153108701184L)', '00 52 22 18 03 ? 2024', 2, 0, 0, '用户 admin@qq.com 于 2024-03-18 22:52:00 定时发布文章 测试定时发布文章', 'admin@qq.com', NULL, '2024-03-18 22:50:31', NULL);
INSERT INTO `tb_scheduler_job` (`id`, `job_name`, `job_group`, `invoke_target`, `cron`, `execute_strategy`, `concurrent`, `status`, `remark`, `create_by`, `update_by`, `create_time`, `update_time`) VALUES (1770103649570127872, '关闭无效定时任务', 'SYSTEM', 'systemTask.closeSchedulerJob()', '10 4 23 * * ?', 2, 0, 1, '定时关闭系统无效任务', 'admin@qq.com', NULL, '2024-03-19 23:02:52', NULL);
COMMIT;

-- ----------------------------
-- Table structure for tb_scheduler_job_log
-- ----------------------------
DROP TABLE IF EXISTS `tb_scheduler_job_log`;
CREATE TABLE `tb_scheduler_job_log` (
  `id` bigint NOT NULL COMMENT '主键',
  `job_id` bigint NOT NULL COMMENT '任务编号',
  `job_name` varchar(64) NOT NULL COMMENT '任务名称',
  `job_group` varchar(64) NOT NULL COMMENT '任务分组',
  `invoke_target` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL COMMENT '调用字符',
  `job_message` text CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci COMMENT '任务信息',
  `status` tinyint NOT NULL DEFAULT '1' COMMENT '执行状态： 1 成功， 0 失败',
  `create_time` datetime DEFAULT NULL COMMENT '执行时间',
  `update_time` datetime DEFAULT NULL COMMENT '更新时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci COMMENT='定时任务调度日志表';

-- ----------------------------
-- Records of tb_scheduler_job_log
-- ----------------------------
BEGIN;
INSERT INTO `tb_scheduler_job_log` (`id`, `job_id`, `job_name`, `job_group`, `invoke_target`, `job_message`, `status`, `create_time`, `update_time`) VALUES (1765995407835398144, 1765995407835398144, '测试', '测试', 'bean.method()', 'hhhhh', 1, '2024-03-08 14:58:35', '2024-03-08 14:58:37');
INSERT INTO `tb_scheduler_job_log` (`id`, `job_id`, `job_name`, `job_group`, `invoke_target`, `job_message`, `status`, `create_time`, `update_time`) VALUES (1766463825709629440, 1763570215796342784, '测试任务', '默认', 'systemTask.noParamsMethod()', NULL, 1, '2024-03-09 21:59:30', NULL);
INSERT INTO `tb_scheduler_job_log` (`id`, `job_id`, `job_name`, `job_group`, `invoke_target`, `job_message`, `status`, `create_time`, `update_time`) VALUES (1766463867593949184, 1763570215796342784, '测试任务', '默认', 'systemTask.noParamsMethod()', NULL, 1, '2024-03-09 21:59:40', NULL);
INSERT INTO `tb_scheduler_job_log` (`id`, `job_id`, `job_name`, `job_group`, `invoke_target`, `job_message`, `status`, `create_time`, `update_time`) VALUES (1766463909566349312, 1763570215796342784, '测试任务', '默认', 'systemTask.noParamsMethod()', NULL, 1, '2024-03-09 21:59:50', NULL);
INSERT INTO `tb_scheduler_job_log` (`id`, `job_id`, `job_name`, `job_group`, `invoke_target`, `job_message`, `status`, `create_time`, `update_time`) VALUES (1766467810302623744, 1763570215796342784, '测试任务', '默认', 'systemTask.noParamsMethod()', '一切OK', 1, '2024-03-09 22:15:20', NULL);
INSERT INTO `tb_scheduler_job_log` (`id`, `job_id`, `job_name`, `job_group`, `invoke_target`, `job_message`, `status`, `create_time`, `update_time`) VALUES (1766467852199526400, 1763570215796342784, '测试任务', '默认', 'systemTask.noParamsMethod()', '一切OK', 1, '2024-03-09 22:15:30', NULL);
INSERT INTO `tb_scheduler_job_log` (`id`, `job_id`, `job_name`, `job_group`, `invoke_target`, `job_message`, `status`, `create_time`, `update_time`) VALUES (1766467894129983488, 1763570215796342784, '测试任务', '默认', 'systemTask.noParamsMethod()', '一切OK', 1, '2024-03-09 22:15:40', NULL);
INSERT INTO `tb_scheduler_job_log` (`id`, `job_id`, `job_name`, `job_group`, `invoke_target`, `job_message`, `status`, `create_time`, `update_time`) VALUES (1766467936060440576, 1763570215796342784, '测试任务', '默认', 'systemTask.noParamsMethod()', '一切OK', 1, '2024-03-09 22:15:50', NULL);
INSERT INTO `tb_scheduler_job_log` (`id`, `job_id`, `job_name`, `job_group`, `invoke_target`, `job_message`, `status`, `create_time`, `update_time`) VALUES (1766468061893754880, 1763570215796342784, '测试任务', '默认', 'systemTask.noParamsMethod()', '一切OK', 1, '2024-03-09 22:16:20', NULL);
INSERT INTO `tb_scheduler_job_log` (`id`, `job_id`, `job_name`, `job_group`, `invoke_target`, `job_message`, `status`, `create_time`, `update_time`) VALUES (1766468103832600576, 1763570215796342784, '测试任务', '默认', 'systemTask.noParamsMethod()', '一切OK', 1, '2024-03-09 22:16:30', NULL);
INSERT INTO `tb_scheduler_job_log` (`id`, `job_id`, `job_name`, `job_group`, `invoke_target`, `job_message`, `status`, `create_time`, `update_time`) VALUES (1766468145784029184, 1763570215796342784, '测试任务', '默认', 'systemTask.noParamsMethod()', '一切OK', 1, '2024-03-09 22:16:40', NULL);
INSERT INTO `tb_scheduler_job_log` (`id`, `job_id`, `job_name`, `job_group`, `invoke_target`, `job_message`, `status`, `create_time`, `update_time`) VALUES (1766468203153719296, 1763570215796342784, '测试任务', '默认', 'systemTask.noParamsMethod()', '一切OK', 1, '2024-03-09 22:17:05', NULL);
INSERT INTO `tb_scheduler_job_log` (`id`, `job_id`, `job_name`, `job_group`, `invoke_target`, `job_message`, `status`, `create_time`, `update_time`) VALUES (1766468607270715392, 1763570215796342784, '测试任务', '默认', 'systemTask.noParamsMethod()', 'java.lang.reflect.InvocationTargetException\n	at java.base/jdk.internal.reflect.DirectMethodHandleAccessor.invoke(DirectMethodHandleAccessor.java:118)\n	at java.base/java.lang.reflect.Method.invoke(Method.java:580)\n	at com.zrkizzy.module.scheduler.utils.SchedulerJobInvokeUtil.invokeMethod(SchedulerJobInvokeUtil.java:57)\n	at com.zrkizzy.module.scheduler.utils.SchedulerJobInvokeUtil.invokeMethod(SchedulerJobInvokeUtil.java:37)\n	at com.zrkizzy.module.scheduler.template.impl.NonConcurrentScheduleJob.executeJob(NonConcurrentScheduleJob.java:31)\n	at com.zrkizzy.module.scheduler.template.AbstractScheduleJob.execute(AbstractScheduleJob.java:53)\n	at org.quartz.core.JobRunShell.run(JobRunShell.java:202)\n	at org.quartz.simpl.SimpleThreadPool$WorkerThread.run(SimpleThreadPool.java:573)\nCaused by: java.lang.ArithmeticException: / by zero\n	at com.zrkizzy.module.scheduler.task.SystemTask.noParamsMethod(SystemTask.java:24)\n	at java.base/jdk.internal.reflect.DirectMethodHandleAccessor.invoke(DirectMethodHandleAccessor.java:103)\n	... 7 more\n', 0, '2024-03-09 22:18:30', NULL);
INSERT INTO `tb_scheduler_job_log` (`id`, `job_id`, `job_name`, `job_group`, `invoke_target`, `job_message`, `status`, `create_time`, `update_time`) VALUES (1766468649100509184, 1763570215796342784, '测试任务', '默认', 'systemTask.noParamsMethod()', 'java.lang.reflect.InvocationTargetException\n	at java.base/jdk.internal.reflect.DirectMethodHandleAccessor.invoke(DirectMethodHandleAccessor.java:118)\n	at java.base/java.lang.reflect.Method.invoke(Method.java:580)\n	at com.zrkizzy.module.scheduler.utils.SchedulerJobInvokeUtil.invokeMethod(SchedulerJobInvokeUtil.java:57)\n	at com.zrkizzy.module.scheduler.utils.SchedulerJobInvokeUtil.invokeMethod(SchedulerJobInvokeUtil.java:37)\n	at com.zrkizzy.module.scheduler.template.impl.NonConcurrentScheduleJob.executeJob(NonConcurrentScheduleJob.java:31)\n	at com.zrkizzy.module.scheduler.template.AbstractScheduleJob.execute(AbstractScheduleJob.java:53)\n	at org.quartz.core.JobRunShell.run(JobRunShell.java:202)\n	at org.quartz.simpl.SimpleThreadPool$WorkerThread.run(SimpleThreadPool.java:573)\nCaused by: java.lang.ArithmeticException: / by zero\n	at com.zrkizzy.module.scheduler.task.SystemTask.noParamsMethod(SystemTask.java:24)\n	at java.base/jdk.internal.reflect.DirectMethodHandleAccessor.invoke(DirectMethodHandleAccessor.java:103)\n	... 7 more\n', 0, '2024-03-09 22:18:40', NULL);
INSERT INTO `tb_scheduler_job_log` (`id`, `job_id`, `job_name`, `job_group`, `invoke_target`, `job_message`, `status`, `create_time`, `update_time`) VALUES (1766468691089686528, 1763570215796342784, '测试任务', '默认', 'systemTask.noParamsMethod()', 'java.lang.reflect.InvocationTargetException\n	at java.base/jdk.internal.reflect.DirectMethodHandleAccessor.invoke(DirectMethodHandleAccessor.java:118)\n	at java.base/java.lang.reflect.Method.invoke(Method.java:580)\n	at com.zrkizzy.module.scheduler.utils.SchedulerJobInvokeUtil.invokeMethod(SchedulerJobInvokeUtil.java:57)\n	at com.zrkizzy.module.scheduler.utils.SchedulerJobInvokeUtil.invokeMethod(SchedulerJobInvokeUtil.java:37)\n	at com.zrkizzy.module.scheduler.template.impl.NonConcurrentScheduleJob.executeJob(NonConcurrentScheduleJob.java:31)\n	at com.zrkizzy.module.scheduler.template.AbstractScheduleJob.execute(AbstractScheduleJob.java:53)\n	at org.quartz.core.JobRunShell.run(JobRunShell.java:202)\n	at org.quartz.simpl.SimpleThreadPool$WorkerThread.run(SimpleThreadPool.java:573)\nCaused by: java.lang.ArithmeticException: / by zero\n	at com.zrkizzy.module.scheduler.task.SystemTask.noParamsMethod(SystemTask.java:24)\n	at java.base/jdk.internal.reflect.DirectMethodHandleAccessor.invoke(DirectMethodHandleAccessor.java:103)\n	... 7 more\n', 0, '2024-03-09 22:18:50', NULL);
INSERT INTO `tb_scheduler_job_log` (`id`, `job_id`, `job_name`, `job_group`, `invoke_target`, `job_message`, `status`, `create_time`, `update_time`) VALUES (1769737062132482048, 1769737029781815296, '定时发布文章 测试定时发布后端逻辑', 'ARTICLE', 'blogArticleTask.publishArticle(1769737029756649472L)', '一切OK', 1, '2024-03-18 22:42:36', NULL);
INSERT INTO `tb_scheduler_job_log` (`id`, `job_id`, `job_name`, `job_group`, `invoke_target`, `job_message`, `status`, `create_time`, `update_time`) VALUES (1769738529501347840, 1769738153117089792, '定时发布文章 测试定时发布文章', 'ARTICLE', 'blogArticleTask.publishArticle(1769738153108701184L)', '一切OK', 1, '2024-03-18 22:48:26', NULL);
INSERT INTO `tb_scheduler_job_log` (`id`, `job_id`, `job_name`, `job_group`, `invoke_target`, `job_message`, `status`, `create_time`, `update_time`) VALUES (1770103981645758464, 1770103649570127872, '关闭无效定时任务', 'SYSTEM', 'systemTask.closeSchedulerJob()', '一切OK', 1, '2024-03-19 23:00:34', NULL);
COMMIT;

-- ----------------------------
-- Table structure for tb_system_config
-- ----------------------------
DROP TABLE IF EXISTS `tb_system_config`;
CREATE TABLE `tb_system_config` (
  `id` bigint NOT NULL COMMENT '主键',
  `avatar` varchar(255) DEFAULT NULL COMMENT '用户默认头像',
  `notice` text COMMENT '系统公告',
  `upload_strategy` varchar(20) DEFAULT NULL COMMENT '文件上传策略：LOCAL 本地策略，OSS OSS策略',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `update_time` datetime DEFAULT NULL COMMENT '更新时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci COMMENT='系统配置表';

-- ----------------------------
-- Records of tb_system_config
-- ----------------------------
BEGIN;
INSERT INTO `tb_system_config` (`id`, `avatar`, `notice`, `upload_strategy`, `create_time`, `update_time`) VALUES (1682761996493127680, 'https://blog-yk0504.oss-cn-hangzhou.aliyuncs.com/avatar/20230529192531.jpg', '<p><strong style=\"color: rgb(96, 98, 102);\">尊敬的用户：</strong></p><p>欢迎来到我的个人博客! 我是Dream_飞翔，非常感谢您的来访。这里是我分享自己生活、学习和工作中的一些体验与见解的地方。您可以随意浏览我的文章以及后台的所有页面，也可以在评论区分享您的想法和反馈。如果您有任何问题或建议，请与我联系，祝您生活愉快！</p><p><strong style=\"color: rgb(96, 98, 102);\"><span class=\"ql-cursor\">﻿</span></strong></p><p><strong style=\"color: rgb(96, 98, 102);\"><img src=\"https://blog-yk0504.oss-cn-hangzhou.aliyuncs.com/article/20230827200219.jpg\" width=\"89\" style=\"cursor: nesw-resize;\"></strong></p><p><strong style=\"color: rgb(96, 98, 102);\">其他：</strong></p><p>目前项目的技术栈为SpringBoot2，后续我会向SpringBoot3进行迁移</p><p><br></p>', 'OSS', '2023-07-22 22:38:28', '2023-08-27 20:02:40');
COMMIT;

-- ----------------------------
-- Table structure for tb_user
-- ----------------------------
DROP TABLE IF EXISTS `tb_user`;
CREATE TABLE `tb_user` (
  `id` bigint NOT NULL COMMENT '主键ID',
  `username` varchar(50) NOT NULL COMMENT '用户名',
  `password` varchar(255) NOT NULL COMMENT '密码',
  `nickname` varchar(50) DEFAULT NULL COMMENT '昵称',
  `avatar` varchar(255) DEFAULT NULL COMMENT '头像',
  `status` tinyint NOT NULL DEFAULT '1' COMMENT '状态，0：禁用，1：启用',
  `remark` varchar(255) DEFAULT NULL COMMENT '备注',
  `create_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_time` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE KEY `username` (`username`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci COMMENT='用户表';

-- ----------------------------
-- Records of tb_user
-- ----------------------------
BEGIN;
INSERT INTO `tb_user` (`id`, `username`, `password`, `nickname`, `avatar`, `status`, `remark`, `create_time`, `update_time`) VALUES (1653794265890816000, 'admin@qq.com', '$2a$10$INorakGPR9WOUBOSQGh3k.QhZLcbjr..hgALnlgdQ1t3D.hgZKqey', '系统管理员', 'https://blog-yk0504.oss-cn-hangzhou.aliyuncs.com/avatar/06bc0e1731caf6e88e8ec580ac0d5ea2.jpg', 1, '自古英雄出炼狱，破马长枪定乾坤！', '2023-05-03 16:15:23', '2024-01-28 02:59:35');
INSERT INTO `tb_user` (`id`, `username`, `password`, `nickname`, `avatar`, `status`, `remark`, `create_time`, `update_time`) VALUES (1684221352883519488, 'test@qq.com', '$2a$10$JcnvFXeXlcU7YFjHje2n5eaiiVxO/byA0hSBMT.RubSbhjXlcKUve', '系统测试员', 'https://blog-yk0504.oss-cn-hangzhou.aliyuncs.com/avatar/20230529192531.jpg', 1, '备注', '2023-07-26 23:17:17', '2024-02-05 13:59:05');
COMMIT;

-- ----------------------------
-- Table structure for tb_user_info
-- ----------------------------
DROP TABLE IF EXISTS `tb_user_info`;
CREATE TABLE `tb_user_info` (
  `id` bigint NOT NULL COMMENT '主键（同user主键）',
  `phone` varchar(50) NOT NULL COMMENT '手机号码',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `update_time` datetime DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci COMMENT='用户信息表';

-- ----------------------------
-- Records of tb_user_info
-- ----------------------------
BEGIN;
COMMIT;

-- ----------------------------
-- Table structure for tb_user_role
-- ----------------------------
DROP TABLE IF EXISTS `tb_user_role`;
CREATE TABLE `tb_user_role` (
  `id` bigint NOT NULL COMMENT '主键',
  `user_id` bigint NOT NULL COMMENT '用户ID',
  `role_id` bigint NOT NULL COMMENT '角色ID',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci COMMENT='用户角色表';

-- ----------------------------
-- Records of tb_user_role
-- ----------------------------
BEGIN;
INSERT INTO `tb_user_role` (`id`, `user_id`, `role_id`) VALUES (1653794265890816001, 1653794265890816000, 1000000000000000000);
INSERT INTO `tb_user_role` (`id`, `user_id`, `role_id`) VALUES (1683843226475167744, 1683843225900548096, 1633657944153260032);
INSERT INTO `tb_user_role` (`id`, `user_id`, `role_id`) VALUES (1684217316075634688, 1684217315379380224, 1633657944153260032);
INSERT INTO `tb_user_role` (`id`, `user_id`, `role_id`) VALUES (1684217357158842368, 1684217356554862592, 1633657944153260032);
INSERT INTO `tb_user_role` (`id`, `user_id`, `role_id`) VALUES (1684221353441361920, 1684221352883519488, 1667607066451116032);
COMMIT;

SET FOREIGN_KEY_CHECKS = 1;
