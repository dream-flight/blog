import Vue from 'vue'
import VueRouter from 'vue-router'

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    name: 'BlogHome',
    component: () => import('../views/Home/index.vue')
  },
  {
    path: '/about',
    name: 'BlogAbout',
    component: () => import('../views/About/index.vue')
  },
  {
    path: '/category',
    name: 'BlogCategory',
    component: () => import('../views/Category/index.vue')
  },
  {
    path: '/tags',
    name: 'BlogTags',
    redirect: 'tags/list',
    component: () => import('../views/Tags/index.vue'),
    children: [
      {
        path: 'list',
        name: 'TagsList',
        component: () => import('../views/Tags/components/TagsList.vue')
      },
      {
        path: 'article-list/:id',
        name: 'TagsArticle',
        component: () => import('../views/Tags/components/TagsArticle.vue')
      }
    ]
  }
]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})

// 获取原型对象push函数
const originalPush = VueRouter.prototype.push
// 获取原型对象replace函数
const originalReplace = VueRouter.prototype.replace

/**
 * 修改原型对象中的push函数
 *
 * @param {*} location 当前要跳转路径的对象
 * @returns
 */
VueRouter.prototype.push = function push (location) {
  // this指向当前VueRouter的实例对象
  return originalPush.call(this, location).catch(err => err)
}

/**
 * 修改原型对象中的replace函数
 *
 * @param {*} location 当前要跳转路径的对象
 * @returns
 */
VueRouter.prototype.replace = function replace (location) {
  return originalReplace.call(this, location).catch(err => err)
}

export default router
