import { WOW } from 'wowjs'
import 'animate.css'

// 创建全局实例
const wow = new WOW({
  // 需要执行动画的元素的 class
  boxClass: 'wow',
  // animation.css 动画的 class
  animateClass: 'animated',
  // 距离可视区域多少开始执行动画
  offset: 0,
  // 是否在移动设备上执行动画
  mobile: true,
  // 异步加载的内容是否有效
  live: true
})

wow.init()

// 定义全局 Vue 插件
export default {
  install (Vue) {
    Vue.prototype.$wow = wow // 将 wow 实例挂载到 Vue 原型上
  }
}
