import Vue from 'vue'
import Toast from 'vue-toastification'
import 'vue-toastification/dist/index.css'

// 配置自定义通知弹窗
const options = {
  // 通知消息出现和消失时的动画效果
  transition: 'Vue-Toastification__bounce',
  // 同时通知的最大数量
  maxToasts: 3,
  // 是否最新的通知应该显示在顶部
  newestOnTop: true,
  // 在页面上的哪个位置显示通知，top-right
  position: 'top-center',
  // 通知消息显示的时间
  timeout: 3000,
  // 当用户点击通知时是否关闭通知
  closeOnClick: true,
  // 用户切换到其他标签或应用程序时，通知是否暂停显示
  pauseOnFocusLoss: true,
  // 当用户将鼠标悬停在通知上时，是否暂停通知
  pauseOnHover: false,
  // 用户是否可以拖动通知消息
  draggable: true,
  // 用户在通知消息上拖动时的拖动百分比
  draggablePercent: 0.7,
  // 关闭按钮是否在用户将鼠标悬停在通知上时显示
  showCloseButtonOnHover: false,
  // 是否在通知消息上显示进度条
  hideProgressBar: true,
  // 通知消息上的关闭按钮的类型
  closeButton: 'button',
  // 是否显示通知消息的图标
  icon: true,
  // 通知消息是否应该从右到左显示
  rtl: false
}

// 安装 Toast 插件
Vue.use(Toast, options)

// 定义全局 Vue 插件
export default {
  install (Vue) {
    // 将 toast 实例挂载到 Vue 原型上
    Vue.prototype.$toast = Vue.$toast
  }
}
