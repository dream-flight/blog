module.exports = {
  /**
   * 博客主题 深色主题blog-dark，浅色主题blog-light
   */
  blogTheme: 'blog-light'
}
