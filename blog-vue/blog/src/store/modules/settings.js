import defaultSettings from '../../settings'
// 从默认的主题配置中获取到默认参数
const { blogTheme } = defaultSettings
// 获取本地配置的参数
const storageSetting = JSON.parse(localStorage.getItem('layout-setting')) || ''

const setting = {
  namespaced: true,
  state: {
    // 主题色
    blogTheme: storageSetting.blogTheme || blogTheme,
    // 是否展示Banner
    showBanner: false
  },
  mutations: {
    /**
     * 修改主题配置
     */
    CHANGE_SETTING: (state) => {
      state.blogTheme = state.blogTheme === 'blog-light' ? 'blog-dark' : 'blog-light'
    }
  },
  // 异步处理方式
  actions: {
    // 修改布局设置
    changeSetting ({ commit }) {
      commit('CHANGE_SETTING')
    }
  }
}

export default setting
