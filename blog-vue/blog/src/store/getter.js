// 快速获取数据
const getters = {
  // 主题色
  blogTheme: state => state.settings.blogTheme,
  // 是否展示Banner
  showBanner: state => state.settings.showBanner
}

export default getters
