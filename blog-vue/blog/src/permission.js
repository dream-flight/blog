import router from './router'
import NProgress from 'nprogress'

// 调整进度条的速度
NProgress.configure({ easing: 'ease', speed: 500 })

// to：当前位置，from：从哪来
router.beforeEach(async (to, from, next) => {
  // 开启页面进度条
  NProgress.start()
  // 放行
  next()
})

router.afterEach((to, from) => {
  // 关闭进度条
  NProgress.done()
  // 切换路由时返回到顶部
  window.scrollTo({
    top: 0,
    behavior: 'instant'
  })
})
