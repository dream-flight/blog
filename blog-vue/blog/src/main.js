import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
import vuetify from './plugins/vuetify'
import SvgIcons from './icons'
import filters from './filter/index'

import './styles/index.scss'
import 'animate.css'
import 'nprogress/nprogress.css'

import './permission'

import WowPlugin from './plugins/wow'
import ToastPlugin from './plugins/toast'
import VueTypedJs from 'vue-typed-js'
import animated from 'wowjs/css/libs/animate.css'
import axios from 'axios'

Vue.config.productionTip = false
Vue.prototype.$axios = axios

// 全局组件挂载
Vue.use(SvgIcons)
Vue.use(animated)
Vue.use(VueTypedJs)
Vue.use(WowPlugin)
Vue.use(ToastPlugin)

// 时间过滤器
Object.keys(filters).forEach((key) => {
  Vue.filter(key, filters[key])
})

new Vue({
  router,
  store,
  vuetify,
  render: h => h(App)
}).$mount('#app')
