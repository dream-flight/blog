// 表单重置
export function resetForm (refName) {
  if (this.$refs[refName]) {
    this.$refs[refName].clearValidate()
    this.$refs[refName].resetFields()
  }
}

// 数据合并
export function mergeRecursive (source, target) {
  for (var p in target) {
    try {
      if (target[p].constructor === Object) {
        source[p] = mergeRecursive(source[p], target[p])
      } else {
        source[p] = target[p]
      }
    } catch (e) {
      source[p] = target[p]
    }
  }
  return source
}
