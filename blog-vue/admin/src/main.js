import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
import SvgIcons from './icons'
import ElementUI from 'element-ui'
import TagCloud from './utils/tag-cloud'
import Vue2Editor from 'vue2-editor'
import MavonEditor from 'mavon-editor'
import filters from './filter/index'
import DictData from './components/DictData'
// Vue自定义指令
import directive from './directive'
import './permission'
import 'element-ui/lib/theme-chalk/index.css'
import 'element-ui/lib/theme-chalk/display.css'
import 'nprogress/nprogress.css'
import 'mavon-editor/dist/css/index.css'
import './style/scss/index.scss'
import './style/css/style.css'

import { resetForm } from './utils/common'
import { getDictDataByType } from './api/system/dict-data'

Vue.use(directive)
Vue.use(ElementUI)
Vue.use(SvgIcons)
Vue.use(TagCloud)
Vue.use(Vue2Editor)
Vue.use(MavonEditor)

// 全局挂载方法
Vue.prototype.resetForm = resetForm
Vue.prototype.getDictDatas = getDictDataByType

DictData.install()

// 时间过滤器
Object.keys(filters).forEach((key) => {
  Vue.filter(key, filters[key])
})

Vue.config.productionTip = false

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')
