const app = {
  namespaced: true,
  state: {
    // 菜单是否折叠
    isCollapse: false,
    // 右侧面板是否显示
    rightPanelShow: false,
    // 标签页
    tagsViewList: [
      {
        fullPath: '/home',
        name: 'Home',
        path: '/home',
        meta: {
          title: '首页',
          icon: 'home'
        }
      }
    ],
    // 首页内容提示框
    homeDialogShow: true,
    // 使用设备
    device: 'desktop',
    // 需要缓存的标签页
    cachedViews: []
  },
  mutations: {
    SET_COLLAPSE (state) {
      state.isCollapse = !state.isCollapse
    },
    CLOSE_COLLAPSE (state) {
      state.isCollapse = true
    },
    SET_RIGHT_PANEL (state) {
      state.rightPanelShow = !state.rightPanelShow
    },
    SET_HOME_DIALOG (state) {
      state.homeDialogShow = !state.homeDialogShow
    },
    // 切换使用设备
    TOGGLE_DEVICE (state, device) {
      state.device = device
    },
    // 添加标签页
    ADD_TAGS_VIEW (state, view) {
      const isFind = state.tagsViewList.find((item) => {
        return item.path === view.path
      })
      // 处理重复
      if (!isFind) {
        // 如果不存在重复的标签页
        state.tagsViewList.push(view)
      }
    },
    // 为指定的 tag 修改 title
    CHANGE_TAGS_VIEW (state, { index, view }) {
      state.tagsViewList[index] = view
    },
    /**
     * 关闭标签页
     *
     * @param { type: 'other' || 'right' || 'index' } payload 载荷
     */
    REMOVE_TAGS_VIEW (state, payload) {
      // 关闭当前页
      if (payload.type === 'index') {
        // 删除当前指定的一项标签页
        state.tagsViewList.splice(payload.index, 1)
      } else if (payload.type === 'other') {
        // 删除当前位置之后的标签页
        state.tagsViewList.splice(
          payload.index + 1,
          state.tagsViewList.length - payload.index + 1
        )
        // 删除当前位置之前的标签页
        state.tagsViewList.splice(1, payload.index - 1)
      } else if (payload.type === 'right') {
        // 删除当前位置之后的标签页
        state.tagsViewList.splice(
          payload.index + 1,
          state.tagsViewList.length - payload.index + 1
        )
      } else if (payload.type === 'left') {
        // 删除除首页外的左侧标签页
        state.tagsViewList.splice(1, payload.index - 1)
      }
      // 如果缓存页中的标签不在页面标签中，则删除
      state.cachedViews = state.cachedViews.filter(name => {
        state.tagsViewList.some(view => view.name === name)
      })
    },
    // 关闭全部标签页
    CLOSE_ALL_TAGS (state) {
      state.tagsViewList = [
        {
          fullPath: '/home',
          name: 'Home',
          path: '/home',
          meta: {
            title: '首页',
            icon: 'home'
          }
        }
      ]
      // 清除缓存的标签页
      state.cachedViews = []
    },
    /**
     * 添加需要缓存的标签页
     */
    ADD_CACHED_VIEW: (state, view) => {
      if (state.cachedViews.includes(view.name)) return
      if (view.meta && view.meta.noCache) {
        state.cachedViews.push(view.name)
      }
    }
  },
  // 异步处理方式
  actions: {
    // 切换设备异步调用方法
    toggleDevice ({ commit }, device) {
      commit('TOGGLE_DEVICE', device)
    },
    // 关闭侧边栏异步调用方法
    closeSideBar ({ commit }) {
      commit('CLOSE_COLLAPSE')
    },
    // 切换侧边栏异步调用
    toggleSideBar ({ commit }) {
      commit('SET_COLLAPSE')
    },
    // 添加标签页
    addTagView ({ commit }, view) {
      commit('ADD_TAGS_VIEW', view)
    },
    // 添加缓存页面
    addCachedView ({ commit }, view) {
      commit('ADD_CACHED_VIEW', view)
    }
  }
}

export default app
