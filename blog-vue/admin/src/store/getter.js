// 快速获取数据
const getters = {
  // 角色
  roles: state => state.user.roles,
  // 头像
  avatar: state => state.user.avatar,
  // 用户昵称
  nickname: state => state.user.nickname,
  // 登录时间
  loginTime: state => state.user.loginTime,
  // 登录IP
  ipAddress: state => state.user.ipAddress,
  // IP属地
  ipLocation: state => state.user.ipLocation,
  // 首页对话框
  homeDialogShow: state => state.app.homeDialogShow,
  // 用户菜单
  sidebarRouters: state => state.permission.sidebarRouters,
  // 是否折叠菜单
  isCollapse: state => state.app.isCollapse,
  // 使用设备
  device: state => state.app.device,
  // 右侧功能栏
  rightPanelShow: state => state.app.rightPanelShow,
  // 标签页
  tagsViewList: state => state.app.tagsViewList,
  // 缓存标签页
  cachedViews: state => state.app.cachedViews,
  // 侧边栏主题
  sideTheme: state => state.settings.sideTheme,
  // 是否开启标签页
  tagsView: state => state.settings.tagsView,
  // 是否固定顶部导航栏
  fixedHeader: state => state.settings.fixedHeader,
  // 是否显示侧边栏Logo
  sidebarLogo: state => state.settings.sidebarLogo,
  // 主题色
  theme: state => state.settings.theme,
  // 标签页主题
  tagsViewTheme: state => state.settings.tagsViewTheme,
  // 数据字典
  dict: state => state.dict.dict
}

export default getters
