import store from '../../store'

const { body } = document
const WIDTH = 992

export default {
  watch: {
    $route (route) {
      // 如果当前设备为手机并且侧边栏展开
      if (this.device === 'mobile' && !store.getters.isCollapse) {
        // 折叠菜单栏
        store.dispatch('app/closeSideBar')
      }
    }
  },
  beforeMount () {
    window.addEventListener('resize', this.$_resizeHandler)
  },
  beforeDestroy () {
    window.removeEventListener('resize', this.$_resizeHandler)
  },
  mounted () {
    const isMobile = this.$_isMobile()
    if (isMobile) {
      store.dispatch('app/toggleDevice', 'mobile')
      store.dispatch('app/closeSideBar')
    }
  },
  methods: {
    $_isMobile () {
      const rect = body.getBoundingClientRect()
      return rect.width - 1 < WIDTH
    },
    $_resizeHandler () {
      if (!document.hidden) {
        const isMobile = this.$_isMobile()
        store.dispatch('app/toggleDevice', isMobile ? 'mobile' : 'desktop')

        if (isMobile) {
          store.dispatch('app/closeSideBar')
        }
      }
    }
  }
}
