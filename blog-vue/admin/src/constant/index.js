// 对外导出常量 token
export const TOKEN = 'token'

// QQ登录
export const QQ = 'QQ'

// Gitee登录
export const GIT_EE = 'GIT_EE'

// 手机登录
export const MOBILE = 'MOBILE'

// 微博登录
export const WEI_BO = 'WEI_BO'

// 系统登录
export const SYSTEM = 'SYSTEM'
