import clipboard from './module/clipboard'

const install = function (Vue) {
  // Vue自定义指令
  Vue.directive('clipboard', clipboard)
}

export default install
