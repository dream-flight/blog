import request from '../../utils/request'

/**
 * 获取关于我信息
 */
export const getAboutMe = () => {
  return request({
    url: '/admin/about-me/getAboutMe',
    method: 'GET'
  })
}

/**
 * 更新关于我信息
 */
export const update = (data) => {
  return request({
    url: '/admin/about-me/update',
    method: 'POST',
    data: data
  })
}
