import request from '../../utils/request'

/**
 * 获取文章列表
 */
export const listArticles = (data) => {
  return request({
    url: '/admin/article/list',
    method: 'POST',
    data: data
  })
}

/**
 * 新增/更新文章信息
 */
export const saveArticle = (data) => {
  return request({
    url: '/admin/article/save',
    method: 'POST',
    data: data
  })
}

/**
 * 获取指定文章信息
 */
export const getArticleById = (id) => {
  return request({
    url: `/admin/article/getArticleById/${id}`,
    method: 'GET'
  })
}

/**
 * 批量删除文章信息
 */
export const deleteArticle = (data) => {
  return request({
    url: '/admin/article/delete',
    method: 'DELETE',
    data: data
  })
}

/**
 * 更新文章设置属性
 */
export const updateSettings = (data) => {
  return request({
    url: '/admin/article/updateSettings',
    method: 'POST',
    data: data
  })
}
