import request from '../../utils/request'

/**
 * 获取博客标签列表
 */
export const listBlogTags = (data) => {
  return request({
    url: '/admin/blog-tags/list',
    method: 'POST',
    data: data
  })
}

/**
 * 新增/更新博客标签信息
 */
export const saveBlogTags = (data) => {
  return request({
    url: '/admin/blog-tags/save',
    method: 'POST',
    data: data
  })
}

/**
 * 获取指定博客标签信息
 */
export const getBlogTagsById = (id) => {
  return request({
    url: `/admin/blog-tags/getBlogTagsById/${id}`,
    method: 'GET'
  })
}

/**
 * 批量删除博客标签信息
 */
export const deleteBlogTags = (data) => {
  return request({
    url: '/admin/blog-tags/delete',
    method: 'DELETE',
    data: data
  })
}

/**
 * 获取所有博客标签
 */
export const listOptions = () => {
  return request({
    url: '/admin/blog-tags/listOptions',
    method: 'GET'
  })
}
