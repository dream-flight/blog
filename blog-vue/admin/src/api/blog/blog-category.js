import request from '../../utils/request'

/**
 * 获取分类列表
 */
export const listBlogCategorys = (data) => {
  return request({
    url: '/admin/blog-category/list',
    method: 'POST',
    data: data
  })
}

/**
 * 新增/更新分类信息
 */
export const saveBlogCategory = (data) => {
  return request({
    url: '/admin/blog-category/save',
    method: 'POST',
    data: data
  })
}

/**
 * 获取指定分类信息
 */
export const getBlogCategoryById = (id) => {
  return request({
    url: `/admin/blog-category/getCategoryById/${id}`,
    method: 'GET'
  })
}

/**
 * 批量删除分类信息
 */
export const deleteBlogCategory = (data) => {
  return request({
    url: '/admin/blog-category/delete',
    method: 'DELETE',
    data: data
  })
}

/**
 * 获取所有博客分类
 */
export const listOptions = () => {
  return request({
    url: '/admin/blog-category/listOptions',
    method: 'GET'
  })
}
