import request from '../../utils/request'

/**
 * 获取代码生成日志列表
 */
export const listGenerateLogs = (data) => {
  return request({
    url: '/admin/generate-log/list',
    method: 'POST',
    data: data
  })
}

/**
 * 根据生成日志下载代码
 */
export const download = (id) => {
  return request({
    url: `/admin/generate-log/download/${id}`,
    responseType: 'blob',
    method: 'GET'
  })
}

/**
 * 批量删除代码生成日志信息
 */
export const deleteGenerateLog = (data) => {
  return request({
    url: '/admin/generate-log/delete',
    method: 'DELETE',
    data: data
  })
}
