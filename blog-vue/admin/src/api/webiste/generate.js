import request from '../../utils/request'

/**
 * 获取数据库表格列表
 */
export const listTables = (data) => {
  return request({
    url: '/admin/generator/listTable',
    method: 'POST',
    data: data
  })
}

/**
 * 预览代码
 */
export const preview = (data) => {
  return request({
    url: '/admin/generator/preview',
    method: 'POST',
    data: data
  })
}

/**
 * 下载代码
 */
export const download = (data) => {
  return request({
    url: '/admin/generator/download',
    method: 'POST',
    responseType: 'blob',
    data: data
  })
}
