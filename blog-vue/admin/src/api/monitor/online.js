import request from '../../utils/request'

/**
 * 获取在线用户
 */
export const listOnline = (data) => {
  return request({
    url: '/admin/online/list',
    method: 'POST',
    data: data
  })
}

/**
 * 下线指定用户
 */
export const offline = (param) => {
  return request({
    url: `/admin/online/offline/${param}`,
    method: 'DELETE'
  })
}
