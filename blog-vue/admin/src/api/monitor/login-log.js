import request from '../../utils/request'

/**
 * 获取用户登录信息列表
 */
export const listLoginLogs = (data) => {
  return request({
    url: '/admin/login-log/list',
    method: 'POST',
    data: data
  })
}

/**
 * 清空所有登录日志信息
 */
export const clearLoginLog = () => {
  return request({
    url: '/admin/login-log/clear',
    method: 'GET'
  })
}

/**
 * 批量删除用户登录信息信息
 */
export const deleteLoginLog = (data) => {
  return request({
    url: '/admin/login-log/delete',
    method: 'DELETE',
    data: data
  })
}
