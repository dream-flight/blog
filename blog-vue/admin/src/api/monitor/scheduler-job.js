import request from '../../utils/request'

/**
 * 获取定时任务列表
 */
export const listSchedulerJob = (data) => {
  return request({
    url: '/admin/scheduler-job/list',
    method: 'POST',
    data: data
  })
}

/**
 * 新增/更新定时任务信息
 */
export const saveSchedulerJob = (data) => {
  return request({
    url: '/admin/scheduler-job/save',
    method: 'POST',
    data: data
  })
}

/**
 * 获取指定定时任务信息
 */
export const getSchedulerJobById = (id) => {
  return request({
    url: `/admin/scheduler-job/getSchedulerJobById/${id}`,
    method: 'GET'
  })
}

/**
 * 批量删除定时任务信息
 */
export const deleteSchedulerJob = (data) => {
  return request({
    url: '/admin/scheduler-job/delete',
    method: 'DELETE',
    data: data
  })
}

/**
 * 更新定时任务状态
 */
export const changeJobStatus = (data) => {
  return request({
    url: '/admin/scheduler-job/changeStatus',
    method: 'PUT',
    data: data
  })
}

/**
 * 执行一次定时任务
 */
export const run = (jobId) => {
  return request({
    url: `/admin/scheduler-job/run/${jobId}`,
    method: 'PUT'
  })
}
