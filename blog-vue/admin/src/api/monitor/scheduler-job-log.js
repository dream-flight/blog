import request from '../../utils/request'

/**
 * 获取定时任务调度日志列表
 */
export const listSchedulerJobLogs = (data) => {
  return request({
    url: '/admin/scheduler-job-log/list',
    method: 'POST',
    data: data
  })
}

/**
 * 获取指定定时任务调度日志信息
 */
export const getSchedulerJobLogById = (id) => {
  return request({
    url: `/admin/scheduler-job-log/getSchedulerJobLogById/${id}`,
    method: 'GET'
  })
}

/**
 * 批量删除定时任务调度日志信息
 */
export const deleteSchedulerJobLog = (data) => {
  return request({
    url: '/admin/scheduler-job-log/delete',
    method: 'DELETE',
    data: data
  })
}
