import request from '../../utils/request'

/**
 * 获取字典类型列表
 */
export const listDictTypes = (data) => {
  return request({
    url: '/admin/dict-type/list',
    method: 'POST',
    data: data
  })
}

/**
 * 新增/更新字典类型信息
 */
export const saveDictType = (data) => {
  return request({
    url: '/admin/dict-type/save',
    method: 'POST',
    data: data
  })
}

/**
 * 获取指定字典类型信息
 */
export const getDictTypeById = (id) => {
  return request({
    url: `/admin/dict-type/getDictTypeById/${id}`,
    method: 'GET'
  })
}

/**
 * 批量删除字典类型信息
 */
export const deleteDictType = (data) => {
  return request({
    url: '/admin/dict-type/delete',
    method: 'DELETE',
    data: data
  })
}

/**
 * 获取字典类型选项
 */
export const listOption = () => {
  return request({
    url: '/admin/dict-type/listOption',
    method: 'GET'
  })
}

/**
 * 刷新字典数据缓存
 */
export const refresh = () => {
  return request({
    url: '/admin/dict-type/refresh',
    method: 'PUT'
  })
}
