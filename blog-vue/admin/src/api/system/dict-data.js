import request from '../../utils/request'

/**
 * 获取字典数据列表
 */
export const listDictData = (data) => {
  return request({
    url: '/admin/dict-data/list',
    method: 'POST',
    data: data
  })
}

/**
 * 新增/更新字典数据信息
 */
export const saveDictData = (data) => {
  return request({
    url: '/admin/dict-data/save',
    method: 'POST',
    data: data
  })
}

/**
 * 获取指定字典数据信息
 */
export const getDictDataById = (id) => {
  return request({
    url: `/admin/dict-data/getDictDataById/${id}`,
    method: 'GET'
  })
}

/**
 * 批量删除字典数据信息
 */
export const deleteDictData = (data) => {
  return request({
    url: '/admin/dict-data/delete',
    method: 'DELETE',
    data: data
  })
}

/**
 * 获取字典类型对应数据
 */
export const getDictDataByType = (dictType) => {
  return request({
    url: `/admin/dict-data/type/${dictType}`,
    method: 'GET'
  })
}
