import request from '../../utils/request'

/**
 * 获取通知公告列表
 */
export const listNotices = (data) => {
  return request({
    url: '/common/notice/list',
    method: 'POST',
    data: data
  })
}

/**
 * 新增/更新通知公告信息
 */
export const saveNotice = (data) => {
  return request({
    url: '/admin/notice/save',
    method: 'POST',
    data: data
  })
}

/**
 * 获取指定通知公告信息
 */
export const getNoticeById = (id) => {
  return request({
    url: `/admin/notice/getNoticeById/${id}`,
    method: 'GET'
  })
}

/**
 * 批量删除通知公告信息
 */
export const deleteNotice = (data) => {
  return request({
    url: '/admin/notice/delete',
    method: 'DELETE',
    data: data
  })
}
