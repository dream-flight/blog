import request from '../utils/request'

/**
 * 获取验证码
 */
export const getCaptcha = () => {
  return request({
    url: '/common/captcha/getCaptcha',
    method: 'GET'
  })
}

/**
 * 获取首页信息
 */
export const getHomeInfo = () => {
  return request({
    url: '/admin/index/getHomeInfo',
    method: 'GET'
  })
}

/**
 * 用户登录
 */
export const login = (data) => {
  return request({
    url: '/common/login/user',
    method: 'POST',
    data: data
  })
}

/**
 * 获取菜单
 */
export const generateRoutes = () => {
  return request({
    url: '/admin/menu/getRoutes',
    method: 'GET'
  })
}

/**
 * 获取系统基本配置
 */
export const getConfig = () => {
  return request({
    url: '/admin/system-config/getSystemConfig',
    method: 'GET'
  })
}

/**
 * 更新系统配置信息
 */
export const saveConfig = (data) => {
  return request({
    url: '/admin/system-config/save',
    method: 'POST',
    data: data
  })
}
